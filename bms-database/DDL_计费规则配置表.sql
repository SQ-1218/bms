Drop Table Fee_Rule_Express;
Drop Table Fee_Rule_Storage;
Drop Table Fee_Rule_Oper_Delivery;
Drop Table Fee_Rule_Oper_Receiving;
Drop Table Fee_Rule_Special;


-- 快递费用规则配置表
create table Fee_Rule_Express
(
  ID              		VARCHAR2(32) default sys_guid() not null,
  Owner_No              VARCHAR2(30) not null,
  Whs_No              	VARCHAR2(30) not null,
  Express_Corp_No       VARCHAR2(30) not null,
  Province          	VARCHAR2(100) not null,
  Weight_Fm   			NUMBER(15,2) not null,
  Weight_To    			NUMBER(15,2) not null,
  Fee_Fixed    			NUMBER(15,2) not null,
  Fee_Extra    			NUMBER(15,2) default 0 not null,
  Remark      			VARCHAR2(300),
  CREATED_TIME    		TIMESTAMP(6) default Sysdate not null,
  CREATED_BY      		VARCHAR2(30) not null,
  UPDATED_TIME    		TIMESTAMP(6),
  UPDATED_BY      		VARCHAR2(30),
  VER            		NUMBER default 0 not null,
  IS_DELETED      		NUMBER(1) default 1 not null
);
alter table Fee_Rule_Express add primary key (ID);
comment on table Fee_Rule_Express is '快递费用规则配置表';
comment on column Fee_Rule_Express.Owner_No is '货主编号';
comment on column Fee_Rule_Express.Whs_No is '仓库编号';
comment on column Fee_Rule_Express.Express_Corp_No is '快递公司编号';
comment on column Fee_Rule_Express.Province is '省份（汉字形式表述）';
comment on column Fee_Rule_Express.Weight_Fm is '重量Fm（KG）';
comment on column Fee_Rule_Express.Weight_To is '重量To（KG）, 值为 Integer.MAX_VALUE(2147483647)时代表无穷大';
comment on column Fee_Rule_Express.Fee_Fixed is '首重（或固定）费用';
comment on column Fee_Rule_Express.Fee_Extra is '续重费用，若为固定费用，则该字段为0';
comment on column Fee_Rule_Express.Remark is '备注';
create index IDX_Fee_Rule_Express_Main on Fee_Rule_Express(Owner_No, Express_Corp_No,Province);


-- 仓储费用规则配置表
create table Fee_Rule_Storage
(
  ID              		VARCHAR2(32) default sys_guid() not null,
  Owner_No              VARCHAR2(30) not null,
  Whs_No              	VARCHAR2(30) not null,
  Cal_Item_Rank 		NUMBER(3,0) not null,
  Cal_Item_Name 		VARCHAR2(200) not null,
  Cal_Item_Code 		VARCHAR2(200) not null,
  Cal_Item_Note			VARCHAR2(200),
  Cal_Way       		NUMBER(3,0) not null,
  Factor1_Note   		VARCHAR2(200),
  Factor1_Value			VARCHAR2(200),
  Factor2_Note   		VARCHAR2(200),
  Factor2_Value			VARCHAR2(200),
  Factor3_Note   		VARCHAR2(200),
  Factor3_Value			VARCHAR2(200),
  Factor4_Note   		VARCHAR2(200),
  Factor4_Value			VARCHAR2(200),
  Factor5_Note   		VARCHAR2(200),
  Factor5_Value			VARCHAR2(200),
  Remark      			VARCHAR2(300),
  CREATED_TIME    		TIMESTAMP(6) default Sysdate not null,
  CREATED_BY      		VARCHAR2(30) not null,
  UPDATED_TIME    		TIMESTAMP(6),
  UPDATED_BY      		VARCHAR2(30),
  VER            		NUMBER default 0 not null,
  IS_DELETED      		NUMBER(1) default 1 not null
);
alter table Fee_Rule_Storage add primary key (ID);
comment on table Fee_Rule_Storage is '仓储费用规则配置表';
comment on column Fee_Rule_Storage.Owner_No is '货主编号';
comment on column Fee_Rule_Storage.Whs_No is '仓库编号';
comment on column Fee_Rule_Storage.Cal_Item_Rank is '计费项目序号（某种费用大类可能需要从多个项目去计算），方便用户阅读、编辑、记忆';
comment on column Fee_Rule_Storage.Cal_Item_Name is '计费项目名称，仓储费(作为费用大类)目前就一个项目：仓储费';
comment on column Fee_Rule_Storage.Cal_Item_Code is '计费项目代码，每个代码对应的有个计算方式的字典';
comment on column Fee_Rule_Storage.Cal_Item_Note is '计费项目解释';
comment on column Fee_Rule_Storage.Cal_Way is '某计费项目的计算方式，1：按托盘数，2：按体积，3：按重量，4：按件数，5：按面积';
comment on column Fee_Rule_Storage.Factor1_Note is '计费公式的系数，系数1的解释';
comment on column Fee_Rule_Storage.Factor1_Value is '计费公式的系数，系数1的值';
create index IDX_Fee_Rule_Storage_Main on Fee_Rule_Storage(Owner_No, Cal_Item_Code);


-- 出库操作费用规则配置表
create table Fee_Rule_Oper_Delivery
(
  ID              		VARCHAR2(32) default sys_guid() not null,
  Owner_No              VARCHAR2(30) not null,
  Whs_No              	VARCHAR2(30) not null,
  Cal_Item_Rank 		NUMBER(3,0) not null,
  Cal_Item_Name 		VARCHAR2(200) not null,
  Cal_Item_Code 		VARCHAR2(200) not null,
  Cal_Item_Note			VARCHAR2(200),
  Doc_Type 				NUMBER(2,0),
  Cal_Way       		NUMBER(3,0) not null,
  Factor1_Note   		VARCHAR2(200),
  Factor1_Value			VARCHAR2(200),
  Factor2_Note   		VARCHAR2(200),
  Factor2_Value			VARCHAR2(200),
  Factor3_Note   		VARCHAR2(200),
  Factor3_Value			VARCHAR2(200),
  Factor4_Note   		VARCHAR2(200),
  Factor4_Value			VARCHAR2(200),
  Factor5_Note   		VARCHAR2(200),
  Factor5_Value			VARCHAR2(200),
  Remark      			VARCHAR2(300),
  CREATED_TIME    		TIMESTAMP(6) default Sysdate not null,
  CREATED_BY      		VARCHAR2(30) not null,
  UPDATED_TIME    		TIMESTAMP(6),
  UPDATED_BY      		VARCHAR2(30),
  VER            		NUMBER default 0 not null,
  IS_DELETED      		NUMBER(1) default 1 not null
);
alter table Fee_Rule_Oper_Delivery add primary key (ID);
comment on table Fee_Rule_Oper_Delivery is '出库操作费用规则配置表';
comment on column Fee_Rule_Oper_Delivery.Owner_No is '货主编号';
comment on column Fee_Rule_Oper_Delivery.Whs_No is '仓库编号';
comment on column Fee_Rule_Oper_Delivery.Cal_Item_Rank is '计费项目序号（某种费用大类可能需要从多个项目去计算），方便用户阅读、编辑、记忆';
comment on column Fee_Rule_Oper_Delivery.Cal_Item_Name is '计费项目名称，出库操作费项目有：基础操作费、订单截停费 等';
comment on column Fee_Rule_Oper_Delivery.Cal_Item_Code is '计费项目代码，每个代码对应的有个计算方式的字典';
comment on column Fee_Rule_Oper_Delivery.Cal_Item_Note is '计费项目解释';
comment on column Fee_Rule_Oper_Delivery.Doc_Type is '单据类型，不填代表所有类型';
comment on column Fee_Rule_Oper_Delivery.Cal_Way is '某计费项目的计算方式，1：按单量(元/单)，2：按件数，3：按箱，4：按体积，5：按重量';
comment on column Fee_Rule_Oper_Delivery.Factor1_Note is '计费公式的系数，系数1的解释';
comment on column Fee_Rule_Oper_Delivery.Factor1_Value is '计费公式的系数，系数1的值';
create index IDX_Fee_Rule_Oper_Dev_Main on Fee_Rule_Oper_Delivery(Owner_No, Cal_Item_Code, Doc_Type);


-- 入库操作费用规则配置表
create table Fee_Rule_Oper_Receiving
(
  ID              		VARCHAR2(32) default sys_guid() not null,
  Owner_No              VARCHAR2(30) not null,
  Whs_No              	VARCHAR2(30) not null,
  Cal_Item_Rank 		NUMBER(3,0) not null,
  Cal_Item_Name 		VARCHAR2(200) not null,
  Cal_Item_Code 		VARCHAR2(200) not null,
  Cal_Item_Note			VARCHAR2(200),
  Doc_Type 				NUMBER(2,0),
  Cal_Way       		NUMBER(3,0) not null,
  Factor1_Note   		VARCHAR2(200),
  Factor1_Value			VARCHAR2(200),
  Factor2_Note   		VARCHAR2(200),
  Factor2_Value			VARCHAR2(200),
  Factor3_Note   		VARCHAR2(200),
  Factor3_Value			VARCHAR2(200),
  Factor4_Note   		VARCHAR2(200),
  Factor4_Value			VARCHAR2(200),
  Factor5_Note   		VARCHAR2(200),
  Factor5_Value			VARCHAR2(200),
  Remark      			VARCHAR2(300),
  CREATED_TIME    		TIMESTAMP(6) default Sysdate not null,
  CREATED_BY      		VARCHAR2(30) not null,
  UPDATED_TIME    		TIMESTAMP(6),
  UPDATED_BY      		VARCHAR2(30),
  VER            		NUMBER default 0 not null,
  IS_DELETED      		NUMBER(1) default 1 not null
);
alter table Fee_Rule_Oper_Receiving add primary key (ID);
comment on table Fee_Rule_Oper_Receiving is '入库操作费用规则配置表';
comment on column Fee_Rule_Oper_Receiving.Owner_No is '货主编号';
comment on column Fee_Rule_Oper_Receiving.Whs_No is '仓库编号';
comment on column Fee_Rule_Oper_Receiving.Cal_Item_Rank is '计费项目序号（某种费用大类可能需要从多个项目去计算），方便用户阅读、编辑、记忆';
comment on column Fee_Rule_Oper_Receiving.Cal_Item_Name is '计费项目名称，出库操作费项目有：基础操作费、订单截停费 等';
comment on column Fee_Rule_Oper_Receiving.Cal_Item_Code is '计费项目代码，每个代码对应的有个计算方式的字典';
comment on column Fee_Rule_Oper_Receiving.Cal_Item_Note is '计费项目解释';
comment on column Fee_Rule_Oper_Receiving.Doc_Type is '单据类型，不填代表所有类型';
comment on column Fee_Rule_Oper_Receiving.Cal_Way is '某计费项目的计算方式，1：按Max(体积，重量)，2：按体积（元/m3），3：按重量（元/吨）';
comment on column Fee_Rule_Oper_Receiving.Factor1_Note is '计费公式的系数，系数1的解释';
comment on column Fee_Rule_Oper_Receiving.Factor1_Value is '计费公式的系数，系数1的值';
create index IDX_Fee_Rule_Oper_Rcv_Main on Fee_Rule_Oper_Receiving(Owner_No, Cal_Item_Code, Doc_Type);


-- 特殊计费规则配置（增收或优惠少收）
create table Fee_Rule_Special
(
  ID              		VARCHAR2(32) default sys_guid() not null,
  Owner_No              VARCHAR2(30) not null,
  Whs_No              	VARCHAR2(30) not null,
  Cal_Item_Rank 		NUMBER(3,0) not null,
  Cal_Item_Name 		VARCHAR2(200) not null,
  Cal_Item_Code 		VARCHAR2(200) not null,
  Cal_Item_Note			VARCHAR2(200),
  Doc_Type 				NUMBER(2,0),
  Cal_Type 				NUMBER(1,0) not null,
  Factor1_Note   		VARCHAR2(200),
  Factor1_Value			VARCHAR2(200),
  Factor2_Note   		VARCHAR2(200),
  Factor2_Value			VARCHAR2(200),
  Factor3_Note   		VARCHAR2(200),
  Factor3_Value			VARCHAR2(200),
  Factor4_Note   		VARCHAR2(200),
  Factor4_Value			VARCHAR2(200),
  Factor5_Note   		VARCHAR2(200),
  Factor5_Value			VARCHAR2(200),
  Factor6_Note   		VARCHAR2(200),
  Factor6_Value			VARCHAR2(200),
  Factor7_Note   		VARCHAR2(200),
  Factor7_Value			VARCHAR2(200),
  Factor8_Note   		VARCHAR2(200),
  Factor8_Value			VARCHAR2(200),
  Factor9_Note   		VARCHAR2(200),
  Factor9_Value			VARCHAR2(200),
  Remark      			VARCHAR2(300),
  CREATED_TIME    		TIMESTAMP(6) default Sysdate not null,
  CREATED_BY      		VARCHAR2(30) not null,
  UPDATED_TIME    		TIMESTAMP(6),
  UPDATED_BY      		VARCHAR2(30),
  VER            		NUMBER default 0 not null,
  IS_DELETED      		NUMBER(1) default 1 not null
);
alter table Fee_Rule_Special add primary key (ID);
comment on table Fee_Rule_Special is '特殊计费规则配置（增收或优惠少收）';
comment on column Fee_Rule_Special.Owner_No is '货主编号';
comment on column Fee_Rule_Special.Whs_No is '仓库编号';
comment on column Fee_Rule_Special.Cal_Item_Rank is '计费项目序号（某种费用大类可能需要从多个项目去计算），方便用户阅读、编辑、记忆';
comment on column Fee_Rule_Special.Cal_Item_Name is '计费项目名称，特殊计费费项目有：各种优惠扣减、各种额外收 等';
comment on column Fee_Rule_Special.Cal_Item_Code is '计费项目代码，此处非单纯地按照什么收费，不必对应计算方式';
comment on column Fee_Rule_Special.Cal_Item_Note is '计费项目解释';
comment on column Fee_Rule_Special.Doc_Type is '单据类型';
comment on column Fee_Rule_Special.Cal_Type is '计算类型：0-增；1-减';
comment on column Fee_Rule_Special.Factor1_Note is '计费公式的系数，系数1的解释';
comment on column Fee_Rule_Special.Factor1_Value is '计费公式的系数，系数1的值';
create index IDX_Fee_Rule_Special_Main on Fee_Rule_Special(Owner_No, Cal_Item_Code, Doc_Type);






 