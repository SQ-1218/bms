Drop Table Fee_Result_Special;
Drop Table Fee_Result_Operation;
Drop Table Fee_Result_Storage;
Drop Table Fee_Result_Express;
Drop Table Fee_Result_Cargo_Owner;



-- 特殊计费结果（增收或优惠少收），支持针对某具体订单录入
create table Fee_Result_Special
(
  ID              		VARCHAR2(32) default sys_guid() not null,
  Owner_No              VARCHAR2(30) not null,
  Whs_No              	VARCHAR2(30) not null,
  Cal_Item_Name 		VARCHAR2(200) not null,
  Cal_Item_Code 		VARCHAR2(200) not null,
  Cal_Type 				NUMBER(1,0) not null,
  Doc_Type 				VARCHAR2(30),
  Doc_No 				VARCHAR2(30),
  Fee			   		NUMBER(20,2)  not null,
  Fee_Occur_Time	   	TIMESTAMP(6)  not null,
  Is_Confirmed     		NUMBER(1) default 1 not null,
  Confirmed_Time	   	TIMESTAMP(6),
  Remark      			VARCHAR2(300),
  CREATED_TIME    		TIMESTAMP(6) default Sysdate not null,
  CREATED_BY      		VARCHAR2(30) not null,
  UPDATED_TIME    		TIMESTAMP(6),
  UPDATED_BY      		VARCHAR2(30),
  VER            		NUMBER default 0 not null,
  IS_DELETED      		NUMBER(1) default 1 not null
);
alter table Fee_Result_Special add primary key (ID);
comment on table Fee_Result_Special is '特殊计费结果（增收或优惠少收），支持针对某具体订单录入';
comment on column Fee_Result_Special.Owner_No is '货主编号';
comment on column Fee_Result_Special.Whs_No is '仓库编号';
comment on column Fee_Result_Special.Cal_Item_Name is '计费项目名称，特殊计费费项目有：各种优惠扣减、各种额外收 等';
comment on column Fee_Result_Special.Cal_Item_Code is '计费项目代码';
comment on column Fee_Result_Special.Cal_Type is '计算类型：0-增；1-减';
comment on column Fee_Result_Special.Doc_Type is '单据类型';
comment on column Fee_Result_Special.Doc_No is '单据号';
comment on column Fee_Result_Special.Fee is '计费结果';
comment on column Fee_Result_Special.Fee_Occur_Time is '费用产生时间';
comment on column Fee_Result_Special.Is_Confirmed is '是否已确认结算';
comment on column Fee_Result_Special.Confirmed_Time is '确认结算时间';
create index IDX_Fee_Result_Special_Main on Fee_Result_Special(Owner_No, Cal_Item_Code, Doc_No);


-- 操作费用计算结果
create table Fee_Result_Operation
(
  ID              		VARCHAR2(32) default sys_guid() not null,
  Owner_No              VARCHAR2(30) not null,
  Whs_No              	VARCHAR2(30) not null,
  Cal_Item_Rank 		NUMBER(3,0) not null,
  Cal_Item_Name 		VARCHAR2(200) not null,
  Cal_Item_Code 		VARCHAR2(200) not null,
  Cal_Way       		NUMBER(3,0) not null,
  Doc_Type 				VARCHAR2(30) not null,
  Doc_No 				VARCHAR2(30) not null,
  Fee			   		NUMBER(20,2) not null,
  Fee_Occur_Time	   	TIMESTAMP(6) not null,
  Is_Confirmed     		NUMBER(1) default 1 not null,
  Confirmed_Time	   	TIMESTAMP(6),
  CREATED_TIME    		TIMESTAMP(6) default Sysdate not null,
  CREATED_BY      		VARCHAR2(30) not null,
  UPDATED_TIME    		TIMESTAMP(6),
  UPDATED_BY      		VARCHAR2(30),
  VER            		NUMBER default 0 not null,
  IS_DELETED      		NUMBER(1) default 1 not null
);
alter table Fee_Result_Operation add primary key (ID);
comment on table Fee_Result_Operation is '出库操作费用规则配置表';
comment on column Fee_Result_Operation.Owner_No is '货主编号';
comment on column Fee_Result_Operation.Whs_No is '仓库编号';
comment on column Fee_Result_Operation.Cal_Item_Rank is '计费项目序号（某种费用大类可能需要从多个项目去计算），方便用户阅读、编辑、记忆';
comment on column Fee_Result_Operation.Cal_Item_Name is '计费项目名称，出库操作费项目有：基础操作费、订单截停费 等';
comment on column Fee_Result_Operation.Cal_Item_Code is '计费项目代码，每个代码对应的有个计算方式的字典';
comment on column Fee_Result_Operation.Cal_Way is '某计费项目的计算方式';
comment on column Fee_Result_Operation.Doc_Type is '0-入库；1-出库';
comment on column Fee_Result_Operation.Doc_No is '单据号';
comment on column Fee_Result_Operation.Fee is '计费结果';
comment on column Fee_Result_Operation.Fee_Occur_Time is '费用产生时间';
comment on column Fee_Result_Operation.Is_Confirmed is '是否已确认结算';
comment on column Fee_Result_Operation.Confirmed_Time is '确认结算时间';
create index IDX_Fee_Result_Operation_Main on Fee_Result_Operation(Owner_No, Cal_Item_Code, Doc_No);


-- 仓储费用计算结果
create table Fee_Result_Storage
(
  ID              		VARCHAR2(32) default sys_guid() not null,
  Owner_No              VARCHAR2(30) not null,
  Whs_No              	VARCHAR2(30) not null,
  Cal_Item_Rank 		NUMBER(3,0) not null,
  Cal_Item_Name 		VARCHAR2(200) not null,
  Cal_Item_Code 		VARCHAR2(200) not null,
  Cal_Way       		NUMBER(3,0) not null,
  Fee			   		NUMBER(20,2) not null,
  Fee_Occur_Time	   	TIMESTAMP(6) not null,
  Is_Confirmed     		NUMBER(1) default 1 not null,
  Confirmed_Time	   	TIMESTAMP(6),
  Remark      			VARCHAR2(300),
  CREATED_TIME    		TIMESTAMP(6) default Sysdate not null,
  CREATED_BY      		VARCHAR2(30) not null,
  UPDATED_TIME    		TIMESTAMP(6),
  UPDATED_BY      		VARCHAR2(30),
  VER            		NUMBER default 0 not null,
  IS_DELETED      		NUMBER(1) default 1 not null
);
alter table Fee_Result_Storage add primary key (ID);
comment on table Fee_Result_Storage is '仓储费用规则配置表';
comment on column Fee_Result_Storage.Owner_No is '货主编号';
comment on column Fee_Result_Storage.Whs_No is '仓库编号';
comment on column Fee_Result_Storage.Cal_Item_Rank is '计费项目序号（某种费用大类可能需要从多个项目去计算），方便用户阅读、编辑、记忆';
comment on column Fee_Result_Storage.Cal_Item_Name is '计费项目名称，仓储费(作为费用大类)目前就一个项目：仓储费';
comment on column Fee_Result_Storage.Cal_Item_Code is '计费项目代码，每个代码对应的有个计算方式的字典';
comment on column Fee_Result_Storage.Cal_Way is '某计费项目的计算方式，1：按托盘数，2：按体积，3：按重量，4：按件数，5：按面积';
comment on column Fee_Result_Storage.Fee is '计费结果';
comment on column Fee_Result_Storage.Fee_Occur_Time is '费用产生时间';
comment on column Fee_Result_Storage.Is_Confirmed is '是否已确认结算';
comment on column Fee_Result_Storage.Confirmed_Time is '确认结算时间';
create index IDX_Fee_Result_Storage_Main on Fee_Result_Storage(Owner_No, Cal_Item_Code, Fee_Occur_Time);



-- 快递费计算结果
create table Fee_Result_Express
(
  ID              		VARCHAR2(32) default sys_guid() not null,
  Owner_No              VARCHAR2(30) not null,
  Whs_No              	VARCHAR2(30) not null,
  Doc_No              	VARCHAR2(30),
  Ref_No              	VARCHAR2(30),
  Express_Corp_No       VARCHAR2(30) not null,
  Express_No       		VARCHAR2(30) not null,
  Province          	VARCHAR2(100) not null,
  Weight   				NUMBER(15,2) not null,
  Fee			   		NUMBER(20,2) not null,
  Fee_Occur_Time	   	TIMESTAMP(6) not null,
  Is_Confirmed     		NUMBER(1) default 1 not null,
  Confirmed_Time	   	TIMESTAMP(6),
  Remark      			VARCHAR2(300),
  CREATED_TIME    		TIMESTAMP(6) default Sysdate not null,
  CREATED_BY      		VARCHAR2(30) not null,
  UPDATED_TIME    		TIMESTAMP(6),
  UPDATED_BY      		VARCHAR2(30),
  VER            		NUMBER default 0 not null,
  IS_DELETED      		NUMBER(1) default 1 not null
);
alter table Fee_Result_Express add primary key (ID);
comment on table Fee_Result_Express is '快递费用规则配置表';
comment on column Fee_Result_Express.Owner_No is '货主编号';
comment on column Fee_Result_Express.Whs_No is '仓库编号';
comment on column Fee_Result_Express.Doc_No is '出库单号';
comment on column Fee_Result_Express.Ref_No is '外部订单号';
comment on column Fee_Result_Express.Express_Corp_No is '快递公司编号';
comment on column Fee_Result_Express.Express_No is '物流单号';
comment on column Fee_Result_Express.Province is '省份（汉字形式表述）';
comment on column Fee_Result_Express.Weight is '重量（KG）';
comment on column Fee_Result_Express.Fee is '计费结果';
comment on column Fee_Result_Express.Fee_Occur_Time is '费用产生时间';
comment on column Fee_Result_Express.Is_Confirmed is '是否已确认结算';
comment on column Fee_Result_Express.Confirmed_Time is '确认结算时间';
create index IDX_Fee_Result_Express_Main on Fee_Result_Express(Owner_No, Ref_No, Express_No);


-- 货主总费用计算结果
create table Fee_Result_Cargo_Owner
(
  ID              		VARCHAR2(32) default sys_guid() not null,
  Owner_No              VARCHAR2(30) not null,
  Whs_No              	VARCHAR2(30) not null,
  Fee			   		NUMBER(20,2) not null,
  Year_Str	   			NUMBER(4,0),
  Month_Str	   			NUMBER(2,0),
  Day_Str	   			NUMBER(2,0),
  Is_Confirmed     		NUMBER(1) default 1 not null,
  Confirmed_Time	   	TIMESTAMP(6), 
  Remark      			VARCHAR2(300),
  CREATED_TIME    		TIMESTAMP(6) default Sysdate not null,
  CREATED_BY      		VARCHAR2(30) not null,
  UPDATED_TIME    		TIMESTAMP(6),
  UPDATED_BY      		VARCHAR2(30),
  VER            		NUMBER default 0 not null,
  IS_DELETED      		NUMBER(1) default 1 not null
);
alter table Fee_Result_Cargo_Owner add primary key (ID);
comment on table Fee_Result_Cargo_Owner is '货主总费用计算结果';
comment on column Fee_Result_Cargo_Owner.Owner_No is '货主编号';
comment on column Fee_Result_Cargo_Owner.Whs_No is '仓库编号';
comment on column Fee_Result_Cargo_Owner.Fee is '计费结果';
comment on column Fee_Result_Cargo_Owner.Year_Str is '费用产生年份';
comment on column Fee_Result_Cargo_Owner.Month_Str is '费用产生月份';
comment on column Fee_Result_Cargo_Owner.Day_Str is '费用产生日份，一般按月计费，故该字段一般为空';
comment on column Fee_Result_Cargo_Owner.Is_Confirmed is '是否已确认结算';
comment on column Fee_Result_Cargo_Owner.Confirmed_Time is '确认结算时间';
create index IDX_Fee_Result_Cg_Owner_Main on Fee_Result_Cargo_Owner(Owner_No, Year_Str, Month_Str);




 