<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>快递费用规则列表</title>
    <meta name="keywords" content="index">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <link rel="icon" type="image/png" href="${mainDomain}/statics/assets/i/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="${mainDomain}/statics/assets/i/app-icon72x72@2x.png">
    <meta name="apple-mobile-web-app-title" content="Amaze UI" />
    <link rel="stylesheet" href="${mainDomain}/statics/assets/css/amazeui.min.css" />
    <link rel="stylesheet" href="${mainDomain}/statics/assets/css/amazeui.datatables.min.css" />
    <link rel="stylesheet" href="${mainDomain}/statics/assets/css/app.css">
    <script src="${mainDomain}/statics/assets/js/jquery.min.js"></script>
    <script>
        var mainDomain = "${mainDomain}";
    </script>
    <style>
        .am-form-group{
            height: 50px;
        }
    </style>
</head>

<body data-type="widgets" class="theme-black">
<div class="tpl-content-wrapper">
    <div class="row-content am-cf">
        <div class="row">
            <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
                <div class="widget am-cf">
                    <div class="widget-head am-cf">
                        <#if courierFeeRule.id != null>
                         <div class="widget-title  am-cf">快递费用规则编辑</div>
                        <#else>
                        <div class="widget-title  am-cf">快递费用规则添加</div>
                        </#if>
                        <div class="am-md-text-right">
                        <button type="button" class="am-btn am-btn-primary am-btn-sm"  onclick="javascript:window.history.back(-1);"><span class="am-icon-backward"></span> 返回</button>
                        </div>
                    </div>
                    <div class="widget-body  am-fr">
                        <form id="feeRuleExpressForm" method="post">
                            <input type="hidden" name="id" value="${courierFeeRule.id}">
                            <div class="am-g bw-style">
                             <div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                             <#if courierFeeRule.owner_no != null>
                             <div class="am-form-group am-form-field">
                                 <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                                     <span class="am-form-label">货主姓名:</span>
                                 </div>
                                 <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-form-group tpl-table-list-select am-md-text-left">
                                     <input  class="am-input-lg am-list-item-text"  type="text"  value="${courierFeeRule.owner_no}" readonly>
                                 </div>
                             </div>
                             <#else>
                                 <div class="am-form-group am-form-field">
                                     <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                                         <span class="am-form-label">货主姓名:</span>
                                     </div>
                                     <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-form-group tpl-table-list-select am-md-text-left">
                                         <select name="owner_no" id="owner_no"  data-am-selected="{maxHeight: 100}" required>
                                             <option value="">请选择货主</option>
                                 <#list owner as owner>
                                     <option value="${owner.no}">${owner.name}</option>
                                 </#list>
                                         </select>
                                     </div>
                                 </div>
                             </#if>
							</div>
							<div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                            <#if courierFeeRule.express_corp_no != null>
                             <div class="am-form-group am-form-field">
                                 <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                                     <span class="am-form-label">快递公司:</span>
                                 </div>
                                 <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-form-group tpl-table-list-select am-md-text-left">
                                     <input  class="am-input-lg am-list-item-text"  type="text"  value="${courierFeeRule.express_corp_no}" readonly>
                                 </div>
                             </div>
                            <#else>
                            <div class="am-form-group am-form-field">
                             <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                            <span class="am-form-label">快递公司:</span>
                            </div>
                            <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-form-group tpl-table-list-select am-md-text-left">
                            <select name="express_corp_no" id="express_corp_no" data-am-selected="{maxHeight: 100}" required>
                                <option value="">请选择快递公司</option>
                                 <#list carrier as carrier>
                                     <option value="${carrier.code}">${carrier.name}</option>
                                 </#list>
                            </select>
                            </div>
                        </div>
                            </#if>
							</div>
						</div>
						<div class="am-g bw-style">
                             <div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                            <#if courierFeeRule.province != null>
                             <div class="am-form-group am-form-field">
                                 <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                                     <span class="am-form-label">省份:</span>
                                 </div>
                                 <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-form-group tpl-table-list-select am-md-text-left">
                                     <input  class="am-input-lg am-list-item-text"  type="text"  value="${courierFeeRule.province}" readonly>
                                 </div>
                             </div>
                            <#else>
                        <div class="am-form-group am-form-field">
                            <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                            <span class="am-form-label" style="margin-right: 10px">省份:</span>
                            </div>
                            <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-form-group tpl-table-list-select am-md-text-left">
                            <select name="province" id="province" data-am-selected="{maxHeight: 100}" required>
                                <option value="">请选择省份</option>
                                    <#list province as province>
                                        <option value="${province.code}">${province.name}</option>
                                    </#list>
                            </select>
                            </div>
                        </div>
                            </#if>
						</div>
						<div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                        <div class="am-form-group am-form-field">
                            <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                            <span class="am-form-label">重量Fm:</span>
                            </div>
                            <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-md-text-left">
                             <input class="am-input-lg am-list-item-text" type="number" placeholder="请输入重量Fm" style="margin-left: 10px" required min="0" id="weight_fm" name="weight_fm" value="${courierFeeRule.weight_fm}">
                            </div>
                            </div>
                       </div>
                      </div>
                      <div class="am-g bw-style">
                             <div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                        <div class="am-form-group am-form-field">
                            <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                            <span class="am-form-label">重量To:</span>
                           </div>
                            <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-md-text-left">
                            <input class="am-input-lg am-list-item-text" type="number" placeholder="请输入重量To" style="margin-left: 10px" required min="0" id="weight_to" name="weight_to" value="${courierFeeRule.weight_to}">
                            </div>
                        </div>
                       </div>
                       <div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                        <div class="am-form-group am-form-field">
                            <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                            <span class="am-form-label">首重费用:</span>
                             </div>
                            <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-md-text-left">
                            <input class="am-input-lg am-list-item-text" type="number" placeholder="请输入首重费用" style="margin-left: 10px" required min="0"  id="fee_fixed"   name="fee_fixed" value="${courierFeeRule.fee_fixed}">
                            </div>
                        </div>
                        </div>
                       </div>
                      <div class="am-g bw-style">
                       <div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                        <div class="am-form-group am-form-field">
                            <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                            <label class="am-form-label">续重费用:</label >
                            </div>
                            <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-md-text-left">
                            <input class="am-input-lg am-list-item-text" type="number" placeholder="请输入续重费用" style="margin-left: 10px" required min="0" id="fee_extra"  name="fee_extra" value="${courierFeeRule.fee_extra}">
                            </div>
                            </div>
                          </div>
                        </div>
                          <div class="am-form-group  am-form-field">
                              <div class="am-md-text-center ">
                                  <button type="reset" class="am-btn am-btn-primary am-btn-sm am-md-text-left" style="margin-right: 50px"><span class="am-icon-refresh"></span> 重置</button>
                                  <button type="button" class="am-btn am-btn-primary am-btn-sm am-md-text-left" onclick="submitForm()"><span class="am-icon-circle-o"></span> 提交</button>
                              </div>
                          </div>
                            <form>
                </div>
    </div>
</div>
</div>
<script src="${mainDomain}/statics/assets/js/amazeui.min.js"></script>
<script src="${mainDomain}/statics/assets/js/amazeui.datatables.min.js"></script>
<script src="${mainDomain}/statics/assets/js/dataTables.responsive.min.js"></script>
<script src="${mainDomain}/statics/assets/js/app.js"></script>
<script src="${mainDomain}/statics/js/front/courier/list.js"></script>
</body>
</html>