<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>货主列表</title>
    <meta name="keywords" content="index">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <link rel="icon" type="image/png" href="${mainDomain}/statics/assets/i/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="${mainDomain}/statics/assets/i/app-icon72x72@2x.png">
    <meta name="apple-mobile-web-app-title" content="Amaze UI" />
    <link rel="stylesheet" href="${mainDomain}/statics/assets/css/amazeui.min.css" />
    <link rel="stylesheet" href="${mainDomain}/statics/assets/css/amazeui.datatables.min.css" />
    <link rel="stylesheet" href="${mainDomain}/statics/assets/css/app.css">
    <script src="${mainDomain}/statics/assets/js/jquery.min.js"></script>
    <script>
        var mainDomain = "${mainDomain}";
    </script>
</head>
<style>
    th, td {
        white-space: nowrap;
        text-align: center;
    };
</style>
<body data-type="widgets" class="theme-black">
<div class="tpl-content-wrapper">
    <div class="row-content am-cf">
        <div class="row">
            <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
                <div class="widget am-cf">
                    <div class="widget-head am-cf">
                        <div class="widget-title  am-cf">月度费用管理列表</div>
                    </div>

                    <div class="widget-body  am-fr">
                        <div class="am-u-sm-12 am-u-md-6 am-u-lg-4">
                            <div class="am-form-group tpl-table-list-select">
                                <span style="margin-right:10px">货主姓名:</span>
                                <select id="owner_no"    data-am-selected="{maxHeight: 100}">
                                    <#--<option value="${owner.no}" selected>点击选择户主</option>-->
                                <#list owner as owner>
                                    <option value="${owner.no}">${owner.name}</option>
                                </#list>
                                </select>
                            </div>
                        </div>
                    <#--   <div class="am-alert am-alert-danger" id="my-alert" style="display: none">
                           <p>请先选择结束日期，开始日期应小于结束日期！</p>
                       </div>
                       <div class="am-u-sm-12 am-u-md-3 am-u-lg-3">
                           <button type="button" class="am-btn am-btn-default am-margin-right" id="my-start">开始日期
                           </button>
                           <span id="my-startDate"><span id="start_date"></span></span><input type="hidden"
                                                                                              id="start_time"
                                                                                              name="start_time"
                                                                                              value="">
                       </div>
                       <div class="am-u-sm-12 am-u-md-3 am-u-lg-3">
                           <button type="button" class="am-btn am-btn-default am-margin-right" id="my-end">结束日期
                           </button>
                           <span id="my-endDate"><span id="end_date"></span></span><input type="hidden" id="end_time"
                                                                                          name="end_time" value="">
                       </div>-->
                        <div class="am-u-sm-12 am-u-md-3 am-u-lg-3">
                            <div class="am-input-group am-datepicker-date"
                                 data-am-datepicker="{format: 'yyyy-mm', viewMode: 'years', minViewMode: 'months'}">
                                <input type="text" class="am-form-field" placeholder="选择月份" readonly id="month_time"
                                       name="month_time" value="">
                                <span class="am-input-group-btn am-datepicker-add-on">
    <button class="am-btn am-btn-default" type="button"><span class="am-icon-calendar"></span> </button>
  </span>
                            </div>
                        </div>
                        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
                            <div class="am-form-group">
                                <div class="am-btn-toolbar">
                                    <div class="am-btn-group am-btn-group-xs">
                                        <button type="button" class="am-btn am-btn-primary am-btn-sm" onclick="search()"><span class="am-icon-search"></span>&nbsp;查询</button>
                                        <button type="button" class="am-btn am-btn-primary" id="doc-confirm-toggle"
                                                style="display : none">运营审核
                                        </button>
                                        <button type="button" class="am-btn am-btn-primary" id="doc-confirm-toggle2"
                                                style="display : none">财务专员审核
                                        </button>
                                        <button type="button" class="am-btn am-btn-primary" id="doc-confirm-toggle3"
                                                style="display : none">财务经理审核
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="tableArea" class="am-u-sm-12">
                            <table id="tb_fee" width="100%" class="am-table am-table-compact am-table-striped tpl-table-black " id="example-r">
                                <thead>
                                <tr>
                                    <th>费用类型</th>
                                    <th>费用总计</th>
                                    <th>审核</th>

                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="am-modal am-modal-confirm" tabindex="-1" id="my-confirm">
    <div class="am-modal-dialog">
        <div class="am-modal-hd"></div>
        <div class="am-modal-bd">
            请审核，确认还是驳回？
        </div>
        <div class="am-modal-footer">
            <span class="am-modal-btn" data-am-modal-cancel id="rejectstatus">驳回</span>
            <span class="am-modal-btn" data-am-modal-confirm id="surestatus">确定</span>
        </div>
    </div>
</div>
<div class="am-modal am-modal-confirm" tabindex="-1" id="my-confirm2">
    <div class="am-modal-dialog">
        <div class="am-modal-hd"></div>
        <div class="am-modal-bd">
            请审核，确认还是驳回？
        </div>
        <div class="am-modal-footer">
            <span class="am-modal-btn" data-am-modal-cancel id="rejectstatus">驳回</span>
            <span class="am-modal-btn" data-am-modal-confirm id="surestatus">确定</span>
        </div>
    </div>
</div>
<div class="am-modal am-modal-confirm" tabindex="-1" id="my-confirm3">
    <div class="am-modal-dialog">
        <div class="am-modal-hd"></div>
        <div class="am-modal-bd">
            请审核，确认还是驳回？
        </div>
        <div class="am-modal-footer">
            <span class="am-modal-btn" data-am-modal-cancel id="rejectstatus">驳回</span>
            <span class="am-modal-btn" data-am-modal-confirm id="surestatus">确定</span>
        </div>
    </div>
</div>
<script src="${mainDomain}/statics/assets/js/amazeui.min.js"></script>
<script src="${mainDomain}/statics/assets/js/amazeui.datatables.min.js"></script>
<script src="${mainDomain}/statics/assets/js/dataTables.responsive.min.js"></script>
<script src="${mainDomain}/statics/assets/js/app.js"></script>
<script src="${mainDomain}/statics/js/front/fee/list.js"></script>
</body>