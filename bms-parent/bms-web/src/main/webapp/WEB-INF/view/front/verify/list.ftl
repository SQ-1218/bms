<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>货主列表</title>
    <meta name="keywords" content="index">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <link rel="icon" type="image/png" href="${mainDomain}/statics/assets/i/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="${mainDomain}/statics/assets/i/app-icon72x72@2x.png">
    <meta name="apple-mobile-web-app-title" content="Amaze UI" />
    <link rel="stylesheet" href="${mainDomain}/statics/assets/css/amazeui.min.css" />
    <link rel="stylesheet" href="${mainDomain}/statics/assets/css/amazeui.datatables.min.css" />
    <link rel="stylesheet" href="${mainDomain}/statics/assets/css/app.css">
    <script src="${mainDomain}/statics/assets/js/jquery.min.js"></script>
    <script>
        var mainDomain = "${mainDomain}";
    </script>
</head>
<style>
    th, td {
        white-space: nowrap;
        text-align: center;
    };
</style>
<body data-type="widgets" class="theme-black">
<div class="tpl-content-wrapper">
    <div class="row-content am-cf">
        <div class="row">
            <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
                <div class="widget am-cf">
                    <div class="widget-head am-cf">
                        <div class="widget-title  am-cf">月度费用审核记录</div>
                    </div>

                    <div class="widget-body  am-fr">
                        <div class="am-u-sm-12 am-u-md-6 am-u-lg-4">
                            <div class="am-form-group tpl-table-list-select">
                                <span style="margin-right:10px">货主姓名:</span>
                                <select id="owner_no"    data-am-selected="{maxHeight: 100}">
                                    <#--<option value="${owner.no}" selected>点击选择户主</option>-->
                                <#list owner as owner>
                                    <option value="${owner.no}">${owner.name}</option>
                                </#list>
                                </select>
                            </div>
                        </div>

                        <div class="am-u-sm-12 am-u-md-3 am-u-lg-3">
                            <div class="am-input-group am-datepicker-date"
                                 data-am-datepicker="{format: 'yyyy-mm', viewMode: 'years', minViewMode: 'months'}">
                                <input type="text" class="am-form-field" placeholder="选择月份" readonly id="months_time"
                                       name="months_time" value="">
                                <span class="am-input-group-btn am-datepicker-add-on">
    <button class="am-btn am-btn-default" type="button"><span class="am-icon-calendar"></span> </button>
  </span>
                            </div>
                        </div>
                        <div class="am-u-sm-12 am-u-md-3 am-u-lg-3">
                            <div class="am-input-group am-datepicker-date" data-am-datepicker="{format: 'yyyy-mm-dd'}">
                                <input type="text" class="am-form-field" placeholder="选择操作时间" readonly id="day_time"
                                       name="day_time" value="">
                                <span class="am-input-group-btn am-datepicker-add-on">
    <button class="am-btn am-btn-default" type="button"><span class="am-icon-calendar"></span> </button>
  </span>
                            </div>
                        </div>
                        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
                            <div class="am-form-group">
                                <div class="am-btn-toolbar">
                                    <div class="am-btn-group am-btn-group-xs">
                                        <button type="button" class="am-btn am-btn-primary am-btn-sm" onclick="search()"><span class="am-icon-search"></span>&nbsp;查询</button>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="tableArea" class="am-u-sm-12">
                            <table id="tb_verify" width="100%" class="am-table am-table-compact am-table-striped tpl-table-black " id="example-r">
                                <thead>
                                <tr>
                                    <th>货主</th>
                                    <th>操作时间</th>
                                    <th>年份</th>
                                    <th>月份</th>
                                    <th>类型</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="${mainDomain}/statics/assets/js/amazeui.min.js"></script>
<script src="${mainDomain}/statics/assets/js/amazeui.datatables.min.js"></script>
<script src="${mainDomain}/statics/assets/js/dataTables.responsive.min.js"></script>
<script src="${mainDomain}/statics/assets/js/app.js"></script>
<script src="${mainDomain}/statics/js/front/verify/list.js"></script>
</body>