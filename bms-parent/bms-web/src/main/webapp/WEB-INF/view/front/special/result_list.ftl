<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>特殊费计费结果列表</title>
    <meta name="keywords" content="index">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <link rel="icon" type="image/png" href="${mainDomain}/statics/assets/i/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="${mainDomain}/statics/assets/i/app-icon72x72@2x.png">
    <meta name="apple-mobile-web-app-title" content="Amaze UI" />
    <link rel="stylesheet" href="${mainDomain}/statics/assets/css/amazeui.min.css" />
    <link rel="stylesheet" href="${mainDomain}/statics/assets/css/amazeui.datatables.min.css" />
    <link rel="stylesheet" href="${mainDomain}/statics/assets/css/app.css">
    <script src="${mainDomain}/statics/assets/js/jquery.min.js"></script>
    <script>
        var mainDomain = "${mainDomain}";
    </script>
</head>
<style>
    th, td {
        white-space: nowrap;
        text-align: center;
    };
</style>
<body data-type="widgets" class="theme-black">
<div class="tpl-content-wrapper">
    <div class="row-content am-cf">
        <div class="row">
            <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
                <div class="widget am-cf">
                    <div class="widget-head am-cf">
                        <div class="widget-title  am-cf">特殊费计费结果列表</div>
                    </div>

                    <div class="widget-body  am-fr">
                    <div class="am-u-sm-12 am-u-md-4 am-u-lg-4">
                            <div class="am-form-group tpl-table-list-select">
                                <span style="margin-right:10px">货主名称:</span>
                                <select id="owner_no"  multiple  data-am-selected="{maxHeight: 100}" >
                                <#list owner as owner>
                                    <option value="${owner.no}">${owner.name}</option>
                                </#list>
                                </select>
                            </div>
                        </div>
                        <div class="am-u-sm-12 am-u-md-4 am-u-lg-5">
                            <div class="am-form-group tpl-table-list-select">
                                <span style="margin-right:10px">计费项目名称:</span>
                                <select id="cal_item_code"  multiple  data-am-selected="{maxHeight: 100}" >
                                <#list resultSpecial as special>
                                    <option value="${special.code}">${special.name}</option>
                                </#list>
                                </select>
                            </div>
                        </div>
                        <div class="am-u-sm-12 am-u-md-3 am-u-lg-4">
                            <div class="am-form-group tpl-table-list-select">
                                <span style="margin-right:10px">单据号:</span>
                                <input type="text" id="doc_no" name="doc_no" style="color: #333333">
                            </div>
                        </div>
                        <div class="am-u-sm-12 am-u-md-4 am-u-lg-5">
                            <div class="am-form-group tpl-table-list-select">
                                <span style="margin-right:10px">是否已确认结算:</span>
                                <select id="is_confirmed" data-am-selected="{maxHeight: 100}" >
                                    <option value="-1">点击选择...</option>
                                    <option value="0">已确认</option>
                                    <option value="1">未确认</option>
                                </select>
                            </div>
                        </div>
                        <div class="am-alert am-alert-danger" id="my-alert" style="display: none">
                            <p>开始日期应小于结束日期！</p>
                        </div>
                        <div class="am-g">
                            <div class="am-u-sm-4" style="margin-bottom: 10px;margin-left:25px">
                                <button type="button" class="am-btn am-btn-default am-margin-right" id="my-start">开始日期</button><span id="my-startDate">2014-12-20</span><input type="hidden" id="start_time" name="start_time" value="">
                            </div>
                            <div class="am-u-sm-6" style="margin-bottom: 10px;margin-right:85px">
                                <button type="button" class="am-btn am-btn-default am-margin-right" id="my-end">结束日期</button><span id="my-endDate">2014-12-25</span><input type="hidden" id="end_time" name="end_time" value="">
                            </div>
                        </div>
                        <div class="am-u-sm-12 am-u-md-8 am-u-lg-8">
                            <div class="am-form-group">
                                <div class="am-btn-toolbar">
                                    <div class="am-btn-group am-btn-group-xs">
                                        <button type="button" class="am-btn am-btn-default am-btn-success" onclick="edit()"><span class="am-icon-plus"></span>新增</button>
                                        <button type="button" class="am-btn am-btn-default am-btn-danger" onclick="batchDelete()"><span class="am-icon-trash-o"></span> 批量删除</button>
                                        <a href="${mainDomain}/bms/resultSpecial/exportExcel" class="am-btn am-btn-default am-btn-danger"><span class="am-icon-upload"></span> 导出</a>
                                        <button type="button" class="am-btn am-btn-primary am-btn-sm" onclick="search()"><span class="am-icon-search"></span>&nbsp;查询</button>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="am-u-sm-12">
                            <table id="tb_result_special" width="100%" class="am-table am-table-compact am-table-striped tpl-table-black " id="example-r">
                                <thead>
                                <tr>
                                    <th><input type="checkbox" class="checkAll">全选</th>
                                    <th>序号</th>
                                    <th>货主名称</th>
                                    <th>计费项名称</th>
                                    <th>计算方式</th>
                                    <th>单据号</th>
                                    <th>计费结果</th>
                                    <th>费用产生时间</th>
                                    <th>是否已结算</th>
                                    <th>备注</th>
                                    <th>确认结算时间</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<button id="np-s" class="am-btn am-btn-primary"style="display: none">$.AMUI.progress.start();</button>
<button id="np-d" class="am-btn am-btn-success" style="display: none">$.AMUI.progress.done();</button>
<script src="${mainDomain}/statics/assets/js/amazeui.min.js"></script>
<script src="${mainDomain}/statics/assets/js/amazeui.datatables.min.js"></script>
<script src="${mainDomain}/statics/assets/js/dataTables.responsive.min.js"></script>
<script src="${mainDomain}/statics/assets/js/app.js"></script>
<script src="${mainDomain}/statics/js/front/special/result_list.js"></script>
</body>