<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>我的资料</title>
    <meta name="keywords" content="index">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <link rel="icon" type="image/png" href="${mainDomain}/statics/assets/i/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="${mainDomain}/statics/assets/i/app-icon72x72@2x.png">
    <meta name="apple-mobile-web-app-title" content="Amaze UI" />
    <link rel="stylesheet" href="${mainDomain}/statics/assets/css/amazeui.min.css" />
    <link rel="stylesheet" href="${mainDomain}/statics/assets/css/amazeui.datatables.min.css" />
    <link rel="stylesheet" href="${mainDomain}/statics/assets/css/app.css">
    <script src="${mainDomain}/statics/assets/js/jquery.min.js"></script>
    <script>
        var mainDomain = "${mainDomain}";
    </script>
    <style>
        .am-form-group{
            height: 50px;
        }
    </style>
</head>

<body data-type="widgets" class="theme-black">
<div class="tpl-content-wrapper">
    <div class="row-content am-cf">
        <div class="row">
            <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
                <div class="widget am-cf">
                    <div class="widget-head am-cf">
                        <#if user.id != null>
                        <div class="widget-title  am-cf">密码修改</div>
                        <#else>
                        <div class="widget-title  am-cf">密码修改</div>
                        </#if>
                        <div class="am-md-text-right">
                        <button type="button" class="am-btn am-btn-primary am-btn-sm"  onclick="javascript:window.history.back(-1);"><span class="am-icon-backward"></span> 返回</button>
                        </div>
                    </div>
                    
                         <li>
                          <div class="am-g bw-style">
                            <div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                            <div class="am-form-group am-form-field">
                                <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                                    <label class="am-form-label">旧密码:</label >
                                </div>
                                <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-md-text-left">
                                    <input class="am-input-lg am-list-item-text" type="text" placeholder="请输入旧密码" style="margin-left: 10px" required id="password"  name="password" value="${user.password}">
                                </div>
                            </div>
                           </div>
                           </li>
                          
                          <li>
                          <div class="am-g bw-style">
                            <div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                            <div class="am-form-group am-form-field">
                                <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                                    <label class="am-form-label">新密码:</label >
                                </div>
                                <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-md-text-left">
                                    <input class="am-input-lg am-list-item-text" type="text" placeholder="请输入新密码" style="margin-left: 10px" required id="newPassword"  name="newPassword" value="${user.newPassword}">
                                </div>
                            </div>
                           </div>
                           </li>
                           
                          <li>
                          <div class="am-g bw-style">
                            <div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                            <div class="am-form-group am-form-field">
                                <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                                    <label class="am-form-label">确认新密码:</label >
                                </div>
                                <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-md-text-left">
                                    <input class="am-input-lg am-list-item-text" type="text" placeholder="请输入新密码" style="margin-left: 10px" required id="newPassword"  name="newPassword" value="${user.newPassword}">
                                </div>
                            </div>
                           </div>
                          </li>
                          
                          <div class="am-form-group  am-form-field">
                              <div class="am-md-text-center ">
                                  <button type="reset" class="am-btn am-btn-primary am-btn-sm am-md-text-left" style="margin-right: 50px"><span class="am-icon-refresh"></span> 重置</button>
                                  <button type="button" class="am-btn am-btn-primary am-btn-sm am-md-text-left" onclick="submitForm()"><span class="am-icon-circle-o"></span>保存</button>
                              </div>
                          </div>
                    <form>
                </div>
        </div>
</div>
</div>

<script src="${mainDomain}/statics/assets/js/amazeui.min.js"></script>
<script src="${mainDomain}/statics/assets/js/amazeui.datatables.min.js"></script>
<script src="${mainDomain}/statics/assets/js/dataTables.responsive.min.js"></script>
<script src="${mainDomain}/statics/assets/js/app.js"></script>
<script src="${mainDomain}/statics/js/front/user/index.js"></script>
</body>
</html>