<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>出库费用规则列表</title>
    <meta name="keywords" content="index">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <link rel="icon" type="image/png" href="${mainDomain}/statics/assets/i/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="${mainDomain}/statics/assets/i/app-icon72x72@2x.png">
    <meta name="apple-mobile-web-app-title" content="Amaze UI" />
    <link rel="stylesheet" href="${mainDomain}/statics/assets/css/amazeui.min.css" />
    <link rel="stylesheet" href="${mainDomain}/statics/assets/css/amazeui.datatables.min.css" />
    <link rel="stylesheet" href="${mainDomain}/statics/assets/css/app.css">
    <script src="${mainDomain}/statics/assets/js/jquery.min.js"></script>
    <script>
        var mainDomain = "${mainDomain}";
    </script>
    <style>
        th,td{
            white-space:nowrap;
            text-align: center;
        }
    </style>
</head>

<body data-type="widgets" class="theme-black">
<div class="tpl-content-wrapper">
    <div class="row-content am-cf">
        <div class="row">
            <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
                <div class="widget am-cf">
                    <div class="widget-head am-cf">
                        <div class="widget-title  am-cf">出库费用规则列表</div>
                    </div>
                    <div class="widget-body  am-fr">
                        <div class="widget-head am-cf">
                        <div class="am-u-sm-12 am-u-md-4 am-u-lg-4">
                            <div class="am-form-group tpl-table-list-select">
                                <span style="margin-right:10px">货主姓名:</span>
                                <select id="owner_no"  multiple  data-am-selected="{maxHeight: 100}">
                                 <#list owner as owner>
                                   <option value="${owner.no}">${owner.name}</option>
                                 </#list>
                                </select>
                            </div>
                        </div>
                        <div class="am-u-sm-12 am-u-md-6 am-u-lg-6">
                            <div class="am-form-group tpl-table-list-select am-md-text-left">
                                <span style="margin-right:10px">计费项名称:</span>
                                <select id="cal_item_code" multiple data-am-selected="{maxHeight: 100}">
                                  <#list feeRuleOperDeliveryItem as item>
                                      <option value="${item.code}">${item.name}</option>
                                  </#list>
                                </select>
                            </div>
                        </div>
                        </div>
                        <div class="am-u-sm-12 am-u-md-6 am-u-lg-6">
                            <div class="am-form-group">
                                <div class="am-btn-toolbar">
                                    <div class="am-btn-group am-btn-group-xs">
                                        <button type="button" class="am-btn am-btn-default am-btn-success" onclick="edit()"><span class="am-icon-plus"></span>新增</button>
                                        <button type="button" class="am-btn am-btn-default am-btn-danger" onclick="batchDelete()"><span class="am-icon-trash-o"></span> 批量删除</button>
                                        <button type="button" class="am-btn am-btn-default am-btn-success" id="doc-prompt-toggle"><span class="am-icon-download"></span>&nbsp;导入</button>
                                        <a href="${mainDomain}/bms/delivery/exportExcel" onclick="exportExcel()" class="am-btn am-btn-danger am-btn-sm"><span class="am-icon-upload">&nbsp;导出</a>
                                        <button type="button" class="am-btn am-btn-primary am-btn-sm" onclick="search()"><span class="am-icon-search"></span>查询</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="am-u-sm-12">
                            <table id="tb_delivery" width="100%" class="am-table am-table-compact am-table-striped tpl-table-black ">
                                <thead>
                                <tr>
                                    <th><input type="checkbox" class="checkAll">全选</th>
                                    <th>序号</th>
                                    <th>货主</th>
                                    <th>计费项序号</th>
                                    <th>计费项名称</th>
                                    <th>计费项解释</th>
                                    <th>单据类型</th>
                                    <th>计算方式</th>
                                    <th>系数1名称</th>
                                    <th>系数1的值</th>
                                    <th>系数2名称</th>
                                    <th>系数2的值</th>
                                    <th>系数3名称</th>
                                    <th>系数3的值</th>
                                    <th>系数4名称</th>
                                    <th>系数4的值</th>
                                    <th>系数5名称</th>
                                    <th>系数5的值</th>
                                    <th>备注</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="am-modal am-modal-prompt" tabindex="-1" id="my-prompt">
    <div class="am-modal-dialog">
        <div class="am-modal-hd">导入EXCEL文件</div>
        <div class="am-modal-bd">
            <form id="importExcel" action="/bms/courier/importExcel" method="post" enctype="multipart/form-data">
                <input id="file" name="file" width="100px" type="file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
            </form>
            <span style="color: gray;margin-right: 100px">导入文件不能超过10M，仅允许导入“xls”或“xlsx”格式文件！</span>
            <a href="${mainDomain}/bms/delivery/download" class="am-btn am-btn-primary am-btn-sm" style="margin-right: 90%"> 下载模板</a>
        </div>
        <div class="am-modal-footer">
            <span class="am-modal-btn" data-am-modal-cancel>取消</span>
            <span class="am-modal-btn" data-am-modal-confirm>提交</span>
        </div>
    </div>
</div>
<div class="am-modal am-modal-alert" tabindex="-1" id="my-alert">
    <div class="am-modal-dialog">
        <div class="am-modal-hd">计费项解释</div>
        <div class="am-modal-bd" id="note">
        </div>
        <div class="am-modal-footer">
            <span class="am-modal-btn">确定</span>
        </div>
    </div>
</div>
</div>
<script src="${mainDomain}/statics/assets/js/amazeui.min.js"></script>
<script src="${mainDomain}/statics/assets/js/amazeui.datatables.min.js"></script>
<script src="${mainDomain}/statics/assets/js/dataTables.responsive.min.js"></script>
<script src="${mainDomain}/statics/assets/js/app.js"></script>
<script src="${mainDomain}/statics/js/front/delivery/list.js"></script>
</body>

</html>