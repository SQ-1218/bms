<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>货主列表</title>
<meta name="keywords" content="index">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" type="image/png" href="${mainDomain}/statics/assets/i/favicon.png">
<link rel="apple-touch-icon-precomposed" href="${mainDomain}/statics/assets/i/app-icon72x72@2x.png">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" href="${mainDomain}/statics/assets/css/amazeui.min.css" />
<link rel="stylesheet" href="${mainDomain}/statics/assets/css/amazeui.datatables.min.css" />
<link rel="stylesheet" href="${mainDomain}/statics/assets/css/app.css">
<script src="${mainDomain}/statics/assets/js/jquery.min.js"></script>
<script>
	var mainDomain = "${mainDomain}";
</script>
</head>

<body data-type="widgets">
        <div class="tpl-content-wrapper">
            <div class="row-content am-cf">
                <div class="row">
                    <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
                        <div class="widget am-cf">
                            <div class="widget-head am-cf">
                                <div class="widget-title  am-cf">货主列表</div>
                            </div>
                            <div class="widget-body  am-fr">
                                <div class="am-u-sm- am-u-md-8 am-u-lg-8">
                                    <div class="am-form-group">
                                        <div class="am-btn-toolbar">
                                            <div class="am-btn-group am-btn-group-xs">
                                                <button type="button" class="am-btn am-btn-default am-btn-success"><span class="am-icon-plus"></span> 新增</button>
                                                <button type="button" class="am-btn am-btn-default am-btn-danger"><span class="am-icon-trash-o"></span> 删除</button>
                                            	<button type="button" class="am-btn am-btn-primary am-btn-sm" onclick="search()"><span class="am-icon-search"></span>查询</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="am-u-sm-12 am-u-md-4 am-u-lg-4">
                                    <div class="am-form-group tpl-table-list-select">
                                    	<span style="margin-right:10px">状态:</span>
                                        <select id="is_deleted" data-am-selected="{btnSize: 'sm'}" onchange="search()">
							              <option value="-99">全部</option>
							              <option value="0">删除</option>
							              <option value="1">未删除</option>
							            </select>
                                    </div>
                                </div>

                                <div id="tableArea" class="am-u-sm-12">
                                    <table id="tb_owner" width="100%" class="am-table am-table-compact am-table-striped tpl-table-black "
                                     id="example-r" style="white-space: nowrap;">
                                        <thead>
                                            <tr>
                                                <th>序号</th>
                                                <th>编号</th>
                                                <th>名称</th>
                                                <th>余额</th>
                                                <th>预警余额</th>
                                                <th>操作</th>
                                                <th>编号</th>
                                                <th>编号</th>
                                                <th>编号</th>
                                                <th>编号</th>
                                                <th>编号</th>
                                                <th>编号</th>
                                                <th>编号</th>
                                                <th>编号</th>
                                                <th>编号</th>
                                                <th>编号</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    </div>
    <script src="${mainDomain}/statics/assets/js/amazeui.min.js"></script>
    <script src="${mainDomain}/statics/assets/js/amazeui.datatables.min.js"></script>
    <script src="${mainDomain}/statics/assets/js/dataTables.responsive.min.js"></script>
    <script src="${mainDomain}/statics/assets/js/app.js"></script>
	<script src="${mainDomain}/statics/js/front/owner/list.js"></script>
</body>

</html>