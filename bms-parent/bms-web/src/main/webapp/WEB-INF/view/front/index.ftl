<!DOCTYPE html>
<html lang="en">
<head> 
<script>
	var mainDomain = "${mainDomain}";
</script> 
<meta charset="utf-8" /> 
<meta http-equiv="X-UA-Compatible" content="IE=edge" /> 
<title>首页</title> 
<meta name="description" content="这是一个 index 页面" /> 
<meta name="keywords" content="index" /> 
<meta name="viewport" content="width=device-width, initial-scale=1" /> 
<meta name="renderer" content="webkit" /> 
<meta http-equiv="Cache-Control" content="no-siteapp" /> 
<link rel="icon" type="image/png" href="${mainDomain}/statics/assets/i/favicon.png" /> 
<link rel="apple-touch-icon-precomposed" href="${mainDomain}/statics/assets/i/app-icon72x72@2x.png" /> 
<meta name="apple-mobile-web-app-title" content="Amaze UI" /> 
<link rel="stylesheet" href="${mainDomain}/statics/assets/css/amazeui.min.css" /> 
<link rel="stylesheet" href="${mainDomain}/statics/assets/css/amazeui.datatables.min.css" /> 
<link rel="stylesheet" href="${mainDomain}/statics/assets/css/app.css" />
<script src="${mainDomain}/statics/assets/js/jquery.min.js"></script>
<script src="${mainDomain}/statics/assets/js/amazeui.min.js"></script>
<script src="${mainDomain}/statics/assets/js/amazeui.datatables.min.js"></script> 
<script src="${mainDomain}/statics/assets/js/dataTables.responsive.min.js"></script> 
<script src="${mainDomain}/statics/assets/js/index_frame.js"></script> 
<script>
    var mainDomain = "${mainDomain}";
</script>
</head> 

<body data-type="index">
    <script src="${mainDomain}/statics/assets/js/theme.js"></script>
    <div class="am-g tpl-g">
        <!-- 头部 -->
        <header>
            <!-- logo -->
            <div class="am-fl tpl-header-logo">
                <a href="javascript:;"><img src="${mainDomain}/statics/assets/img/logo.png" alt=""></a>
            </div>
            <!-- 右侧内容 -->
            <div class="tpl-header-fluid">
                <!-- 侧边切换 -->
                <div class="am-fl tpl-header-switch-button am-icon-list">
                    <span>
                </span>
                </div>
                <!-- 其它功能-->
                <div class="am-fr tpl-header-navbar">
                    <ul>
                        <!-- 欢迎语 -->
                        <li class="am-text-sm tpl-header-navbar-welcome">
                            <a href="javascript:;">欢迎您, <span><#if sysUser.full_name??>${sysUser.full_name}</#if></span> </a>
                        </li>
                        
                        <!-- 我的资料（密码修改） -->
                        <li id="edit" class="am-text-sm">
                            <a href="javascript:;">
                                <span>我的资料 </span> 
                            </a> 
  
                        <!-- 退出 -->
                        <li id = "logout" class="am-text-sm">
                           <#-- <a href="/sys/login/sys/login"> -->
                           <a href="javascript:;">
                                <span class="am-icon-sign-out"></span> 退出
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
        <!-- 侧边导航栏 -->
        <div class="left-sidebar">
            <!-- 菜单 -->
            <ul class="sidebar-nav" id="menu">
                <li class="sidebar-nav-link">
                    <a href="${mainDomain}/bms/forward/index" class="active">
                        <i class="am-icon-home sidebar-nav-link-logo"></i> 首页
                    </a>
                </li>
                <li class="sidebar-nav-link">
                    <a href="javascript:;" class="sidebar-nav-sub-title">
                        <i class="am-icon-table sidebar-nav-link-logo"></i> 计费规则
                        <span class="am-icon-chevron-down am-fr am-margin-right-sm sidebar-nav-sub-ico"></span>
                    </a>
                    <ul class="sidebar-nav sidebar-nav-sub"  style="margin-top: 5px;">
                        <li class="sidebar-nav-link">
                            <a href="${mainDomain}/bms/courier/forward/list" target="iframe">
                            <span class="am-icon-angle-right sidebar-nav-link-logo"></span> 快递计费规则
                        </a>
                        </li>
                    </ul>
                    <ul class="sidebar-nav sidebar-nav-sub"  style="margin-top: 5px;">
                        <li class="sidebar-nav-link">
                            <a href="${mainDomain}/bms/storage/forward/list" target="iframe">
                                <span class="am-icon-angle-right sidebar-nav-link-logo"></span> 仓储费用规则
                            </a>
                        </li>
                    </ul>
                    <ul class="sidebar-nav sidebar-nav-sub"  style="margin-top: 5px;">
                        <li class="sidebar-nav-link">
                            <a href="${mainDomain}/bms/receiving/forward/list" target="iframe">
                                <span class="am-icon-angle-right sidebar-nav-link-logo"></span> 入库计费规则
                            </a>
                        </li>
                    </ul>
                    <ul class="sidebar-nav sidebar-nav-sub"  style="margin-top: 5px;">
                        <li class="sidebar-nav-link">
                            <a href="${mainDomain}/bms/delivery/forward/list" target="iframe">
                            <span class="am-icon-angle-right sidebar-nav-link-logo"></span> 出库计费规则
                        </a>
                        </li>
                    </ul>
                    <ul class="sidebar-nav sidebar-nav-sub" style="margin-top: 5px;">
                        <li class="sidebar-nav-link">
                            <a href="${mainDomain}/bms/special/forward/list" target="iframe">
                            <span class="am-icon-angle-right sidebar-nav-link-logo"></span> 特殊计费规则
                        </a>
                        </li>
                    </ul>
                    <ul class="sidebar-nav sidebar-nav-sub" style="margin-top: 5px;">
                        <li class="sidebar-nav-link">
                            <a href="${mainDomain}/bms/bdPackMaterial/forward/list" target="iframe">
                                <span class="am-icon-angle-right sidebar-nav-link-logo"></span> 包材管理
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-nav-link">
                    <a href="javascript:;" class="sidebar-nav-sub-title">

                        <i class="am-icon-table sidebar-nav-link-logo"></i> 计费结果

                        <span class="am-icon-chevron-down am-fr am-margin-right-sm sidebar-nav-sub-ico"></span>
                    </a>
                    <ul class="sidebar-nav sidebar-nav-sub"  style="margin-top: 5px;">
                        <li class="sidebar-nav-link">
                            <a href="${mainDomain}/bms/resultStorage/forward/result_list" target="iframe">
                                <span class="am-icon-angle-right sidebar-nav-link-logo"></span> 仓储计费结果
                            </a>
                        </li>
                    </ul>
                    <ul class="sidebar-nav sidebar-nav-sub"  style="margin-top: 5px;">
                        <li class="sidebar-nav-link">
                            <a href="${mainDomain}/bms/express/forward/list" target="iframe">
                                <span class="am-icon-angle-right sidebar-nav-link-logo"></span> 快递计费结果
                            </a>
                        </li>
                    </ul>
                    <ul class="sidebar-nav sidebar-nav-sub"  style="margin-top: 5px;">
                        <li class="sidebar-nav-link">
                            <a href="${mainDomain}/bms/resultOperation/forward/list" target="iframe">
                                <span class="am-icon-angle-right sidebar-nav-link-logo"></span> 操作计费结果
                            </a>
                        </li>
                    </ul>
                    <ul class="sidebar-nav sidebar-nav-sub"  style="margin-top: 5px;">
                        <li class="sidebar-nav-link">
                            <a href="${mainDomain}/bms/resultSpecial/forward/result_list" target="iframe">
                                <span class="am-icon-angle-right sidebar-nav-link-logo"></span> 特殊计费结果
                            </a>
                        </li>
                    </ul>
                    <ul class="sidebar-nav sidebar-nav-sub"  style="margin-top: 5px;">
                        <li class="sidebar-nav-link">
                            <a href="${mainDomain}/bms/resultPackMaterial/forward/result_list" target="iframe">
                                <span class="am-icon-angle-right sidebar-nav-link-logo"></span> 包装计费结果
                            </a>
                        </li>
                    </ul>
                    <ul class="sidebar-nav sidebar-nav-sub"  style="margin-top: 5px;">
                        <li class="sidebar-nav-link">
                            <a href="${mainDomain}/bms/fee/forward/list" target="iframe">
                                <span class="am-icon-angle-right sidebar-nav-link-logo"></span> 月度费用管理
                            </a>
                        </li>
                    </ul>
                    <ul class="sidebar-nav sidebar-nav-sub"  style="margin-top: 5px;">
                    <li class="sidebar-nav-link">
                        <a href="${mainDomain}/bms/verify/forward/list" target="iframe">
                            <span class="am-icon-angle-right sidebar-nav-link-logo"></span> 月度费用审核记录
                        </a>
                    </li>
                </ul>
                    <ul class="sidebar-nav sidebar-nav-sub"  style="margin-top: 5px;">
                        <li class="sidebar-nav-link">
                            <a href="${mainDomain}/bms/owner/forward/balance_list" target="iframe">
                                <span class="am-icon-angle-right sidebar-nav-link-logo"></span> 货主余额
                            </a>
                        </li>
                    </ul>
                    <ul class="sidebar-nav sidebar-nav-sub"  style="margin-top: 5px;">
                        <li class="sidebar-nav-link">
                            <a href="${mainDomain}/bms/owner/forward/log_list" target="iframe">
                                <span class="am-icon-angle-right sidebar-nav-link-logo"></span> 货主余额变更记录
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="body_cont">
            <iframe id="mainiframe" src="${mainDomain}/bms/forward/index_body" name="iframe" frameborder="0"></iframe>
        </div>
    </div>

    <script>
    //iframe自适应高度
    function changeFrameHeight() {
        var ifm = document.getElementById("mainiframe");
        ifm.height = document.documentElement.clientHeight - 66;
    }
    changeFrameHeight()
    window.onresize = function() {
        changeFrameHeight();
    }
    </script>

   <script src="${mainDomain}/statics/js/front/user/index.js"></script>
    
</body>   
</html>