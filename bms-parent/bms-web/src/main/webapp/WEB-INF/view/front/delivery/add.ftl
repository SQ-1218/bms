<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>出库费用规则列表</title>
    <meta name="keywords" content="index">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <link rel="icon" type="image/png" href="${mainDomain}/statics/assets/i/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="${mainDomain}/statics/assets/i/app-icon72x72@2x.png">
    <meta name="apple-mobile-web-app-title" content="Amaze UI" />
    <link rel="stylesheet" href="${mainDomain}/statics/assets/css/amazeui.min.css" />
    <link rel="stylesheet" href="${mainDomain}/statics/assets/css/amazeui.datatables.min.css" />
    <link rel="stylesheet" href="${mainDomain}/statics/assets/css/app.css">
    <script src="${mainDomain}/statics/assets/js/jquery.min.js"></script>
    <script>
        var mainDomain = "${mainDomain}";
    </script>
    <style>
        .am-form-group{
            height: 50px;
        }
    </style>
</head>

<body data-type="widgets" class="theme-black">
<div class="tpl-content-wrapper">
    <div class="row-content am-cf">
        <div class="row">
            <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
                <div class="widget am-cf">
                    <div class="widget-head am-cf">
                        <#if feeRuleOperDelivery.id != null>
                        <div class="widget-title  am-cf">出库费用规则编辑</div>
                        <#else>
                        <div class="widget-title  am-cf">出库费用规则添加</div>
                        </#if>
                        <div class="am-md-text-right">
                        <button type="button" class="am-btn am-btn-primary am-btn-sm"  onclick="javascript:window.history.back(-1);"><span class="am-icon-backward"></span> 返回</button>
                        </div>
                    </div>
                    <div class="widget-body  am-fr">
                        <form id="feeRuleOperDelivery" method="post">
                            <input type="hidden" name="id" value="${feeRuleOperDelivery.id}">
                           <div class="am-g bw-style">
                           <div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                             <#if feeRuleOperDelivery.owner_no != null>
                             <div class="am-form-group am-form-field">
                                 <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                                     <span class="am-form-label">货主姓名:</span>
                                 </div>
                                 <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-form-group tpl-table-list-select am-md-text-left">
                                     <input  class="am-input-lg am-list-item-text"  type="text"  value="${feeRuleOperDelivery.owner_no}" readonly>
                                 </div>
                             </div>
                             <#else>
                                 <div class="am-form-group am-form-field">
                                     <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                                         <span class="am-form-label">货主姓名:</span>
                                     </div>
                                     <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-form-group tpl-table-list-select am-md-text-left">
                                         <select name="owner_no" id="owner_no"  data-am-selected="{maxHeight: 100}" required onchange="getCount()">
                                             <option value="">请选择货主姓名</option>
                                            <#list owner as owner>
                                            <option value="${owner.no}">${owner.name}</option>
                                            </#list>
                                         </select>
                                     </div>
                                 </div>
                             </#if>
							</div>
                           <div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                            <#if feeRuleOperDelivery.cal_item_name != null>
                             <div class="am-form-group am-form-field">
                                 <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                                     <span class="am-form-label">计费项名称:</span>
                                 </div>
                                 <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-form-group tpl-table-list-select am-md-text-left">
                                     <input  class="am-input-lg am-list-item-text"  type="text"  value="${feeRuleOperDelivery.cal_item_name}" readonly>
                                 </div>
                             </div>
                            <#else>
                            <div class="am-form-group am-form-field">
                             <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                            <span class="am-form-label">计费项名称:</span>
                            </div>
                            <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-form-group tpl-table-list-select am-md-text-left">
                                <select name="cal_item_code" id="cal_item_code" data-am-selected="{maxHeight: 100}" required onchange="getItemName()">
                                    <option value="">请选择计费项名称</option>
                                      <#list item as item>
                                        <option value="${item.code}">${item.name}</option>
                                      </#list>
                                </select>
                                <input type="hidden" value="" id="cal_item_name" name="cal_item_name">
                            </div>
                        </div>
                            </#if>
						</div>
					  </div>
					  
					  <div class="am-g bw-style">
                       <div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                        <div class="am-form-group am-form-field">
                            <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                            <span class="am-form-label" style="margin-right: 10px">计算方式:</span>
                            </div>
                            <div id="cal_way"
                                 class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-form-group tpl-table-list-select am-md-text-left"
                                 style="display: block;float: right;">
                                <select name="cal_way" id="calWay" data-am-selected="{maxHeight: 100}" required
                                        onchange="change_cal_way()">
                                    <option class="" value="0" selected="selected">请选择计算方式</option>
                                    <#list cal as cal>
                                        <option value="${cal.code}" <#if cal.name==feeRuleOperDelivery.cal_way>  selected="selected"</#if>>${cal.name}</option>
                                    </#list>
                            </select>
                            </div>
                            <div id="cal_way_other"
                                 class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-form-group tpl-table-list-select am-md-text-left"
                                 style="display: none;float: right;">
                                <select name="cal_way_other" id="calWayOther" data-am-selected="{maxHeight: 100}"
                                        required onchange="change_cal_way()">
                                    <option class="" value="0" selected="selected">请选择计算方式</option>
                                    <option value="2">按件数</option>
                                </select>
                            </div>
                        </div>
                       </div>
                       <div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                        <div class="am-form-group am-form-field">
                            <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                                <span class="am-form-label" style="margin-right: 10px">单据类型:</span>
                            </div>
                            <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-form-group tpl-table-list-select am-md-text-left">
                                <select name="doc_type" id="doc_type" data-am-selected="{maxHeight: 100}" >
                                    <option value="-1">请选择单据类型</option>
                                <#list doc as doc>
                                        <option value="${doc.code}" <#if doc.name==feeRuleOperDelivery.doc_type>  selected="selected"</#if>>${doc.name}</option>
                                    </#list>
                                </select>
                            </div>
                        </div>
                       </div>
                      </div>
                      
                      <div class="am-g bw-style">

                       <div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                           <div class="am-form-group am-form-field">
                               <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                                   <label class="am-form-label">备注:</label >
                               </div>
                               <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-md-text-left">
                                   <input class="am-input-lg am-list-item-text" type="text"
                                          style="margin-left: 10px ;width: 720px" required id="remark" name="remark"
                                          value="${feeRuleOperDelivery.remark}">
                               </div>
                           </div>
                       </div>
                      </div>
                      
                      <div class="am-g bw-style">
                       <div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                        <div class="am-form-group am-form-field">
                            <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                            <label class="am-form-label">计算系数1名称:</label >
                            </div>
                            <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-md-text-left">
                                <input class="am-input-lg am-list-item-text" type="text" style="margin-left: 10px"
                                       required id="factor1_note" name="factor1_note"
                                       value="${feeRuleOperDelivery.factor1_note}">
                            </div>
                         </div>
                        </div>
                        <div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                        <div class="am-form-group am-form-field">
                                <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                                    <label class="am-form-label">计算系数1值:</label >
                                </div>
                                <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-md-text-left">
                                    <input class="am-input-lg am-list-item-text" type="text" style="margin-left: 10px"
                                           required id="factor1_value" name="factor1_value"
                                           value="${feeRuleOperDelivery.factor1_value}">
                                </div>
                            </div>
                        </div>
                       </div>

                            <div class="am-g bw-style">
                                <div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                                    <div class="am-form-group am-form-field">
                                        <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                                            <label class="am-form-label">计算系数2名称:</label>
                                        </div>
                                        <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-md-text-left">
                                            <input class="am-input-lg am-list-item-text" type="text"
                                                   style="margin-left: 10px" required id="factor2_note"
                                                   name="factor2_note" value="${feeRuleStorage.factor2_note}">
                                        </div>
                                    </div>
                                </div>
                                <div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                                    <div class="am-form-group am-form-field">
                                <#if feeRuleStorage.cal_way == "按件数" >
                                    <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                                        <label class="am-form-label">计算系数2值:</label>
                                    </div>
                                    <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-md-text-left" id="qty">
                                        <select name="factor2_value1" id="cal_way_qty"
                                                data-am-selected="{maxHeight: 100}" required>
                                            <option value="">请选择计费件型</option>
                                    <#list calWayByQty as qty>
                                        <option value="${qty.code}" <#if qty.code==feeRuleOperDelivery.factor2_value>
                                                selected="selected"</#if>>${qty.name}</option>
                                    </#list>
                                        </select>
                                    </div>
                                <#else>
                                  <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                                      <label class="am-form-label">计算系数2值:</label>
                                  </div>
                                <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-md-text-left" id="factor2_name"
                                     style="display: block;margin-left: 75px">
                                    <input class="am-input-lg am-list-item-text" type="text" style="margin-left: 10px"
                                           required id="factor2_value" name="factor2_value"
                                           value="${feeRuleStorage.factor2_value}">
                                </div>
                                <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-md-text-left" id="qty"
                                     style="display: none;float: right;">
                                    <select name="factor2_value" id="cal_way_qty" data-am-selected="{maxHeight: 100}"
                                            required>
                                        <option value="">请选择计费件型</option>
                                    <#list calWayByQty as qty>
                                        <option value="${qty.code}" <#if qty.name==feeRuleOperDelivery.factor2_value>
                                                selected="selected"</#if>>${qty.name}</option>
                                    </#list>
                                    </select>
                                </div>
                                </#if>
                                    </div>
                                </div>

                            </div>
                           
                          <div class="am-g bw-style">
                           <div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                            <div class="am-form-group am-form-field">
                                <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                                    <label class="am-form-label">计算系数3名称:</label >
                                </div>
                                <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-md-text-left">
                                    <input class="am-input-lg am-list-item-text" type="text" style="margin-left: 10px"
                                           required id="factor3_note" name="factor3_note"
                                           value="${feeRuleOperDelivery.factor3_note}">
                                </div>
                            </div>
                           </div>
                          <div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                            <div class="am-form-group am-form-field">
                                <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                                    <label class="am-form-label">计算系数3值:</label >
                                </div>
                                <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-md-text-left">
                                    <input class="am-input-lg am-list-item-text" type="text" style="margin-left: 10px"
                                           required id="factor3_value" name="factor3_value"
                                           value="${feeRuleOperDelivery.factor3_value}">
                                </div>
                            </div>
                            </div>
                           </div>
                           
                           <div class="am-g bw-style">
                           <div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                            <div class="am-form-group am-form-field">
                                <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                                    <label class="am-form-label">计算系数4名称:</label >
                                </div>
                                <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-md-text-left">
                                    <input class="am-input-lg am-list-item-text" type="text" style="margin-left: 10px"
                                           required id="factor4_note" name="factor4_note"
                                           value="${feeRuleOperDelivery.factor4_note}">
                                </div>
                            </div>
                            </div>
                           <div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                            <div class="am-form-group am-form-field">
                                <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                                    <label class="am-form-label">计算系数4值:</label >
                                </div>
                                <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-md-text-left">
                                    <input class="am-input-lg am-list-item-text" type="text" style="margin-left: 10px"
                                           required id="factor4_value" name="factor4_value"
                                           value="${feeRuleOperDelivery.factor4_value}">
                                </div>
                            </div>
                            </div>
                           </div>
                           
                           <div class="am-g bw-style">
                           <div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                            <div class="am-form-group am-form-field">
                                <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                                    <label class="am-form-label">计算系数5名称:</label >
                                </div>
                                <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-md-text-left">
                                    <input class="am-input-lg am-list-item-text" type="text" style="margin-left: 10px"
                                           required id="factor5_note" name="factor5_note"
                                           value="${feeRuleOperDelivery.factor5_note}">
                                </div>
                            </div>
                           </div>
                           <div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                            <div class="am-form-group am-form-field">
                                <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                                    <label class="am-form-label">计算系数5值:</label >
                                </div>
                                <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-md-text-left">
                                    <input class="am-input-lg am-list-item-text" type="text" style="margin-left: 10px"
                                           required id="factor5_value" name="factor5_value"
                                           value="${feeRuleOperDelivery.factor5_value}">
                                </div>
                            </div>
                           </div>
                          </div>
                          <div class="am-g bw-style">
                           <div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                               <div class="am-form-group am-form-field">
                                   <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                                       <span class="am-form-label">计费项解释:</span>
                                   </div>
                                   <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-md-text-left">
                                       <input class="am-input-lg am-list-item-text" type="text"
                                              style="margin-left: 10px" required min="0" id="cal_item_note"
                                              name="cal_item_note" value="${feeRuleOperDelivery.cal_item_note}">
                                   </div>
                               </div>
                           </div>
                              <div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                                  <div class="am-form-group am-form-field">
                                      <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                                          <span class="am-form-label">计算项序号:</span>
                                      </div>
                                      <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-md-text-left">
                                          <input class="am-input-lg am-list-item-text" type="text"
                                                 style="margin-left: 10px" required placeholder="请先选择货主"
                                                 id="cal_item_rank" name="cal_item_rank"
                                                 value="${feeRuleOperDelivery.cal_item_rank}" readonly>
                                      </div>
                                  </div>
                              </div>
                           </div>
                          <div class="am-form-group  am-form-field">
                              <div class="am-md-text-center ">
                                  <button type="reset" class="am-btn am-btn-primary am-btn-sm am-md-text-left" style="margin-right: 50px"><span class="am-icon-refresh"></span> 重置</button>
                                  <button type="button" class="am-btn am-btn-primary am-btn-sm am-md-text-left" onclick="submitForm()"><span class="am-icon-circle-o"></span> 提交</button>
                              </div>
                          </div>
                            <form>
                </div>
    </div>
</div>
</div>
<script src="${mainDomain}/statics/assets/js/amazeui.min.js"></script>
<script src="${mainDomain}/statics/assets/js/amazeui.datatables.min.js"></script>
<script src="${mainDomain}/statics/assets/js/dataTables.responsive.min.js"></script>
<script src="${mainDomain}/statics/assets/js/app.js"></script>
<script src="${mainDomain}/statics/js/front/delivery/list.js"></script>
</body>
</html>