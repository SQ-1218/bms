<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>后台页面</title>
    <meta name="description" content="这是一个 index 页面">
    <meta name="keywords" content="index">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <script src="${mainDomain}/statics/assets/js/echarts.min.js"></script>
    <link rel="icon" type="image/png" href="${mainDomain}/statics/assets/i/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="${mainDomain}/statics/assets/i/app-icon72x72@2x.png">
    <link rel="stylesheet" href="${mainDomain}/statics/assets/css/amazeui.min.css" />
    <link rel="stylesheet" href="${mainDomain}/statics/assets/css/amazeui.datatables.min.css" />
    <link rel="stylesheet" href="${mainDomain}/statics/assets/css/app.css">
    <meta name="apple-mobile-web-app-title" content="Amaze UI" />  
    <script src="${mainDomain}/statics/assets/js/jquery.min.js"></script>
    <script src="${mainDomain}/statics/assets/js/theme.js"></script>
</head>
<body class="theme-white" data-type="index">
 <!-- 首页 -->
  <div class="tpl-content-wrapper">
        <div class="row am-cf">
        <#--<img class="am-radius" src="${mainDomain}/statics/assets/img/user08.png" alt="1200*1000" width="1200" height="1000"> -->
        <img class="am-radius" src="${mainDomain}/statics/assets/img/user08.png" alt=" ">
        </div>  
  </div>
    <script src="${mainDomain}/statics/assets/js/amazeui.min.js"></script>
    <script src="${mainDomain}/statics/assets/js/amazeui.datatables.min.js"></script>
    <script src="${mainDomain}/statics/assets/js/dataTables.responsive.min.js"></script>
    <script src="${mainDomain}/statics/assets/js/app.js"></script>
</body>
</html>