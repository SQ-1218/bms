<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>特殊费用计费添加列表</title>
    <meta name="keywords" content="index">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <link rel="icon" type="image/png" href="${mainDomain}/statics/assets/i/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="${mainDomain}/statics/assets/i/app-icon72x72@2x.png">
    <meta name="apple-mobile-web-app-title" content="Amaze UI" />
    <link rel="stylesheet" href="${mainDomain}/statics/assets/css/amazeui.min.css" />
    <link rel="stylesheet" href="${mainDomain}/statics/assets/css/amazeui.datatables.min.css" />
    <link rel="stylesheet" href="${mainDomain}/statics/assets/css/app.css">
    <script src="${mainDomain}/statics/assets/js/jquery.min.js"></script>
    <script>
        var mainDomain = "${mainDomain}";
    </script>
    <style>
        .am-form-group{
            height: 50px;
        }
    </style>
</head>

<body data-type="widgets" class="theme-black">
<div class="tpl-content-wrapper">
    <div class="row-content am-cf">
        <div class="row">
            <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
                <div class="widget am-cf">
                    <div class="widget-head am-cf">
                        <#if feeResultSpecialModel.id != null>
                        <div class="widget-title  am-cf">特殊费用计费编辑</div>
                        <#else>
                        <div class="widget-title  am-cf">特殊费用计费添加</div>
                        </#if>
                        <div class="am-md-text-right">
                        <button type="button" class="am-btn am-btn-primary am-btn-sm"  onclick="javascript:window.history.back(-1);"><span class="am-icon-backward"></span> 返回</button>
                        </div>
                    </div>
                    <div class="widget-body  am-fr">
                        <form id="feeResultSpecial" method="post">
                            <input type="hidden" name="id" value="${feeResultSpecialModel.id}">
                           <div class="am-g bw-style">
                            <div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                             <#if feeResultSpecialModel.owner_no != null>
                             <div class="am-form-group am-form-field">
                                 <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                                     <span class="am-form-label">货主姓名:</span>
                                 </div>
                                 <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-form-group tpl-table-list-select am-md-text-left">
                                     <input  class="am-input-lg am-list-item-text"  type="text"  value="${feeResultSpecialModel.owner_no}" readonly>
                                 </div>
                             </div>
                             <#else>
                                 <div class="am-form-group am-form-field">
                                     <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                                         <span class="am-form-label">货主姓名:</span>
                                     </div>
                                     <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-form-group tpl-table-list-select am-md-text-left">
                                         <select name="owner_no" id="owner_no"  data-am-selected="{maxHeight: 100}" required">
                                             <option value="">请选择货主</option>
                                 <#list owner as owner>
                                     <option value="${owner.no}">${owner.name}</option>
                                 </#list>
                                         </select>
                                     </div>
                                 </div>
                             </#if>
							    </div>
                            	<div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                            <#if feeResultSpecialModel.cal_item_name != null>
                             <div class="am-form-group am-form-field">
                                 <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                                     <span class="am-form-label">计费项名称:</span>
                                 </div>
                                 <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-form-group tpl-table-list-select am-md-text-left">
                                     <input  class="am-input-lg am-list-item-text"  type="text"  value="${feeResultSpecialModel.cal_item_name}" readonly>
                                 </div>
                             </div>
                            <#else>
                            <div class="am-form-group am-form-field">
                             <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                            <span class="am-form-label">计费项名称:</span>
                            </div>
                            <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-form-group tpl-table-list-select am-md-text-left">
                                <select name="cal_item_code" id="cal_item_code" data-am-selected="{maxHeight: 100}" required onchange="getItemName()">
                                    <option value="">请选择计费项名称</option>
                                    <#list item as item>
                                        <option value="${item.code}">${item.name}</option>
                                    </#list>
                                </select>
                                <input type="hidden" value="" id="cal_item_name" name="cal_item_name">
                            </div>
                        </div>
                            </#if>
						 </div>
						</div>
						
						<div class="am-g bw-style">
                            <div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                            <#if feeResultSpecialModel.cal_type != null>
                             <div class="am-form-group am-form-field">
                                 <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                                     <span class="am-form-label">计算方式:</span>
                                 </div>
                                 <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-form-group tpl-table-list-select am-md-text-left">
                                     <input  class="am-input-lg am-list-item-text"  type="text"  value="${feeResultSpecialModel.cal_type}" readonly>
                                 </div>
                             </div>
                            <#else>
                        <div class="am-form-group am-form-field">
                            <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                            <span class="am-form-label" style="margin-right: 10px">计算方式:</span>
                            </div>
                            <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-form-group tpl-table-list-select am-md-text-left">
                            <select name="cal_type" id="cal_type" data-am-selected="{maxHeight: 100}" required>
                                <option value="">请选择计算方式</option>
                                    <#list cal as cal>
                                        <option value="${cal.code}">${cal.name}</option>
                                    </#list>
                            </select>
                            </div>
                        </div>
                            </#if>
						</div>
						<div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                        <div class="am-form-group am-form-field">
                            <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                            <span class="am-form-label">单据号:</span>
                            </div>
                            <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-md-text-left">
                             <input class="am-input-lg am-list-item-text" type="text" style="margin-left: 10px" required placeholder="请输入单据号" id="doc_no" name="doc_no" value="${feeResultSpecialModel.doc_no}">
                            </div>
                            </div>
                         </div>
                        </div>
                        
                        <div class="am-g bw-style">
                           <div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                        <div class="am-form-group am-form-field">
                            <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                            <span class="am-form-label">计费结果:</span>
                           </div>
                            <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-md-text-left">
                            <input class="am-input-lg am-list-item-text" type="number" placeholder="计费结果" style="margin-left: 10px" required min="0" id="fee" name="fee" value="${feeResultSpecialModel.fee}">
                            </div>
                        </div>
                       </div>
                       <div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                        <div class="am-form-group am-form-field">
                            <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                            <span class="am-form-label">备注:</span >
                            </div>
                            <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-md-text-left">
                            <input class="am-input-lg am-list-item-text" type="text" placeholder="请输入备注" style="margin-left: 10px" required id="remark"  name="remark" value="${feeResultSpecialModel.remark}">
                            </div>
                            </div>
                         </div>
                        </div>
                        
                        <div class="am-g bw-style">
                            <div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                        <div class="am-form-group am-form-field">
                                <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                                    <span class="am-form-label">费用产生时间:</span >
                                </div>
                                <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-md-text-left">
                                    <div class="am-input-group am-datepicker-date" data-am-datepicker="{format: 'yyyy-mm-dd'}">
                                        <input id="time" name="time" type="text" class="am-form-field"  readonly value="${feeResultSpecialModel.start_time}">
                                        <span class="am-input-group-btn am-datepicker-add-on">
                                       <button class="am-btn am-btn-default" type="button"><span class="am-icon-calendar"></span> </button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                           </div>
                           </div>
                          <div class="am-form-group  am-form-field">
                              <div class="am-md-text-center ">
                                  <button type="reset" class="am-btn am-btn-primary am-btn-sm am-md-text-left" style="margin-right: 50px"><span class="am-icon-refresh"></span> 重置</button>
                                  <button type="button" class="am-btn am-btn-primary am-btn-sm am-md-text-left" onclick="submitForm()"><span class="am-icon-circle-o"></span> 提交</button>
                              </div>
                          </div>
                            <form>
                </div>
    </div>
</div>
</div>
<script src="${mainDomain}/statics/assets/js/amazeui.min.js"></script>
<script src="${mainDomain}/statics/assets/js/amazeui.datatables.min.js"></script>
<script src="${mainDomain}/statics/assets/js/dataTables.responsive.min.js"></script>
<script src="${mainDomain}/statics/assets/js/app.js"></script>
<script src="${mainDomain}/statics/js/front/special/result_list.js"></script>
</body>
</html>