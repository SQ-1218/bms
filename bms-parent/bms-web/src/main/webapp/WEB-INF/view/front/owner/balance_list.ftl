<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>货主余额</title>
<meta name="keywords" content="index">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />

<link rel="icon" type="image/png" href="${mainDomain}/statics/assets/i/favicon.png">
<link rel="apple-touch-icon-precomposed" href="${mainDomain}/statics/assets/i/app-icon72x72@2x.png">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" href="${mainDomain}/statics/assets/css/amazeui.min.css" />
<link rel="stylesheet" href="${mainDomain}/statics/assets/css/amazeui.datatables.min.css" />
<link rel="stylesheet" href="${mainDomain}/statics/assets/css/app.css">
<script src="${mainDomain}/statics/assets/js/jquery.min.js"></script>
<script>
	var mainDomain = "${mainDomain}";
</script>
</head>

<body data-type="widgets" class="theme-black">
        <div class="tpl-content-wrapper">
            <div class="row-content am-cf">
                <div class="row">
                    <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
                        <div class="widget am-cf">
                            <div class="widget-head am-cf">
                                <div class="widget-title    am-cf">货主余额列表</div>
                            </div>
                            <div class="widget-body  am-fr">
                                    <div class="am-u-sm-12 am-u-md-4 am-u-lg-4">
                                        <div class="am-form-group tpl-table-list-select">
                                            <span style="margin-right:10px">货主姓名:</span>
                                            <select id="owner_no"  multiple  data-am-selected="{maxHeight: 100}">
                                 <#list owner as owner>
                                     <option value="${owner.no}">${owner.name}</option>
                                 </#list>
                                            </select>
                                        </div>
                                    </div>
                                <div class="am-u-sm-12 am-u-md-9 am-u-lg-9">
                                    <div class="am-form-group">
                                        <div class="am-btn-toolbar">
                                            <div class="am-btn-group am-btn-group-xs">
                                              <button type="button" class="am-btn am-btn-primary am-btn-sm" onclick="search()"><span class="am-icon-search"></span>查询</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="tableArea" class="am-u-sm-12">
                                    <table id="tb_balance_owner" width="100%" class="am-table am-table-compact am-table-striped tpl-table-black "
                                     id="example-r" style="white-space: nowrap;">
                                        <thead>
                                            <tr>
                                                <th><input type="checkbox" class="checkAll">全选</th>
                                                <th>序号</th>
                                                <th>货主</th>
                                                <th>余额</th>
                                                <th>预警余额</th>
                                                <th>操作</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
        <div class="am-modal am-modal-prompt" tabindex="-1" id="my-prompt">
            <div class="am-modal-dialog">
                <div class="am-modal-hd">编辑</div>
                <div class="am-modal-bd">
                   <table>
                       <tr>
                           <td>&nbsp;&nbsp;余额 : <input type="text" id="balance" name="balance"></td>
                           <td>预警余额 : <input type="text" id="warning_balance"></td>
                       </tr>
                   </table>
                </div>
                <div class="am-modal-footer">
                    <span class="am-modal-btn" data-am-modal-cancel>取消</span>
                    <span class="am-modal-btn" data-am-modal-confirm>提交</span>
                </div>
            </div>
        </div>
    </div>
    <script src="${mainDomain}/statics/assets/js/amazeui.min.js"></script>
    <script src="${mainDomain}/statics/assets/js/amazeui.datatables.min.js"></script>
    <script src="${mainDomain}/statics/assets/js/dataTables.responsive.min.js"></script>
    <script src="${mainDomain}/statics/assets/js/app.js"></script>
	<script src="${mainDomain}/statics/js/front/owner/balance_list.js"></script>
</body>

</html>