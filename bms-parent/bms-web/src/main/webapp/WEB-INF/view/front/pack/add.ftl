<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>包材列表</title>
    <meta name="keywords" content="index">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <link rel="icon" type="image/png" href="${mainDomain}/statics/assets/i/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="${mainDomain}/statics/assets/i/app-icon72x72@2x.png">
    <meta name="apple-mobile-web-app-title" content="Amaze UI" />
    <link rel="stylesheet" href="${mainDomain}/statics/assets/css/amazeui.min.css" />
    <link rel="stylesheet" href="${mainDomain}/statics/assets/css/amazeui.datatables.min.css" />
    <link rel="stylesheet" href="${mainDomain}/statics/assets/css/app.css">
    <script src="${mainDomain}/statics/assets/js/jquery.min.js"></script>
    <script>
        var mainDomain = "${mainDomain}";
    </script>
    <style>
        .am-form-group{
            height: 50px;
        }
    </style>
</head>

<body data-type="widgets" class="theme-black">
<div class="tpl-content-wrapper">
    <div class="row-content am-cf">
        <div class="row">
            <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
                <div class="widget am-cf">
                    <div class="widget-head am-cf">
                    <#if bdPackMaterial.id != null>
                        <div class="widget-title  am-cf">包材管理编辑</div>
                    <#else>
                        <div class="widget-title  am-cf">包材管理添加</div>
                    </#if>
                        <div class="am-md-text-right">
                            <button type="button" class="am-btn am-btn-primary am-btn-sm"  onclick="javascript:window.history.back(-1);"><span class="am-icon-backward"></span> 返回</button>
                        </div>
                    </div>
                    <div class="widget-body  am-fr">
                        <form id="bdPackMaterial" method="post">
                            <input type="hidden" name="id" value="${bdPackMaterial.id}">
                        <div class="am-g bw-style">
                             <div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                             <#if bdPackMaterial.id == null>
                        <#if bdPackMaterial.owner_no != null >
                            <div class="am-form-group am-form-field">
                                <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                                    <span class="am-form-label">货主姓名:</span>
                                </div>
                                <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-form-group tpl-table-list-select am-md-text-left">
                                    <input  class="am-input-lg am-list-item-text"  type="text"  value="${bdPackMaterial.owner_no}" readonly>
                                </div>
                            </div>
                        <#else>
                            <div class="am-form-group am-form-field">
                                <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                                    <span class="am-form-label">货主姓名:</span>
                                </div>
                                <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-form-group tpl-table-list-select am-md-text-left">
                                    <select name="cargo_owner_no" id="cargo_owner_no"  data-am-selected="{maxHeight: 100}" required onchange="getNumber()">
                                        <option value="">请选择货主姓名</option>
                                        <#list owner as owner>
                                            <option value="${owner.no}">${owner.name}</option>
                                        </#list>
                                    </select>
                                </div>
                            </div>
                        </#if>
					 </div>
                              <div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                                <div class="am-form-group am-form-field">
                                    <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                                        <span class="am-form-label">包材编码:</span>
                                    </div>
                                    <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-md-text-left">
                                        <input class="am-input-lg am-list-item-text" type="text" placeholder="请输入编码" style="margin-left: 10px" required id="barcode" name="barcode" value="${bdPackMaterial.barcode}">
                                    </div>
                                </div>
                             </div>
                             </#if>
					  </div>
					  <div class="am-g bw-style">
                          <div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                            <div class="am-form-group am-form-field">
                                <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                                    <span class="am-form-label">包材名称:</span>
                                </div>
                                <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-form-group tpl-table-list-select am-md-text-left">
                                    <input  class="am-input-lg am-list-item-text"  type="text"  name="name" id="name" value="${bdPackMaterial.name}" >
                                </div>
                            </div>
                          </div>

                           <div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                            <div class="am-form-group am-form-field">
                                <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                                    <label class="am-form-label">长(mm):</label >
                                </div>
                                <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-md-text-left">
                                    <input class="am-input-lg am-list-item-text" type="text" placeholder="请输入长度" style="margin-left: 10px" required id="length"  name="LENGTH" value="${bdPackMaterial.length}">
                                </div>
                            </div>
                           </div>
                          </div>
                          
                         <div class="am-g bw-style">
                           <div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                            <div class="am-form-group am-form-field">
                                <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                                    <label class="am-form-label">宽(mm):</label >
                                </div>
                                <div3 class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-md-text-left">
                                    <input class="am-input-lg am-list-item-text" type="text" placeholder="请输入宽度" style="margin-left: 10px" required id="width"  name="width" value="${bdPackMaterial.width}">
                                </div3>
                            </div>
                           </div>
                           <div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                            <div class="am-form-group am-form-field">
                                <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                                    <label class="am-form-label">高(mm):</label >
                                </div>
                                <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-md-text-left">
                                    <input class="am-input-lg am-list-item-text" type="text" placeholder="请输入高度" style="margin-left: 10px" required id="height"  name="height" value="${bdPackMaterial.height}">
                                </div>
                            </div>
                           </div>
                          </div>
                          
                         <div class="am-g bw-style">
                           <div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                            <div class="am-form-group am-form-field">
                                <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                                    <label class="am-form-label">重量(g):</label >
                                </div>
                                <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-md-text-left">
                                    <input class="am-input-lg am-list-item-text" type="text" placeholder="请输入重量" style="margin-left: 10px" required id="weight"  name="weight" value="${bdPackMaterial.weight}">
                                </div>
                            </div>
                           </div>
                             <div class="am-u-lg-6 am-u-md-6 am-u-sm-6">
                                 <div class="am-form-group am-form-field">
                                     <div class="am-u-sm-12 am-u-md-4 am-u-lg-4 am-md-text-right">
                                         <span class="am-form-label">价格(元):</span>
                                     </div>
                                     <div class="am-u-sm-12 am-u-md-6 am-u-lg-6 am-md-text-left">
                                         <input class="am-input-lg am-list-item-text" type="text" placeholder="请输入价钱" style="margin-left: 10px" required min="0" id="price" name="price" value="${bdPackMaterial.price}">
                                     </div>
                                 </div>
                             </div>
                          </div>
                            <div class="am-form-field">
                                <div class="am-md-text-center ">
                                    <button type="reset" class="am-btn am-btn-primary am-btn-sm am-md-text-left" style="margin-right: 50px"><span class="am-icon-refresh"></span> 重置</button>
                                    <button type="button" class="am-btn am-btn-primary am-btn-sm am-md-text-left" onclick="submitForm()"><span class="am-icon-circle-o"></span> 提交</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script src="${mainDomain}/statics/assets/js/amazeui.min.js"></script>
        <script src="${mainDomain}/statics/assets/js/amazeui.datatables.min.js"></script>
        <script src="${mainDomain}/statics/assets/js/dataTables.responsive.min.js"></script>
        <script src="${mainDomain}/statics/assets/js/app.js"></script>
        <script src="${mainDomain}/statics/js/front/pack/list.js"></script>
</body>

</html>