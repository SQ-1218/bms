$(function() {
	autoLeftNav();
    $(window).resize(function() {
        autoLeftNav();
        console.log($(window).width())
    });
    
    $('.tpl-header-switch-button').on('click', function() {
        if ($('.left-sidebar').is('.active')) {
            if ($(window).width() > 1024) {
                $('.tpl-content-wrapper').removeClass('active');
            }
            $('.left-sidebar').removeClass('active');
        } else {

            $('.left-sidebar').addClass('active');
            if ($(window).width() > 1024) {
                $('.tpl-content-wrapper').addClass('active');
            }
        }
        //右边内容
        if ($('.body_cont').is('.active')) {
            if ($(window).width() > 1024) {
                $('.tpl-content-wrapper').removeClass('active');
            }
            $('.body_cont').removeClass('active');
        } else {
            $('.body_cont').addClass('active');
            if ($(window).width() > 1024) {
                $('.tpl-content-wrapper').addClass('active');
            }
        }
    })
    // 侧边菜单开关
    function autoLeftNav() {

        if ($(window).width() < 1024) {
            $('.left-sidebar').addClass('active');
            $('.body_cont').addClass('active');
        } else {
            $('.left-sidebar').removeClass('active');
            $('.body_cont').removeClass('active');
        }
    }


    // 侧边菜单
    $('.sidebar-nav-sub-title').on('click', function() {
        $(this).siblings('.sidebar-nav-sub').slideToggle(80)
            .end()
            .find('.sidebar-nav-sub-ico').toggleClass('sidebar-nav-sub-ico-rotate');
    })
    
    //菜单高亮
    $(".sidebar-nav li").click(function() {
        $(this).children('a').addClass('active').parent().parent().siblings().find('a').removeClass('active');
        $(this).children('a').parent().siblings().find('a').removeClass('active');
    })
})
