$(function(){
	
	$("#edit").click(function() {
		edit();
	});
	
	//退出
	$("#logout").click(function() {
		logout();
	});
});

//跳转新增和编辑
function edit(){
    window.location=mainDomain+"/bms/user/userInfo";
}

//新增编辑验证
function submitForm() {
    var results=false;
    var password=$("#password").val();
    var newPassword=$("#newPassword").val();

    if(password==''){
        results=false;
        alert("请输入旧密码");
    } else if(newPassword==''){
        results=false;
        alert("请输入新密码");
    } else if(newPassword==''){
        results=false;
        alert("请输入新密码");
    }else {
        results= true;
    }
    if(results){
        $.ajax({
            type: 'POST',
            url: mainDomain + "/bms/user/save",
            data:$('#user').serialize(),
            dataType: 'json',
            success: function (data) {
                alert(data)
                if(data=="新增成功!"||data=="修改成功!"){
                javascript:history.go(-1)
                }
            },
            error: function (data) {
                alert("操作失败")
            }
        })
    }
}

//注销退出
function logout(){
	var url=mainDomain+'/sys/login/logout.action';
	$.post(url,{},
	function(result){
		if(result.status==0){
			top.location.href=mainDomain+'/sys/login/sys/login.action';
 		}	
	},'json');
}