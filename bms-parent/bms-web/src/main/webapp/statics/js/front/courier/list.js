var table
$(function() {
	init();
    table= $('#tb_courier').DataTable();
})
//初始化表格
function init(){
	$('#tb_courier').dataTable().fnDestroy();
	oTable = $('#tb_courier').DataTable({
		"processing" : true, //加载数据时显示正在加载信息   
		"serverSide" : true, //指定从服务器端获取数据   
		"searching" : false,
		"lengthChange" : false, //用户不可改变每页显示数量   
		"ordering" : false,
        "scrollX": true,
        "width" : "10px",
		"aLengthMenu" : [ 10 ],//设置一页展示10条记录
		"ajax" : {
			"url" : mainDomain + "/bms/courier/getList",
			"dataType" : "json",
			"type" : "post",
			"dataSrc" : "rowsData",
            "data" : function(d) {
			    d.is_deleted = "1";
			    d.owner_no=$("#owner_no").val()!=null?$("#owner_no").val().toString():"";
			    d.express_corp_no=$("#express_corp_no").val()!=null?$("#express_corp_no").val().toString():"";
			    d.province=$("#province").val()!=null?$("#province").val().toString():"";
			}


		},
		"columns" : [{
            "data" : null
        }, {
			"data" : null
		},{
			"data" : "owner_no"
		}, {
			"data" : "express_corp_no"
		}, {
			"data" : "province"
		}, {
			"data" : "weight_fm"
		},{
            "data" : "weight_to"
        }, {
			"data" : "fee_fixed"
		}, {
            "data" : "fee_extra"
        }, {
            "data" : null
        } ],
		"columnDefs" : [{
            "targets" : 0,
            "width" : "10%",
            "render" : function(data, type, row, meta) {
                // 显示多选框
                var checkbox="<input type='checkbox' class=checkchild value='"+row.id+"'>";
                return checkbox;
            }
        },{
			"targets" : 1,
			"width" : "10%",
			"render" : function(data, type, row, meta) {
				// 显示行号
                var startIndex = meta.settings._iDisplayStart;
                return startIndex +meta.row + 1;
			}
		},{
			"targets" : 2,
			"width" : "10%"
		},{
			"targets" : 3,
			"width" : "10%"
		},{
			"targets" : 4,
			"width" : "10%"
		},{
			"targets" : 5,
			"width" : "10%"
		},{
            "targets" : 6,
            "width" : "10%"
        },{
            "targets" : 7,
            "width" : "10%"
        },{
			"targets" : 8,
			"width" : "10%"
            },{
			"targets" : 9,
			"width" : '30%',
			"render" : function(data, type, row, meta) {
				var buttons = "";
				buttons += "<a href='"+mainDomain+"/bms/courier/add?id="+row.id+"' class='am-btn am-btn-xs am-text-secondary'><span class='am-icon-newspaper-o'></span> 编辑</a>";
				return buttons;
			}
		}]
	});
}
//查询
function search(){
	oTable.draw();
}
// 全选
$(".checkAll").click(function () {
    $('[class=checkchild]:checkbox').prop('checked', this.check);
});

// 反选
$(".checkAll").click(function(){
    $('[class=checkchild]:checkbox').each(function(){
        this.checked = !this.checked;
    });
});
//批量删除
function batchDelete() {
    var ids =new Array();
    $.each($('input[class=checkchild]:checkbox:checked'),function(i,a){
        ids.push($(this).val());
    });
    if(ids == false) {
        alert("请选择至少一项！");
        return;
    }
    var progress = $.AMUI.progress;
   var message=confirm("确认删除数据");
    if(message){
        progress.start();
        $.ajax({
            type: 'POST',
            url:  mainDomain + "/bms/courier/delete",
            dataType: 'json',
            data:{"ids":ids.toString()},
            success: function (data) {
                progress.done();
                alert(data)
                reloadhtml();
            },
            error: function (data) {
                alert(data)
            }
    });
    }
}
//跳转新增和编辑
function edit(){
	window.location=mainDomain+"/bms/courier/add";
}
//刷新表格
function reloadhtml() {
    table.ajax.reload();

}
//新增编辑验证
function submitForm() {
    var results=false;
    var owner_no=$("#owner_no").val();
    var express_corp_no=$("#express_corp_no").val();
    var province=$("#province").val();
    var weight_fm=$("#weight_fm").val();
    var weight_to=$("#weight_to").val();
    var fee_fixed=$("#fee_fixed").val();
    var fee_extra=$("#fee_extra").val();
    if(owner_no==''){
    	results=false;
        alert("请选择货主！");
	}else if(express_corp_no==''){
        results=false;
        alert("请选择快递公司！");
	}else if(province==''){
        results=false;
        alert("请选择省份！");
	}else if(weight_fm==''){
        results=false;
        alert("请输入重量FM！");
	}else if(weight_to==''){
        results=false;
        alert("请输入重量TO！");
    }else if(fee_fixed==''){
        results=false;
        alert("请输入首重！");
    }else if(fee_extra==''){
        alert("请输入续重！");
        results=false;
    }else if(!(parseInt(weight_to)>parseInt(weight_fm))){
        results=false;
        alert("请输入重量TO要大于重量FM！");
	}else{
    	results= true;
	}
	if(results){
        $.ajax({
            type: 'POST',
            url: mainDomain + "/bms/courier/save",
            data:$('#feeRuleExpressForm').serialize(),
            dataType: 'json',
            success: function (data) {
               alert(data);
               javascript:history.go(-1)
            },
            error: function (data) {
                alert("操作失败")
            }
        })
    }
}
//导入excel
$(function() {
    $('#doc-prompt-toggle').on('click', function() {
        var progress = $.AMUI.progress;
        $("#doc-prompt-toggle").disabled=true;
        $('#my-prompt').modal({
            relatedTarget: this,
            onConfirm: function(e) {
                progress.start();
                var files = $('#file').prop('files');
                var data = new FormData();
                data.append('file', files[0]);
                $.ajax({
                    type: 'POST',
                    url: mainDomain + "/bms/courier/importExcel",
                    data:data,
                    dataType: 'json',
                    cache: false,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                         progress.done();
                        $("#doc-prompt-toggle").disabled=false;
                         alert(data.msg);
                         reloadhtml();
                    },
                    error: function (data) {
                        alert(data)
                    }
                })
            },
            onCancel: function(e) {

            }
        });
    });
});
function exportExcel() {
}