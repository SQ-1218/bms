var table
$(function() {
	init();
    table= $('#tb_delivery').DataTable();
})
//初始化表格
function init(){
	$('#tb_delivery').dataTable().fnDestroy();
	oTable = $('#tb_delivery').DataTable({
		"processing" : true, //加载数据时显示正在加载信息   
		"serverSide" : true, //指定从服务器端获取数据   
		"searching" : false,
		"lengthChange" : false, //用户不可改变每页显示数量   
		"ordering" : false,
        "scrollX": true,
        "width" : "10px",
		"aLengthMenu" : [ 10 ],//设置一页展示10条记录
		"ajax" : {
			"url" : mainDomain + "/bms/delivery/getList",
			"dataType" : "json",
			"type" : "post",
			"dataSrc" : "rowsData",
			"data" : function(d) {
                d.is_deleted = "1";
                d.owner_no=$("#owner_no").val()!=null?$("#owner_no").val().toString():"";
                d.cal_item_code=$("#cal_item_code").val()!=null?$("#cal_item_code").val().toString():"";
			}
		},
		"columns" : [{
            "data" : null
        }, {
			"data" : null
		},{
			"data" : "owner_no"
		}, {
			"data" : "cal_item_rank"
		}, {
			"data" : "cal_item_name"
		},{
            "data" : "cal_item_note"
        }, {
			"data" : "doc_type"
		}, {
            "data" : "cal_way"
        }, {
            "data" : "factor1_note"
        }, {
            "data" : "factor1_value"
        } , {
            "data" : "factor2_note"
        } , {
            "data" : "factor2_value"
        } , {
            "data" : "factor3_note"
        } , {
            "data" : "factor3_value"
        } , {
            "data" : "factor4_note"
        } , {
            "data" : "factor4_value"
        } , {
		    "data" : "factor5_note"
        } , {
            "data" : "factor5_value"
        } , {
            "data" : "remark"
        } , {
            "data" : null
        }  ],
		"columnDefs" : [{
            "targets" : 0,
            "width" : "10%",
            "render" : function(data, type, row, meta) {
                // 显示多选框
                var checkbox="<input type='checkbox' class=checkchild value='"+row.id+"'>";
                return checkbox;
            }
        },{
			"targets" : 1,
			"width" : "10%",
			"render" : function(data, type, row, meta) {
				// 显示行号
                var startIndex = meta.settings._iDisplayStart;
                return startIndex +meta.row + 1;
			}
		},{
			"targets" : 2,
			"width" : "10%"
		},{
			"targets" : 3,
			"width" : "10%"
		},{
			"targets" : 4,
			"width" : "10%"
		},{
            "targets" : 5,
            "width": "10%",
            "cellType": "td",
            "render": function (data, type, row, meta) {
                // 显示数据
                var value = row.cal_item_note;
                var value2 = row.cal_item_note;
                if (value.length > 7) {
                    value = value.substring(0, 8) + "...";

                }
                var value1 = '<a  href="#" style="color: white" onclick="showNote(\'' + value2 + '\')" data-am-modal="{target: \'#my-alert\'}">' + value + '</a>';
                if (value == null) {
                    value1 = "无";
                }
                return value1;
            }
        },{
            "targets" : 6,
            "width" : "10%"
        },{
			"targets" : 7,
			"width" : "10%"
        },{
            "targets" : 8,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor1_note;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets" : 9,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor1_value;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets" : 10,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor2_note;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets" : 11,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor2_value;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets" : 12,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor3_note;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets" : 13,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor3_value;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets" : 14,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor4_note;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets" : 15,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor4_value;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets" : 16,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor5_note;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets" : 17,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor5_value;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets" : 18,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.remark;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
			"targets" : 19,
			"width" : '30%',
			"render" : function(data, type, row, meta) {
				var buttons = "";
				buttons += "<a href='"+mainDomain+"/bms/delivery/add?id="+row.id+"' class='am-btn am-btn-xs am-text-secondary'><span class='am-icon-newspaper-o'></span> 编辑</a>";
				return buttons;
			}
		}]
	});
}
//查询
function search(){
	oTable.draw();
}
// 全选
$(".checkAll").click(function () {
    $('[class=checkchild]:checkbox').prop('checked', this.check);
});

//显示计费项解释
function showNote(obj) {
    $("#note").text(obj);
}
// 反选
$(".checkAll").click(function(){
    $('[class=checkchild]:checkbox').each(function(){
        this.checked = !this.checked;
    });
});
//批量删除
function batchDelete() {
    var ids =new Array();
    $.each($('input[class=checkchild]:checkbox:checked'),function(i,a){
        ids.push($(this).val());
    });
    if(ids == false) {
        alert("请选择至少一项！");
        return;
    }
   var message=confirm("确认删除数据");
    if(message){
        $.ajax({
            type: 'POST',
            url:  mainDomain + "/bms/courier/delete",
            dataType: 'json',
            data:{"ids":ids.toString()},
            success: function (data) {
                alert(data)
                reloadhtml();
            },
            error: function (data) {
                alert(data)
            }
    });
    }
}
//跳转新增和编辑
function edit(){
	window.location=mainDomain+"/bms/delivery/add";
}
//新增的时候获取序号
function getCount() {
    var owner_no=$("#owner_no").val();
$.ajax({
    type: 'POST',
    url:  mainDomain + "/bms/delivery/getCount",
    dataType: 'json',
    data:{"owner_no":owner_no},
    success: function (data) {
       $("#cal_item_rank").val(data);
    },
    error: function (data) {
        alert(data)
    }
})
}

//定义数组
var index = 0;//便于记录数量
var selectVal = new Array();//存放value
var selectContent = new Array();  //存放text内容
$("#calWay option").each(function (i) { //selectGroupHide为select下拉框id
    selectVal[index] = this.value;
    selectContent[index] = this.innerHTML;
    index++;
});
//新增是获取计费项名称

function getItemName() {
    $("#calWay").val(0);
    $("#calWayOther").val(0);
    $("#factor1_note").val("");
    $("#factor2_note").val("");
    $("#factor3_note").val("");
    $("#factor4_note").val("");
    $("#cal_item_note").val("");
    $("#factor2_name").show();
    $("#qty").hide();
    var name = $("#cal_item_code").find("option:selected").text();
   $("#cal_item_name").val(name);
    var osel = document.getElementById("calWay"); //得到select的ID
    var opts = osel.getElementsByTagName("option");//得到数组option
    //当计费项目名称为大单的时候只能按件数
    if ("2" == $("#cal_item_code").val() || "5" == $("#cal_item_code").val()) {
        $("#cal_way").hide();
        $("#cal_way_other").show();
    } else {
        opts[0].selected = true
        $("#cal_way_other").hide();
        $("#cal_way").show();
    }
}
//刷新表格
function reloadhtml() {
    table.ajax.reload();

}
//新增编辑验证
function submitForm() {
    var results=false;
    var owner_no=$("#owner_no").val();
    var cal_item_code=$("#cal_item_code").val();
    if ($("#calWay").val() == '') {
        var cal_way = $("#calWayOther").val();
    }
    if ($("#calWayOther").val() == '') {
        var cal_way = $("#calWay").val();
    }
    var cal_item_note=$("#cal_item_note").val();
    if(owner_no==''){
    	results=false;
        alert("请选择货主！");
	}else if(cal_item_code==''){
        results=false;
        alert("请选择计费项名称！");
	}else if(cal_way==''){
        results=false;
        alert("请选择计算方式！");
	}else if(cal_item_note==''){
        results=false;
        alert("请输入计费项解释！");
	}else {
    	results= true;
	}
    if(results){
        $.ajax({
            type: 'POST',
            url: mainDomain + "/bms/delivery/save",
            data:$('#feeRuleOperDelivery').serialize(),
            dataType: 'json',
            success: function (data) {
                alert(data)
                if(data=="新增成功!"||data=="修改成功!"){
                javascript:history.go(-1)
                }
            },
            error: function (data) {
                alert("操作失败")
            }
        })
    }
}
//批量删除
function batchDelete() {
    var ids = new Array();
    $.each($('input[class=checkchild]:checkbox:checked'), function (i, a) {
        ids.push($(this).val());
    });
    if (ids == false) {
        alert("请选择至少一项！");
        return;
    }
    console.log(ids.toString());
    var progress = $.AMUI.progress;
    var message = confirm("确认删除数据");
    if (message) {
        progress.start();
        $.ajax({
            type: 'POST',
            url: mainDomain + "/bms/delivery/delete",
            dataType: 'json',
            data: {"ids": ids.toString()},
            success: function (data) {
                progress.done();
                alert(data)
                if(data=="删除成功!"){
                    reloadhtml();
                }
            },
            error: function (data) {
                alert(data)
            }
        });
    }

}
//导入excel
$(function() {
    $('#doc-prompt-toggle').on('click', function() {
        var progress = $.AMUI.progress;
        $('#my-prompt').modal({
            relatedTarget: this,
            onConfirm: function(e) {
                progress.start();
                var files = $('#file').prop('files');
                var data = new FormData();
                data.append('file', files[0]);
                $.ajax({
                    type: 'POST',
                    url: mainDomain + "/bms/delivery/importExcel",
                    data:data,
                    dataType: 'json',
                    cache: false,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        progress.done();
                        alert(data);
                        reloadhtml();
                    },
                    error: function (data) {
                        alert(data)
                    }
                })
            },
            onCancel: function(e) {

            }
        });
    });
});

function clearNote() {
    $("#factor1_note").val("");
    $("#factor2_note").val("");
    $("#factor3_note").val("");
    $("#factor4_note").val("");
    $("#cal_item_note").val("");
}

function change_cal_way() {
    clearNote();
    if ($("#cal_item_code").val() == "1") {
        if ($("#calWay").val() == "1") {
            $("#factor1_note").val("单价 （元/单）");
            $("#cal_item_note").val("费用 = 单价*单量");
        } else if ($("#calWay").val() == "2") {
            $("#factor1_note").val("单价 （元/件）");
            $("#cal_item_note").val("费用 = 单价*总件数");
            $("#factor2_note").val("件型");
            $("#factor2_name").hide();
            $("#qty").show();
        } else {
            clearNote();
            $("#factor2_name").show();
            $("#qty").hide();
        }
    } else if (($("#cal_item_code").val() == "2")) {
        if ($("#calWay").val() == "2" || $("#calWayOther").val() == "2") {
            $("#factor2_name").hide();
            $("#qty").show();
            $("#factor1_note").val("单价 （元/件）");
            $("#cal_item_note").val("订单的发货商品>N1件时额外收费[(发货件数-N1)*单价]；订单的发货商品>N2种时额外收费[(发货件数-首件N3)*单价]");
            $("#factor2_note").val("N1的值");
            $("#factor3_note").val("N2的值");
            $("#factor4_note").val("N3的值");
        } else {
            $("#factor2_name").show();
            $("#qty").hide();
            clearNote();
        }
    } else if (($("#cal_item_code").val() == "3")) {
        if ($("#calWay").val() == "1") {
            $("#factor1_note").val("单价 （元/单）");
            $("#cal_item_note").val("费用 = 单价*单量");
        } else if ($("#calWay").val() == "2") {
            $("#factor1_note").val("单价 （元/件）");
            $("#cal_item_note").val("费用 = 单价*总件数");
            $("#factor2_note").val("件型");
            $("#factor2_name").hide();
            $("#qty").show();
        } else {
            clearNote();
            $("#factor2_name").show();
            $("#qty").hide();
        }
    } else if (($("#cal_item_code").val() == "4")) {
        if ($("#calWay").val() == "1") {
            $("#factor1_note").val("单价 （元/单）");
            $("#cal_item_note").val("费用 = 单价*单量(已拣货后被拦截的)");
        } else if ($("#calWay").val() == "2") {
            $("#factor1_note").val("单价 （元/件）");
            $("#cal_item_note").val("费用 = 单价*总件数(已拣货后被拦截的)");
            $("#factor2_note").val("件型");
            $("#factor2_name").hide();
            $("#qty").show();
        } else {
            clearNote();
            $("#factor2_name").show();
            $("#qty").hide();
        }

    } else if (($("#cal_item_code").val() == "5")) {
        if ($("#calWay").val() == "2" || $("#calWayOther").val() == "2") {
            $("#factor2_name").hide();
            $("#qty").show();
            $("#factor1_note").val("单价 （元/件）");
            $("#cal_item_note").val("订单的发货商品>N1件时额外收费[(发货件数-N1)*单价]；订单的发货商品>N2种时额外收费[(发货件数-首件N3)*单价]");
            $("#factor2_note").val("N1的值");
            $("#factor3_note").val("N2的值");
            $("#factor4_note").val("N3的值");
        } else {
            $("#factor2_name").show();
            $("#qty").hide();
            clearNote();
        }
    }

}