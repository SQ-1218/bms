$(function(){
	//登录按钮
	$("#login").click(function() {
		login();
	});
	//密码输入框、验证码输入框回车
	$("#password").keydown(function(e){
		e.stopPropagation();
		if(e.keyCode == 13){
			login();
		}
	});
});

	
//登录
function login(){
	//参数校验
	if($("#userName").val() == ''){
		$.messager.alert('提示',"请填写用户名",'info');
		return;
	}
	if($("#password").val() == ''){
		$.messager.alert('提示',"请填写密码",'info');
		return;
	}
	
	//获取页面数据
	var sysUserDto = {
			//用户名
			'login_name' : $("#userName").val(),
			//密码
			'password' : $("#password").val(),
	};
	
	var url=mainDomain+'/sys/login/login.action';
	
	$.post(url,sysUserDto,
	function(result){
		if(result.sysUser.staus==0){
			top.location.href=mainDomain+'/bms/forward/index.action';
 		}else{
 			if(result.message == "用户不存在"){
 				$("#userName").focus().select();
 			}else if(result.message == "密码错误"){
 				$("#password").focus().select();
 			}
 		}
	},'json');
}

	