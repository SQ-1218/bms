$(function() {
	init();
})

function init(){
	$('#tb_owner').dataTable().fnDestroy();
	oTable = $('#tb_owner').DataTable({
		"processing" : true, //加载数据时显示正在加载信息   
		"serverSide" : true, //指定从服务器端获取数据   
		"searching" : false,
		"lengthChange" : false, //用户不可改变每页显示数量   
		"ordering" : false,
		"scrollX": true,
		"aLengthMenu" : [ 10 ],//设置一页展示10条记录
		"ajax" : {
			"url" : mainDomain + "/bms/owner/getList",
			"dataType" : "json",
			"type" : "post",
			"dataSrc" : "rowsData",
			"data" : function(d) {
				d.is_deleted = $.trim($('#is_deleted').val());
			}
		},
		"columns" : [{
			"data" : null
		},{
			"data" : "no"
		}, {
			"data" : "name"
		}, {
			"data" : "balance"
		}, {
			"data" : "warning_balance"
		},  {
			"data" : null
		},{
			"data" : "no"
		},{
			"data" : "no"
		},{
			"data" : "no"
		},{
			"data" : "no"
		},{
			"data" : "no"
		},{
			"data" : "no"
		},{
			"data" : "no"
		},{
			"data" : "no"
		},{
			"data" : "no"
		},{
			"data" : "no"
		} ],
		"columnDefs" : [{
			"targets" : 0,
			"width" : "10px",
			"render" : function(data, type, row, meta) {
				// 显示行号  
                var startIndex = meta.settings._iDisplayStart;  
                return startIndex + meta.row + 1;  
			}
		},{
			"targets" : 1,
			"width" : "30px",
			"orderable": true
		},{
			"targets" : 2,
			"width" : "60px",
			"orderable": true
		},{
			"targets" : 3,
			"width" : "20px"
		},{
			"targets" : 4,
			"width" : "20px"
		},{
			"targets" : 5,
			"width" : '60px',
			"render" : function(data, type, row, meta) {
				var buttons = "";
				
				buttons += "<a href='/btsb/mxlistindex?hzdh="+row.hzdh+"' class='am-btn am-btn-xs am-text-secondary'><span class='am-icon-newspaper-o'></span> 编辑</a>";
					
				return buttons;
			}
		},{
			"targets" : 6,
			"width" : "30px",
			"orderable": true
		},{
			"targets" : 7,
			"width" : "30px%",
			"orderable": true
		},{
			"targets" : 8,
			"width" : "30px%",
			"orderable": true
		},{
			"targets" : 9,
			"width" : "30px%",
			"orderable": true
		},{
			"targets" : 10,
			"width" : "30px%",
			"orderable": true
		},{
			"targets" : 11,
			"width" : "30px",
			"orderable": true
		},{
			"targets" : 12,
			"width" : "30px%",
			"orderable": true
		},{
			"targets" : 13,
			"width" : "30px%",
			"orderable": true
		},{
			"targets" : 14,
			"width" : "30px%",
			"orderable": true
		},{
			"targets" : 15,
			"width" : "30px%",
			"orderable": true
		}]
	});
}

function search(){
	oTable.draw();
}