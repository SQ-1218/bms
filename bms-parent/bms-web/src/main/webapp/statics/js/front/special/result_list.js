$(function() {
    initDate()
	init();
    table= $('#tb_result_special').DataTable();
})
function initDate() {
    //昨天的时间
    var day1 = new Date();
    day1.setTime(day1.getTime()-24*60*60*1000);
    var s1 = day1.getFullYear()+"-" + (day1.getMonth()+1) + "-" + day1.getDate();
    //今天的时间
    var day2 = new Date();
    day2.setTime(day2.getTime());
    var s2 = day2.getFullYear()+"-" + (day2.getMonth()+1) + "-" + day2.getDate();
    $("#my-startDate").html(s1);
    $("#my-endDate").html(s2);
    $("#start_time").val(s1);
    var endTime = addDate(s2, 1);
    $("#end_time").val(endTime);
}
//时间转换(long转换)
function datetimeFormat_1(longTypeDate){
    var datetimeType = "";
    var date = new Date();
    date.setTime(longTypeDate);
    datetimeType+= date.getFullYear();   //年
    datetimeType+= "-" + getMonth(date); //月
    datetimeType += "-" + getDay(date);   //日
    datetimeType+= "&nbsp;&nbsp;" + getHours(date);   //时
    datetimeType+= ":" + getMinutes(date);      //分
    datetimeType+= ":" + getSeconds(date);      //分
    return datetimeType;
}
//返回 01-12 的月份值
function getMonth(date){
    var month = "";
    month = date.getMonth() + 1; //getMonth()得到的月份是0-11
    if(month<10){
        month = "0" + month;
    }
    return month;
}
//返回01-30的日期
function getDay(date){
    var day = "";
    day = date.getDate();
    if(day<10){
        day = "0" + day;
    }
    return day;
}
//返回小时
function getHours(date){
    var hours = "";
    hours = date.getHours();
    if(hours<10){
        hours = "0" + hours;
    }
    return hours;
}
//返回分
function getMinutes(date){
    var minute = "";
    minute = date.getMinutes();
    if(minute<10){
        minute = "0" + minute;
    }
    return minute;
}
//返回秒
function getSeconds(date){
    var second = "";
    second = date.getSeconds();
    if(second<10){
        second = "0" + second;
    }
    return second;
}
function init(){
	$('#tb_result_special').dataTable().fnDestroy();
	oTable = $('#tb_result_special').DataTable({
		"processing" : true, //加载数据时显示正在加载信息   
		"serverSide" : true, //指定从服务器端获取数据   
		"searching" : false,
		"lengthChange" : false, //用户不可改变每页显示数量   
		"ordering" : false,
        "scrollX": true,
        "width" : "10px",
		"aLengthMenu" : [ 10 ],//设置一页展示10条记录
		"ajax" : {
			"url" : mainDomain + "/bms/resultSpecial/getList",
			"dataType" : "json",
			"type" : "post",
			"dataSrc" : "rowsData",
			"data" : function(d) {
                d.owner_no=$("#owner_no").val()!=null?$("#owner_no").val().toString():"";
                d.cal_item_code=$("#cal_item_code").val()!=null?$("#cal_item_code").val().toString():"";
                d.is_confirmed=$("#is_confirmed").val()!=null?$("#is_confirmed").val().toString():"";
                d.start_time=$("#start_time").val()!=null?$("#start_time").val().toString():"";
			    d.end_time=$("#end_time").val()!=null?$("#end_time").val().toString():"";
                d.doc_no=$("#doc_no").val()!=null?$("#doc_no").val().toString():"";

            }
		},
		"columns" : [{
			"data" : null
		},{
            "data" : null
        },{
			"data" : "owner_no"
		}, {
			"data" : "cal_item_name"
		},{
            "data" : "cal_type"
        },{
            "data" : "doc_no"
        },{
            "data" : "fee"
        },{
            "data" : "fee_occur_time"
        },{
            "data" : "is_confirmed"
        },{
            "data" : "remark"
        },{
            "data" : "confirmed_time"
        },{
            "data" : null
        }],
		"columnDefs" : [{
			"targets" : 0,
			"width" : "10%",
			"render" : function(data, type, row, meta) {
                // 显示多选框
                if(row.is_confirmed=="1"){
                    var checkbox="<input type='checkbox' class=checkconfirmed value='"+row.id+"'>";
                    return checkbox
                }
                var checkbox="<input type='checkbox' class=checkchild value='"+row.id+"'>";

                return checkbox;
            }
        },{
			"targets" : 1,
			"width" : "10%",
          // 显示行号
            "render" : function(data, type, row, meta) {
                // 显示行号
                var startIndex = meta.settings._iDisplayStart;
                return startIndex + meta.row + 1;
            }
		},{
			"targets" : 2,
			"width" : "10%"
		},{
			"targets" : 3,
			"width" : "10%"
		},{
            "targets" : 4,
            "width" : "10%",
        },{
            "targets" : 5,
            "width": "10%",
            "render": function (data, type, row, meta) {
                // 显示数据
                var value = row.doc_no;
                if (value == null) {
                    value = "无";
                }
                return value;
            }
        },{
            "targets" : 6,
            "width" : "10%"
        },{
            "targets" : 7,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.fee_occur_time;
                var value1='';
                if(value!=null) {
                   value1=datetimeFormat_1(value);
                }
                return value1;
            }
        },{
            "targets" : 8,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.is_confirmed;
                if(value==1) {
                    value="未确认";
                }else{
                    value="已确认";
                }
                return value;
            }
        },{
            "targets" : 9,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.remark;
                if(value==null) {
                    value="无";
                }
                return value;
            }
            },{
            "targets" : 10,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.confirmed_time;
                if(value==null) {
                    value="无";
                }else{
                    value=datetimeFormat_1(value);
                }
                return value;
            }
            },{
            "targets" : 11,
            "width" : '30%',
            "render" : function(data, type, row, meta) {
                var buttons = "";
                buttons += "<a href='"+mainDomain+"/bms/resultSpecial/add?id="+row.id+"' class='am-btn am-btn-xs am-text-secondary'><span class='am-icon-newspaper-o'></span> 编辑</a>";
                return buttons;
            }
        }]
	});
}
//查询
function search(){
	oTable.draw();
}
// 全选
$(".checkAll").click(function () {
    $('[class=checkconfirmed]:checkbox').prop('checked', this.check);
});

// 反选
$(".checkAll").click(function(){
    $('[class=checkconfirmed]:checkbox').each(function(){
        this.checked = !this.checked;
    });
});
//刷新表格
function reloadhtml() {
    table.ajax.reload();
}
//跳转新增和编辑
function edit(){
    window.location=mainDomain+"/bms/resultSpecial/add";
}
//批量删除
function batchDelete() {
    var ids = new Array();
    $.each($('input[class=checkconfirmed]:checkbox:checked'), function (i, a) {
        ids.push($(this).val());
    });
    if (ids==false) {
        alert("请选择至少一项");
        return
    }
    console.log(ids.toString());
    var progress = $.AMUI.progress;
    var message = confirm("确认删除数据");
    if (message) {
        progress.start();
        $.ajax({
            type: 'POST',
            url: mainDomain + "/bms/resultSpecial/delete",
            dataType: 'json',
            data:{"ids":ids.toString()},
            success: function (data) {
                progress.done();
                alert(data)
                reloadhtml();
            },
            error: function (data) {
                alert(data)
            }
        });
    }
}
function addDate(date,days){
    var d=new Date(date);
    d.setDate(d.getDate()+days);
    var month=d.getMonth()+1;
    var day = d.getDate();
    if(month<10){
        month = "0"+month;
    }
    if(day<10){
        day = "0"+day;
    }
    var val = d.getFullYear()+"-"+month+"-"+day;
    return val;
}
//时间日期区间
$(function() {
    var day1 = new Date();
    day1.setTime(day1.getTime() - 24 * 60 * 60 * 1000);
    var day2 = new Date();
    day2.setTime(day2.getTime());
    var startDate = day1;
    var endDate = day2;
    var $alert = $('#my-alert');
    $('#my-start').datepicker().
    on('changeDate.datepicker.amui', function(event) {
        if (event.date.valueOf() > endDate.valueOf()) {
            $alert.find('p').text('开始日期应小于结束日期！').end().show();
        } else {
            $alert.hide();
            startDate = new Date(event.date);
            $('#my-startDate').text($('#my-start').data('date'));
            $('#start_time').val($('#my-start').data('date'));
        }
        $(this).datepicker('close');
    });

    $('#my-end').datepicker().
    on('changeDate.datepicker.amui', function(event) {
        if (event.date.valueOf() < startDate.valueOf()) {
            $alert.find('p').text('结束日期应大于开始日期！').end().show();
        } else {
            $alert.hide();
            endDate = new Date(event.date);
            $('#my-endDate').text($('#my-end').data('date'));
            var endTime = addDate($('#my-end').data('date'),1);
            $('#end_time').val(endTime);
        }
        $(this).datepicker('close');
    });
});
//新增时获取计费项名称
function getItemName() {
    var name = $("#cal_item_code").find("option:selected").text();
    $("#cal_item_name").val(name);
}
//新增编辑验证
function submitForm() {
    var results=false;
    var owner_no=$("#owner_no").val();
    var cal_item_code=$("#cal_item_code").val();
    var cal_way=$("#cal_way").val();
    var fee=$("#fee").val();
    var time=$("#time").val();
    var doc_no = $('input[name=doc_no]').val();//需要校验的输入框值
    var reg=/^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{16}$/;
    var result= reg.test(doc_no);
    if(doc_no!==""&&!result){
        results= false;
        alert("订单号是16位由字母加数字组合,请重新输入！");
    }else if(owner_no==''){
        results=false;
        alert("请选择货主！");
    }else if(cal_item_code==''){
        results=false;
        alert("请选择计费项名称！");
    }else if(cal_way==''){
        results=false;
        alert("请选择计算方式！");
    }else if(fee==''){
        results=false;
        alert("请输入费用结果！");
    }else if(time==''){
        results=false;
        alert("请输入时间！");
    } else {
        results= true;
    }
    if(results){
        $.ajax({
            type: 'POST',
            url: mainDomain + "/bms/resultSpecial/save",
            data:$('#feeResultSpecial').serialize(),
            dataType: 'json',
            success: function (data) {
                alert(data)
                if(data=="新增成功!"||data=="修改成功!"){
                    javascript:history.go(-1)
                }
            },
            error: function (data) {
                alert("操作失败")
            }
        })
    }
}
$(function() {
    var nowTemp = new Date();
    var nowDay = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0).valueOf();
    var nowMoth = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), 1, 0, 0, 0, 0).valueOf();
    var nowYear = new Date(nowTemp.getFullYear(), 0, 1, 0, 0, 0, 0).valueOf();
    var $myStart2 = $('#time');

    var checkin = $myStart2.datepicker({
        onRender: function(date, viewMode) {
            // 默认 days 视图，与当前日期比较
            var viewDate = nowDay;

            switch (viewMode) {
                // moths 视图，与当前月份比较
                case 1:
                    viewDate = nowMoth;
                    break;
                // years 视图，与当前年份比较
                case 2:
                    viewDate = nowYear;
                    break;
            }

            return date.valueOf() < viewDate ? 'am-disabled' : '';
        }
    }).data('amui.datepicker');
});
