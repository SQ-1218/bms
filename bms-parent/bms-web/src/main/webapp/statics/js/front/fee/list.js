$(function () {
    init();
    table = $('#tb_fee').DataTable();
})


function init() {
    $('#tb_fee').dataTable().fnDestroy();
    oTable = $('#tb_fee').DataTable({
        "processing": true, //加载数据时显示正在加载信息
        "serverSide": true, //指定从服务器端获取数据
        "searching": false,
        "lengthChange": false, //用户不可改变每页显示数量
        "ordering": false,
        "scrollX": true,
        "width": "10px",
        "paging": false,       //是否显示分页
        "info": false,        //是否显示表格左下角的信息
        "aLengthMenu": [10],//设置一页展示10条记录
        "ajax": {
            "url": mainDomain + "/bms/fee/getList",
            "dataType": "json",
            "type": "post",
            "dataSrc": "rowsData",
            "dataSrc": function (result) {
                val = result.successed;
                if (val == true) {
                    return result.list;
                } else {
                    val1 = result.msg;
                    alert(val1);
                    return result.list;
                }
            },
            "data": function (d) {
                d.is_deleted = "1";
                d.is_confirmed = "1";
                d.owner_no = $("#owner_no").val() != null && $("#owner_no").val() != "" ? $("#owner_no").val().toString() : "";
                d.month_time = $("#month_time").val() != null && $("#month_time").val() != "" ? $("#month_time").val().toString() : "";
            }
        },
        "columns": [{
            "data": "name"
        }, {
            "data": "fee"
        }, {
            "data": "status"
        }],
        "columnDefs": [{
            "targets": 0,
            "width": "20%",
            "orderable": true
        }, {
            "targets": 1,
            "width": "20%",
            "orderable": true
        }, {
            "targets": 2,
            "width": "20%",
            "render": function (data, type, row, meta) {
                // 显示数据
                var value = row.status;
                if (value == -1) {
                    value = "待FSC初审"
                    $("#doc-confirm-toggle").hide();
                    $("#doc-confirm-toggle2").hide();
                    $("#doc-confirm-toggle3").hide();
                    value += "<input type='hidden' value=-1 name='status'>"
                } else if (value == 0) {
                    value = "初始化"
                    $("#doc-confirm-toggle").show();
                    $("#doc-confirm-toggle2").hide();
                    $("#doc-confirm-toggle3").hide();
                    value += "<input type='hidden' value=0 name='status'>"
                } else if (value == 1) {
                    value = "运营已确认"
                    $("#doc-confirm-toggle").hide();
                    $("#doc-confirm-toggle2").show();
                    $("#doc-confirm-toggle3").hide();
                    value += "<input type='hidden' value=1 name='status'>"
                } else if (value == 2) {
                    value = "财务专员已确认"
                    $("#doc-confirm-toggle").hide();
                    $("#doc-confirm-toggle2").hide();
                    $("#doc-confirm-toggle3").show();
                    value += "<input type='hidden' value=2 name='status'>"
                } else if (value == 3) {
                    value = "财务经理已确认"
                    value += "<input type='hidden' value=3 name='status'>"
                    $("#doc-confirm-toggle").hide();
                    $("#doc-confirm-toggle2").hide();
                    $("#doc-confirm-toggle3").hide();
                } else if (value == 4) {
                    value = "FSC已复核"
                    value += "<input type='hidden' value=3 name='status'>"
                    $("#doc-confirm-toggle").hide();
                    $("#doc-confirm-toggle2").hide();
                    $("#doc-confirm-toggle3").hide();
                    }
                return value;
            }
        }]
    });
}
//查询
function search() {
    if (($("#owner_no").val() != null && $("#owner_no").val() != "") && ($("#month_time").val() != null && $("#month_time").val() != "")) {
    } else {
        alert("请选择查询的月份日期！！！")
        return
    }
    oTable.draw();
}

$(function () {
    $('#doc-modal-list').find('.am-icon-close').add('#doc-confirm-toggle').on('click', function () {
        if ($("#owner_no").val() != null && $("#owner_no").val() != "" && ($("#month_time").val() != null && $("#month_time").val() != "")) {
        } else {
            alert("请选择查审核的月份日期！！！")
            return
        }
        $('#my-confirm').modal({
            relatedTarget: this,
            onConfirm: function (options) {
                var owner_no = $("#owner_no").val() != null && $("#owner_no").val() != "" ? $("#owner_no").val().toString() : "";
                var surestatus = $("input[name='status']").val().toString();
                month_time = $("#month_time").val() != null && $("#month_time").val() != "" ? $("#month_time").val().toString() : "";
                $.ajax({
                    type: 'POST',
                    url: mainDomain + "/bms/fee/update",
                    dataType: 'json',
                    data: {"owner_no": owner_no, "month_time": month_time, "surestatus": surestatus},
                    success: function (data) {
                        if (data == "审核通过") {
                            alert(data);
                            reloadhtml()
                        } else {
                            alert(data)
                        }
                    },
                    error: function (data) {
                        alert(data)
                    }
                })
            },
            // closeOnConfirm: false,
            onCancel: function () {
                var owner_no = $("#owner_no").val() != null && $("#owner_no").val() != "" ? $("#owner_no").val().toString() : "";
                var rejectstatus = $("input[name='status']").val().toString();
                month_time = $("#month_time").val() != null && $("#month_time").val() != "" ? $("#month_time").val().toString() : "";
                $.ajax({
                    type: 'POST',
                    url: mainDomain + "/bms/fee/replace",
                    dataType: 'json',
                    data: {"owner_no": owner_no, "month_time": month_time, "rejectstatus": rejectstatus},
                    success: function (data) {
                        if (data == "驳回成功") {
                            alert(data);
                            reloadhtml()
                        } else {
                            alert(data)
                        }
                    },
                    error: function (data) {
                        alert(data)
                    }
                })
            }
        });
    });
});
$(function () {
    $('#doc-modal-list').find('.am-icon-close').add('#doc-confirm-toggle2').on('click', function () {
        $('#my-confirm2').modal({
            relatedTarget: this,
            onConfirm: function (options) {
                var owner_no = $("#owner_no").val() != null && $("#owner_no").val() != "" ? $("#owner_no").val().toString() : "";
                var surestatus = $("input[name='status']").val().toString();
                month_time = $("#month_time").val() != null && $("#month_time").val() != "" ? $("#month_time").val().toString() : "";
                $.ajax({
                    type: 'POST',
                    url: mainDomain + "/bms/fee/update",
                    dataType: 'json',
                    data: {"owner_no": owner_no, "month_time": month_time, "surestatus": surestatus},
                    success: function (data) {
                        if (data == "审核通过") {
                            alert(data);
                            reloadhtml()
                        } else {
                            alert(data)
                        }
                    },
                    error: function (data) {
                        alert(data)
                    }
                })
            },
            // closeOnConfirm: false,
            onCancel: function () {
                var owner_no = $("#owner_no").val() != null && $("#owner_no").val() != "" ? $("#owner_no").val().toString() : "";
                var rejectstatus = $("input[name='status']").val().toString();
                month_time = $("#month_time").val() != null && $("#month_time").val() != "" ? $("#month_time").val().toString() : "";
                $.ajax({
                    type: 'POST',
                    url: mainDomain + "/bms/fee/replace",
                    dataType: 'json',
                    data: {"owner_no": owner_no, "month_time": month_time, "rejectstatus": rejectstatus},
                    success: function (data) {
                        if (data =="驳回成功") {
                            alert(data);
                            reloadhtml()
                        } else {
                            alert(data)
                        }
                    },
                    error: function (data) {
                        alert(data)
                    }
                })
            }
        });
    });
});
$(function () {
    $('#doc-modal-list').find('.am-icon-close').add('#doc-confirm-toggle3').on('click', function () {

        $('#my-confirm3').modal({
            relatedTarget: this,
            onConfirm: function (options) {
                var owner_no = $("#owner_no").val() != null && $("#owner_no").val() != "" ? $("#owner_no").val().toString() : "";
                var surestatus = $("input[name='status']").val().toString();
                month_time = $("#month_time").val() != null && $("#month_time").val() != "" ? $("#month_time").val().toString() : "";
                $.ajax({
                    type: 'POST',
                    url: mainDomain + "/bms/fee/update",
                    dataType: 'json',
                    data: {"owner_no": owner_no, "month_time": month_time, "surestatus": surestatus},
                    success: function (data) {
                        if (data =="审核通过") {
                            alert(data);
                            reloadhtml()
                        } else {
                            alert(data)
                        }
                    },
                    error: function (data) {
                        alert(data)
                    }
                })
            },
            // closeOnConfirm: false,
            onCancel: function () {
                var owner_no = $("#owner_no").val() != null && $("#owner_no").val() != "" ? $("#owner_no").val().toString() : "";
                var rejectstatus = $("input[name='status']").val().toString();
                month_time = $("#month_time").val() != null && $("#month_time").val() != "" ? $("#month_time").val().toString() : "";
                $.ajax({
                    type: 'POST',
                    url: mainDomain + "/bms/fee/replace",
                    dataType: 'json',
                    data: {"owner_no": owner_no, "month_time": month_time, "rejectstatus": rejectstatus},
                    success: function (data) {
                        if (data =="驳回成功") {
                            alert(data);
                            reloadhtml()
                        } else {
                            alert(data)
                        }
                    },
                    error: function (data) {
                        alert(data)
                    }
                })
            }
        });
    });
});

//刷新表格
function reloadhtml() {
    table.ajax.reload();
}






