$(function() {
	init();
    table= $('#tb_receiving').DataTable();
})

function init(){
	$('#tb_receiving').dataTable().fnDestroy();
	oTable = $('#tb_receiving').DataTable({
		"processing" : true, //加载数据时显示正在加载信息   
		"serverSide" : true, //指定从服务器端获取数据   
		"searching" : false,
		"lengthChange" : false, //用户不可改变每页显示数量   
		"ordering" : false,
        "scrollX": true,
        "width" : "10px",
		"aLengthMenu" : [ 10 ],//设置一页展示10条记录
		"ajax" : {
			"url" : mainDomain + "/bms/receiving/api/getList",
			"dataType" : "json",
			"type" : "post",
			"dataSrc" : "rowsData",
			"data" : function(d) {
				d.is_deleted ="1";
                d.owner_no=$("#owner_no").val()!=null?$("#owner_no").val().toString():"";
                d.cal_item_code=$("#cal_item_code").val()!=null?$("#cal_item_code").val().toString():"";
			}
		},
		"columns" : [{
			"data" : null
		},{
            "data" : null
        },{
			"data" : "owner_no"
		}, {
			"data" : "cal_item_rank"
		}, {
			"data" : "cal_item_name"
		},  {
			"data" : "cal_item_note"
		},{
            "data" : "doc_type"
        },{
            "data" : "cal_way"
        },{
            "data" : "factor1_note"
        },{
            "data" : "factor1_value"
        },{
            "data" : "factor2_note"
        },{
            "data" : "factor2_value"
        },{
            "data" : "factor3_note"
        },{
            "data" : "factor3_value"
        },{
            "data" : "factor4_note"
        },{
            "data" : "factor4_value"
        },{
            "data" : "factor5_note"
        },{
            "data" : "factor5_value"
        },{
            "data" : "remark"
        },{
            "data" : "null"
        } ],
		"columnDefs" : [{
			"targets" : 0,
			"width" : "10%",
			"render" : function(data, type, row, meta) {
                // 显示多选框
                var checkbox="<input type='checkbox' class=checkchild value='"+row.id+"'>";
                return checkbox;
            }
        },{
			"targets" : 1,
			"width" : "10%",
          // 显示行号
            "render" : function(data, type, row, meta) {
                // 显示行号
                var startIndex = meta.settings._iDisplayStart;
                return startIndex + meta.row + 1;
            }
		},{
			"targets" : 2,
			"width" : "10%"
        },{
			"targets" : 3,
			"width" : "10%"
        },{
			"targets" : 4,
			"width" : "10%"
        },{
            "targets" : 5,
            "width" : "10%"
        },{
            "targets" : 6,
            "width" : "10%"
        },{
            "targets" : 7,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.cal_way;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets" : 8,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor1_note;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets" : 9,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor1_value;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets" : 10,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor2_note;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets" : 11,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor2_value;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets" : 12,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor3_note;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets" : 13,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor3_value;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets" : 14,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor4_note;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets" : 15,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor4_value;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        }, {
                "targets" : 16,
                "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor5_note;
                if(value==null) {
                    value="无";
                }
                return value;
            }
            },{
            "targets" : 17,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor5_value;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets" : 18,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.remark;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
			"targets" : 19,
			"width" : '30%',
			"render" : function(data, type, row, meta) {
				var buttons = "";
				
				buttons += "<a href='"+mainDomain+"/bms/receiving/api/add?id="+row.id+"' class='am-btn am-btn-xs am-text-secondary'><span class='am-icon-newspaper-o'></span> 编辑</a>";
					
				return buttons;
			}
		}]
	});
}
//查询
function search(){
    oTable.draw();
}
// 全选
$(".checkAll").click(function () {
    $('[class=checkchild]:checkbox').prop('checked', this.check);
});

// 反选
$(".checkAll").click(function(){
    $('[class=checkchild]:checkbox').each(function(){
        this.checked = !this.checked;
    });
});
//跳转新增和编辑
function edit(){
    window.location=mainDomain+"/bms/receiving/api/add";
}
//新增的时候获取序号
function getCount() {
    var owner_no=$("#owner_no").val();
    $.ajax({
        type: 'POST',
        url:  mainDomain + "/bms/receiving/getCount",
        dataType: 'json',
        data:{"owner_no":owner_no},
        success: function (data) {
            $("#cal_item_rank").val(data);
        },
        error: function (data) {
            alert(data)
        }
    })
}
//新增是获取计费项名称
function getName() {
    var name= $("#cal_item_code").find("option:selected").text();
    $("#cal_item_name").val(name);
}
//刷新表格
function reloadhtml() {
    table.ajax.reload();
}
//新增编辑验证
function submitForm() {
    var results=false;
    var owner_no=$("#owner_no").val();
    var cal_item_code=$("#cal_item_code").val();
    var cal_way=$("#cal_way").val();
    var cal_item_note=$("#cal_item_note").val();
    var doc_type=$("#doc_type").val();
    var factor1_note=$("#factor1_note").val();
    var factor1_value=$("#factor1_value").val();
    var factor2_note=$("#factor2_note").val();
    var factor2_value=$("#factor1_value").val();
    var factor3_note=$("#factor3_note").val();
    var factor3_value=$("#factor3_value").val();
    var factor4_note=$("#factor4_note").val();
    var factor4_value=$("#factor4_value").val();
    var factor5_note=$("#factor5_note").val();
    var factor5_value=$("#factor5_value").val();
    if(owner_no==''){
        results=false;
        alert("请选择货主！");
    }else if(cal_item_code==''){
        results=false;
        alert("请选择计费项名称！");
    }else if(cal_way==''){
        results=false;
        alert("请选择计算方式！");
    }else if(cal_item_note==''){
        results=false;
        alert("请输入计费项解释！");
    }else if(doc_type==''){
        results=false;
        alert("请输入单据类型！");
    }else {
        results= true;
    }
    if(results){
        $.ajax({
            type: 'POST',
            url: mainDomain + "/bms/receiving/api/gave",
            data:$('#feeRuleOperReceiving').serialize(),
            dataType: 'json',
            success: function (data) {
                alert(data)
                if(data=="新增成功!"||data=="修改成功!"){
                    javascript:history.go(-1);
                }
            },
            error: function (data) {
                alert("操作失败")
            }
        })
    }
}
//批量删除
function deleteIds() {
    var ids = new Array();
    $.each($('input[class=checkchild]:checkbox:checked'), function (i, a) {
        ids.push($(this).val());
    });
    if (ids.length < 0) {
        alert("请选择至少一项");
        return
    }
    console.log(ids.toString());
    var progress = $.AMUI.progress;
    var message = confirm("确认删除数据");
    if (message) {
        $.ajax({
            type: 'post',
            url: mainDomain + "/bms/receiving/api/delete",
            dataType: 'json',
            data:{"ids":ids.toString()},
            success: function (data) {
                progress.done();
                alert(data)
                reloadhtml();
            },
            error: function (data) {
                alert(data)
            }
        });
    }
}

//导入
$(function() {
    var progress = $.AMUI.progress;
    $('#doc-prompt-toggle').on('click', function() {
        $('#my-prompt').modal({
            relatedTarget: this,
            onConfirm: function(e) {
                progress.start();
                var files = $('#file').prop('files');
                var data = new FormData();
                data.append('file', files[0]);
                console.log(file);
                $.ajax({
                    type: 'post',
                    url: mainDomain + "/bms/receiving/api/importExcel",
                    data:data,
                    dataType: 'json',
                    cache: false,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        progress.done();
                        if(data=="1"){
                            alert("导入成功");
                            reloadhtml();
                        }
                    },
                    error: function (data) {
                        alert("导入失败")
                    }
                })
            },
            onCancel: function(e) {
            }
        });
    });
});
function exportExcel() {

}
//计算方式改变赋值
function ModifyCalWay() {
    $("#factor1_note").val("");
    $("#factor2_note").val("");
    $("#cal_item_note").val("");
    if ($("#cal_way").val() == "1") {
        $("#cal_item_note").val("费用 = 单价*数量");
        $("#factor1_note").val("单价 （元/方或吨");
    } else if ($("#cal_way").val() == "2") {
        $("#cal_item_note").val("费用 = 体积单价*总体积");
        $("#factor1_note").val("单价 （元/方）");
    } else if ($("#cal_way").val() == "3") {
        $("#cal_item_note").val("费用 = 重量单价*总重量");
        $("#factor1_note").val("单价 （元/吨）");
    } else if ($("#cal_way").val() == "4") {
        $("#cal_item_note").val("费用 = 件型单价*总件数；多件型配置多条规则");
        $("#factor1_note").val("单价 （元/件）");
        $("#factor2_note").val("件型")
    } else if ($("#cal_way").val() == "5") {
        $("#cal_item_note").val("费用 = 箱单价*总箱数");
        $("#factor1_note").val("单价 （元/箱）");
    }
}