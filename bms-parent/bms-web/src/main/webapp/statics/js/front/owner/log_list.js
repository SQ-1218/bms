var table
$(function() {
    initDate();
	init();
    table= $('#tb_log_balance').DataTable();
})
function initDate() {
    //昨天的时间
    var day1 = new Date();
    day1.setTime(day1.getTime()-24*60*60*1000);
    var s1 = day1.getFullYear()+"-" + (day1.getMonth()+1) + "-" + day1.getDate();
    //今天的时间
    var day2 = new Date();
    day2.setTime(day2.getTime());
    var s2 = day2.getFullYear()+"-" + (day2.getMonth()+1) + "-" + day2.getDate();
    $("#my-startDate").html(s1);
    $("#my-endDate").html(s2);
    $("#start_time").val(s1);
    var endTime = addDate(s2, 1);
    $("#end_time").val(endTime);
}
//时间转换(long转换)
function datetimeFormat_1(longTypeDate){
    var datetimeType = "";
    var date = new Date();
    date.setTime(longTypeDate);
    datetimeType+= date.getFullYear();   //年
    datetimeType+= "-" + getMonth(date); //月
    datetimeType += "-" + getDay(date);   //日
    datetimeType+= "&nbsp;&nbsp;" + getHours(date);   //时
    datetimeType+= ":" + getMinutes(date);      //分
    datetimeType+= ":" + getSeconds(date);      //分
    return datetimeType;
}
//返回 01-12 的月份值
function getMonth(date){
    var month = "";
    month = date.getMonth() + 1; //getMonth()得到的月份是0-11
    if(month<10){
        month = "0" + month;
    }
    return month;
}
//返回01-30的日期
function getDay(date){
    var day = "";
    day = date.getDate();
    if(day<10){
        day = "0" + day;
    }
    return day;
}
//返回小时
function getHours(date){
    var hours = "";
    hours = date.getHours();
    if(hours<10){
        hours = "0" + hours;
    }
    return hours;
}
//返回分
function getMinutes(date){
    var minute = "";
    minute = date.getMinutes();
    if(minute<10){
        minute = "0" + minute;
    }
    return minute;
}
//返回秒
function getSeconds(date){
    var second = "";
    second = date.getSeconds();
    if(second<10){
        second = "0" + second;
    }
    return second;
}
function init(){
	$('#tb_log_balance').dataTable().fnDestroy();
	oTable = $('#tb_log_balance').DataTable({
		"processing" : true, //加载数据时显示正在加载信息   
		"serverSide" : true, //指定从服务器端获取数据   
		"searching" : false,
		"lengthChange" : false, //用户不可改变每页显示数量   
		"ordering" : false,
		"scrollX": true,
		"aLengthMenu" : [ 10 ],//设置一页展示10条记录
		"ajax" : {
			"url" : mainDomain + "/bms/owner/getLogList",
			"dataType" : "json",
			"type" : "post",
			"dataSrc" : "rowsData",
			"data" : function(d) {
				d.is_deleted = 1;
				d.is_available =0;
                d.owner_no=$("#cargo_owner_no").val()!=null?$("#cargo_owner_no").val().toString():"";
                d.start_time=$("#start_time").val()!=null?$("#start_time").val().toString():"";
                d.end_time=$("#end_time").val()!=null?$("#end_time").val().toString():"";
                d.created_by=$("#created_by").val()!=null?$("#created_by").val().toString():"";
            }
		},
		"columns" : [{
			"data" : null
		},{
            "data" : null
        }, {
			"data" : "cargo_owner_no"
		}, {
			"data" : "balance_fm"
		}, {
			"data" : "balance_to"
		}, {
            "data" : "created_by"
        }, {
            "data" : "created_time"
        }],
		"columnDefs" : [{
            "targets" : 0,
            "width" : "20px",
            "render" : function(data, type, row, meta) {
                // 显示多选框
                var checkbox="<input type='checkbox' class=checkchild value='"+row.id+"'>";
                return checkbox;
            }
        },{
			"targets" : 1,
			"width" : "20px",
			"render" : function(data, type, row, meta) {
				// 显示行号  
                var startIndex = meta.settings._iDisplayStart;  
                return startIndex + meta.row + 1;  
			}
		},{
			"targets" : 2,
			"width" : "30px",
			"orderable": true
		},{
			"targets" : 3,
			"width" : "60px",
			"orderable": true
		},{
			"targets" : 4,
			"width" : "20px"
		},{
            "targets" : 5,
            "width" : '20px'
        },{
            "targets" : 6,
            "width" : '30px',
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.created_time;
                var value1='';
                if(value!=null) {
                    value1=datetimeFormat_1(value);
                }
                return value1;
            }
        }]
	});
}

function search(){

    oTable.draw();
}
//刷新表格
function reloadhtml() {
    table.ajax.reload();
}
function addDate(date,days){
    var d=new Date(date);
    d.setDate(d.getDate()+days);
    var month=d.getMonth()+1;
    var day = d.getDate();
    if(month<10){
        month = "0"+month;
    }
    if(day<10){
        day = "0"+day;
    }
    var val = d.getFullYear()+"-"+month+"-"+day;
    return val;
}
//时间日期区间
$(function() {
    var day1 = new Date();
    day1.setTime(day1.getTime() - 24 * 60 * 60 * 1000);
    var day2 = new Date();
    day2.setTime(day2.getTime());
    var startDate = day1;
    var endDate = day2;
    var $alert = $('#my-alert');
    $('#my-start').datepicker().
    on('changeDate.datepicker.amui', function(event) {
        if (event.date.valueOf() > endDate.valueOf()) {
            $alert.find('p').text('开始日期应小于结束日期！').end().show();
        } else {
            $alert.hide();
            startDate = new Date(event.date);
            $('#my-startDate').text($('#my-start').data('date'));
            $('#start_time').val($('#my-start').data('date'));
        }
        $(this).datepicker('close');
    });

    $('#my-end').datepicker().
    on('changeDate.datepicker.amui', function(event) {
        if (event.date.valueOf() < startDate.valueOf()) {
            $alert.find('p').text('结束日期应大于开始日期！').end().show();
        } else {
            $alert.hide();
            endDate = new Date(event.date);
            $('#my-endDate').text($('#my-end').data('date'));
            var endTime = addDate($('#my-end').data('date'),1);
            $('#end_time').val(endTime);

        }
        $(this).datepicker('close');
    });
});