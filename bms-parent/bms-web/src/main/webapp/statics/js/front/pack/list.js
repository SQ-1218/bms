$(function() {
	init();
    table= $('#tb_bd_pack_material').DataTable();
})

function init(){
	$('#tb_bd_pack_material').dataTable().fnDestroy();
	oTable = $('#tb_bd_pack_material').DataTable({
		"processing" : true, //加载数据时显示正在加载信息   
		"serverSide" : true, //指定从服务器端获取数据   
		"searching" : false,
		"lengthChange" : false, //用户不可改变每页显示数量   
		"ordering" : false,
        "scrollX": true,
        "width" : "10px",
		"aLengthMenu" : [ 10 ],//设置一页展示10条记录
		"ajax" : {
			"url" : mainDomain + "/bms/bdPackMaterial/getList",
			"dataType" : "json",
			"type" : "post",
			"dataSrc" : "rowsData",
			"data" : function(d) {
				d.is_deleted ="1";
                d.owner_no=$("#owner_no").val()!=null?$("#owner_no").val().toString():"";
                d.name=$("#name").val()!=null?$("#name").val().toString():"";
                d.barcode=$("#barcode").val()!=null?$("#barcode").val().toString():"";
			}
		},
		"columns" : [{
			"data" : null
		},{
            "data" : null
        },{
			"data" : "cargo_owner_no"
		}, {
			"data" : "name"
		}, {
			"data" : "barcode"
		},  {
			"data" : "price"
		},{
            "data" : "length"
        },{
            "data" : "width"
        },{
            "data" : "height"
        },{
            "data" : "weight"
        },{
            "data" : "null"
        } ],
		"columnDefs" : [{
			"targets" : 0,
			"width" : "10%",
			"render" : function(data, type, row, meta) {
                // 显示多选框
                var checkbox="<input type='checkbox' class=checkchild value='"+row.id+"'>";
                return checkbox;
            }
        },{
			"targets" : 1,
			"width" : "10%",
          // 显示行号
            "render" : function(data, type, row, meta) {
                // 显示行号
                var startIndex = meta.settings._iDisplayStart;
                return startIndex + meta.row + 1;
            }
		},{
			"targets" : 2,
			"width" : "10%"
		},{
			"targets" : 3,
			"width" : "10%"
		},{
			"targets" : 4,
			"width" : "10%"
		},{
            "targets" : 5,
            "width" : "10%"
        },{
            "targets" : 6,
            "width" : "10%"
        },{
            "targets" : 7,
            "width" : "10%"

        },{
            "targets" : 8,
            "width" : "10%",
        },{
            "targets" : 9,
            "width" : "10%"
        },{
            "targets" : 10,
            "width" : "10%",
            "render" : function(data, type, row, meta) {
                var buttons = "";

                buttons += "<a href='"+mainDomain+"/bms/bdPackMaterial/add?id="+row.id+"' class='am-btn am-btn-xs am-text-secondary'><span class='am-icon-newspaper-o'></span> 编辑</a>";

                return buttons;
            }
        }]
	});
}
//查询
function search(){
    oTable.draw();
}
// 全选
$(".checkAll").click(function () {
    $('[class=checkchild]:checkbox').prop('checked', this.check);
});

// 反选
$(".checkAll").click(function(){
    $('[class=checkchild]:checkbox').each(function(){
        this.checked = !this.checked;
    });
});
//跳转新增和编辑
function edit() {
    window.location=mainDomain +"/bms/bdPackMaterial/add"
}

//刷新表格
function reloadhtml() {
    table.ajax.reload();
}
//新增编辑验证
function submitForm() {
    var results=false;
    var name=$("#name").val();
    var width=$("#width").val();
    var height=$("#height").val();
    var weight=$("#weight").val();
    var price=$("#price").val();
    if(name==''){
        results=false;
        alert("请输入包材名称！");
    }else if(width==''){
        results=false;
        alert("请输入宽！");
    }else if(height==''){
        results=false;
        alert("请输入高！");
    }else if(weight==''){
        results=false;
        alert("请输入重量！");
    }else if(price==''){
        results=false;
        alert("请输入价钱！");
    }else{
        results= true;
    }
    if(results){
        $.ajax({
            type: 'POST',
            url: mainDomain + "/bms/bdPackMaterial/save",
            data:$('#bdPackMaterial').serialize(),
            dataType: 'json',
            success: function (data) {
                alert(data)
                if(data=="新增成功!"||data=="修改成功!"){
                    javascript:history.go(-1);
                }
            },
            error: function (data) {
                alert("操作失败")
            }
        })
    }
}
//批量删除
function deleteStorage() {
    var ids = new Array();
    $.each($('input[class=checkchild]:checkbox:checked'), function (i, a) {
        ids.push($(this).val());
    });
    if (ids==false) {
        alert("请选择至少一项");
        return
    }
    var progress = $.AMUI.progress;
    var message = confirm("确认删除数据");
    if (message) {
        progress.start();
        $.ajax({
            type: 'POST',
            url: mainDomain + "/bms/bdPackMaterial/delete",
            dataType: 'json',
            data:{"ids":ids.toString()},
            success: function (data) {
                progress.done();
                alert(data)
                reloadhtml();
            },
            error: function (data) {
                alert(data)
            }
        });
    }
}

//导入excel
$(function() {
    var progress = $.AMUI.progress;
    $('#doc-prompt-toggle').on('click', function() {
        $('#my-prompt').modal({
            relatedTarget: this,
            onConfirm: function(e) {
                progress.start();
                var files = $('#file').prop('files');
                var data = new FormData();
                data.append('file', files[0]);
                console.log(file);
                $.ajax({
                    type: 'post',
                    url: mainDomain + "/bms/bdPackMaterial/importExcel",
                    data:data,
                    dataType: 'json',
                    cache: false,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        progress.done();
                        if(data=="1"){
                            alert("导入成功");
                            reloadhtml();
                        }
                    },
                    error: function (data) {
                        alert("导入失败")
                    }
                })
            },
            onCancel: function(e) {
            }
        });
    });
});
//导出excel函数
function exportExcel() {

}
