var table
$(function() {
	init();
    table= $('#tb_balance_owner').DataTable();
})

function init(){
	$('#tb_balance_owner').dataTable().fnDestroy();
	oTable = $('#tb_balance_owner').DataTable({
		"processing" : true, //加载数据时显示正在加载信息   
		"serverSide" : true, //指定从服务器端获取数据   
		"searching" : false,
		"lengthChange" : false, //用户不可改变每页显示数量   
		"ordering" : false,
		"scrollX": true,
		"aLengthMenu" : [ 10 ],//设置一页展示10条记录
		"ajax" : {
			"url" : mainDomain + "/bms/owner/getList",
			"dataType" : "json",
			"type" : "post",
			"dataSrc" : "rowsData",
			"data" : function(d) {
				d.is_deleted = 1;
				d.is_available =0;
                d.owner_no=$("#owner_no").val()!=null?$("#owner_no").val().toString():"";

            }
		},
		"columns" : [{
			"data" : null
		},{
            "data" : null
        }, {
			"data" : "name"
		}, {
			"data" : "balance"
		}, {
			"data" : "warning_balance"
		},  {
			"data" : null
		}],
		"columnDefs" : [{
            "targets" : 0,
            "width" : "10%",
            "render" : function(data, type, row, meta) {
                // 显示多选框
                var checkbox="<input type='checkbox' class=checkchild value='"+row.id+"'>";
                return checkbox;
            }
        },{
			"targets" : 1,
			"width" : "10px",
			"render" : function(data, type, row, meta) {
				// 显示行号  
                var startIndex = meta.settings._iDisplayStart;  
                return startIndex + meta.row + 1;  
			}
		},{
			"targets" : 2,
			"width" : "30px",
			"orderable": true
		},{
			"targets" : 3,
			"width" : "60px",
			"orderable": true
		},{
			"targets" : 4,
			"width" : "20px"
		},{
            "targets" : 5,
            "width" : '30%',
            "render" : function(data, type, row, meta) {
        var button1 = '<a onclick="edit(\'' + data.no + '\',\'' + data.balance + '\',\'' + data.warning_balance + '\') "class="am-btn am-btn-xs am-text-secondary"><span class="am-icon-newspaper-o"></span> 编辑</a>';
        return button1;
    }
        }]
	});
}

function search(){
	oTable.draw();
}
//刷新表格
function reloadhtml() {
    table.ajax.reload();
}

function edit(owner_no, balance_fm, warning_balance) {
    $("#warning_balance").val(warning_balance);
    $("#balance").val(balance_fm);
    $('#my-prompt').modal({
        relatedTarget: this,
        onConfirm: function(e) {
        	var warning_balance=$("#warning_balance").val();
            var balance= $("#balance").val();
            if(warning_balance==""||balance==""){
            	alert("请输入对应的值!");
            	return;
			}else{
            $.ajax({
                type: 'POST',
                url: mainDomain + "/bms/owner/save",
                data:{warning_balance:warning_balance,balance:balance,owner_no:owner_no,balance_fm:balance_fm},
                dataType: 'json',
                cache: false,
                success: function (data) {
                    alert(data);
                    reloadhtml();
                },
                error: function (data) {
                    alert(data)
                }
            })
        }
        },
        onCancel: function(e) {

        }
});
}

