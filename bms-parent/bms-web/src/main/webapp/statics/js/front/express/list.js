$(function() {
    initDate();
	init();
    table= $('#tb_express').DataTable();

})
//时间转换(long转换)函数
function datetimeFormat_1(longTypeDate){
    var datetimeType = "";
    var date = new Date();
    date.setTime(longTypeDate);
    datetimeType+= date.getFullYear();   //年
    datetimeType+= "-" + getMonth(date); //月
    datetimeType += "-" + getDay(date);   //日
    datetimeType+= "&nbsp;&nbsp;" + getHours(date);   //时
    datetimeType+= ":" + getMinutes(date);      //分
    datetimeType+= ":" + getSeconds(date);      //秒
	return datetimeType;
}
//返回 01-12 的月份值
function getMonth(date){
    var month = "";
    month = date.getMonth() + 1; //getMonth()得到的月份是0-11
    if(month<10){
        month = "0" + month;
    }
    return month;
}
//返回01-30的日期
function getDay(date){
    var day = "";
    day = date.getDate();
    if(day<10){
        day = "0" + day;
    }
    return day;
}
//返回小时
function getHours(date){
    var hours = "";
    hours = date.getHours();
    if(hours<10){
        hours = "0" + hours;
    }
    return hours;
}
//返回分
function getMinutes(date){
    var minute = "";
    minute = date.getMinutes();
    if(minute<10){
        minute = "0" + minute;
    }
    return minute;
}
function getSeconds(date) {
    var second = "";
    second = date.getSeconds();
    if (second < 10) {
        second = "0" + second;
    }
    return second;
}
//返回秒

function init(){
	$('#tb_express').dataTable().fnDestroy();
	oTable = $('#tb_express').DataTable({
		"processing" : true, //加载数据时显示正在加载信息   
		"serverSide" : true, //指定从服务器端获取数据   
		"searching" : false,
		"lengthChange" : false, //用户不可改变每页显示数量   
		"ordering" : false,
		"scrollX": true,
		"aLengthMenu" : [ 10 ],//设置一页展示10条记录
		"ajax" : {
			"url" : mainDomain + "/bms/express/getList",
			"dataType" : "json",
			"type" : "post",
			"dataSrc" : "rowsData",
			"data" : function(d) {
				d.is_deleted = "1";
                d.owner_no=$("#owner_no").val()!=null?$("#owner_no").val().toString():"";
                d.express_corp_no=$("#express_corp_no").val()!=null?$("#express_corp_no").val().toString():"";
                d.province=$("#province").val()!=null?$("#province").val().toString():"";
                d.doc_no=$("#doc_no").val()!=null?$("#doc_no").val().toString():"";
                d.ref_no=$("#ref_no").val()!=null?$("#ref_no").val().toString():"";
                d.express_no=$("#express_no").val()!=null?$("#express_no").val().toString():"";
                d.is_confirmed=$("#is_confirmed").val()!=null?$("#is_confirmed").val().toString():"";
                d.start_time=$("#start_time").val()!=null?$("#start_time").val().toString():"";
                d.end_time=$("#end_time").val()!=null?$("#end_time").val().toString():"";
            }
		},
		"columns" : [{
			"data" : null
		},{
			"data" : "owner_no"
		}, {
			"data" : "doc_no"
		}, {
			"data" : "ref_no"
		}, {
			"data" : "express_corp_no"
		},  {
			"data" : "express_no"
		},{
			"data" : "province"
		},{
			"data" : "weight"
		},{
			"data" : "fee"
		},{
			"data" : "fee_occur_time"
		},{
			"data" : "is_confirmed"
		},{
			"data" : "confirmed_time"
        }, {
            "data": "remark"
        }],
		"columnDefs" : [{
			"targets" : 0,
			"width" : "10%",
			"render" : function(data, type, row, meta) {
				// 显示行号
                var startIndex = meta.settings._iDisplayStart;
                return startIndex + meta.row + 1;
			}
		},{
			"targets" : 1,
			"width" : "10%",
			"orderable": true
		},{
			"targets" : 2,
			"width" : "10%",
			"orderable": true
		},{
			"targets" : 3,
			"width" : "10%"
		},{
			"targets" : 4,
			"width" : "10%"
		},{
			"targets" : 5,
			"width" : '10%',
		},{
			"targets" : 6,
			"width" : "10%",
			"orderable": true
		},{
			"targets" : 7,
			"width" : "10%",
			"orderable": true
		},{
			"targets" : 8,
			"width" : "10%",
			"orderable": true
		},{
			"targets" : 9,
			"width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.fee_occur_time;
                var value1=''
                if(value!=null) {
                    value1=datetimeFormat_1(value)
                }
                return value1
            }
		},{
			"targets" : 10,
			"width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.is_confirmed;
                if(value==1) {
                    value="未结算";
                }else if(value==0){
                    value="已结算"
                }
                return value
            }
		},{
			"targets" : 11,
			"width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.confirmed_time;
                var value1=''
                if(value!=null) {
                    value1=datetimeFormat_1(value)
                }else if(value==null){
                    value1="无"
                }
                return value1
            }
        }, {
            "targets": 12,
            "width": "10%",
            "render": function (data, type, row, meta) {
                // 显示数据
                var value = row.remark;
                if (value == null) {
                    value = "无";
                }
                return value
            }
        }]
	});
}
//查询
function search(){

    oTable.draw();
	var doc_no = $('input[name=doc_no]').val();//需要校验的输入框值
    var reg=/^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{16}$/;
    var result= reg.test(doc_no);
    if(doc_no!==""&&!result){
        alert("订单号是16位由字母加数字组合,请重新输入！");
        return;
    }
    var ref_no = $('input[name=ref_no]').val();//需要校验的输入框值
    var reg=/^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{10}$/;
    var result= reg.test(ref_no);
    if(ref_no!==""&&!result){
        alert("关联单号是10位由字母加数字组合,请重新输入！");
        return;
    }
    var express_no = $('input[name=express_no]').val();//需要校验的输入框值
    var reg=/^[0-9]{13}$/;
    var result= reg.test(express_no);
    if(express_no!==""&&!result){
        alert("快递单号是13位纯数字组合，请重新输入！");
        return;
    }

}
function initDate() {
    //昨天的时间
    var day1 = new Date();
    day1.setTime(day1.getTime() - 24 * 60 * 60 * 1000);
    var s1 = day1.getFullYear() + "-" + (day1.getMonth() + 1) + "-" + day1.getDate();
    //今天的时间
    var day2 = new Date();
    day2.setTime(day2.getTime());
    var s2 = day2.getFullYear() + "-" + (day2.getMonth() + 1) + "-" + day2.getDate();
    $("#my-startDate").html(s1);
    $("#my-endDate").html(s2);
    $("#start_time").val(s1);
    var endTime = addDate(s2, 1);
    $("#end_time").val(endTime);
}
//日期增加或减少一天
function addDate(date,days){
    var d=new Date(date);
    d.setDate(d.getDate()+days);
    var month=d.getMonth()+1;
    var day = d.getDate();
    if(month<10){
        month = "0"+month;
    }
    if(day<10){
        day = "0"+day;
    }
    var val = d.getFullYear()+"-"+month+"-"+day;
    return val;
}
//时间日期区间
$(function() {
    var day1 = new Date();
    day1.setTime(day1.getTime() - 24 * 60 * 60 * 1000);
    var day2 = new Date();
    day2.setTime(day2.getTime());
    var startDate = day1;
    var endDate = day2;
    var $alert = $('#my-alert');
    $('#my-start').datepicker().
    on('changeDate.datepicker.amui', function(event) {
        if (event.date.valueOf() > endDate.valueOf()) {
            $alert.find('p').text('开始日期应小于结束日期！').end().show();
        } else {
            $alert.hide();
            startDate = new Date(event.date);
            $('#my-startDate').text($('#my-start').data('date'));
            $('#start_time').val($('#my-start').data('date'));
        }
        $(this).datepicker('close');
    });

    $('#my-end').datepicker().
    on('changeDate.datepicker.amui', function(event) {
        if (event.date.valueOf() < startDate.valueOf()) {
            $alert.find('p').text('结束日期应大于开始日期！').end().show();
        } else {
            $alert.hide();
            endDate = new Date(event.date);
            $('#my-endDate').text($('#my-end').data('date'));
            var endTime = addDate($('#my-end').data('date'),1);
            $('#end_time').val(endTime);
        }
        $(this).datepicker('close');
    });
});




