var table
$(function() {
    init();
    table= $('#tb_special').DataTable();
})
//初始化表格
function init(){
    $('#tb_special').dataTable().fnDestroy();
    oTable = $('#tb_special').DataTable({
        "processing" : true, //加载数据时显示正在加载信息
        "serverSide" : true, //指定从服务器端获取数据
        "searching" : false,
        "lengthChange" : false, //用户不可改变每页显示数量
        "ordering" : false,
        "scrollX": true,
        "aLengthMenu" : [ 10 ],//设置一页展示10条记录
        "ajax" : {
            "url" : mainDomain + "/bms/special/getList",
            "dataType" : "json",
            "type" : "post",
            "dataSrc" : "rowsData",
            "data" : function(d) {
                d.is_deleted = "1";
                d.owner_no=$("#owner_no").val()!=null?$("#owner_no").val().toString():"";
                d.cal_item_code=$("#cal_item_code").val()!=null?$("#cal_item_code").val().toString():"";
                d.cal_type=$("#cal_type").val()!=null?$("#cal_type").val().toString():"";
            }
        },
        "columns" : [{
            "data" : null
        }, {
            "data" : null
        },{
            "data" : "owner_no"
        }, {
            "data" : "cal_item_rank"
        }, {
            "data" : "cal_item_name"
        },{
            "data" : "cal_item_note"
        }, {
            "data" : "cal_type"
        }, {
            "data" : "factor1_note"
        }, {
            "data" : "factor1_value"
        } , {
            "data": "factor1_note_wh"
        }, {
            "data" : "factor2_note"
        } , {
            "data" : "factor2_value"
        } , {
            "data": "factor2_note_wh"
        }, {
            "data" : "factor3_note"
        } , {
            "data" : "factor3_value"
        } , {
            "data": "factor3_note_wh"
        }, {
            "data" : "factor4_note"
        } , {
            "data" : "factor4_value"
        } , {
            "data": "factor4_note_wh"
        }, {
            "data" : "factor5_note"
        } , {
            "data" : "factor5_value"
        }, {
            "data": "factor5_note_wh"
        }, {
            "data" : "factor6_note"
        } , {
            "data" : "factor6_value"
        } , {
            "data": "factor6_note_wh"
        }, {
            "data" : "factor7_note"
        } , {
            "data" : "factor7_value"
        } , {
            "data": "factor7_note_wh"
        }, {
            "data" : "factor8_note"
        } , {
            "data" : "factor8_value"
        } , {
            "data": "factor8_note_wh"
        }, {
            "data" : "factor9_note"
        } , {
            "data" : "factor9_value"
        } , {
            "data": "factor9_note_wh"
        }, {
            "data" : "remark"
        } , {
            "data" : null
        }  ],
        "columnDefs" : [{
            "targets" : 0,
            "width" : "10%",
            "render" : function(data, type, row, meta) {
                // 显示多选框
                var checkbox="<input type='checkbox' class=checkchild value='"+row.id+"'>";
                return checkbox;
            }
        },{
            "targets" : 1,
            "width" : "10%",
            "render" : function(data, type, row, meta) {
                // 显示行号
                var startIndex = meta.settings._iDisplayStart;
                return startIndex +meta.row + 1;
            }
        },{
            "targets" : 2,
            "width" : "10%"
        },{
            "targets" : 3,
            "width" : "10%"
        },{
            "targets" : 4,
            "width" : "10%"
        },{
            "targets" : 5,
            "width": "5%",
            "cellType": "td",
            "render": function (data, type, row, meta) {
                // 显示数据
                var value = row.cal_item_note;
                var value2 = row.cal_item_note;
                if (value.length > 7) {
                    value = value.substring(0, 8) + "...";

                }
                var value1 = '<a  href="#" style="color: white" onclick="showNote(\'' + value2 + '\')" data-am-modal="{target: \'#my-alert\'}">' + value + '</a>';
                if (value == null) {
                    value1 = "无";
                }
                return value1;
            }
        },{
            "targets" : 6,
            "width" : "10%"
        },{
            "targets" : 7,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor1_note;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets" : 8,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor1_value;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets" : 9,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor1_note_wh;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets" : 10,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor2_note;
                if (value == null) {
                    value = "无";
                }
                return value;
            }
        }, {
            "targets": 11,
            "width": "10%",
            "render": function (data, type, row, meta) {
                // 显示数据
                var value = row.factor2_value;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets": 12,
            "width": "10%",
            "render": function (data, type, row, meta) {
                // 显示数据
                var value = row.factor2_note_wh;
                if (value == null) {
                    value = "无";
                }
                return value;
            }
        }, {
            "targets": 13,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor3_note;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets": 14,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor3_value;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets": 15,
            "width": "10%",
            "render": function (data, type, row, meta) {
                // 显示数据
                var value = row.factor3_note_wh;
                if (value == null) {
                    value = "无";
                }
                return value;
            }
        }, {
            "targets": 16,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor4_note;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets": 17,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor4_value;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets": 18,
            "width": "10%",
            "render": function (data, type, row, meta) {
                // 显示数据
                var value = row.factor4_note_wh;
                if (value == null) {
                    value = "无";
                }
                return value;
            }
        }, {
            "targets": 19,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor5_note;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets": 20,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor5_value;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets": 21,
            "width": "10%",
            "render": function (data, type, row, meta) {
                // 显示数据
                var value = row.factor5_note_wh;
                if (value == null) {
                    value = "无";
                }
                return value;
            }
        }, {
            "targets": 22,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor6_note;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets": 23,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor6_value;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets": 24,
            "width": "10%",
            "render": function (data, type, row, meta) {
                // 显示数据
                var value = row.factor6_note_wh;
                if (value == null) {
                    value = "无";
                }
                return value;
            }
        }, {
            "targets": 25,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor7_note;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets": 26,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor7_value;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets": 27,
            "width": "10%",
            "render": function (data, type, row, meta) {
                // 显示数据
                var value = row.factor7_note_wh;
                if (value == null) {
                    value = "无";
                }
                return value;
            }
        }, {
            "targets": 28,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor8_note;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets": 29,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor8_value;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets": 30,
            "width": "10%",
            "render": function (data, type, row, meta) {
                // 显示数据
                var value = row.factor8_note_wh;
                if (value == null) {
                    value = "无";
                }
                return value;
            }
        }, {
            "targets": 31,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor9_note;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets": 32,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor9_value;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets": 33,
            "width": "10%",
            "render": function (data, type, row, meta) {
                // 显示数据
                var value = row.factor9_note_wh;
                if (value == null) {
                    value = "无";
                }
                return value;
            }
        }, {
            "targets": 34,
            "width": "10%",
            "render": function (data, type, row, meta) {
                // 显示数据
                var value = row.remark;
                if (value == null) {
                    value = "无";
                }
                return value;
            }
        },{
            "targets": 35,
            "width" : '30%',
            "render" : function(data, type, row, meta) {
                var buttons = "";
                buttons += "<a href='"+mainDomain+"/bms/special/add?id="+row.id+"' class='am-btn am-btn-xs am-text-secondary'><span class='am-icon-newspaper-o'></span> 编辑</a>";
                return buttons;
            }
        }]
    });
}
//查询
function search(){
    oTable.draw();
}
// 全选
$(".checkAll").click(function () {
    $('[class=checkchild]:checkbox').prop('checked', this.check);
});

// 反选
$(".checkAll").click(function(){
    $('[class=checkchild]:checkbox').each(function(){
        this.checked = !this.checked;
    });
});

//显示计费项解释
function showNote(obj) {
    $("#note").text(obj);
}
//批量删除
function batchDelete() {
    var ids = new Array();
    $.each($('input[class=checkchild]:checkbox:checked'), function (i, a) {
        ids.push($(this).val());
    });
    if (ids == false) {
        alert("请选择至少一项！");
        return;
    }
    var message = confirm("确认删除数据");
    if (message) {
        $.ajax({
            type: 'POST',
            url: mainDomain + "/bms/special/delete",
            dataType: 'json',
            data: {"ids": ids.toString()},
            success: function (data) {
                alert(data)
                reloadhtml();
            },
            error: function (data) {
                alert(data)
            }
        });
    }
}
//跳转新增和编辑
function edit(){
    window.location=mainDomain+"/bms/special/add";
}
//刷新表格
function reloadhtml() {
    table.ajax.reload();
}
//新增的时候获取序号
function getCount() {
    var owner_no=$("#owner_no").val();
    $.ajax({
        type: 'POST',
        url:  mainDomain + "/bms/special/getCount",
        dataType: 'json',
        data:{"owner_no":owner_no},
        success: function (data) {
            $("#cal_item_rank").val(data);
        },
        error: function (data) {
            alert(data)
        }
    })
}
//新增时获取计费项名称
function getItemName() {
    var name= $("#cal_item_code").find("option:selected").text();
    $("#cal_item_name").val(name);
    $("#factor1_note").val("");
    $("#factor2_note").val("");
    $("#factor3_note").val("");
    $("#factor4_note").val("");
    $("#factor5_note").val("");
    $("#factor6_note").val("");
    $("#remark").val("");

    //仓储优惠-日均托盘量
    //仓储优惠-库存周转率
    //仓储优惠-每日单量
    if($("#cal_item_code").val()=='-1'){
        $("#factor1_note").val("托盘量1的值");
        $("#factor2_note").val("折扣1的值");
        $("#factor3_note").val("托盘量2的值");
        $("#factor4_note").val("折扣2的值");
        $("#factor5_note").val("托盘量3的值");
        $("#factor6_note").val("折扣3的值");
        $("#remark").val("若日均储位托盘量达到托盘量1、2、3，则储位托盘仓储费相应打折扣1、2、3；请从大到小依次填入托盘量1、2、3；折扣填写格式为“0.98”代表98折");
    }else if($("#cal_item_code").val()=='-2'){
        $("#factor1_note").val("周转率1的值");
        $("#factor2_note").val("折扣1的值");
        $("#factor3_note").val("周转率2的值");
        $("#factor4_note").val("折扣2的值");
        $("#factor5_note").val("周转率3的值");
        $("#factor6_note").val("折扣3的值");
        $("#remark").val("若库存周转率达到周转率1、2、3，则总仓储费相应打折扣1、2、3；请从大到小依次填入周转率1、2、3；折扣填写格式为“0.98”代表98折");
    }else if($("#cal_item_code").val()=='-3'){
        $("#factor1_note").val("N1的值");
        $("#factor2_note").val("N2的值");
        $("#remark").val("每出1单，储位托盘仓储费减N1元，单量为总托盘量的N2倍时仓储费免受 ");
    }else if($("#cal_item_code").val()=='0'){
        $("#remark").val("目前支持配置最低仓储费和出库操作费 ");
        $("#factor1_note").val("最低仓储费的值");
        $("#factor2_note").val("最低出库操作费的值");
    }


}
//新增编辑验证
function submitForm() {
    var results=false;
    var owner_no=$("#owner_no").val();
    var cal_item_code=$("#cal_item_code").val();
    var cal_way=$("#cal_way").val();
    var cal_item_note=$("#cal_item_note").val();
    if(owner_no==''){
        results=false;
        alert("请选择货主！");
    }else if(cal_item_code==''){
        results=false;
        alert("请选择计费项名称！");
    }else if(cal_way==''){
        results=false;
        alert("请选择计算方式！");
    }else if(cal_item_note==''){
        results=false;
        alert("请输入计费项解释！");
    }else {
        results= true;
    }
    if(results){
        $.ajax({
            type: 'POST',
            url: mainDomain + "/bms/special/save",
            data:$('#feeRuleSpecial').serialize(),
            dataType: 'json',
            success: function (data) {
                alert(data)
                if(data=="新增成功!"||data=="修改成功!"){
                javascript:history.go(-1)
                }
            },
            error: function (data) {
                alert("操作失败")
            }
        })
    }
}
$(function() {
    $('#doc-prompt-toggle').on('click', function() {
        var progress = $.AMUI.progress;
        $('#my-prompt').modal({
            relatedTarget: this,
            onConfirm: function(e) {
                progress.start();
                var files = $('#file').prop('files');
                var data = new FormData();
                data.append('file', files[0]);
                $.ajax({
                    type: 'POST',
                    url: mainDomain + "/bms/special/importExcel",
                    data:data,
                    dataType: 'json',
                    cache: false,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        progress.done();
                        alert(data);
                        reloadhtml();
                    },
                    error: function (data) {
                        alert(data)
                    }
                })
            },
            onCancel: function(e) {

            }
        });
    });
});
