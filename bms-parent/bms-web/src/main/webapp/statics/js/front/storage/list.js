$(function() {
	init();
    table= $('#tb_storage').DataTable();
})

function init(){
	$('#tb_storage').dataTable().fnDestroy();
	oTable = $('#tb_storage').DataTable({
		"processing" : true, //加载数据时显示正在加载信息   
		"serverSide" : true, //指定从服务器端获取数据   
		"searching" : false,
		"lengthChange" : false, //用户不可改变每页显示数量   
		"ordering" : false,
        "scrollX": true,
        "bAutoWidth": false,                  // 是否非自动宽度  设置为false
        "width" : "10px",
		"aLengthMenu" : [ 10 ],//设置一页展示10条记录
		"ajax" : {
			"url" : mainDomain + "/bms/storage/api/getList",
			"dataType" : "json",
			"type" : "post",
			"dataSrc" : "rowsData",
			"data" : function(d) {
				d.is_deleted ="1";
                d.owner_no=$("#owner_no").val()!=null?$("#owner_no").val().toString():"";
                d.cal_item_code=$("#cal_item_code").val()!=null?$("#cal_item_code").val().toString():"";
			}
		},
		"columns" : [{
			"data" : null
		},{
            "data" : null
        },{
			"data" : "owner_no"
		}, {
			"data" : "cal_item_rank"
		}, {
			"data" : "cal_item_name"
		},  {
			"data" : "cal_item_note"
		},{
            "data" : "cal_way"
        },{
            "data" : "factor1_note"
        },{
            "data" : "factor1_value"
        },{
            "data" : "factor2_note"
        },{
            "data" : "factor2_value"
        },{
            "data" : "factor3_note"
        },{
            "data" : "factor3_value"
        },{
            "data" : "factor4_note"
        },{
            "data" : "factor4_value"
        },{
            "data" : "factor5_note"
        },{
            "data" : "factor5_value"
        },{
            "data" : "remark"
        },{
            "data" : "null"
        } ],
        "columnDefs": [{
			"targets" : 0,
			"width" : "10%",
			"render" : function(data, type, row, meta) {
                // 显示多选框
                var checkbox="<input type='checkbox' class=checkchild value='"+row.id+"'>";
                return checkbox;
            }
        },{
			"targets" : 1,
			"width" : "10%",
          // 显示行号
            "render" : function(data, type, row, meta) {
                // 显示行号
                var startIndex = meta.settings._iDisplayStart;
                return startIndex + meta.row + 1;
            }
		},{
			"targets" : 2,
			"width" : "10%"
		},{
			"targets" : 3,
			"width" : "10%"
		},{
			"targets" : 4,
			"width" : "10%"
		},{
            "targets" : 5,
            "type": "date",
            "width": "50px"
        },{
            "targets" : 6,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.cal_way;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets" : 7,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor1_note;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets" : 8,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor1_value;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets" : 9,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor2_note;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets" : 10,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor2_value;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets" : 11,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor3_note;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets" : 12,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor3_value;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets" : 13,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor4_note;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets" : 14,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor4_value;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
            "targets" : 15,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor5_note;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        }, {
                "targets" : 16,
                "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.factor5_value;
                if(value==null) {
                    value="无";
                }
                return value;
            }
            },{
            "targets" : 17,
            "width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.remark;
                if(value==null) {
                    value="无";
                }
                return value;
            }
        },{
			"targets" : 18,
			"width" : '30%',
			"render" : function(data, type, row, meta) {
				var buttons = "";
				
				buttons += "<a href='"+mainDomain+"/bms/storage/api/add?id="+row.id+"' class='am-btn am-btn-xs am-text-secondary'><span class='am-icon-newspaper-o'></span> 编辑</a>";
					
				return buttons;
			}
		}]
	});
}
//查询
function search(){
    oTable.draw();
}
// 全选
$(".checkAll").click(function () {
    $('[class=checkchild]:checkbox').prop('checked', this.check);
});

//显示计费项解释
function showNote(obj) {
    $("#note").text(obj);
}
// 反选
$(".checkAll").click(function(){
    $('[class=checkchild]:checkbox').each(function(){
        this.checked = !this.checked;
    });
});
//跳转新增和编辑
function edit() {
    window.location=mainDomain +"/bms/storage/api/add"
}

//新增是获取编号
function getNumber(){
    var owner_no=$("#owner_no").val();
    $.ajax({
        type: 'POST',
        url:mainDomain +"/bms/storage/getnumber",
        dataType: 'json',
        data:{"owner_no":owner_no},
        success:function(data){
            $("#cal_item_rank").val(data);
        },
        error: function (data) {
            alert(data)
        }
    })
}
//新增时获取计费项名称
function getName() {
    var name= $("#cal_item_code").find("option:selected").text();
    $("#cal_item_name").val(name);
}
//刷新表格
function reloadhtml() {
    table.ajax.reload();
}
//新增编辑验证
function submitForm() {
    var results=false;
    var owner_no=$("#owner_no").val();
    var cal_item_rank=$("#cal_item_rank").val();
    var cal_item_code=$("#cal_item_code").val();
    var cal_item_note=$("#cal_item_note").val();
    var cal_way=$("#cal_way").val();
    if(owner_no==''){
        results=false;
        alert("请选择货主！");
    }else if(cal_item_code==''){
        results=false;
        alert("请选择计费项目名称！");
    }else if(cal_item_note==''){
        results=false;
        alert("请输入计费解释！");
    }else if(cal_way==''){
        results=false;
        alert("请输入计算方式！");
    }else{
        results= true;
    }
    if(results){
        $.ajax({
            type: 'POST',
            url: mainDomain + "/bms/storage/api/give",
            data:$('#feeRulestorage').serialize(),
            dataType: 'json',
            success: function (data) {
                alert(data)
                if(data=="新增成功!"||data=="修改成功!"){
                    javascript:history.go(-1);
                }
            },
            error: function (data) {
                alert("操作失败")
            }
        })
    }
}
//批量删除
function deleteStorage() {
    var ids = new Array();
    $.each($('input[class=checkchild]:checkbox:checked'), function (i, a) {
        ids.push($(this).val());
    });
    if (ids==false) {
        alert("请选择至少一项");
        return
    }
    console.log(ids.toString());
    var progress = $.AMUI.progress;
    var message = confirm("确认删除数据");
    if (message) {
        progress.start();
        $.ajax({
            type: 'POST',
            url: mainDomain + "/bms/storage/api/delete",
            dataType: 'json',
            data:{"ids":ids.toString()},
            success: function (data) {
                progress.done();
                alert(data)
                reloadhtml();
            },
            error: function (data) {
                alert(data)
            }
        });
    }
}

//导入excel
$(function() {
    var progress = $.AMUI.progress;
    $('#doc-prompt-toggle').on('click', function() {
        $('#my-prompt').modal({
            relatedTarget: this,
            onConfirm: function(e) {
                progress.start();
                var files = $('#file').prop('files');
                var data = new FormData();
                data.append('file', files[0]);
                console.log(file);
                $.ajax({
                    type: 'post',
                    url: mainDomain + "/bms/storage/api/importExcel",
                    data:data,
                    dataType: 'json',
                    cache: false,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        progress.done();
                        if(data=="1"){
                            alert("导入成功");
                            reloadhtml();
                        }
                    },
                    error: function (data) {
                        alert("导入失败")
                    }
                })
            },
            onCancel: function(e) {
            }
        });
    });
});
//导出excel函数
function exportExcel() {

}
//计算方式改变事件

function changeCalWay() {
    $("#factor1_note").val("");
    $("#factor2_note").val("");
    $("#cal_item_note").val("");
    if ($("#cal_way").val() == "1") {
        $("#factor1_note").val("存储托盘单价（元/托/日）");
        $("#factor2_note").val("零拣托盘单价（元/托/日）");
        $("#cal_item_note").val("费用 = 零拣托盘单价*数量 + 存储托盘单价*数量");
    } else if ($("#cal_way").val() == "2") {
        $("#factor1_note").val("体积单价（元/方/日）");
        $("#cal_item_note").val("费用 = 体积单价*总体积");
    } else if ($("#cal_way").val() == "3") {
        $("#factor1_note").val("体积单价（元/吨/日）");
        $("#cal_item_note").val("费用 = 重量单价*总重量");
    } else if ($("#cal_way").val() == "5") {
        $("#factor1_note").val(" 面积单价（元/平/日）");
        $("#factor2_note").val("租仓面积");
        $("#cal_item_note").val("费用 = 面积单价*租仓面积");
    }
    if($("#cal_way").val()=="4"){
     $("#factor2_name").hide();
     $("#qty").show();
        $("#factor1_note").val("单价 （元/件/日）");
        $("#factor2_note").val("件型");
        $("#cal_item_note").val("费用 = 件型单价*商品数量；多件型配置多条规则");
    }else{
        $("#factor2_name").show();
        $("#qty").hide();
    }

}

