$(function() {
	init();
    table= $('#tb_verify').DataTable();
})

function init(){
	$('#tb_verify').dataTable().fnDestroy();
	oTable = $('#tb_verify').DataTable({
		"processing" : true, //加载数据时显示正在加载信息   
		"serverSide" : true, //指定从服务器端获取数据   
		"searching" : false,
		"lengthChange" : false, //用户不可改变每页显示数量   
		"ordering" : false,
        "scrollX": true,
        "bAutoWidth": false,                  // 是否非自动宽度  设置为false
        "width" : "10px",
		"aLengthMenu" : [ 10 ],//设置一页展示10条记录
		"ajax" : {
			"url" : mainDomain + "/bms/verify/getList",
			"dataType" : "json",
			"type" : "post",
			"dataSrc" : "rowsData",
			"data" : function(d) {
				d.is_deleted ="1";
                d.owner_no=$("#owner_no").val()!=null?$("#owner_no").val().toString():"";
                d.months_time = $("#months_time").val() != null && $("#months_time").val() != "" ? $("#months_time").val().toString() : "";
                d.day_time = $("#day_time").val() != null && $("#day_time").val() != "" ? $("#day_time").val().toString() : "";
			}
		},
		"columns" : [
			{
			"data" : "cargo_owner_no"
		}, {
			"data" : "created_time"
		}, {
			"data" : "year_str"
		},  {
			"data" : "month_str"
		},{
            "data" : "type"
        }],
        "columnDefs": [{
			"targets" : 0,
			"width" : "10%",
            "orderable": true
        },{
			"targets" : 1,
			"width" : "10%",
            "render":function(data, type, row, meta) {
        // 显示数据
        var value = row.created_time;
        var value1='';
        if(value!=null) {
            value1=datetimeFormat_1(value);
        }
        return value1;
    }
		},{
			"targets" : 2,
			"width" : "10%",
            "orderable": true
		},{
			"targets" : 3,
			"width" : "10%",
            "orderable": true
		},{
			"targets" : 4,
			"width" : "10%",
            "render":function(data, type, row, meta) {
                // 显示数据
                var value = row.type;
                if(value==0) {
                    value="通过";
                }else if(value==1){
                    value="驳回"
                }
                return value;
            }
		}
		]
	});
}
//查询
function search(){
    oTable.draw();
}

//时间转换(long转换)
function datetimeFormat_1(longTypeDate){
    var datetimeType = "";
    var date = new Date();
    date.setTime(longTypeDate);
    datetimeType+= date.getFullYear();   //年
    datetimeType+= "-" + getMonth(date); //月
    datetimeType += "-" + getDay(date);   //日
    datetimeType+= "&nbsp;&nbsp;" + getHours(date);   //时
    datetimeType+= ":" + getMinutes(date);      //分
    datetimeType+= ":" + getSeconds(date);      //分
    return datetimeType;
}
//返回 01-12 的月份值
function getMonth(date){
    var month = "";
    month = date.getMonth() + 1; //getMonth()得到的月份是0-11
    if(month<10){
        month = "0" + month;
    }
    return month;
}
//返回01-30的日期
function getDay(date){
    var day = "";
    day = date.getDate();
    if(day<10){
        day = "0" + day;
    }
    return day;
}
//返回小时
function getHours(date){
    var hours = "";
    hours = date.getHours();
    if(hours<10){
        hours = "0" + hours;
    }
    return hours;
}
//返回分
function getMinutes(date){
    var minute = "";
    minute = date.getMinutes();
    if(minute<10){
        minute = "0" + minute;
    }
    return minute;
}
//返回秒
function getSeconds(date){
    var second = "";
    second = date.getSeconds();
    if(second<10){
        second = "0" + second;
    }
    return second;
}


