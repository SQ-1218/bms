package com.bms.web.controller;

import com.bms.common.table.vo.DataTablesVO;
import com.bms.service.dto.bd.BdPackMaterialDto;
import com.bms.service.model.bd.BdPackMaterialModel;
import com.bms.service.profiles.service.bd.IBdCargoOwnerService;
import com.bms.service.profiles.service.bd.IBdPackMaterialService;
import com.bms.service.vo.bd.BdCargoOwnerVo;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "bms/bdPackMaterial")
public class BdPackMaterialController {
    @Resource
    private IBdCargoOwnerService bdCargoOwnerService;
    @Resource
    private IBdPackMaterialService bdPackMaterialService;
    private static Log logger = LogFactory.getLog(BdPackMaterialController.class);
    /**
     * 跳转页面
     * @param
     * @return
     */
    @RequestMapping("forward/{path}")
    public ModelAndView forward(@PathVariable("path") String path, ModelAndView mv) throws Exception {
        mv = new ModelAndView("front/pack/" + path);
        //获取查询条件的信息
        List<BdCargoOwnerVo> ownerList = bdCargoOwnerService.getOwner();
        mv.addObject("owner", ownerList);
        return mv;
    }
    /**
     * 获取数据(跳转页面)
     * @param
     * @return
     */
    @RequestMapping("/getList")
    @ResponseBody
    public DataTablesVO<BdPackMaterialModel> getList(@ModelAttribute BdPackMaterialDto bdPackMaterialDto, HttpServletRequest request) throws Exception {
        String owner_no=request.getParameter("owner_no");
        String name=request.getParameter("name");
        String barcode=request.getParameter("barcode");
        //分割多条件查询
        bdPackMaterialDto.setOwner_noList(search(owner_no));
        //把数据放入session导出时调用
        HttpSession session = request.getSession();
        session.setAttribute("bdPackMaterialExcel", bdPackMaterialDto);
        return bdPackMaterialService.getBdPackMaterialList(bdPackMaterialDto);
    }
    //多条件分割函数
    public List<String> search(String searchCondition){
        List<String> list=new ArrayList<>();
        if(searchCondition==""&&"".equals(searchCondition)){
            list=null;
        }else{
            String[] searchArray=searchCondition.split(",");
            for(String searchResult:searchArray){
                list.add(searchResult);
            }
        }
        return list;
    }
    /**
     * 新增和修改(跳转页面)
     * @param
     * @return
     */
    @RequestMapping("/add")
    public ModelAndView add(BdPackMaterialDto bdPackMaterialDto, ModelAndView mv) throws Exception {
        mv = new ModelAndView("front/pack/add");
        if(bdPackMaterialDto.getId()==null){
            //添加
            List<BdCargoOwnerVo> ownerList = bdCargoOwnerService.getOwner();
            mv.addObject("owner", ownerList);

        }else{
            //编辑
            String id=bdPackMaterialDto.getId();
            BdPackMaterialModel bdPackMaterialModel=bdPackMaterialService.getById(id);
            mv.addObject("bdPackMaterial",bdPackMaterialModel);
        }
        return mv;
    }
    /**
     * 新增和修改
     * @param
     * @return
     */
    @RequestMapping("/save")
    @ResponseBody
    public String save(BdPackMaterialModel bdPackMaterialModel, ModelAndView mv) throws Exception {
        String msg="";
        try{
            if(bdPackMaterialModel.getId()==""){
                //增加
                bdPackMaterialModel.setCreated_by("admin");
                String no=bdPackMaterialModel.getCargo_owner_no()+bdPackMaterialModel.getBarcode();
                bdPackMaterialModel.setNo(no);
                if( bdPackMaterialService.insert(bdPackMaterialModel)>0){
                    msg="新增成功!";
                }else{
                    msg="新增失败!";
                }
            }else {
                //编辑
                bdPackMaterialModel.setUpdated_by("admin");
                bdPackMaterialService.updateByIdAndVer(bdPackMaterialModel);
                msg="修改成功！";
            }
        }catch (Exception e){
            msg="操作失败";
            e.printStackTrace();
        }
        return msg;
    }


    /**
     * 批量删除
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping("/delete")
    public String batchDelete(HttpServletRequest request) {
        String ids =request.getParameter("ids");
        int status = 0;
        String msg = " ";
        if (StringUtils.isNotBlank(ids)) {
            List<String> idList = new ArrayList<String>();
            for (String id : ids.split(",")) {
                idList.add(id);
            }
            try {
                int index = bdPackMaterialService.batchDeleteByIds(idList);
                if (index > 0) {
                    status =1;
                    msg = "删除成功！";
                }
            } catch (Exception e) {
                msg += e.getMessage();
                logger.error("【批量删除数据】" + e.getMessage());
            }
        }
        return msg;
    }
    /**
     * 导出Excel数据
     * @param response
     * @return
     */
    @RequestMapping("/exportExcel")
    public void exportExcel(HttpServletResponse response, BdPackMaterialDto bdPackMaterialDto, HttpServletRequest request) throws Exception {
        response.reset(); //清除buffer缓存
        Map<String,Object> map=new HashMap<String,Object>();
        // 指定下载的文件名，浏览器都会使用本地编码，即GBK，浏览器收到这个文件名后，用ISO-8859-1来解码，然后用GBK来显示
        // 所以我们用GBK解码，ISO-8859-1来编码，在浏览器那边会反过来执行。
        response.setHeader("Content-Disposition", "attachment;filename=" + new String("包材规则费用.xlsx".getBytes("GBK"),"ISO-8859-1"));
        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);
        XSSFWorkbook workbook=null;
        //导出Excel对象
        //从session里面获取数据
        HttpSession session = request.getSession();
        bdPackMaterialDto = (BdPackMaterialDto) session.getAttribute("bdPackMaterialExcel");
        workbook = bdPackMaterialService.exportExcelInfo(bdPackMaterialDto);
        OutputStream output;
        try {
            output = response.getOutputStream();
            BufferedOutputStream bufferedOutPut = new BufferedOutputStream(output);
            bufferedOutPut.flush();
            workbook.write(bufferedOutPut);
            bufferedOutPut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * 下载Excel模板
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("/download")
    public void download(HttpServletRequest request,HttpServletResponse response) throws Exception {
        response.reset(); //清除buffer缓存
        Map<String,Object> map=new HashMap<String,Object>();
        // 指定下载的文件名，浏览器都会使用本地编码，即GBK，浏览器收到这个文件名后，用ISO-8859-1来解码，然后用GBK来显示
        // 所以我们用GBK解码，ISO-8859-1来编码，在浏览器那边会反过来执行。
        response.setHeader("Content-Disposition", "attachment;filename=" + new String("包材管理模板.xlsx".getBytes("GBK"),"ISO-8859-1"));
        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);
        XSSFWorkbook workbook=null;
        //导出Excel对象
        BdPackMaterialDto bdPackMaterialDto=new BdPackMaterialDto();
        bdPackMaterialDto.setCargo_owner_no("CHONGXIN");
        bdPackMaterialDto.setBarcode("11624469");
        bdPackMaterialDto.setName("印刷胶带45*65*72");
        bdPackMaterialDto.setLENGTH(Double.parseDouble("45"));
        bdPackMaterialDto.setWidth(Double.parseDouble("65"));
        bdPackMaterialDto.setHeight(Double.parseDouble("72"));
        bdPackMaterialDto.setWeight(Double.parseDouble("0"));
        workbook = bdPackMaterialService.downloadExcelInfo(bdPackMaterialDto);
        OutputStream output;
        try {
            output = response.getOutputStream();
            BufferedOutputStream bufferedOutPut = new BufferedOutputStream(output);
            bufferedOutPut.flush();
            workbook.write(bufferedOutPut);
            bufferedOutPut.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    /**
     * 数据导入
     * @param file
     * @param response
     * @return
     */
    @RequestMapping("/importExcel")
    @ResponseBody
    public String importExcel(@RequestParam MultipartFile file, HttpServletResponse response, ModelAndView mv) throws IOException {
        String msg="";
        //如果文件不为空，写入上传路径
        if(!file.isEmpty()) {
            //上传文件名
            InputStream in = file.getInputStream();
            int row=bdPackMaterialService.importExcelInfo(in,file);
            in.close();
            if(row>0){
                msg="1";
            }else {
                msg="0";
            }
        }
        return msg;
    }
}
