package com.bms.web.controller;


import com.bms.common.table.vo.DataTablesVO;
import com.bms.service.dto.bd.BdCargoOwnerDto;
import com.bms.service.dto.bd.FeeRuleStorageDto;
import com.bms.service.model.bd.FeeRuleStorageModel;
import com.bms.service.profiles.service.bd.IBdCargoOwnerService;
import com.bms.service.profiles.service.bd.IFeeRuleStorageService;
import com.bms.service.vo.bd.BdCargoOwnerVo;
import com.bms.service.vo.bd.FeeRuleStorageWayVo;
import com.bms.service.vo.bd.FeeRuleStoragenameVo;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jsc on 2018/8/22.
 */
@Controller
@RequestMapping("bms/storage")
public class StorageController {
    private static Log logger = LogFactory.getLog(StorageController.class);
    @Resource
    private IBdCargoOwnerService iBdCargoOwnerService;
    @Resource
     private IFeeRuleStorageService iFeeRuleStorageService;
    /**
     * 跳转页面
     * @param
     * @return
     */
      @RequestMapping("forward/{path}")
    public ModelAndView forward(@PathVariable("path") String path, ModelAndView modelAndView, BdCargoOwnerDto bdCargoOwnerDto) throws Exception{
          modelAndView=new ModelAndView("front/storage/"+path);
          //获取查询条件
          List<BdCargoOwnerVo> ownerlist=iBdCargoOwnerService.getOwner();
          List<FeeRuleStoragenameVo> storagelist=iFeeRuleStorageService.getStorage();
          modelAndView.addObject("owner",ownerlist);
          modelAndView.addObject("storage",storagelist);
          return modelAndView;
      }
      /*获取数据跳转页面*/
    @RequestMapping(value = "api/getList")
    @ResponseBody
    public DataTablesVO<FeeRuleStorageModel> getList(@ModelAttribute FeeRuleStorageDto feeRuleStorageDto, HttpServletRequest request) throws Exception {
        String owner_no=request.getParameter("owner_no");
        String cal_item_code=request.getParameter("cal_item_code");
       feeRuleStorageDto.setIs_deleted(Double.valueOf("1"));
        //分割多条件查询
        feeRuleStorageDto.setOwner_noList(search(owner_no));
        feeRuleStorageDto.setCal_item_codeList(search(cal_item_code));
        //把数据放入session导出时调用
        HttpSession session = request.getSession();
        session.setAttribute("feeRuleStorageExcel",feeRuleStorageDto);
        return iFeeRuleStorageService.getFeeRuleStorageList(feeRuleStorageDto);
    }
    //多条件的分割函数
    private List<String> search(String searchCondition) {
        List<String> list=new ArrayList<>();
        if(searchCondition==""&&"".equals(searchCondition)){
            list=null;
        }else{
            String[] searchArray=searchCondition.split(",");
            for(String searchResult:searchArray){
                list.add(searchResult);
            }
        }
        return list;
    }
    //添加
    @RequestMapping(value = "api/add")
 public ModelAndView addStorage(FeeRuleStorageDto feeRuleStorageDto, ModelAndView modelAndView) throws Exception {
          modelAndView=new ModelAndView("front/storage/add");
          if(feeRuleStorageDto.getId()==null){
             //添加
             //货主
              List<BdCargoOwnerVo> ownerlist=iBdCargoOwnerService.getOwner();
              //计费项目名称
              List<FeeRuleStoragenameVo> storagelist=iFeeRuleStorageService.getStorage();
              //计费方式
              List<FeeRuleStorageWayVo> feeRuleStorageWayVoList=iFeeRuleStorageService.getStorageWay();
              //按件计费
              List<FeeRuleStorageWayVo> feeRuleStorageWayVoList1=iFeeRuleStorageService.getCalWayByQty();
              modelAndView.addObject("owner",ownerlist);
              modelAndView.addObject("storage",storagelist);
              modelAndView.addObject("way",feeRuleStorageWayVoList);
              modelAndView.addObject("calWayByQty",feeRuleStorageWayVoList1);
          }else{
              //编辑
              String id=feeRuleStorageDto.getId();
              //计算方式
              List<FeeRuleStorageWayVo> feeRuleStorageWayVoList=iFeeRuleStorageService.getStorageWay();
              List<FeeRuleStorageWayVo> feeRuleStorageWayVoList1=iFeeRuleStorageService.getCalWayByQty();
              FeeRuleStorageModel feeRuleStorageModel=iFeeRuleStorageService.getById(id);
              modelAndView.addObject("feeRuleStorage",feeRuleStorageModel);
              modelAndView.addObject("way",feeRuleStorageWayVoList);
              modelAndView.addObject("calWayByQty",feeRuleStorageWayVoList1);
          }
   return modelAndView;
 }
    //选择货主时改变序号
    @ResponseBody
    @RequestMapping("/getnumber")
    public Integer getnumber(HttpServletRequest request) throws Exception {
        String owner_no =request.getParameter("owner_no");
        //计费项序号
        int count=iFeeRuleStorageService.getCount(owner_no);
        return (count+1);
    }
    //新增和编辑
 @RequestMapping(value = "api/give")
 @ResponseBody
 public String giveStorage(FeeRuleStorageModel feeRuleStorageModel, ModelAndView modelAndView,HttpServletRequest request){
     String msg="";
     try{
         if (feeRuleStorageModel.getFactor2_value() == null) {
             String factor_value2 = request.getParameter("factor2_value1");
             feeRuleStorageModel.setFactor2_value(factor_value2);
         }
         if(feeRuleStorageModel.getId()==""&&"".equals(feeRuleStorageModel.getId())){
             //增加
             feeRuleStorageModel.setCreated_by("admin");
             if(iFeeRuleStorageService.insert(feeRuleStorageModel)>0){
                 msg="新增成功!";
             }
         } else {
             //编辑
             String factor_value=request.getParameter("factor2_value1");
             feeRuleStorageModel.setFactor2_value(factor_value);
             feeRuleStorageModel.setUpdated_by("admin");
             iFeeRuleStorageService.updateByIdAndVer(feeRuleStorageModel);
             msg="修改成功!";
         }
     }catch (Exception e){
         e.printStackTrace();
     }
     return msg;
      }
      //删除
  @RequestMapping(value = "api/delete")
    @ResponseBody
    public String deleteStorage(HttpServletRequest request){
           String ids=request.getParameter("ids");
           int i=0;
           String msg="";
          if(StringUtils.isNotBlank(ids)){
        List<String> idlist=new ArrayList<>();
        for (String id : ids.split(",")){
            idlist.add(id);
        }
              try {
                  int t=iFeeRuleStorageService.delectByIds(idlist);
                  if(t>0){
                      i=1;
                      msg = "删除成功！";
                  }
              } catch (Exception e) {
                  msg += e.getMessage();
                  logger.error("【批量删除数据】" + e.getMessage());
              }
          }
          return msg;
  }

  /**
   * 导入文件
   * @param
   * @param
   * @return
   */
  @RequestMapping(value = "api/importExcel")
  @ResponseBody
   public String importExcel(@RequestParam MultipartFile file, HttpServletResponse response, ModelAndView modelAndView) throws IOException{
      String msg="";
    //如果文件不为空，写入上传路径
      if(!file.isEmpty()) {
        //上传文件名
        InputStream in = file.getInputStream();
          try {
              iFeeRuleStorageService.importExcelInfo(in,file);
          } catch (Exception e1) {
              e1.printStackTrace();
          }
          in.close();
        msg="1";
    }
    return msg;
  }
    /**
     * 导出Excel模板
     * @param request
     * @param response
     * @return
     */
  @RequestMapping(value = "api/exportExcel")
  public  void exportExcel(HttpServletRequest request, HttpServletResponse response, FeeRuleStorageDto feeRuleStorageDto) throws Exception{
      response.reset(); //清除buffer缓存
      Map<String,Object> map=new HashMap<String,Object>();
      // 指定下载的文件名，浏览器都会使用本地编码，即GBK，浏览器收到这个文件名后，用ISO-8859-1来解码，然后用GBK来显示
      // 所以我们用GBK解码，ISO-8859-1来编码，在浏览器那边会反过来执行。
      response.setHeader("Content-Disposition", "attachment;filename=" + new String("仓储操作规则.xlsx".getBytes("GBK"),"ISO-8859-1"));
      response.setContentType("application/vnd.ms-excel;charset=UTF-8");
      response.setHeader("Pragma", "no-cache");
      response.setHeader("Cache-Control", "no-cache");
      response.setDateHeader("Expires", 0);
      XSSFWorkbook workbook=null;
      //导出Excel对象
      //从session里面获取数据
      HttpSession session = request.getSession();
       feeRuleStorageDto= (FeeRuleStorageDto) session.getAttribute("feeRuleStorageExcel");
      workbook = iFeeRuleStorageService.exportExcelInfo(feeRuleStorageDto);
      OutputStream output;
      try {
          output = response.getOutputStream();
          BufferedOutputStream bufferedOutPut = new BufferedOutputStream(output);
          bufferedOutPut.flush();
          workbook.write(bufferedOutPut);
          bufferedOutPut.close();
      } catch (IOException e) {
          e.printStackTrace();
      }

  }
    /**
     * 下载Excel模板
     * @param request
     * @param response
     * @return
     */@RequestMapping("api/download")
    public void download(HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.reset(); //清除buffer缓存
        Map<String,Object> map=new HashMap<String,Object>();
        // 指定下载的文件名，浏览器都会使用本地编码，即GBK，浏览器收到这个文件名后，用ISO-8859-1来解码，然后用GBK来显示
        // 所以我们用GBK解码，ISO-8859-1来编码，在浏览器那边会反过来执行。
        response.setHeader("Content-Disposition", "attachment;filename=" + new String("仓储费模板.xlsx".getBytes("GBK"),"ISO-8859-1"));
        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);
        XSSFWorkbook workbook=null;
        //导出Excel对象
        FeeRuleStorageDto feeRuleStorageDto=new FeeRuleStorageDto();
        feeRuleStorageDto.setOwner_no("TAPIN");
        feeRuleStorageDto.setCal_item_rank(Double.parseDouble("1"));
        feeRuleStorageDto.setCal_item_name("仓储费");
        feeRuleStorageDto.setCal_item_code("3");
        feeRuleStorageDto.setCal_item_note("货物存储费用");
        feeRuleStorageDto.setCal_way("4");
        feeRuleStorageDto.setFactor1_note("按托盘称重");
        feeRuleStorageDto.setFactor1_value("2");
        feeRuleStorageDto.setFactor2_note("按体积称重");
        feeRuleStorageDto.setFactor2_value("3");
        feeRuleStorageDto.setFactor3_note("按面积称重");
        feeRuleStorageDto.setFactor3_value("4");
        feeRuleStorageDto.setFactor4_note("按件数");
        feeRuleStorageDto.setFactor4_value("5");
        feeRuleStorageDto.setFactor5_note("按重量");
        feeRuleStorageDto.setFactor5_value("6");
        feeRuleStorageDto.setRemark("单价乘数量");
        workbook = iFeeRuleStorageService.downloadExcelInfo(feeRuleStorageDto);
        OutputStream output;
        try {
            output = response.getOutputStream();
            BufferedOutputStream bufferedOutPut = new BufferedOutputStream(output);
            bufferedOutPut.flush();
            workbook.write(bufferedOutPut);
            bufferedOutPut.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
