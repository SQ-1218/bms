package com.bms.web.controller;


import com.bms.common.table.vo.DataTablesVO;
import com.bms.service.dto.bd.BdCargoOwnerDto;
import com.bms.service.dto.bd.FeeRuleStorageDto;
import com.bms.service.dto.bd.LogVerifyFeeResultDto;
import com.bms.service.model.bd.FeeRuleStorageModel;
import com.bms.service.model.bd.LogVerifyFeeResultModel;
import com.bms.service.profiles.service.bd.IBdCargoOwnerService;
import com.bms.service.profiles.service.bd.IFeeRuleStorageService;
import com.bms.service.profiles.service.bd.ILogVerifyFeeResultService;
import com.bms.service.vo.bd.BdCargoOwnerVo;
import com.bms.service.vo.bd.FeeRuleStorageWayVo;
import com.bms.service.vo.bd.FeeRuleStoragenameVo;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jsc on 2018/8/22.
 */
@Controller
@RequestMapping("bms/verify")
public class LogVerifyFeeResultController {
    private static Log logger = LogFactory.getLog(LogVerifyFeeResultController.class);
    @Resource
    private IBdCargoOwnerService iBdCargoOwnerService;
    @Resource
     private ILogVerifyFeeResultService iLogVerifyFeeResultService;
    /**
     * 跳转页面
     * @param
     * @return
     */
      @RequestMapping("forward/{path}")
    public ModelAndView forward(@PathVariable("path") String path, ModelAndView modelAndView, BdCargoOwnerDto bdCargoOwnerDto) throws Exception{
          modelAndView=new ModelAndView("front/verify/"+path);
          //获取查询条件
          List<BdCargoOwnerVo> ownerlist=iBdCargoOwnerService.getOwner();
          modelAndView.addObject("owner",ownerlist);
          return modelAndView;
      }
      /*获取数据跳转页面*/
    @RequestMapping(value = "getList")
    @ResponseBody
    public DataTablesVO<LogVerifyFeeResultModel> getList(@ModelAttribute LogVerifyFeeResultDto logVerifyFeeResultDto, HttpServletRequest request) throws Exception {
        //按月份
        String owner_no=request.getParameter("owner_no");
        //按月份
        String months_time = request.getParameter("months_time");
        //按操作时间
        String day_time = request.getParameter("day_time");
        logVerifyFeeResultDto.setIs_deleted(1);
  if(!months_time.equals("")){
      //替代字符串为(yyyymm)
      String month_time1 = months_time.replace("-", "");
      //获取年份
      String year_time = month_time1.substring(0, 4);
      //获取月份
      String month_time = month_time1.substring(4, 6);
      //分割多条件查询
      logVerifyFeeResultDto.setOwner_noList(search(owner_no));
      logVerifyFeeResultDto.setYear_time(year_time);
      logVerifyFeeResultDto.setMonth_time(month_time);
      logVerifyFeeResultDto.setDay_time(day_time);
  }else{
        //分割多条件查询
        logVerifyFeeResultDto.setOwner_noList(search(owner_no));
        logVerifyFeeResultDto.setDay_time(day_time);}
        return iLogVerifyFeeResultService.getLogVerifyFeeResultList(logVerifyFeeResultDto);
    }
    //多条件的分割函数
    private List<String> search(String searchCondition) {
        List<String> list=new ArrayList<>();
        if(searchCondition==""&&"".equals(searchCondition)){
            list=null;
        }else{
            String[] searchArray=searchCondition.split(",");
            for(String searchResult:searchArray){
                list.add(searchResult);
            }
        }
        return list;
    }

}
