package com.bms.web.controller;

import com.bms.common.table.vo.DataTablesVO;
import com.bms.service.dto.bd.BdCargoOwnerDto;
import com.bms.service.dto.bd.FeeResultOperationDto;
import com.bms.service.dto.bd.FeeResultPackMaterialDto;
import com.bms.service.dto.bd.FeeResultStorageDto;
import com.bms.service.model.bd.FeeResultPackMaterialModel;
import com.bms.service.model.bd.FeeResultStorageModel;
import com.bms.service.profiles.service.bd.IBdCargoOwnerService;
import com.bms.service.profiles.service.bd.IFeeResultPackMaterialService;
import com.bms.service.vo.bd.BdCargoOwnerVo;
import com.bms.service.vo.bd.FeeRuleStoragenameVo;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("bms/resultPackMaterial")
public class FeeResultPackMaterialController {
    @Resource
    private IBdCargoOwnerService bdCargoOwnerService;
    @Resource
    private IFeeResultPackMaterialService feeResultPackMaterialService;
    /**
     * 跳转页面
     * @param
     * @return
     */
    @RequestMapping("forward/{path}")
    public ModelAndView forward(@PathVariable("path") String path, ModelAndView modelAndView, BdCargoOwnerDto bdCargoOwnerDto) throws Exception{
        modelAndView=new ModelAndView("front/pack/"+path);
        //获取查询条件
        List<BdCargoOwnerVo> ownerList=bdCargoOwnerService.getOwner();
        modelAndView.addObject("owner",ownerList);
        return modelAndView;
    }
    /*获取数据跳转页面*/
    @RequestMapping(value = "getList")
    @ResponseBody
    public DataTablesVO<FeeResultPackMaterialModel> getList(@ModelAttribute FeeResultPackMaterialDto feeResultPackMaterialDto, HttpServletRequest request) throws Exception {
        String owner_no=request.getParameter("owner_no");
        String cal_item_code=request.getParameter("cal_item_code");
        String start_time=request.getParameter("start_time");
        String end_time=request.getParameter("end_time");
        String is_confirmed = request.getParameter("is_confirmed");
        feeResultPackMaterialDto.setIs_deleted(1);
        //分割多条件查询
        feeResultPackMaterialDto.setOwner_noList(search(owner_no));
        feeResultPackMaterialDto.setStart_time(start_time);
        feeResultPackMaterialDto.setEnd_time(end_time);
        feeResultPackMaterialDto.setIs_confirmed(is_confirmed);
        //把数据放入session导出时调用
        HttpSession session = request.getSession();
        session.setAttribute("feeResultPackMaterialExcel",feeResultPackMaterialDto);
       return feeResultPackMaterialService.getFeeResultPackMaterial(feeResultPackMaterialDto);
    }
    //多条件分割函数
    private List<String> search(String searchCondition) {
        List<String> list=new ArrayList<>();
        if(searchCondition==""&&"".equals(searchCondition)){
            list=null;
        }else{
            String[] searchArray=searchCondition.split(",");
            for(String searchResult:searchArray){
                list.add(searchResult);
            }
        }
        return list;
    }

    //导出
    /**
     * 导出Excel数据
     * @param response
     * @return
     */
    @RequestMapping("/exportExcel")
    public void exportExcel(HttpServletResponse response, FeeResultPackMaterialDto feeResultPackMaterialDto, HttpServletRequest request) throws Exception {
        response.reset(); //清除buffer缓存
        Map<String,Object> map=new HashMap<String,Object>();
        // 指定下载的文件名，浏览器都会使用本地编码，即GBK，浏览器收到这个文件名后，用ISO-8859-1来解码，然后用GBK来显示
        // 所以我们用GBK解码，ISO-8859-1来编码，在浏览器那边会反过来执行。
        response.setHeader("Content-Disposition", "attachment;filename=" + new String("包裹费计费结果.xlsx".getBytes("GBK"),"ISO-8859-1"));
        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);
        XSSFWorkbook workbook=null;
        //导出Excel对象
        //从session里面获取数据
        HttpSession session = request.getSession();
        feeResultPackMaterialDto = (FeeResultPackMaterialDto) session.getAttribute("feeResultPackMaterialExceli");
       workbook = feeResultPackMaterialService.exportExcelInfo(feeResultPackMaterialDto);
        OutputStream output;
        try {
            output = response.getOutputStream();
            BufferedOutputStream bufferedOutPut = new BufferedOutputStream(output);
            bufferedOutPut.flush();
            workbook.write(bufferedOutPut);
            bufferedOutPut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
