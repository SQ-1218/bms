package com.bms.web.controller;

import com.bms.common.table.vo.DataTablesVO;
import com.bms.service.dto.bd.FeeRuleSpecialDto;
import com.bms.service.model.bd.FeeRuleSpecialModel;
import com.bms.service.profiles.service.bd.IBdCargoOwnerService;
import com.bms.service.profiles.service.bd.IFeeRuleSpecialService;
import com.bms.service.vo.bd.BdCargoOwnerVo;
import com.bms.service.vo.bd.FeeRuleSpecialCalVo;
import com.bms.service.vo.bd.FeeRuleSpecialItemVo;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "bms/special")
public class FeeRuleSpecialController {
    private static Log logger = LogFactory.getLog(FeeRuleOperDeliveryController.class);
    @Resource
    private IBdCargoOwnerService bdCargoOwnerService;
    @Resource
    private IFeeRuleSpecialService feeRuleSpecialService;
    @RequestMapping("forward/{path}")
    public ModelAndView forward(@PathVariable("path") String path, ModelAndView mv) throws Exception {
        mv = new ModelAndView("front/special/" + path);
        //获取查询条件的信息
        List<BdCargoOwnerVo> ownerList = bdCargoOwnerService.getOwner();
        List<FeeRuleSpecialItemVo> feeRuleSpecialVoList=feeRuleSpecialService.getFeeRuleSpecialItem();
        List<FeeRuleSpecialCalVo> feeRuleSpecialCalVoList=feeRuleSpecialService.getFeeRuleSpecialCal();
        mv.addObject("owner", ownerList);
        mv.addObject("feeRuleSpecialItem",feeRuleSpecialVoList);
        mv.addObject("cal",feeRuleSpecialCalVoList);
        return mv;
    }
    @RequestMapping("/getList")
    @ResponseBody
    public DataTablesVO<FeeRuleSpecialModel> getList(@ModelAttribute FeeRuleSpecialDto feeRuleSpecialDto, HttpServletRequest request) throws Exception {
        feeRuleSpecialDto.setIs_deleted("1");
        String owner_no=request.getParameter("owner_no");
        String cal_item_code=request.getParameter("cal_item_code");
        String cal_type=request.getParameter("cal_type");
        feeRuleSpecialDto.setOwner_noList(search(owner_no));
        feeRuleSpecialDto.setCal_item_codeList(search(cal_item_code));
        feeRuleSpecialDto.setCal_typeList(searchInteger(cal_type));
        //把数据放入session导出时调用
        HttpSession session = request.getSession();
        session.setAttribute("feeRuleSpecialExcel",feeRuleSpecialDto);
        return feeRuleSpecialService.getFeeRuleSpecialList(feeRuleSpecialDto);
    }
    //多条件分割函数
    public List<String> search(String searchConditions){
        List<String> searchList=new ArrayList<>();
        if("".equals(searchConditions)&&searchConditions==""){
            searchList=null;
        }else{
            String[] searchArray=searchConditions.split(",");
            for(String searchResult:searchArray){
                searchList.add(searchResult);
            }
        }
        return searchList;
    }
    //多条件分割函数
    public List<Integer> searchInteger(String searchConditions){
        List<Integer> searchList=new ArrayList<>();
        if("".equals(searchConditions)&&searchConditions==""){
            searchList=null;
        }else{
            String[] searchArray=searchConditions.split(",");
            Integer[] searchInteger=new Integer[searchArray.length];
            for(int i=0;i<searchArray.length;i++){
                searchInteger[i]=Integer.parseInt(searchArray[i]);
            }
            for(Integer searchResult:searchInteger){
                searchList.add(searchResult);
            }
        }
        return searchList;
    }
    /**
     * 批量删除
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping("/delete")
    public String batchDelete(HttpServletRequest request) {
        String ids =request.getParameter("ids");
        int status = 0;
        String msg = "";
        if (StringUtils.isNotBlank(ids)) {
            List<String> idList = new ArrayList<String>();
            for (String id : ids.split(",")) {
                idList.add(id);
            }
            try {
                if (feeRuleSpecialService.batchDeleteByIds(idList)>0) {
                    status =1;
                    msg = "删除成功!";
                }else{
                    msg="删除失败!";
                }
            } catch (Exception e) {
                msg += e.getMessage();
                logger.error("【批量删除数据】" + e.getMessage());
            }
        }
        return msg;
    }
    //新增和修改
    @RequestMapping("/add")
    public ModelAndView add(FeeRuleSpecialDto feeRuleSpecialDto, ModelAndView mv) throws Exception {
        mv = new ModelAndView("front/special/add");
        if(feeRuleSpecialDto.getId()==null){
            //添加
            //货主
            List<BdCargoOwnerVo> ownerList = bdCargoOwnerService.getOwner();
            //计费项名称
            List<FeeRuleSpecialItemVo> feeRuleSpecialItemVoList=feeRuleSpecialService.getFeeRuleSpecialItem();
            //计算方式
            List<FeeRuleSpecialCalVo> feeRuleSpecialCalVoList=feeRuleSpecialService.getFeeRuleSpecialCal();
            mv.addObject("owner", ownerList);
            mv.addObject("item", feeRuleSpecialItemVoList);
            mv.addObject("cal",feeRuleSpecialCalVoList);
        }else{
            //编辑
            String id=feeRuleSpecialDto.getId();
            FeeRuleSpecialModel feeRuleSpecialModel=feeRuleSpecialService.getById(id);
            mv.addObject("feeRuleSpecial",feeRuleSpecialModel);
        }
        return mv;
    }
    //选择货主时改变序号
    @ResponseBody
    @RequestMapping("/getCount")
    public Integer getCount(HttpServletRequest request) throws Exception {
        String owner_no =request.getParameter("owner_no");
        //计费项序号
        int count=feeRuleSpecialService.getFeeRuleSpecialCount(owner_no);
        return (count+1);
    }
    //保存新增和编辑
    @RequestMapping("/save")
    @ResponseBody
    public String save(FeeRuleSpecialModel feeRuleSpecialModel, ModelAndView mv) throws Exception {
        String msg="";
        try{
            if(feeRuleSpecialModel.getId()==""){
                //增加
                feeRuleSpecialModel.setCreated_by("admin");
                if(feeRuleSpecialService.insert(feeRuleSpecialModel)>0){
                    msg="新增成功!";
                }else{
                    msg="新增失败!";
                }

            }else {
                //编辑
                feeRuleSpecialModel.setUpdated_by("admin");
                feeRuleSpecialService.updateByIdAndVer(feeRuleSpecialModel);
                msg="修改成功!";
            }
        }catch (Exception e){
            msg="操作失败!";
            e.printStackTrace();
        }
        return msg;
    }
    /**
     * 数据导入
     * @param file
     * @param response
     * @return
     */
    @RequestMapping("/importExcel")
    @ResponseBody
    public String importExcel(@RequestParam MultipartFile file, HttpServletResponse response, ModelAndView mv) throws IOException {
        String msg="";
        //如果文件不为空，写入上传路径
        if(!file.isEmpty()) {
            //上传文件名
            InputStream in = file.getInputStream();
            int row= feeRuleSpecialService.importExcelInfo(in,file);
            if(row>0){
                msg="导入数据成功";
            }else{
                msg="导入数据失败，请检查EXCEL表格";
            }
            in.close();

        }
        return msg;
    }
    /**
     * 下载Excel模板
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("/download")
    public void download(HttpServletRequest request,HttpServletResponse response) throws Exception {
        response.reset(); //清除buffer缓存
        Map<String,Object> map=new HashMap<String,Object>();
        // 指定下载的文件名，浏览器都会使用本地编码，即GBK，浏览器收到这个文件名后，用ISO-8859-1来解码，然后用GBK来显示
        // 所以我们用GBK解码，ISO-8859-1来编码，在浏览器那边会反过来执行。
        response.setHeader("Content-Disposition", "attachment;filename=" + new String("特殊配置费用模板.xlsx".getBytes("GBK"),"ISO-8859-1"));
        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);
        XSSFWorkbook workbook=null;
        //导出Excel对象
        FeeRuleSpecialDto feeRuleSpecialDto=new FeeRuleSpecialDto();
        feeRuleSpecialDto.setOwner_no("CHONGXIN");
        feeRuleSpecialDto.setCal_item_name("加班费");
        feeRuleSpecialDto.setCal_item_code("4");
        feeRuleSpecialDto.setCal_item_note("加班费");
        feeRuleSpecialDto.setCal_type("1");
        feeRuleSpecialDto.setFactor1_note("单价（元/单）");
        feeRuleSpecialDto.setFactor1_value("1.1");
        feeRuleSpecialDto.setFactor2_note("单价（元/单）");
        feeRuleSpecialDto.setFactor2_value("1.1");
        feeRuleSpecialDto.setFactor2_note("单价（元/单）");
        feeRuleSpecialDto.setFactor2_value("1.1");
        feeRuleSpecialDto.setFactor3_note("单价（元/单）");
        feeRuleSpecialDto.setFactor3_value("1.1");
        feeRuleSpecialDto.setFactor4_note("单价（元/单）");
        feeRuleSpecialDto.setFactor4_value("1.1");
        feeRuleSpecialDto.setFactor5_note("单价（元/单）");
        feeRuleSpecialDto.setFactor5_value("1.1");
        feeRuleSpecialDto.setFactor6_note("单价（元/单）");
        feeRuleSpecialDto.setFactor6_value("1.1");
        feeRuleSpecialDto.setFactor7_note("单价（元/单）");
        feeRuleSpecialDto.setFactor7_value("1.1");
        feeRuleSpecialDto.setFactor8_note("单价（元/单）");
        feeRuleSpecialDto.setFactor8_value("1.1");
        feeRuleSpecialDto.setFactor9_note("单价（元/单）");
        feeRuleSpecialDto.setFactor9_value("1.1");
        feeRuleSpecialDto.setRemark("单价乘以数量");
        workbook = feeRuleSpecialService.downloadExcelInfo(feeRuleSpecialDto);
        OutputStream output;
        try {
            output = response.getOutputStream();
            BufferedOutputStream bufferedOutPut = new BufferedOutputStream(output);
            bufferedOutPut.flush();
            workbook.write(bufferedOutPut);
            bufferedOutPut.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    /**
     * 导出Excel数据
     * @param response
     * @return
     */
    @RequestMapping("/exportExcel")
    public void exportExcel(HttpServletResponse response, FeeRuleSpecialDto feeRuleSpecialDto, HttpServletRequest request) throws Exception {
        response.reset(); //清除buffer缓存
        Map<String,Object> map=new HashMap<String,Object>();
        // 指定下载的文件名，浏览器都会使用本地编码，即GBK，浏览器收到这个文件名后，用ISO-8859-1来解码，然后用GBK来显示
        // 所以我们用GBK解码，ISO-8859-1来编码，在浏览器那边会反过来执行。
        response.setHeader("Content-Disposition", "attachment;filename=" + new String("特殊配置费用.xlsx".getBytes("GBK"),"ISO-8859-1"));
        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);
        XSSFWorkbook workbook=null;
        //导出Excel对象
        //从session里面获取数据
        HttpSession session = request.getSession();
        feeRuleSpecialDto = (FeeRuleSpecialDto) session.getAttribute("feeRuleSpecialExcel");
        workbook = feeRuleSpecialService.exportExcelInfo(feeRuleSpecialDto);
        OutputStream output;
        try {
            output = response.getOutputStream();
            BufferedOutputStream bufferedOutPut = new BufferedOutputStream(output);
            bufferedOutPut.flush();
            workbook.write(bufferedOutPut);
            bufferedOutPut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
