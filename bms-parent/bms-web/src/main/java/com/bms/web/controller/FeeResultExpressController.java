package com.bms.web.controller;

import com.bms.common.table.vo.DataTablesVO;
import com.bms.service.dto.bd.FeeResultExpressDto;
import com.bms.service.model.bd.FeeResultExpressModel;
import com.bms.service.profiles.service.bd.IBdCargoOwnerService;
import com.bms.service.profiles.service.bd.IFeeResultExpressService;
import com.bms.service.vo.bd.BdCargoOwnerVo;
import com.bms.service.vo.bd.CarrierVo;
import com.bms.service.vo.bd.ProvinceVo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jsc on 2018/10/6.
 */
@Controller
@RequestMapping(value = "bms/express")
public class FeeResultExpressController {
    private static Log logger = LogFactory.getLog(FeeResultExpressController.class);
    @Resource
    private IFeeResultExpressService iFeeResultExpressService;
    @Resource
    private IBdCargoOwnerService iBdCargoOwnerService;

    @RequestMapping("forward/{path}")
    public ModelAndView forward(@PathVariable("path") String path, ModelAndView modelAndView) throws Exception {
        modelAndView = new ModelAndView("front/express/" + path);
        //获取查询条件的信息
        List<BdCargoOwnerVo> ownerList = iBdCargoOwnerService.getOwner();
        List<CarrierVo> carrierList = iFeeResultExpressService.getCarrier();
        List<ProvinceVo> provinceList = iFeeResultExpressService.getProvince();
        modelAndView.addObject("owner", ownerList);
        modelAndView.addObject("carrier", carrierList);
        modelAndView.addObject("province", provinceList);
        return modelAndView;
    }

    @RequestMapping(value = "getList")
    @ResponseBody
    public DataTablesVO<FeeResultExpressModel> getlist(@ModelAttribute FeeResultExpressDto feeResultExpressDto, HttpServletRequest request) throws Exception {
        String owner_no = request.getParameter("owner_no");
        String express_corp_no = request.getParameter("express_corp_no");
        String province = request.getParameter("province");
        String doc_no = request.getParameter("doc_no");
        String ref_no = request.getParameter("ref_no");
        String express_no = request.getParameter("express_no");
        int is_confirmed = Integer.parseInt(request.getParameter("is_confirmed"));
        String start_time = request.getParameter("start_time");
        String end_time = request.getParameter("end_time");
        feeResultExpressDto.setIs_deleted(1);
        //分割的多条件查询
        feeResultExpressDto.setOwner_noList(search(owner_no));
        feeResultExpressDto.setExpress_corp_noList(search(express_corp_no));
        feeResultExpressDto.setProvinceList(search(province));
        feeResultExpressDto.setDoc_no(doc_no);
        feeResultExpressDto.setRef_no(ref_no);
        feeResultExpressDto.setExpress_no(express_no);
        feeResultExpressDto.setIs_confirmed(is_confirmed);
        feeResultExpressDto.setStart_time(start_time);
        feeResultExpressDto.setEnd_time(end_time);
        HttpSession session = request.getSession();
        session.setAttribute("feeResultExpressExcel", feeResultExpressDto);
        return iFeeResultExpressService.getExpressList(feeResultExpressDto);
    }

    private List<String> search(String searchCondition) {
        List<String> list = new ArrayList<>();
        if (searchCondition == "" && "".equals(searchCondition)) {
            list = null;
        } else {
            String[] searchArray = searchCondition.split(",");
            for (String searchResult : searchArray) {
                list.add(searchResult);
            }
        }
        return list;
    }

    //导出
    @RequestMapping(value = "api/exportExcel")
    public void exportExcel(HttpServletRequest request, HttpServletResponse response, FeeResultExpressDto feeResultExpressDto) throws Exception {
        response.reset(); //清除buffer缓存
        Map<String, Object> map = new HashMap<String, Object>();
        // 指定下载的文件名，浏览器都会使用本地编码，即GBK，浏览器收到这个文件名后，用ISO-8859-1来解码，然后用GBK来显示
        // 所以我们用GBK解码，ISO-8859-1来编码，在浏览器那边会反过来执行。
        response.setHeader("Content-Disposition", "attachment;filename=" + new String("快递计费结果.xlsx".getBytes("GBK"), "ISO-8859-1"));
        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);
        XSSFWorkbook workbook = null;
        //导出Excel对象
        //从session里面获取数据
        HttpSession session = request.getSession();
        feeResultExpressDto = (FeeResultExpressDto) session.getAttribute("feeResultExpressExcel");
        workbook = iFeeResultExpressService.exportExcelInfo(feeResultExpressDto);
        OutputStream output;
        try {
            output = response.getOutputStream();
            BufferedOutputStream bufferedOutPut = new BufferedOutputStream(output);
            bufferedOutPut.flush();
            workbook.write(bufferedOutPut);
            bufferedOutPut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}
