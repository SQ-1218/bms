package com.bms.web.system.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bms.service.dto.sys.SysUserDto;
import com.bms.service.model.sys.SysUserModel;
import com.bms.service.profiles.service.sys.ISysUserService;


@Controller
@RequestMapping(value = "bms/user")
public class BackOperatorController {
	
    @Resource(name="iSysUserService")
    private ISysUserService iSysUserService;
	
    /**
     * 跳转页面
     * @param
     * @return
     */
    @RequestMapping("forward/{path}")
    public ModelAndView forward(@PathVariable("path") String path, ModelAndView mv) throws Exception {
        mv = new ModelAndView("front/user/" + path);
        return mv;
    }
    
    /**
     * 新增和修改(跳转页面)
     * @param
     * @return
     */
    @RequestMapping("/userInfo")
    public ModelAndView userInfo(SysUserDto sysUserDto, ModelAndView mv) throws Exception {
        mv = new ModelAndView("front/user/userInfo");
        if(sysUserDto.getId()!=null){

            //编辑
            String id=sysUserDto.getId();
            SysUserModel sysUserModel=iSysUserService.getById(id);
            mv.addObject("user",sysUserModel);
        }
      return mv;
    }
    
    /**
     * 新增和修改
     * @param
     * @return
     */
    @RequestMapping("/save")
    @ResponseBody
    public String save(SysUserModel sysUserModel, ModelAndView mv) throws Exception {
        String msg="";
       try{
           if(sysUserModel.getId()==""){
           //增加
        	   sysUserModel.setCreated_by("admin");
              if(iSysUserService.insert(sysUserModel)>0){
                  msg="新增成功!";
              }else{
                  msg="新增失败!";
              }
           }else {
           //编辑

        	   sysUserModel.setUpdated_by("admin");
        	   iSysUserService.updateByIdAndVer(sysUserModel);
               msg="修改成功！";
           }
       }catch (Exception e){
           msg="操作失败";
           e.printStackTrace();
       }
        return msg;
    }

}
