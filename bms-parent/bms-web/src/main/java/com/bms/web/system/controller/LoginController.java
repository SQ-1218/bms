package com.bms.web.system.controller;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.druid.support.logging.Log;
import com.alibaba.druid.support.logging.LogFactory;
import com.base.common.constant.AnalyzeKeyConstant;
import com.base.common.constant.CookieNameConstant;
import com.base.common.core.cache.util.RedisUtils;
import com.base.common.core.model.CtxSessionBag;
import com.base.common.core.model.SysUser;
import com.base.common.core.utils.DesEncrypt;
import com.base.common.exception.BusinessException;
import com.bms.service.dto.sys.SysUserDto;
import com.bms.service.profiles.service.sys.ISysUserService;
import com.bms.service.vo.sys.SysUserVo;

@Controller
@RequestMapping(value = "sys/login")
public class LoginController {
	
	@Resource
	private ISysUserService sysUserService;
	
    private final Log log = LogFactory.getLog(getClass());
   
    @Resource
    private RedisUtils redisUtils;

	@RequestMapping("sys/{path}")
	public String forward(@PathVariable("path") String path ) {
		return "front/login/" + path;
	}
	
	
	/**
	 * 登录
	 *
	 */
    @RequestMapping(value = "login")
    @ResponseBody
    public Map<String, Object> login(HttpServletResponse resp,SysUserDto sysUserDto,
    		HttpSession httpSession)throws Exception{
    	
    	log.info(LoginController.class.getName()+":login");
    	
    	Map<String, Object> map = new HashMap<String, Object>();
        
    	SysUser sysUser = new SysUser();
    	SysUserVo sysUserVo = new SysUserVo();
    	try {
    		// 登录校验
        	sysUserVo = sysUserService.login(sysUserDto);
        	
        	if (sysUserVo!=null) {
        		BeanUtils.copyProperties(sysUserVo,sysUser);
        		map.put("sysUser",sysUser);
        		httpSession.setAttribute("sysUser",sysUser);
    		}
		} catch (BusinessException e) {
			// TODO: handle exception
			map.put("status", "查询用户信息出错:" + e.getMessage());
			return map;
		}
	
		return map;
    }
    
    /**
     * Description ： 退出、注销
     * 
     * 
     * @since
     *
     */
    @RequestMapping(value = "logout")
    @ResponseBody
    public Map<String, Object> logout(HttpServletRequest request, HttpServletResponse response) {
    	 
   	 // 清除TreadLocal信息
        CtxSessionBag.clear();
        
        Map<String, Object> map = new HashMap<String,Object>();
        
       // 清除cookie信息
       Cookie[] cookies = request.getCookies();
       if (cookies != null) {
           for (Cookie cookie : cookies) {
               if (CookieNameConstant.LOGIN_COOKIE.equals(cookie.getName())) {
                   // 设置cookie的生命为0（删除）
                   cookie.setMaxAge(0);
                   // 必须设置路径，否则无效
                   cookie.setPath("/");
                   response.addCookie(cookie);
                   // 清除redies
                   try {
                       String redisKey = DesEncrypt.decrypt(cookie.getValue(), AnalyzeKeyConstant.LOGIN_KEY);
                       redisUtils.del(redisKey);
                   } catch (Exception e) {
                       log.error("用户注销删除缓存信息失败");
                       map.put("status", "用户注销删除缓存信息失败");
                       return map;
                   }
               }
           }
       }
       
       // 清除缓存
       map.put("status", 0);
       return map;
   }
}