package com.bms.web.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bms.service.profiles.service.bd.IBdCargoOwnerService;

/**
 * 首页
 * @author chenchang
 *
 */
@Controller
@RequestMapping(value = "bms")
public class IndexController{
	
	@Resource
	private IBdCargoOwnerService iBdCargoOwnerService;

	@RequestMapping("forward/{path}")
	public String forward(@PathVariable("path") String path ) {
		return "front/" + path;
	}
}
