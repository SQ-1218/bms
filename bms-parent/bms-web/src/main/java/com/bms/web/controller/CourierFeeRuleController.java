package com.bms.web.controller;

import com.bms.common.response.BaseResponse;
import com.bms.common.table.vo.DataTablesVO;
import com.bms.service.dto.bd.FeeRuleExpressDto;
import com.bms.service.model.bd.FeeRuleExpressModel;
import com.bms.service.profiles.service.bd.IBdCargoOwnerService;
import com.bms.service.profiles.service.bd.IFeeRuleExpressService;
import com.bms.service.vo.bd.BdCargoOwnerVo;
import com.bms.service.vo.bd.CarrierVo;
import com.bms.service.vo.bd.ProvinceVo;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "bms/courier")
public class CourierFeeRuleController {
    @Resource
    private IBdCargoOwnerService bdCargoOwnerService;
    @Resource
    private IFeeRuleExpressService feeRuleExpressService;

    private static Log logger = LogFactory.getLog(CourierFeeRuleController.class);
    /**
     * 跳转页面
     * @param
     * @return
     */
    @RequestMapping("forward/{path}")
    public ModelAndView forward(@PathVariable("path") String path, ModelAndView mv) throws Exception {
        mv = new ModelAndView("front/courier/" + path);
        //获取查询条件的信息
        List<BdCargoOwnerVo> ownerList = bdCargoOwnerService.getOwner();
        List<CarrierVo> carrierList = feeRuleExpressService.getCarrier();
        List<ProvinceVo> provinceList=feeRuleExpressService.getProvince();
        mv.addObject("owner", ownerList);
        mv.addObject("carrier", carrierList);
        mv.addObject("province",provinceList);
        return mv;
    }
    /**
     * 获取数据(跳转页面)
     * @param
     * @return
     */
    @RequestMapping("/getList")
    @ResponseBody
    public DataTablesVO<FeeRuleExpressModel> getList(@ModelAttribute FeeRuleExpressDto feeRuleExpressDto, HttpServletRequest request) throws Exception {
        String owner_no=request.getParameter("owner_no");
        String express_corp_no=request.getParameter("express_corp_no");
        String province=request.getParameter("province");
        feeRuleExpressDto.setIs_deleted("1");
        //分割多条件查询
        feeRuleExpressDto.setOwner_noList(search(owner_no));
        feeRuleExpressDto.setExpress_corp_noList(search(express_corp_no));
        feeRuleExpressDto.setProvinceList(search(province));
        //把数据放入session导出时调用
        HttpSession session = request.getSession();
        session.setAttribute("feeRuleExpressExcel",feeRuleExpressDto);
        return feeRuleExpressService.getFeeRuleExpressList(feeRuleExpressDto);
    }
    //多条件分割函数
    public List<String> search(String searchCondition){
        List<String> list=new ArrayList<>();
        if(searchCondition==""&&"".equals(searchCondition)){
         list=null;
        }else{
        String[] searchArray=searchCondition.split(",");
        for(String searchResult:searchArray){
            list.add(searchResult);
        }
        }
        return list;
    }
    /**
     * 新增和修改(跳转页面)
     * @param
     * @return
     */
    @RequestMapping("/add")
    public ModelAndView add(FeeRuleExpressDto feeRuleExpressDto, ModelAndView mv) throws Exception {
        mv = new ModelAndView("front/courier/add");
        if(feeRuleExpressDto.getId()==null){
            //添加
            List<BdCargoOwnerVo> ownerList = bdCargoOwnerService.getOwner();
            List<CarrierVo> carrierList = feeRuleExpressService.getCarrier();
            List<ProvinceVo> provinceList=feeRuleExpressService.getProvince();
            mv.addObject("owner", ownerList);
            mv.addObject("carrier", carrierList);
            mv.addObject("province",provinceList);
        }else{
            //编辑
            String id=feeRuleExpressDto.getId();
            FeeRuleExpressModel feeRuleExpressModel=feeRuleExpressService.getById(id);
            mv.addObject("courierFeeRule",feeRuleExpressModel);
        }
      return mv;
    }
    /**
     * 新增和修改
     * @param
     * @return
     */
    @RequestMapping("/save")
    @ResponseBody
    public String save(FeeRuleExpressModel feeRuleExpressModel, ModelAndView mv) throws Exception {
        String msg="";
       try{
           if(feeRuleExpressModel.getId()==""){
           //增加
              feeRuleExpressModel.setCreated_by("admin");
              if(feeRuleExpressService.insert(feeRuleExpressModel)>0){
                  msg="新增成功!";
              }else{
                  msg="新增失败!";
              }
           }else {
           //编辑
               feeRuleExpressModel.setWhs_no("1");
               feeRuleExpressModel.setUpdated_by("admin");
               feeRuleExpressService.updateByIdAndVer(feeRuleExpressModel);
               msg="修改成功！";
           }
       }catch (Exception e){
           msg="操作失败";
           e.printStackTrace();
       }
        return msg;
    }
    /**
     * 批量删除
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping("/delete")
    public String batchDelete(HttpServletRequest request) {
       String ids =request.getParameter("ids");
        int status = 0;
        String msg = " ";
       if (StringUtils.isNotBlank(ids)) {
            List<String> idList = new ArrayList<String>();
            for (String id : ids.split(",")) {
                idList.add(id);
            }
            try {
               int row = feeRuleExpressService.batchDeleteByIds(idList);
                if (row > 0) {
                    status =1;
                    msg = "删除成功！";
                }
            } catch (Exception e) {
                msg += e.getMessage();
                logger.error("【批量删除数据】" + e.getMessage());
            }
        }
        return msg;
    }
    /**
     * 数据导入
     * @param file
     * @param response
     * @return
     */
    @RequestMapping("/importExcel")
    @ResponseBody
    public BaseResponse importExcel(@RequestParam MultipartFile file, HttpServletResponse response, ModelAndView mv) throws IOException {

        BaseResponse baseResponse=new BaseResponse();
        //如果文件不为空，写入上传路径
        if(!file.isEmpty()) {
            //上传文件名
            InputStream in = file.getInputStream();
            baseResponse=feeRuleExpressService.importExcelInfo(in,file);
            in.close();
        }
        return baseResponse;
    }
    /**
     * 下载Excel模板
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("/download")
    public void download(HttpServletRequest request,HttpServletResponse response) throws Exception {
        response.reset(); //清除buffer缓存
        Map<String,Object> map=new HashMap<String,Object>();
        // 指定下载的文件名，浏览器都会使用本地编码，即GBK，浏览器收到这个文件名后，用ISO-8859-1来解码，然后用GBK来显示
        // 所以我们用GBK解码，ISO-8859-1来编码，在浏览器那边会反过来执行。
        response.setHeader("Content-Disposition", "attachment;filename=" + new String("快递配置费用模板.xlsx".getBytes("GBK"),"ISO-8859-1"));
        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);
        XSSFWorkbook workbook=null;
        //导出Excel对象
        FeeRuleExpressDto feeRuleExpressDto=new FeeRuleExpressDto();
        feeRuleExpressDto.setOwner_no("CHONGXIN");
        feeRuleExpressDto.setExpress_corp_no("ANKD");
        feeRuleExpressDto.setProvince("天津");
        feeRuleExpressDto.setWeight_fm(Double.parseDouble("5"));
        feeRuleExpressDto.setWeight_to(Double.parseDouble("6"));
        feeRuleExpressDto.setFee_fixed(Double.parseDouble("1"));
        feeRuleExpressDto.setFee_extra(Double.parseDouble("0"));
        workbook = feeRuleExpressService.downloadExcelInfo(feeRuleExpressDto);
        OutputStream output;
        try {
            output = response.getOutputStream();
            BufferedOutputStream bufferedOutPut = new BufferedOutputStream(output);
            bufferedOutPut.flush();
            workbook.write(bufferedOutPut);
            bufferedOutPut.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    /**
     * 导出Excel数据
     * @param response
     * @return
     */
    @RequestMapping("/exportExcel")
    public void exportExcel(HttpServletResponse response, FeeRuleExpressDto feeRuleExpressDto, HttpServletRequest request) throws Exception {
        response.reset(); //清除buffer缓存
        Map<String,Object> map=new HashMap<String,Object>();
        // 指定下载的文件名，浏览器都会使用本地编码，即GBK，浏览器收到这个文件名后，用ISO-8859-1来解码，然后用GBK来显示
        // 所以我们用GBK解码，ISO-8859-1来编码，在浏览器那边会反过来执行。
        response.setHeader("Content-Disposition", "attachment;filename=" + new String("快递配置费用.xlsx".getBytes("GBK"),"ISO-8859-1"));
        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);
        XSSFWorkbook workbook=null;
        //导出Excel对象
       //从session里面获取数据
        HttpSession session = request.getSession();
        feeRuleExpressDto= (FeeRuleExpressDto) session.getAttribute("feeRuleExpressExcel");
        workbook = feeRuleExpressService.exportExcelInfo(feeRuleExpressDto);
        OutputStream output;
        try {
            output = response.getOutputStream();
            BufferedOutputStream bufferedOutPut = new BufferedOutputStream(output);
            bufferedOutPut.flush();
            workbook.write(bufferedOutPut);
            bufferedOutPut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
