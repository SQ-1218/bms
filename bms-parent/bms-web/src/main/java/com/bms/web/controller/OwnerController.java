package com.bms.web.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import com.bms.service.dto.bd.LogChangeOwnerBalanceDto;
import com.bms.service.model.bd.LogChangeOwnerBalanceModel;
import com.bms.service.profiles.service.bd.ILogChangeOwnerBalanceService;
import com.bms.service.vo.bd.BdCargoOwnerVo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bms.common.table.vo.DataTablesVO;
import com.bms.service.dto.bd.BdCargoOwnerDto;
import com.bms.service.model.bd.BdCargoOwnerModel;
import com.bms.service.profiles.service.bd.IBdCargoOwnerService;
import org.springframework.web.servlet.ModelAndView;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 管理页面
 * @author chenchang
 *
 */
@Controller
@RequestMapping(value = "bms/owner")
public class OwnerController{
	private static Log logger = LogFactory.getLog(OwnerController.class);
	
	@Resource
	private IBdCargoOwnerService iBdCargoOwnerService;

	@Resource
	private ILogChangeOwnerBalanceService logChangeOwnerBalanceService;

	@RequestMapping("forward/{path}")
	public  ModelAndView forward(@PathVariable("path") String path , ModelAndView mv ) throws Exception         {
        mv = new ModelAndView( "front/owner/" + path);
        //获取查询条件的信息
        List<BdCargoOwnerVo> ownerList = iBdCargoOwnerService.getOwner();
        mv.addObject("owner", ownerList);
        return mv;
	}
	//多条件分割函数
	public List<String> search(String searchCondition){
		List<String> list=new ArrayList<>();
		if(searchCondition==""&&"".equals(searchCondition)){
			list=null;
		}else{
			String[] searchArray=searchCondition.split(",");
			for(String searchResult:searchArray){
				list.add(searchResult);
			}
		}
		return list;
	}
	@RequestMapping(value="getList")
	@ResponseBody
	public DataTablesVO<BdCargoOwnerModel> getList(@ModelAttribute BdCargoOwnerDto bdCargoOwnerDto,HttpServletRequest request) throws Exception{
		String owner_no=request.getParameter("owner_no");
		//分割多条件查询
		bdCargoOwnerDto.setNoList(search(owner_no));
		return iBdCargoOwnerService.getOwnerList(bdCargoOwnerDto);
	}
	@RequestMapping(value="getLogList")
	@ResponseBody
	public DataTablesVO<LogChangeOwnerBalanceModel> getLogList(@ModelAttribute LogChangeOwnerBalanceDto logChangeOwnerBalanceDto, HttpServletRequest request) throws Exception{
		String owner_no=request.getParameter("owner_no");
		String start_time=request.getParameter("start_time");
		String end_time=request.getParameter("end_time");
		String created_by=request.getParameter("created_by");
		//分割多条件查询
		logChangeOwnerBalanceDto.setNoList(search(owner_no));
		logChangeOwnerBalanceDto.setStart_time(start_time);
		logChangeOwnerBalanceDto.setEnd_time(end_time);
		logChangeOwnerBalanceDto.setCreated_by(created_by);
		return logChangeOwnerBalanceService.getLogBalanceList(logChangeOwnerBalanceDto);
	}
	@RequestMapping(value="save")
	@ResponseBody
	public String save(HttpServletRequest request) throws Exception{
		//预警余额
      double warning_balance=Double.parseDouble(request.getParameter("warning_balance"));
        //余额
      double balance=Double.parseDouble(request.getParameter(	"balance"));
        //修改之前的余额
      double balance_fm=Double.parseDouble(request.getParameter("balance_fm"));
      String owner_no=request.getParameter("owner_no");
     //修改余额
      BdCargoOwnerModel bdCargoOwnerModel=new BdCargoOwnerModel();
      bdCargoOwnerModel.setWarning_balance(warning_balance);
      bdCargoOwnerModel.setBalance(balance);
      bdCargoOwnerModel.setNo(owner_no);
      iBdCargoOwnerService.updateBalance(bdCargoOwnerModel);
      //新增记录
	   LogChangeOwnerBalanceModel logChangeOwnerBalanceModel=new LogChangeOwnerBalanceModel();
	   logChangeOwnerBalanceModel.setCargo_owner_no(owner_no);
	   logChangeOwnerBalanceModel.setBalance_fm(balance_fm);
	   logChangeOwnerBalanceModel.setBalance_to(balance);
	   logChangeOwnerBalanceModel.setCreated_by("admin");
		Date date = new Date();
		Timestamp ts = new Timestamp(date.getTime());
	   logChangeOwnerBalanceModel.setCreated_time(ts);
	   iBdCargoOwnerService.insertLogBalance(logChangeOwnerBalanceModel);
		return "修改成功!";
	}
}
