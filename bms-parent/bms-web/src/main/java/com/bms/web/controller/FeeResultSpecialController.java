package com.bms.web.controller;

import com.bms.common.table.vo.DataTablesVO;
import com.bms.service.dto.bd.BdCargoOwnerDto;
import com.bms.service.dto.bd.FeeResultSpecialDto;
import com.bms.service.dto.bd.FeeResultStorageDto;
import com.bms.service.dto.bd.FeeRuleSpecialDto;
import com.bms.service.model.bd.FeeResultSpecialModel;
import com.bms.service.model.bd.FeeResultStorageModel;
import com.bms.service.profiles.service.bd.IBdCargoOwnerService;
import com.bms.service.profiles.service.bd.IFeeResultSpecialService;
import com.bms.service.profiles.service.bd.IFeeRuleSpecialService;
import com.bms.service.vo.bd.BdCargoOwnerVo;
import com.bms.service.vo.bd.FeeRuleSpecialCalVo;
import com.bms.service.vo.bd.FeeRuleSpecialItemVo;
import com.bms.service.vo.bd.FeeRuleStoragenameVo;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by zqy on 2018/9/30.
 */
@Controller
@RequestMapping("bms/resultSpecial")
public class FeeResultSpecialController {
    private static Log logger = LogFactory.getLog(FeeResultSpecialController.class);
    @Resource
    private IBdCargoOwnerService bdCargoOwnerService;
    @Resource
    private IFeeRuleSpecialService feeRuleSpecialService;
    @Resource
    private IFeeResultSpecialService feeResultSpecialService;
    /**
     * 跳转页面
     * @param
     * @return
     */
    @RequestMapping("forward/{path}")
    public ModelAndView forward(@PathVariable("path") String path, ModelAndView modelAndView, BdCargoOwnerDto bdCargoOwnerDto) throws Exception{
        modelAndView=new ModelAndView("front/special/"+path);
        //获取查询条件
        List<BdCargoOwnerVo> ownerList=bdCargoOwnerService.getOwner();
        List<FeeRuleSpecialItemVo> feeRuleSpecialItemVoList=feeRuleSpecialService.getFeeRuleSpecialItem();
        modelAndView.addObject("owner",ownerList);
        modelAndView.addObject("resultSpecial",feeRuleSpecialItemVoList);
        return modelAndView;
    }
    /*获取数据跳转页面*/
    @RequestMapping(value = "/getList")
    @ResponseBody
    public DataTablesVO<FeeResultSpecialModel> getList(@ModelAttribute FeeResultSpecialDto feeResultSpecialDto, HttpServletRequest request) throws Exception {
        String owner_no=request.getParameter("owner_no");
        String cal_item_code=request.getParameter("cal_item_code");
        String doc_no=request.getParameter("doc_no");
        String is_confirmed = request.getParameter("is_confirmed");
        if ("-1".equals(is_confirmed)) {
            is_confirmed = null;
        }
        String start_time=request.getParameter("start_time");
        String end_time=request.getParameter("end_time");
        feeResultSpecialDto.setIs_deleted(1);
        //分割多条件查询
        feeResultSpecialDto.setOwner_noList(search(owner_no));
        feeResultSpecialDto.setCal_item_codeList(search(cal_item_code));
        feeResultSpecialDto.setIs_confirmed(is_confirmed);
        feeResultSpecialDto.setDoc_no(doc_no);
        feeResultSpecialDto.setStart_time(start_time);
        feeResultSpecialDto.setEnd_time(end_time);
        //把数据放入session导出时调用
        HttpSession session = request.getSession();
        session.setAttribute("feeResultSpecialExcel",feeResultSpecialDto);
        return feeResultSpecialService.getFeeResultSpecialList(feeResultSpecialDto);
    }
    //多条件分割函数
    private List<String> search(String searchCondition) {
        List<String> list=new ArrayList<>();
        if(searchCondition==""&&"".equals(searchCondition)){
            list=null;
        }else{
            String[] searchArray=searchCondition.split(",");
            for(String searchResult:searchArray){
                list.add(searchResult);
            }
        }
        return list;
    }
    @RequestMapping("/add")
    public ModelAndView add(FeeResultSpecialDto feeResultSpecialDto, ModelAndView mv) throws Exception {
        mv = new ModelAndView("front/special/result_add");
        if(feeResultSpecialDto.getId()==null){
            //添加
            //货主
            List<BdCargoOwnerVo> ownerList = bdCargoOwnerService.getOwner();
            //计费项名称
            List<FeeRuleSpecialItemVo> feeRuleSpecialItemVoList =feeRuleSpecialService.getFeeRuleSpecialItem();
            //计算方式
            List<FeeRuleSpecialCalVo> feeRuleSpecialCalVoList=feeRuleSpecialService.getFeeRuleSpecialCal();
            mv.addObject("owner", ownerList);
            mv.addObject("item", feeRuleSpecialItemVoList);
            mv.addObject("cal",feeRuleSpecialCalVoList);
        }else{
            //编辑
           String id=feeResultSpecialDto.getId();
           FeeResultSpecialModel feeResultSpecialModel=feeResultSpecialService.getById(id);
            SimpleDateFormat sdf =   new SimpleDateFormat( " yyyy-MM-dd" );
            String nowTime = sdf.format(feeResultSpecialModel.getFee_occur_time());
            feeResultSpecialModel.setStart_time(nowTime);
           mv.addObject("feeResultSpecialModel",feeResultSpecialModel);
        }
        return mv;
    }
    //保存新增和编辑
    @RequestMapping("/save")
    @ResponseBody
    public String save(FeeResultSpecialModel feeResultSpecialModel, ModelAndView mv,HttpServletRequest request) throws Exception {
        String msg="";
        String time=request.getParameter("time");
        time =time +" 00:00:00.000000";
        Timestamp ts = Timestamp.valueOf(time);
        feeResultSpecialModel.setFee_occur_time(ts);
        try{
            if(feeResultSpecialModel.getId()==""){
                //增加
                feeResultSpecialModel.setCreated_by("admin");
                feeResultSpecialModel.setIs_deleted(1);
                if(feeResultSpecialService.insert(feeResultSpecialModel)>0){
                    msg="新增成功!";
                }else{
                    msg="新增失败!";
                }

            }else {
                //编辑
                feeResultSpecialModel.setUpdated_by("admin");
                feeResultSpecialService.updateByIdAndVer(feeResultSpecialModel);
                msg="修改成功!";
            }
        }catch (Exception e){
            msg="操作失败!";
            e.printStackTrace();
        }
        return msg;
    }
    /**
     * 批量删除
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping("/delete")
    public String batchDelete(HttpServletRequest request) {
        String ids =request.getParameter("ids");
        int status = 0;
        String msg = "";
        if (StringUtils.isNotBlank(ids)) {
            List<String> idList = new ArrayList<String>();
            for (String id : ids.split(",")) {
                idList.add(id);
            }
            try {
                if (feeResultSpecialService.batchDeleteByIds(idList)>0) {
                    status =1;
                    msg = "删除成功!";
                }else{
                    msg="删除失败!";
                }
            } catch (Exception e) {
                msg += e.getMessage();
                logger.error("【批量删除数据】" + e.getMessage());
            }
        }
        return msg;
    }
    /**
     * 导出Excel数据
     * @param response
     * @return
     */
    @RequestMapping("/exportExcel")
    public void exportExcel(HttpServletResponse response, FeeResultSpecialDto feeResultSpecialDto, HttpServletRequest request) throws Exception {
        response.reset(); //清除buffer缓存
        Map<String,Object> map=new HashMap<String,Object>();
        // 指定下载的文件名，浏览器都会使用本地编码，即GBK，浏览器收到这个文件名后，用ISO-8859-1来解码，然后用GBK来显示
        // 所以我们用GBK解码，ISO-8859-1来编码，在浏览器那边会反过来执行。
        response.setHeader("Content-Disposition", "attachment;filename=" + new String("特殊配置费用.xlsx".getBytes("GBK"),"ISO-8859-1"));
        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);
        XSSFWorkbook workbook=null;
        //导出Excel对象
        //从session里面获取数据
        HttpSession session = request.getSession();
        feeResultSpecialDto = (FeeResultSpecialDto) session.getAttribute("feeResultSpecialExcel");
        workbook = feeResultSpecialService.exportExcelInfo(feeResultSpecialDto);
        OutputStream output;
        try {
            output = response.getOutputStream();
            BufferedOutputStream bufferedOutPut = new BufferedOutputStream(output);
            bufferedOutPut.flush();
            workbook.write(bufferedOutPut);
            bufferedOutPut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
