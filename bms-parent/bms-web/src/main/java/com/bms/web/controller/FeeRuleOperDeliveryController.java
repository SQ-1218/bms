package com.bms.web.controller;

import com.bms.common.table.vo.DataTablesVO;
import com.bms.service.dto.bd.FeeRuleOperDeliveryDto;
import com.bms.service.model.bd.FeeRuleOperDeliveryModel;
import com.bms.service.profiles.service.bd.IBdCargoOwnerService;
import com.bms.service.profiles.service.bd.IFeeRuleOperDeliveryService;
import com.bms.service.profiles.service.bd.IFeeRuleStorageService;
import com.bms.service.vo.bd.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "bms/delivery")
public class FeeRuleOperDeliveryController {
    private static Log logger = LogFactory.getLog(FeeRuleOperDeliveryController.class);
    @Resource
    private IBdCargoOwnerService bdCargoOwnerService;
    @Resource
    private IFeeRuleOperDeliveryService feeRuleOperDeliveryService;
    @Resource
    private IFeeRuleStorageService feeRuleStorageService;
    @RequestMapping("forward/{path}")
    public ModelAndView forward(@PathVariable("path") String path, ModelAndView mv) throws Exception {
        mv = new ModelAndView("front/delivery/" + path);
        //获取查询条件的信息
        List<BdCargoOwnerVo> ownerList = bdCargoOwnerService.getOwner();
        List<FeeRuleOperDeliveryItemVo> feeRuleOperDeliveryItemVoList =feeRuleOperDeliveryService.getFeeOperDeliveryItemList();
        mv.addObject("owner", ownerList);
        mv.addObject("feeRuleOperDeliveryItem", feeRuleOperDeliveryItemVoList);
        return mv;
    }
    @RequestMapping("/getList")
    @ResponseBody
    public DataTablesVO<FeeRuleOperDeliveryModel> getList(@ModelAttribute FeeRuleOperDeliveryDto feeRuleOperDeliveryDto, HttpServletRequest request) throws Exception {
        feeRuleOperDeliveryDto.setIs_deleted("1");
        String owner_no=request.getParameter("owner_no");
        String cal_item_code=request.getParameter("cal_item_code");
        feeRuleOperDeliveryDto.setOwner_noList(search(owner_no));
        feeRuleOperDeliveryDto.setCal_item_codeList(search(cal_item_code));
        //把数据放入session导出时调用
        HttpSession session = request.getSession();
        session.setAttribute("feeRuleOperDeliveryExcel",feeRuleOperDeliveryDto);
        return feeRuleOperDeliveryService.getFeeRuleOperDeliveryList(feeRuleOperDeliveryDto);
    }
    //多条件分割函数
    public List<String> search(String searchConditions){
        List<String> searchList=new ArrayList<>();
        if("".equals(searchConditions)&&searchConditions==""){
            searchList=null;
        }else{
            String[] searchArray=searchConditions.split(",");
            for(String searchResult:searchArray){
                searchList.add(searchResult);
            }
        }
        return searchList;
    }
    /**
     * 新增和修改(跳转页面)
     * @param
     * @return
     */
    @RequestMapping("/add")
    public ModelAndView add(FeeRuleOperDeliveryDto feeRuleOperDeliveryDto, ModelAndView mv) throws Exception {
        mv = new ModelAndView("front/delivery/add");
        if(feeRuleOperDeliveryDto.getId()==null){
            //添加
            //货主
            List<BdCargoOwnerVo> ownerList = bdCargoOwnerService.getOwner();
            //计费项名称
            List<FeeRuleOperDeliveryItemVo> feeRuleOperDeliveryItemVoList =feeRuleOperDeliveryService.getFeeOperDeliveryItemList();
            //计算方式
            List<FeeRuleOperDeliveryCalVo> feeOpereDeliveryCalVoList=feeRuleOperDeliveryService.getFeeOperDeliveryCalList();
            //单据类型
            List<FeeRuleOperDeliveryDocVo> feeRuleOperDeliveryDocVoList=feeRuleOperDeliveryService.getFeeOperDeliveryDocList();
            //按件计费
            List<FeeRuleStorageWayVo> feeRuleStorageWayVoList1 = feeRuleStorageService.getCalWayByQty();


            mv.addObject("owner", ownerList);
            mv.addObject("item", feeRuleOperDeliveryItemVoList);
            mv.addObject("cal",feeOpereDeliveryCalVoList);
            mv.addObject("doc",feeRuleOperDeliveryDocVoList);
            mv.addObject("calWayByQty", feeRuleStorageWayVoList1);
        }else{
            //编辑
            String id=feeRuleOperDeliveryDto.getId();
            //单据类型
            List<FeeRuleOperDeliveryDocVo> feeRuleOperDeliveryDocVoList=feeRuleOperDeliveryService.getFeeOperDeliveryDocList();
            //计算方式
            List<FeeRuleOperDeliveryCalVo> feeOpereDeliveryCalVoList=feeRuleOperDeliveryService.getFeeOperDeliveryCalList();
            FeeRuleOperDeliveryModel feeRuleOperDeliveryModel=feeRuleOperDeliveryService.getById(id);
            //按件计费
            List<FeeRuleStorageWayVo> feeRuleStorageWayVoList1 = feeRuleStorageService.getCalWayByQty();

            mv.addObject("feeRuleOperDelivery",feeRuleOperDeliveryModel);
            mv.addObject("doc",feeRuleOperDeliveryDocVoList);
            mv.addObject("cal",feeOpereDeliveryCalVoList);
            mv.addObject("calWayByQty", feeRuleStorageWayVoList1);
        }
        return mv;
    }
    //选择货主时改变序号
    @ResponseBody
    @RequestMapping("/getCount")
    public Integer getCount(HttpServletRequest request) throws Exception {
        String owner_no =request.getParameter("owner_no");
        //计费项序号
        int count=feeRuleOperDeliveryService.getFeeOperDeliveryCount(owner_no);
        return (count+1);
    }
    //保存新增和编辑
    @RequestMapping("/save")
    @ResponseBody
    public String save(FeeRuleOperDeliveryModel feeRuleOperDeliveryModel, ModelAndView mv, HttpServletRequest request) throws Exception {
        String msg="";
        try{
            if ("" == feeRuleOperDeliveryModel.getCal_way()) {
                String cal_way = request.getParameter("cal_way_other");
                feeRuleOperDeliveryModel.setCal_way(cal_way);
            }
            if(feeRuleOperDeliveryModel.getId()==""&&"".equals(feeRuleOperDeliveryModel.getId())){
                feeRuleOperDeliveryModel.setCreated_by("admin");
               if(feeRuleOperDeliveryService.insert(feeRuleOperDeliveryModel)>0){
                   msg="新增成功!";
               }else{
                   msg="新增失败!";
               }

            }else {
                //编辑
                 String doc_type=feeRuleOperDeliveryModel.getDoc_type();
                if("-1".equals(doc_type)){
                    feeRuleOperDeliveryModel.setDoc_type(null);
                }
                feeRuleOperDeliveryModel.setUpdated_by("admin");
                feeRuleOperDeliveryService.updateByIdAndVer(feeRuleOperDeliveryModel);
                msg="修改成功!";
            }
        }catch (Exception e){
            msg="操作失败!";
            e.printStackTrace();
        }
        return msg;
    }
    /**
     * 批量删除
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping("/delete")
    public String batchDelete(HttpServletRequest request) {
        String ids =request.getParameter("ids");
        int status = 0;
        String msg = "";
        if (StringUtils.isNotBlank(ids)) {
            List<String> idList = new ArrayList<String>();
            for (String id : ids.split(",")) {
                idList.add(id);
            }
            try {
                if (feeRuleOperDeliveryService.batchDeleteByIds(idList)>0) {
                    status =1;
                    msg = "删除成功!";
                }else{
                    msg="删除失败!";
                }
            } catch (Exception e) {
                msg += e.getMessage();
                logger.error("【批量删除数据】" + e.getMessage());
            }
        }
        return msg;
    }
    /**
     * 数据导入
     * @param file
     * @param response
     * @return
     */
    @RequestMapping("/importExcel")
    @ResponseBody
    public String importExcel(@RequestParam MultipartFile file, HttpServletResponse response, ModelAndView mv) throws IOException {
        String msg="";
        //如果文件不为空，写入上传路径
        if(!file.isEmpty()) {
            //上传文件名
            InputStream in = file.getInputStream();
           int row= feeRuleOperDeliveryService.importExcelInfo(in,file);
           if(row>0){
               msg="导入数据成功";
           }else{
               msg="导入数据失败，请检查EXCEL表格";
           }
            in.close();

        }
        return msg;
    }
    /**
     * 下载Excel模板
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("/download")
    public void download(HttpServletRequest request,HttpServletResponse response) throws Exception {
        response.reset(); //清除buffer缓存
        Map<String,Object> map=new HashMap<String,Object>();
        // 指定下载的文件名，浏览器都会使用本地编码，即GBK，浏览器收到这个文件名后，用ISO-8859-1来解码，然后用GBK来显示
        // 所以我们用GBK解码，ISO-8859-1来编码，在浏览器那边会反过来执行。
        response.setHeader("Content-Disposition", "attachment;filename=" + new String("出库配置费用模板.xlsx".getBytes("GBK"),"ISO-8859-1"));
        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader( "Expires", 0);
        XSSFWorkbook workbook=null;
        //导出Excel对象
        FeeRuleOperDeliveryDto feeRuleOperDeliveryDto=new FeeRuleOperDeliveryDto();
        feeRuleOperDeliveryDto.setOwner_no("CHONGXIN");
        feeRuleOperDeliveryDto.setCal_item_name("秒杀按单");
        feeRuleOperDeliveryDto.setCal_item_code("5");
        feeRuleOperDeliveryDto.setCal_item_note("秒杀按单");
        feeRuleOperDeliveryDto.setCal_way("1");
        feeRuleOperDeliveryDto.setDoc_type("1");
        feeRuleOperDeliveryDto.setFactor1_note("单价（元/单）");
        feeRuleOperDeliveryDto.setFactor1_value("1.1");
        feeRuleOperDeliveryDto.setFactor2_note("单价（元/单）");
        feeRuleOperDeliveryDto.setFactor2_value("1.1");
        feeRuleOperDeliveryDto.setFactor2_note("单价（元/单）");
        feeRuleOperDeliveryDto.setFactor2_value("1.1");
        feeRuleOperDeliveryDto.setFactor3_note("单价（元/单）");
        feeRuleOperDeliveryDto.setFactor3_value("1.1");
        feeRuleOperDeliveryDto.setFactor4_note("单价（元/单）");
        feeRuleOperDeliveryDto.setFactor4_value("1.1");
        feeRuleOperDeliveryDto.setFactor5_note("单价（元/单）");
        feeRuleOperDeliveryDto.setFactor5_value("1.1");
        feeRuleOperDeliveryDto.setRemark("单价乘以数量");
        workbook = feeRuleOperDeliveryService.downloadExcelInfo(feeRuleOperDeliveryDto);
        OutputStream output;
        try {
            output = response.getOutputStream();
            BufferedOutputStream bufferedOutPut = new BufferedOutputStream(output);
            bufferedOutPut.flush();
            workbook.write(bufferedOutPut);
            bufferedOutPut.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 导出Excel数据
     * @param response
     * @return
     */
    @RequestMapping("/exportExcel")
    public void exportExcel(HttpServletResponse response, FeeRuleOperDeliveryDto feeRuleOperDeliveryDto, HttpServletRequest request) throws Exception {
        response.reset(); //清除buffer缓存
        Map<String,Object> map=new HashMap<String,Object>();
        // 指定下载的文件名，浏览器都会使用本地编码，即GBK，浏览器收到这个文件名后，用ISO-8859-1来解码，然后用GBK来显示
        // 所以我们用GBK解码，ISO-8859-1来编码，在浏览器那边会反过来执行。
        response.setHeader("Content-Disposition", "attachment;filename=" + new String("出库配置费用.xlsx".getBytes("GBK"),"ISO-8859-1"));
        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);
        XSSFWorkbook workbook=null;
        //导出Excel对象
        //从session里面获取数据
        HttpSession session = request.getSession();
        feeRuleOperDeliveryDto = (FeeRuleOperDeliveryDto) session.getAttribute("feeRuleOperDeliveryExcel");
        workbook = feeRuleOperDeliveryService.exportExcelInfo(feeRuleOperDeliveryDto);
        OutputStream output;
        try {
            output = response.getOutputStream();
            BufferedOutputStream bufferedOutPut = new BufferedOutputStream(output);
            bufferedOutPut.flush();
            workbook.write(bufferedOutPut);
            bufferedOutPut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
