package com.bms.web.controller;

import com.bms.common.table.vo.DataTablesVO;
import com.bms.service.dto.bd.BdCargoOwnerDto;
import com.bms.service.dto.bd.FeeRuleOperReceivingDto;
import com.bms.service.model.bd.FeeRuleOperReceivingModel;
import com.bms.service.profiles.service.bd.IBdCargoOwnerService;
import com.bms.service.profiles.service.bd.IFeeRuleOperReceivingService;
import com.bms.service.profiles.service.bd.IFeeRuleStorageService;
import com.bms.service.vo.bd.*;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jsc on 2018/8/27.
 */
 @Controller
 @RequestMapping(value = "bms/receiving")
public class ReceivingController {
    private static Log logger = LogFactory.getLog(ReceivingController.class);
   @Resource
    private IFeeRuleOperReceivingService iFeeRuleOperReceivingService;
    @Resource
    private IBdCargoOwnerService iBdCargoOwnerService;
    @Resource
    private IFeeRuleStorageService feeRuleStorageService;
    @RequestMapping("forward/{path}")
    public ModelAndView forward(@PathVariable("path") String path, ModelAndView modelAndView, BdCargoOwnerDto bdCargoOwnerDto) throws Exception{
        modelAndView=new ModelAndView("front/receiving/"+path);
        //获取查询条件
        List<BdCargoOwnerVo> ownerlist=iBdCargoOwnerService.getOwner();
        List<FeeRuleOperReceiVo> receivinglist=iFeeRuleOperReceivingService.getReceiving();
        modelAndView.addObject("owner",ownerlist);
        modelAndView.addObject("receiving",receivinglist);
        return modelAndView;
    }
    /*获取数据跳转页面*/
    @RequestMapping(value = "api/getList")
    @ResponseBody
    public DataTablesVO<FeeRuleOperReceivingModel> getList(@ModelAttribute FeeRuleOperReceivingDto feeRuleOperReceivingDto, HttpServletRequest request) throws Exception {
        String owner_no=request.getParameter("owner_no");
        String cal_item_code=request.getParameter("cal_item_code");
        feeRuleOperReceivingDto.setIs_deleted(Double.valueOf("1"));
        //分割多条件查询
        feeRuleOperReceivingDto.setOwner_noList(search(owner_no));
        feeRuleOperReceivingDto.setCal_item_codeList(search(cal_item_code));
        //把数据放入session导出时调用
        HttpSession session = request.getSession();
        session.setAttribute("feeRuleOperReceivingExcel",feeRuleOperReceivingDto);
        return iFeeRuleOperReceivingService.getReceivingList(feeRuleOperReceivingDto);
    }
    //多条件的分割函数
    private List<String> search(String searchCondition) {
        List<String> list=new ArrayList<>();
        if(searchCondition==""&&"".equals(searchCondition)){
            list=null;
        }else{
            String[] searchArray=searchCondition.split(",");
            for(String searchResult:searchArray){
                list.add(searchResult);
            }
        }
        return list;
    }
    /**
     * 新增和修改(跳转页面)
     * @param
     * @return
     */
    @RequestMapping("api/add")
    public ModelAndView add(FeeRuleOperReceivingDto feeRuleOperReceivingDto, ModelAndView modelAndView) throws Exception {
        modelAndView = new ModelAndView("front/receiving/add");
        if(feeRuleOperReceivingDto.getId()==null){
            //添加
            //货主
            List<BdCargoOwnerVo> ownerList = iBdCargoOwnerService.getOwner();
            //计费项名称
            List<FeeRuleOperReceiVo> feeRuleOperReceiVoList =iFeeRuleOperReceivingService.getReceiving();
            //计算方式
            List<FeeRuleOperReceivingWayVo> feeRuleOperReceivingWayVoList=iFeeRuleOperReceivingService.getReceivingWay();
            //单据类型
            List<FeeRuleOperReceiDocVo> feeRuleOperReceiDocVoList=iFeeRuleOperReceivingService.getReceivingDoc();
            //按件型收费
            List<FeeRuleStorageWayVo> feeRuleStorageWayVoList1=feeRuleStorageService.getCalWayByQty();
            modelAndView.addObject("owner", ownerList);
            modelAndView.addObject("receiving", feeRuleOperReceiVoList);
            modelAndView.addObject("way",feeRuleOperReceivingWayVoList);
            modelAndView.addObject("doc",feeRuleOperReceiDocVoList);
            modelAndView.addObject("calWayByQty",feeRuleStorageWayVoList1);
        }else{
            //编辑
            String id=feeRuleOperReceivingDto.getId();
            //单据类型
            List<FeeRuleOperReceiDocVo> feeRuleOperReceiDocVoList=iFeeRuleOperReceivingService.getReceivingDoc();
            //计算方式
            List<FeeRuleOperReceivingWayVo> feeRuleOperReceivingWayVoList=iFeeRuleOperReceivingService.getReceivingWay();
            FeeRuleOperReceivingModel feeRuleOperReceivingModel=iFeeRuleOperReceivingService.getById(id);
           //按件型收费
            List<FeeRuleStorageWayVo> feeRuleStorageWayVoList1=feeRuleStorageService.getCalWayByQty();
            modelAndView.addObject("feeRuleOperReceiving",feeRuleOperReceivingModel);
            modelAndView.addObject("way",feeRuleOperReceivingWayVoList);
            modelAndView.addObject("doc",feeRuleOperReceiDocVoList);
            modelAndView.addObject("calWayByQty",feeRuleStorageWayVoList1);
        }
        return modelAndView;
    }
    //选择货主时改变序号
    @ResponseBody
    @RequestMapping("/getCount")
    public Integer getCount(HttpServletRequest request) throws Exception {
        String owner_no =request.getParameter("owner_no");
        //计费项序号
        int count=iFeeRuleOperReceivingService.getReceivingNumber(owner_no);
        return (count+1);
    }
    //保存新增和编辑
    @RequestMapping("api/gave")
    @ResponseBody
    public String gave(FeeRuleOperReceivingModel feeRuleOperReceivingModel, ModelAndView modelAndView ,HttpServletRequest request) throws Exception {
        String msg="";
        try{
            String factor_value2=request.getParameter("factor2_value1");
            feeRuleOperReceivingModel.setFactor2_value(factor_value2);
            if(feeRuleOperReceivingModel.getId()==""&&"".equals(feeRuleOperReceivingModel.getId())){
                feeRuleOperReceivingModel.setCreated_by("admin");
                if(iFeeRuleOperReceivingService.insert(feeRuleOperReceivingModel)>0){
                    msg="新增成功!";
                }else{
                    msg="新增失败!";
                }

            }else {
                //编辑
                String factor_value=request.getParameter("factor2_value1");
                feeRuleOperReceivingModel.setFactor2_value(factor_value);
                String doc_type=feeRuleOperReceivingModel.getDoc_type();
                if("-1".equals(doc_type)){
                    feeRuleOperReceivingModel.setDoc_type(null);
                }
                feeRuleOperReceivingModel.setUpdated_by("admin");
                iFeeRuleOperReceivingService.updateByIdAndVer(feeRuleOperReceivingModel);
                msg="修改成功!";
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return msg;
    }
    @RequestMapping(value = "api/delete")
    @ResponseBody
    public String deleteStorage(HttpServletRequest request){
        String ids=request.getParameter("ids");
        int i=0;
        String msg="";
        if(StringUtils.isNotBlank(ids)){
            List<String> idlist=new ArrayList<>();
            for (String id : ids.split(",")){
                idlist.add(id);
            }
            try {
                int t=iFeeRuleOperReceivingService.delectByIds(idlist);
                if(t>0){
                    i=1;
                    msg = "删除成功！";
                }
            } catch (Exception e) {
                msg += e.getMessage();
                logger.error("【批量删除数据】" + e.getMessage());
            }
        }
        return msg;
    }
    /**
     * 导入文件
     * @param
     * @param
     * @return
     */
    @RequestMapping(value = "api/importExcel")
    @ResponseBody
    public String importExcel(@RequestParam MultipartFile file, HttpServletResponse response, ModelAndView modelAndView) throws IOException {
        String msg="";
        //如果文件不为空，写入上传路径
        if(!file.isEmpty()) {
            //上传文件名
            InputStream in = file.getInputStream();
            try {
                iFeeRuleOperReceivingService.importExcelInfo(in,file);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            in.close();
            msg="1";
        }
        return msg;
    }
    /**
     * 导出Excel模板
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "api/exportExcel")
    public  void exportExcel(HttpServletRequest request, HttpServletResponse response, FeeRuleOperReceivingDto feeRuleOperReceivingDto) throws Exception{
        response.reset(); //清除buffer缓存
        Map<String,Object> map=new HashMap<String,Object>();
        // 指定下载的文件名，浏览器都会使用本地编码，即GBK，浏览器收到这个文件名后，用ISO-8859-1来解码，然后用GBK来显示
        // 所以我们用GBK解码，ISO-8859-1来编码，在浏览器那边会反过来执行。
        response.setHeader("Content-Disposition", "attachment;filename=" + new String("入库规则费用.xlsx".getBytes("GBK"),"ISO-8859-1"));
        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);
        XSSFWorkbook workbook=null;
        //导出Excel对象
        //从session里面获取数据
        HttpSession session = request.getSession();
        feeRuleOperReceivingDto= (FeeRuleOperReceivingDto) session.getAttribute("feeRuleOperReceivingExcel");
        workbook = iFeeRuleOperReceivingService.exportExcelInfo(feeRuleOperReceivingDto);
        OutputStream output;
        try {
            output = response.getOutputStream();
            BufferedOutputStream bufferedOutPut = new BufferedOutputStream(output);
            bufferedOutPut.flush();
            workbook.write(bufferedOutPut);
            bufferedOutPut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * 下载Excel模板
     * @param request
     * @param response
     * @return
     */@RequestMapping("api/download")
    public void download(HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.reset(); //清除buffer缓存
        Map<String,Object> map=new HashMap<String,Object>();
        // 指定下载的文件名，浏览器都会使用本地编码，即GBK，浏览器收到这个文件名后，用ISO-8859-1来解码，然后用GBK来显示
        // 所以我们用GBK解码，ISO-8859-1来编码，在浏览器那边会反过来执行。
        response.setHeader("Content-Disposition", "attachment;filename=" + new String("入库配置费用模板.xlsx".getBytes("GBK"),"ISO-8859-1"));
        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);
        XSSFWorkbook workbook=null;
        //导出Excel对象
        FeeRuleOperReceivingDto feeRuleOperReceivingDto=new FeeRuleOperReceivingDto();
        feeRuleOperReceivingDto.setOwner_no("TAPIN");
        feeRuleOperReceivingDto.setCal_item_rank(Double.parseDouble("1"));
        feeRuleOperReceivingDto.setCal_item_name("卸货费-Max(V,W)");
        feeRuleOperReceivingDto.setCal_item_code("1");
        feeRuleOperReceivingDto.setCal_item_note("卸货费-按Max(体积，重量)");
        feeRuleOperReceivingDto.setDoc_type("0");
        feeRuleOperReceivingDto.setCal_way("1");
        feeRuleOperReceivingDto.setFactor1_note("按体积（元/m3）");
        feeRuleOperReceivingDto.setFactor1_value("10.2");
        feeRuleOperReceivingDto.setFactor2_note("按重量（元/吨）");
        feeRuleOperReceivingDto.setFactor2_value("10");
        feeRuleOperReceivingDto.setFactor3_note("按件数");
        feeRuleOperReceivingDto.setFactor3_value("10");
        feeRuleOperReceivingDto.setFactor4_note("按面积");
        feeRuleOperReceivingDto.setFactor4_value("10");
        feeRuleOperReceivingDto.setFactor5_note("按托盘");
        feeRuleOperReceivingDto.setFactor5_value("10");
        feeRuleOperReceivingDto.setRemark("单价乘数量");
        workbook = iFeeRuleOperReceivingService.downloadExcelInfo(feeRuleOperReceivingDto);
        OutputStream output;
        try {
            output = response.getOutputStream();
            BufferedOutputStream bufferedOutPut = new BufferedOutputStream(output);
            bufferedOutPut.flush();
            workbook.write(bufferedOutPut);
            bufferedOutPut.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
