package com.bms.web.controller;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import com.bms.service.profiles.service.bd.IFeeService;
import com.bms.service.profiles.service.bd.ILogVerifyFeeResultService;
import com.bms.service.vo.bd.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.druid.support.logging.Log;
import com.alibaba.druid.support.logging.LogFactory;
import com.alibaba.fastjson.JSONObject;
import com.base.common.util.HttpUtil;
import com.bms.common.constant.ConstClass.Type_Fee;
import com.bms.common.util.ListUtil;
import com.bms.service.dto.bd.FeeResultDeliveryFeeDto;
import com.bms.service.dto.bd.FeeResultExpressFeeDto;
import com.bms.service.dto.bd.FeeResultOwnerMonthlyDto;
import com.bms.service.dto.bd.FeeResultPackMaterialFeeDto;
import com.bms.service.dto.bd.FeeResultReceivingFeeDto;
import com.bms.service.dto.bd.FeeResultSpecialFeeDto;
import com.bms.service.dto.bd.FeeResultStorageFeeDto;
import com.bms.service.dto.bd.SumFeeDto;
import com.bms.service.dto.intf.AccountingDTO;
import com.bms.service.dto.intf.ReceivableHeaderDTO;
import com.bms.service.dto.intf.ReceivableLineDTO;
import com.bms.service.dto.intf.VerifyResultDto;
import com.bms.service.model.bd.FeeResultDeliveryFeeModel;
import com.bms.service.model.bd.FeeResultExpressFeeModel;
import com.bms.service.model.bd.FeeResultOwnerMonthlyModel;
import com.bms.service.model.bd.FeeResultPackMaterialFeeModel;
import com.bms.service.model.bd.FeeResultReceivingFeeModel;
import com.bms.service.model.bd.FeeResultSpecialFeeModel;
import com.bms.service.model.bd.FeeResultStorageFeeModel;
import com.bms.service.model.bd.LogVerifyFeeResultModel;
import com.bms.service.model.bd.SumFeeModel;
import com.bms.service.profiles.service.bd.IBdCargoOwnerService;
import com.bms.service.profiles.service.bd.IFeeService;
import com.bms.service.profiles.service.bd.ILogVerifyFeeResultService;
import com.bms.service.vo.bd.BdCargoOwnerVo;
import com.bms.service.vo.bd.FeeResultExpressSkuVo;
import com.bms.service.vo.bd.FeeResultOwnerMonthlyVo;
import com.bms.service.vo.bd.FeeRuleSpecialVo;
import com.bms.service.vo.bd.FeeRuleStorageVo;
import com.bms.service.vo.bd.SnapshotStkTotalInfoVo;

/**
 * Created by jsc on 2018/10/11.
 */
@Controller
@RequestMapping(value = "bms/fee")
public class FeeController {

	private static final String URL_ACCOUNTING_TO_FSCC = "http://10.100.30.10:28094/services/cndiPushAccountsReceivableService?wsdl";


    private static Log logger = LogFactory.getLog(FeeController.class);
    @Resource
    private IBdCargoOwnerService iBdCargoOwnerService;
    @Resource
    private IFeeService iFeeService;
    @Resource
    private ILogVerifyFeeResultService iLogVerifyFeeResultService;

    @RequestMapping("forward/{path}")
    public ModelAndView forward(@PathVariable("path") String path, ModelAndView modelAndView) throws Exception {
        modelAndView = new ModelAndView("front/fee/" + path);
        //获取查询条件的信息
        List<BdCargoOwnerVo> ownerList = iBdCargoOwnerService.getOwner();
        modelAndView.addObject("owner", ownerList);
        return modelAndView;
    }

    //获取(快递费用、仓储费用、出库操作费用、入库操作费用、特殊费用的总费用)
    @ResponseBody
    @RequestMapping(value = "verifySettlement")
	public String verifySettlement(@ModelAttribute List<VerifyResultDto> dtoList, HttpServletRequest request) {
		HashMap<String, Object> map = new HashMap<>();
		map.put("code", 0);
		map.put("msg", "success");
		
		try {
			iLogVerifyFeeResultService.verifySettlement(dtoList);
		} catch (Exception e) {
			logger.error("审核异常", e);
			map.put("code", 1);
			map.put("msg", "审核异常," + e.getMessage());
		}
		return JSONObject.toJSONString(map);
	}
    
    //获取(快递费用、仓储费用、出库操作费用、入库操作费用、特殊费用的总费用)
    @RequestMapping(value = "getList")
    @ResponseBody
    public HashMap<String, Object> getList(@ModelAttribute FeeResultExpressFeeDto feeResultExpressFeeDto, FeeResultStorageFeeDto feeResultStorageFeeDto, FeeResultDeliveryFeeDto feeResultDeliveryFeeDto, FeeResultSpecialFeeDto feeResultSpecialFeeDto, FeeResultReceivingFeeDto feeResultReceivingFeeDto, FeeResultPackMaterialFeeDto feeResultPackMaterialFeeDto, SumFeeDto sumFeeDto, HttpServletRequest request) throws Exception {
        HashMap<String, Object> map = new HashMap<>();
        String msg = "";
        //查询条件
        //户主
        String owner_no = request.getParameter("owner_no");

        //按月份
        String month_time = request.getParameter("month_time");
        feeResultExpressFeeDto.setIs_deleted(1);
        feeResultStorageFeeDto.setIs_deleted(1);
        feeResultDeliveryFeeDto.setIs_deleted(1);
        feeResultReceivingFeeDto.setIs_deleted(1);
        feeResultSpecialFeeDto.setIs_deleted(1);
        feeResultPackMaterialFeeDto.setIs_deleted(1);
        feeResultExpressFeeDto.setIs_confirmed(1);
        feeResultStorageFeeDto.setIs_confirmed(1);
        feeResultDeliveryFeeDto.setIs_confirmed(1);
        feeResultReceivingFeeDto.setIs_confirmed(1);
        feeResultSpecialFeeDto.setIs_confirmed(1);
        feeResultPackMaterialFeeDto.setIs_confirmed(1);
        //分割多条件查询
        feeResultExpressFeeDto.setOwner_noList(search(owner_no));
        feeResultExpressFeeDto.setMonth_time(month_time);
        feeResultStorageFeeDto.setOwner_noList(search(owner_no));
        feeResultStorageFeeDto.setMonth_time(month_time);
        feeResultDeliveryFeeDto.setOwner_noList(search(owner_no));
        feeResultDeliveryFeeDto.setMonth_time(month_time);
        feeResultReceivingFeeDto.setOwner_noList(search(owner_no));
        feeResultReceivingFeeDto.setMonth_time(month_time);
        feeResultSpecialFeeDto.setOwner_noList(search(owner_no));
        feeResultSpecialFeeDto.setMonth_time(month_time);
        feeResultPackMaterialFeeDto.setOwner_noList(search(owner_no));
        feeResultPackMaterialFeeDto.setMonth_time(month_time);
        //获取快递总费用
        List<FeeResultExpressFeeModel> expressfeelist = iFeeService.getexpressfee(feeResultExpressFeeDto);
        //把费用类型加到list中
        for (FeeResultExpressFeeModel feeResultExpressFeeModel : expressfeelist) {
            feeResultExpressFeeModel.setStatus(0);
            feeResultExpressFeeModel.setName("快递费用");
        }
        //获取仓储总费用
        List<FeeResultStorageFeeModel> feeResultStorageFeeModelList = iFeeService.getstoragefee(feeResultStorageFeeDto);
        for (FeeResultStorageFeeModel feeResultStorageFeeModel : feeResultStorageFeeModelList) {
            feeResultStorageFeeModel.setStatus(0);
            feeResultStorageFeeModel.setName("仓储费用");
        }
        //获取出库总费用
        List<FeeResultDeliveryFeeModel> feeResultDeliveryFeeModelList = iFeeService.getdeliveryfee(feeResultDeliveryFeeDto);
        for (FeeResultDeliveryFeeModel feeResultDeliveryFeeModel : feeResultDeliveryFeeModelList) {
            feeResultDeliveryFeeModel.setStatus(0);
            feeResultDeliveryFeeModel.setName("出库费用");
        }
        //获取入库总费用
        List<FeeResultReceivingFeeModel> feeResultReceivingFeeModelList = iFeeService.getreceivingfee(feeResultReceivingFeeDto);
        for (FeeResultReceivingFeeModel feeResultReceivingFeeModel : feeResultReceivingFeeModelList) {
            feeResultReceivingFeeModel.setStatus(0);
            feeResultReceivingFeeModel.setName("入库费用");
        }
        //获取包裹总费用
        List<FeeResultSpecialFeeModel> feeResultSpecialFeeModelList = iFeeService.getspeciatfee(feeResultSpecialFeeDto);
        for (FeeResultSpecialFeeModel feeResultSpecialFeeModel : feeResultSpecialFeeModelList) {
            feeResultSpecialFeeModel.setStatus(0);
            feeResultSpecialFeeModel.setName("特殊费用");
        }
        //获取包材总费用
        List<FeeResultPackMaterialFeeModel> feeResultPackMaterialFeeModelList = iFeeService.getMaterialfee(feeResultPackMaterialFeeDto);
        for (FeeResultPackMaterialFeeModel feeResultPackMaterialFeeModel : feeResultPackMaterialFeeModelList) {
            feeResultPackMaterialFeeModel.setStatus(0);
            feeResultPackMaterialFeeModel.setName("包材费用");
        }
        List<SumFeeModel> list = new ArrayList<SumFeeModel>();
        if (month_time == "") {
            //把6个list合在一个list里面
            list = ListUtil.mergeLists(new List[]{expressfeelist, feeResultStorageFeeModelList, feeResultDeliveryFeeModelList, feeResultReceivingFeeModelList, feeResultSpecialFeeModelList, feeResultPackMaterialFeeModelList});
            map.put("list", list);
            map.put("successed", true);
            return map;
        } else {
            //替代字符串为(yyyymm)
            String month_time1 = month_time.replace("-", "");
            //获取年份
            String year = month_time1.substring(0, 4);
            //获取月份
            String month = month_time1.substring(4, 6);
            //我要获取当前的日期
            Date date = new Date();
            //设置要获取到什么样的时间
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
            //获取String类型的时间
            String createdate = sdf.format(date);
            Calendar a = Calendar.getInstance();
            a.set(Calendar.YEAR, Integer.parseInt(year));
            a.set(Calendar.MONTH, Integer.parseInt(month) - 1);
            a.set(Calendar.DATE, 1);
            a.roll(Calendar.DATE, -1);
            //获取当前月天数
            int getDaysByYearMonth = a.get(Calendar.DATE);
            SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
            //获取指定年月第一天 
            Calendar calstar = Calendar.getInstance();
            calstar.set(Calendar.YEAR, Integer.parseInt(year));
            calstar.set(Calendar.MONTH, Integer.parseInt(month) - 1);
            calstar.set(Calendar.DAY_OF_MONTH, 1);
            String starDay = sm.format(calstar.getTime());
            //获取指定年月最后一天
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, Integer.parseInt(year));
            calendar.set(Calendar.MONTH, Integer.parseInt(month) - 1);
            int day = calendar.getActualMaximum(Calendar.DATE);
            calendar.set(Calendar.DAY_OF_MONTH, day);
            String lastDay = sm.format(calendar.getTime());
            if (Integer.parseInt(month_time1) < Integer.parseInt(createdate)) {
                //快递费用
                double expressfee = 0.0;
                //遍历list集合
                for (int i = 0; i < expressfeelist.size(); i++) {
                    //取出每一条记录
                    FeeResultExpressFeeModel feeResultExpressFeeModel = expressfeelist.get(i);
                    //取出该条记录中的属性
                    expressfee = feeResultExpressFeeModel.getFee();
                }
                //仓储总费用
                double storageFee = 0.0;
                //最终仓储总费用(折扣或者正常费用)
                double dctstoragefee = 0.0;
                //月度储位托盘费用
                double strtrayfee = 0.0;
                //遍历list集合
                for (int i = 0; i < feeResultStorageFeeModelList.size(); i++) {
                    //取出每一条记录
                    FeeResultStorageFeeModel feeResultStorageFeeModel = feeResultStorageFeeModelList.get(i);
                    //取出该条记录中的属性
                    storageFee = feeResultStorageFeeModel.getFee();
                }
                //判断货主存在类型为“仓储优惠-库存周转率”
                FeeResultOwnerMonthlyVo feeResultOwnerMonthlyVo1 = iFeeService.getRateitem(owner_no);
                //判断货主存在类型为“仓储优惠-日均托盘量”
                FeeResultOwnerMonthlyVo feeResultOwnerMonthlyVo2 = iFeeService.getCodeitem(owner_no);
                //判断货主存在类型为“仓储优惠-库存周转率”
                if (feeResultOwnerMonthlyVo1.getRate() > 0) {
                    //库存周转率
                    int result = 0;
                    //获取当前月订单的出库总件数之和
                    FeeResultOwnerMonthlyVo feeResultOwnerMonthlyVo3 = iFeeService.getOwnerdelivery(owner_no, month_time);
                    //获取指定月储位托盘数量 丶库存总量
                    SnapshotStkTotalInfoVo snapshotStkTotalInfoVo = iFeeService.getTotal(owner_no, starDay, lastDay);
                    //当月订单的出库总件数之和除以当月每日库存总量之和，结果取整
                    result = feeResultOwnerMonthlyVo3.getCount() / snapshotStkTotalInfoVo.getQty();
                    //获取仓储优惠-库存周转率的对应的值
                    FeeRuleSpecialVo feeRuleSpecialVo = iFeeService.getTurnover(owner_no);
                    //周转率1的值
                    String Factor1_value = feeRuleSpecialVo.getFactor1_value();
                    if (result == Integer.parseInt(Factor1_value)) {
                        //仓储总费用乘以折扣
                        dctstoragefee = storageFee * Double.parseDouble(feeRuleSpecialVo.getFactor2_value());
                    } else if (result == Integer.parseInt(feeRuleSpecialVo.getFactor3_value())) {
                        //仓储总费用乘以折扣
                        dctstoragefee = storageFee * Double.parseDouble(feeRuleSpecialVo.getFactor4_value());
                    } else if (result == Integer.parseInt(feeRuleSpecialVo.getFactor5_value())) {
                        //仓储总费用乘以折扣
                        dctstoragefee = storageFee * Double.parseDouble(feeRuleSpecialVo.getFactor6_value());
                    } else {
                        dctstoragefee = storageFee;
                    }
                }
                //判断货主存在类型为“仓储优惠-日均托盘量”
                else if (feeResultOwnerMonthlyVo2.getCode() > 0) {
                    //日均托盘量
                    int rult = 0;
                    //获取指定月储位托盘数量 丶库存总量
                    SnapshotStkTotalInfoVo snapshotStkTotalInfoVo = iFeeService.getTotal(owner_no, starDay, lastDay);
                    //当月每日储位托盘数量之和除以当月天数，结果取整
                    rult = snapshotStkTotalInfoVo.getLoc() / getDaysByYearMonth;
                    //获取储位托盘仓储费托盘量和对应的值
                    FeeRuleSpecialVo feeRuleSpecialVo = iFeeService.getPlt(owner_no);
                    //获取规则中按存储托盘方式计费的价格
                    FeeRuleStorageVo feeRuleStorageVo = iFeeService.getTray(owner_no);
                    if (rult == Integer.parseInt(feeRuleSpecialVo.getFactor1_value())) {
                        //月度储位托盘费用（托盘数量*规则托盘价格）
                        strtrayfee = snapshotStkTotalInfoVo.getLoc() * Double.parseDouble(feeRuleStorageVo.getFactor2_value());
                        //优惠过后实际仓储费（仓储费-月度储位托盘费用）+（月度储位托盘费用*优惠的折扣）
                        dctstoragefee = (storageFee - strtrayfee) + (strtrayfee * Double.parseDouble(feeRuleSpecialVo.getFactor2_value()));
                    } else if (rult == Integer.parseInt(feeRuleSpecialVo.getFactor3_value())) {
                        //月度储位托盘费用（托盘数量*规则托盘价格）
                        strtrayfee = snapshotStkTotalInfoVo.getLoc() * Integer.parseInt(feeRuleStorageVo.getFactor2_value());
                        //优惠过后实际仓储费（仓储费-月度储位托盘费用）+（月度储位托盘费用*优惠的折扣）
                        dctstoragefee = (storageFee - strtrayfee) + (strtrayfee * Double.parseDouble(feeRuleSpecialVo.getFactor4_value()));
                    } else if (rult == Integer.parseInt(feeRuleSpecialVo.getFactor5_value())) {
                        //月度储位托盘费用（托盘数量*规则托盘价格）
                        strtrayfee = snapshotStkTotalInfoVo.getLoc() * Integer.parseInt(feeRuleStorageVo.getFactor2_value());
                        //优惠过后实际仓储费（仓储费-月度储位托盘费用）+（月度储位托盘费用*优惠的折扣）
                        dctstoragefee = (storageFee - strtrayfee) + (strtrayfee * Double.parseDouble(feeRuleSpecialVo.getFactor6_value()));
                    } else {
                        dctstoragefee = storageFee;
                    }
                } else {
                    dctstoragefee = storageFee;
                }

                //出库费用
                double deliveryfee = 0.0;
                //遍历list集合
                for (int i = 0; i < feeResultDeliveryFeeModelList.size(); i++) {
                    //取出每一条记录
                    FeeResultDeliveryFeeModel feeResultDeliveryFeeModel = feeResultDeliveryFeeModelList.get(i);
                    //取出该条记录中的属性
                    deliveryfee = feeResultDeliveryFeeModel.getFee();
                }
                //判断是否存在保底费类型
                FeeRuleSpecialVo feeRuleSpecialVo1 = iFeeService.getSafetyitem(owner_no);
                if (feeRuleSpecialVo1 != null) {
                    //最低仓储费的值跟优惠或者正常计算的仓储费比较取大
                    if (Double.parseDouble(feeRuleSpecialVo1.getFactor1_value()) > dctstoragefee) {
                        dctstoragefee = Double.parseDouble(feeRuleSpecialVo1.getFactor1_value());
                    } else {
                        dctstoragefee = dctstoragefee;
                    }
                    //最低出库操作费的值跟出库费用比较取大
                    if (Double.parseDouble(feeRuleSpecialVo1.getFactor2_value()) > deliveryfee) {
                        deliveryfee = Double.parseDouble(feeRuleSpecialVo1.getFactor2_value());
                    } else {
                        deliveryfee = deliveryfee;
                    }
                } else {
                    dctstoragefee = dctstoragefee;
                    deliveryfee = deliveryfee;
                }
                //入库费用
                double receivingfee = 0.0;
                //遍历list集合
                for (int i = 0; i < feeResultReceivingFeeModelList.size(); i++) {
                    //取出每一条记录
                    FeeResultReceivingFeeModel feeResultReceivingFeeModel = feeResultReceivingFeeModelList.get(i);
                    //取出该条记录中的属性
                    receivingfee = feeResultReceivingFeeModel.getFee();
                }
                //特殊费用
                double specialfee = 0.0;
                //遍历list集合
                for (int i = 0; i < feeResultSpecialFeeModelList.size(); i++) {
                    //取出每一条记录
                    FeeResultSpecialFeeModel feeResultSpecialFeeModel = feeResultSpecialFeeModelList.get(i);
                    //取出该条记录中的属性
                    specialfee = feeResultSpecialFeeModel.getFee();
                }
                //包材费用
                double packmaterialfee = 0.0;
                for (int i = 0; i < feeResultPackMaterialFeeModelList.size(); i++) {
                    //取出每一条记录
                    FeeResultPackMaterialFeeModel feeResultPackMaterialFeeModel = feeResultPackMaterialFeeModelList.get(i);
                    //取出该条记录中的属性
                    packmaterialfee = feeResultPackMaterialFeeModel.getFee();
                }
                //判断当前本月是否产生费用
                if (expressfee >= 0 && dctstoragefee >= 0 && deliveryfee >= 0 && receivingfee >= 0 && specialfee >= 0 && packmaterialfee >= 0) {
                    FeeResultOwnerMonthlyVo feeResultOwnerMonthlyVo = iFeeService.getRecord(owner_no, year, month);
                    if (feeResultOwnerMonthlyVo.getRecord() > 0) {
                        sumFeeDto.setOwner_no(owner_no);
                        sumFeeDto.setYear(year);
                        sumFeeDto.setMonth(month);
                        sumFeeDto.setIs_deleted(1);
                        List<SumFeeModel> sumFeeModelList = iFeeService.getOwnerMonth(sumFeeDto);
                        list = sumFeeModelList;
                        map.put("list", list);
                        map.put("successed", true);
                        return map;
                    } else {
                        FeeResultExpressSkuVo feeResultExpressSkuVo = iFeeService.getCount(owner_no, month_time);
                        if ((feeResultExpressSkuVo.getRemark()).equals("0")) {
                            iFeeService.save(owner_no,expressfee,year,month,dctstoragefee,deliveryfee,specialfee,receivingfee,packmaterialfee);
                            sumFeeDto.setOwner_no(owner_no);
                            sumFeeDto.setYear(year);
                            sumFeeDto.setMonth(month);
                            sumFeeDto.setIs_deleted(1);
                            List<SumFeeModel> sumFeeModelList = iFeeService.getOwnerMonth(sumFeeDto);


							// 发送数据到FSCC做初审
							HttpUtil.doJsonPost(URL_ACCOUNTING_TO_FSCC, null, genAccountInfoJson(sumFeeModelList, true));

                            list = sumFeeModelList;
                            map.put("list", list);
                            map.put("successed", true);
                            return map;
                        } else {
                            msg = "有计算异常的快递费未处理";
                            //把6个list合在一个list里面
                            list = ListUtil.mergeLists(new List[]{expressfeelist, feeResultStorageFeeModelList, feeResultDeliveryFeeModelList, feeResultReceivingFeeModelList, feeResultSpecialFeeModelList, feeResultPackMaterialFeeModelList});
                            map.put("list", list);
                            map.put("msg", msg);
                            map.put("successed", false);
                            return map;
                        }
                    }
                }
            } else if (Integer.parseInt(month_time1) == Integer.parseInt(createdate)) {
                //把6个list合在一个list里面
                list = ListUtil.mergeLists(new List[]{expressfeelist, feeResultStorageFeeModelList, feeResultDeliveryFeeModelList, feeResultReceivingFeeModelList, feeResultSpecialFeeModelList, feeResultPackMaterialFeeModelList});
                map.put("list", list);
                map.put("successed", true);
                return map;
            }
        }

        map.put("list", list);
        map.put("successed", true);
        return map;

    }

    private String genAccountInfoJson(List<SumFeeModel> sumFeeModelList, boolean isTempEstimate) {
        SumFeeModel firstSumFeeModel = sumFeeModelList.get(0);
        String owner_no = firstSumFeeModel.getOwner_no();
        String srcMonthStr = firstSumFeeModel.getMonth();
        String month_str = srcMonthStr.length() == 1 ? "0" + srcMonthStr : srcMonthStr;
        String year_month_str = firstSumFeeModel.getYear() + month_str;
        String settleNo = owner_no + "^" + year_month_str;
        BigDecimal moneyTotal = BigDecimal.ZERO;
        for (SumFeeModel sumFeeModel : sumFeeModelList) {
            moneyTotal.add(BigDecimal.valueOf(sumFeeModel.getFee()));
        }

        AccountingDTO returnDto = new AccountingDTO();

        ReceivableHeaderDTO headerDTO = new ReceivableHeaderDTO();
        headerDTO.setSourceSystemId("WMS");
        headerDTO.setSourceSystemName("WMS");
        headerDTO.setUserCode("wms_admin");
        headerDTO.setUserName("wms_admin");
        String nowTimeStr = new Timestamp(System.currentTimeMillis()).toString().substring(0, 19);
        headerDTO.setLastUpdateDate(nowTimeStr);
        headerDTO.setCompanyCode(owner_no);
        headerDTO.setCompanyName(owner_no);
        headerDTO.setSettleNo(settleNo);
        headerDTO.setSettleMoney(moneyTotal.setScale(2, BigDecimal.ROUND_HALF_UP).toString());
        headerDTO.setOperationTypeCode("6001020200");//仓储服务信息费
        headerDTO.setSettleDateStart(year_month_str + "01");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, Integer.valueOf(firstSumFeeModel.getYear()));
        cal.set(Calendar.MONTH, Integer.valueOf(firstSumFeeModel.getMonth()));
        headerDTO.setSettleDateEnd(year_month_str + cal.getActualMaximum(Calendar.DATE));
        headerDTO.setTempEstimateStatus("1");//是否暂估，0是，1否
        if (isTempEstimate) {
            headerDTO.setTempEstimateStatus("0");
        }
        returnDto.setReceivableHeaderDTO(headerDTO);

        List<ReceivableLineDTO> lineList = new ArrayList<ReceivableLineDTO>();
        for (SumFeeModel sumFeeModel : sumFeeModelList) {
            ReceivableLineDTO lineDTO = new ReceivableLineDTO();
            lineDTO.setCostAccount(BigDecimal.valueOf(sumFeeModel.getFee()).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
            if (Type_Fee.EXPRESS.equals(sumFeeModel.getFee_type())) {
                lineDTO.setTaxableGoodsCode("304040799");
                lineDTO.setTaxableGoodsName("其他现代服务");
            } else if (Type_Fee.STORAGE.equals(sumFeeModel.getFee_type())) {
                lineDTO.setTaxableGoodsCode("30499");
                lineDTO.setTaxableGoodsName("其他仓储服务");
            } else {
                lineDTO.setTaxableGoodsCode("3040408");
                lineDTO.setTaxableGoodsName("装卸搬运服务");
            }
            lineList.add(lineDTO);
        }
        returnDto.setReceivableLineList(lineList);

        return JSONObject.toJSONString(Arrays.asList(returnDto));
    }

    private List<String> search(String searchCondition) {
        List<String> list = new ArrayList<>();
        if (searchCondition == "" && "".equals(searchCondition)) {
            list = null;
        } else {
            String[] searchArray = searchCondition.split(",");
            for (String searchResult : searchArray) {
                list.add(searchResult);
            }
        }
        return list;
    }

    //通过更新(月份结果表的审核的状态)
    @RequestMapping(value = "update")
    @ResponseBody
    public String update(@ModelAttribute FeeResultOwnerMonthlyDto feeResultOwnerMonthlyDto, HttpServletRequest request) throws Exception {
        String msg = "";
        //更新条件
        String owner_no = request.getParameter("owner_no");
        //按月份
        String month_time = request.getParameter("month_time");
        //运营审核状态
        String surestatus = request.getParameter("surestatus");
        //去除
        String month_time1 = month_time.replace("-", "");
        //获取年份
        String year = month_time1.substring(0, 4);
        //获取月份
        String month = month_time1.substring(4, 6);
        feeResultOwnerMonthlyDto.setOwner_no(owner_no);
        feeResultOwnerMonthlyDto.setMonth_str(month);
        feeResultOwnerMonthlyDto.setYear_str(year);
        feeResultOwnerMonthlyDto.setIs_deleted(1);
        //获取当前的日期
        Date date = new Date();
        //设置要获取到什么样的时间
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
        //获取String类型的时间
        String createdate = sdf.format(date);

        //比较月份大小
        if (Integer.parseInt(month_time1) < Integer.parseInt(createdate)) {
            FeeResultExpressSkuVo feeResultExpressSkuVo = iFeeService.getCount(owner_no, month_time);
            if ((feeResultExpressSkuVo.getRemark()).equals("0")) {
                if (surestatus.equals("0")) {
                    List<FeeResultOwnerMonthlyModel> feeResultOwnerMonthlyModelList = iFeeService.getOwnerMonthly(owner_no, year, month);
                    int status = -1;
                    for (int i = 0; i < feeResultOwnerMonthlyModelList.size(); i++) {
                        //取出每一条记录
                        FeeResultOwnerMonthlyModel feeResultOwnerMonthlyModel = feeResultOwnerMonthlyModelList.get(i);
                        //取出该条记录中的属性
                        status = feeResultOwnerMonthlyModel.getStatus();
                    }
                    if (status == 0) {
                        int result= iFeeService.operationAuditModify(owner_no,year,month);
                        if (result>0){
                            msg="审核通过";
                        }else {
                            msg="审核失败请重试";
                        }
                    } else {
                        msg = "状态不对，请刷新页面重试";
                    }

                } else if (surestatus.equals("1")) {
                    List<FeeResultOwnerMonthlyModel> feeResultOwnerMonthlyModelList = iFeeService.getOwnerMonthly(owner_no, year, month);
                    int status = -1;
                    for (int i = 0; i < feeResultOwnerMonthlyModelList.size(); i++) {
                        //取出每一条记录
                        FeeResultOwnerMonthlyModel feeResultOwnerMonthlyModel = feeResultOwnerMonthlyModelList.get(i);
                        //取出该条记录中的属性
                        status = feeResultOwnerMonthlyModel.getStatus();
                    }
                    if (status == 1) {
                        int result= iFeeService.financeCommissionerModify(owner_no,year,month);
                        if (result>0){
                            msg="审核通过";
                        }else {
                            msg="审核失败请重试";
                        }
                    } else {
                        msg = "状态不对，请刷新页面重试";
                    }
                } else if (surestatus.equals("2")) {
                    List<FeeResultOwnerMonthlyModel> feeResultOwnerMonthlyModelList = iFeeService.getOwnerMonthly(owner_no, year, month);
                    int status = -1;
                    for (int i = 0; i < feeResultOwnerMonthlyModelList.size(); i++) {
                        //取出每一条记录
                        FeeResultOwnerMonthlyModel feeResultOwnerMonthlyModel = feeResultOwnerMonthlyModelList.get(i);
                        //取出该条记录中的属性
                        status = feeResultOwnerMonthlyModel.getStatus();
                    }
                    if (status == 2) {
                        int result= iFeeService.financialManagerModify(owner_no,year,month);
                        if (result>0){
                            msg="审核通过";
                        }else {
                            msg="审核失败请重试";
                        }
                    } else {
                        msg = "状态不对，请刷新页面重试";
                    }
                }
            } else {
                msg = "有计算异常的快递费未处理";
            }

        } else if (Integer.parseInt(month_time1) > Integer.parseInt(createdate)) {
            msg = "该月未产生任何费用，不能审核！";
        } else {
            msg = "当前月未结束，不能审核本月费用！";
        }
        return msg;
    }

    //驳回更新(月份结果表的审核的状态)
    @RequestMapping(value = " replace")
    @ResponseBody
    public String replace(@ModelAttribute FeeResultOwnerMonthlyDto feeResultOwnerMonthlyDto, HttpServletRequest request) throws Exception {
        String msg = "";
        //更新条件
        String owner_no = request.getParameter("owner_no");
        //按月份
        String month_time = request.getParameter("month_time");
        //审核状态
        String rejectstatus = request.getParameter("rejectstatus");
        //替代字符串为(yyyymm)
        String month_time1 = month_time.replace("-", "");
        //获取年份
        String year = month_time1.substring(0, 4);
        //获取月份
        String month = month_time1.substring(4, 6);

        //获取当前的日期
        Date date = new Date();
        //设置要获取到什么样的时间
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
        //获取String类型的时间
        String createdate = sdf.format(date);

        //比较月份大小
        if (Integer.parseInt(month_time1) < Integer.parseInt(createdate)) {

            if (rejectstatus.equals("0")) {
                List<FeeResultOwnerMonthlyModel> feeResultOwnerMonthlyModelList = iFeeService.getOwnerMonthly(owner_no, year, month);
                int status = -1;
                for (int i = 0; i < feeResultOwnerMonthlyModelList.size(); i++) {
                    //取出每一条记录
                    FeeResultOwnerMonthlyModel feeResultOwnerMonthlyModel = feeResultOwnerMonthlyModelList.get(i);
                    //取出该条记录中的属性
                    status = feeResultOwnerMonthlyModel.getStatus();
                }
                if (status == 0) {
                    int result=iFeeService.operationAuditReject(owner_no,year,month);
                    if (result>=0){
                        msg="驳回成功";
                    }else {
                        msg="驳回失败请重试";
                    }
                } else {
                    msg = "状态不对，请刷新页面重试";
                }

            } else if (rejectstatus.equals("1")) {
                List<FeeResultOwnerMonthlyModel> feeResultOwnerMonthlyModelList = iFeeService.getOwnerMonthly(owner_no, year, month);
                int status = -1;
                for (int i = 0; i < feeResultOwnerMonthlyModelList.size(); i++) {
                    //取出每一条记录
                    FeeResultOwnerMonthlyModel feeResultOwnerMonthlyModel = feeResultOwnerMonthlyModelList.get(i);
                    //取出该条记录中的属性
                    status = feeResultOwnerMonthlyModel.getStatus();
                }
                if (status == 1) {
                    int result=iFeeService.financeCommissionerReject(owner_no,year,month);
                    if (result>=0){
                        msg="驳回成功";
                    }else {
                        msg="驳回失败请重试";
                    }
                } else {
                    msg = "状态不对，请刷新页面重试";
                }
            } else if (rejectstatus.equals("2")) {
                List<FeeResultOwnerMonthlyModel> feeResultOwnerMonthlyModelList = iFeeService.getOwnerMonthly(owner_no, year, month);
                int status = -1;
                for (int i = 0; i < feeResultOwnerMonthlyModelList.size(); i++) {
                    //取出每一条记录
                    FeeResultOwnerMonthlyModel feeResultOwnerMonthlyModel = feeResultOwnerMonthlyModelList.get(i);
                    //取出该条记录中的属性
                    status = feeResultOwnerMonthlyModel.getStatus();
                }
                if (status == 2) {
                    int result=iFeeService.financialManagerReject(owner_no,year,month);
                    if (result>=0){
                        msg="审核通过";
                    }else {
                        msg="审核失败请重试";
                    }
                } else {
                    msg = "状态不对，请刷新页面重试";
                }

            }

        } else if (Integer.parseInt(month_time1) > Integer.parseInt(createdate)) {
            msg = "该月未产生任何费用，不能审核！";
        } else {
            msg = "当前月未结束，不能审核本月费用！";
        }
        return msg;
    }
}