package com.bms.common.table.vo;

import java.util.ArrayList;

import com.base.common.core.base.DTO;
import com.base.common.core.enums.Constant.DbTypes;
import com.base.common.core.pager.model.PagerModel;
import com.base.common.core.pager.service.IPagerService;
import com.base.common.exception.AlikAssert;

public class AmazuePagerUtil {
	
	public static <T> DataTablesVO<T> getPagerModel(IPagerService<T> iPagerService, DTO dto) throws Exception {
		AlikAssert.isNotNull(dto, "the parameter DTO is can not be null !!!!");
		PagerModel pagerModel = new PagerModel();
		DataTablesVO<T> dataTablesDto = new DataTablesVO<T>();
		if (dto.getPage() == null) {
			dto.setPage(Integer.valueOf(1));
		}

		if (dto.getRows() == null || dto.getRows().intValue() > 100) {
			dto.setRows(Integer.valueOf(20));
		}

		dto.setDbtype("oracle");
		pagerModel.setPageSize(dto.getRows());
		pagerModel.setCurrentPage(dto.getPage());
		pagerModel.setTotal(Integer.valueOf(iPagerService.queryCount(dto)));
		pagerModel.setStartIndex(Integer
				.valueOf((pagerModel.getCurrentPage().intValue() - 1) * pagerModel.getPageSize().intValue() + 1));
		pagerModel.setEndIndex(
				Integer.valueOf(pagerModel.getStartIndex().intValue() + pagerModel.getPageSize().intValue() - 1));
		pagerModel.setPageCount(
				Integer.valueOf(pagerModel.getTotal().intValue() % pagerModel.getPageSize().intValue() == 0
						? pagerModel.getTotal().intValue() / pagerModel.getPageSize().intValue()
						: pagerModel.getTotal().intValue() / pagerModel.getPageSize().intValue() + 1));
		if (pagerModel.getCurrentPage().intValue() >= pagerModel.getPageCount().intValue()) {
			pagerModel.setCurrentPage(pagerModel.getPageCount());
			pagerModel.setNextPage(pagerModel.getPageCount());
		} else {
			pagerModel.setNextPage(Integer.valueOf(pagerModel.getCurrentPage().intValue() + 1));
		}

		if (pagerModel.getPriviousPage() != null && pagerModel.getPriviousPage().intValue() > 1) {
			pagerModel.setPriviousPage(Integer.valueOf(pagerModel.getCurrentPage().intValue() - 1));
		} else {
			pagerModel.setPriviousPage(Integer.valueOf(1));
		}

		if (DbTypes.isMysql(dto.getDbtype())) {
			if (pagerModel.getStartIndex().intValue() <= 0) {
				dto.setRowstartindex(Integer.valueOf(0));
				dto.setRowendindex(Integer.valueOf(0));
			} else {
				dto.setRowstartindex(Integer.valueOf(pagerModel.getStartIndex().intValue() - 1));
				dto.setRowendindex(pagerModel.getPageSize());
			}
		} else {
			dto.setRowstartindex(pagerModel.getStartIndex());
			dto.setRowendindex(pagerModel.getEndIndex());
		}

		if (pagerModel.getTotal().intValue() >= 1) {
			pagerModel.setRows(iPagerService.queryList(dto));
		} else {
			pagerModel.setRows(new ArrayList());
		}
		
		dataTablesDto.setiTotalDisplayRecords(pagerModel.getTotal());
		dataTablesDto.setiTotalRecords(pagerModel.getTotal());
		dataTablesDto.setRowsData(pagerModel.getRows());

		return dataTablesDto;
	}
}
