package com.bms.common.table.vo;

import com.base.common.core.base.DTO;

public class BmsBaseDto extends DTO{
	
	private Integer start;
	
	private Integer length;

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
		setPage((start / length)+1);
	}

	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
		setRows(length);
	}
}
