package com.bms.common.out.bean;

import java.io.Serializable;

public class OutBaseBean implements Serializable{
	
	/**
	 * success|failure
	 */
	private String flag;
	
	/**
	 * 响应码
	 */
	private String code;
	
	/**
	 * 响应信息
	 */
	private String message;

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
