package com.bms.common.constant;

public class ConstClass {

	public class Status_Receipt {
		public static final int WAIT = -10;
		public static final int NOT_START = 0;// 未开始
		public static final int RECEIVING = 1;// 收货中
		public static final int RECEIVED = 2;// 收货完成
		public static final int ORDER_CLOSED = 4;
		public static final int ORDER_CANCEL = 5;
	}

	public class Status_Delivery {
		public static final int WAIT = -10;
		public static final int NOT_START = 0;
		public static final int ALLOCATED = 10;
		public static final int PICKING = 15;
		public static final int PICKED = 20;
		public static final int SORTING = 25;
		public static final int SORTED = 30;
		public static final int RECHECKING = 35;
		public static final int RECHECKED = 40;
		public static final int PACKAGING = 45;
		public static final int PACKED = 50;
		public static final int LOADING = 55;
		public static final int LOADED = 60;
		public static final int DELIVERED = 70;// 已出库
		public static final int RETURNING = 95;
		public static final int CANCELLED = 99;// 已取消
	}

	public class Type_Receipt {
		public static final int NORMAL = 0;// 采购入库
		public static final int CROSS_DOCK = 1;
		public static final int ALLOT = 2;
		public static final int SELL_RETURN = 3;
		public static final int FICTITIOUS = 4;
		public static final int PROCESS = 5;
		public static final int ACTIVE_RETURN = 6;
		public static final int OTHER = 9;
	}

	public class Type_Delivery {
		public static final int NORMAL = 0;// 销售（正常）出库
		public static final int ALLOT = 1;// 调拨出库
		public static final int RTV = 2;// 退供出库
		public static final int CROSS_DOCK = 3;// 越库出库
		public static final int FICTITIOUS = 4;// 虚拟出库
	}

	public class YesNo_Int {
		public static final int YES = 0;
		public static final int NO = 1;
	}

	public class YesNo_Str {
		public static final String YES = "0";
		public static final String NO = "1";
	}

	// 			1-快递费，2-仓储费，3-出库费，4-入库费，5-包材费，6-特殊费用
	public class Type_Fee {
		public static final String EXPRESS = "1";
		public static final String STORAGE = "2";
		public static final String OPER_DEV = "3";
		public static final String OPER_RCV = "4";
		public static final String PACK_MATERIAL = "5";
		public static final String SPECIAL = "6";
	}

}
