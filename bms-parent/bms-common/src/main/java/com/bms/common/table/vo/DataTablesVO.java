package com.bms.common.table.vo;

import java.util.List;
import java.util.Map;

public class DataTablesVO<T> {
	private List<T> rowsData; 
	private int iTotalDisplayRecords;
	private int iTotalRecords;
	private Map<String,Object> attach;

	public Map<String, Object> getAttach() {
		return attach;
	}

	public void setAttach(Map<String, Object> attach) {
		this.attach = attach;
	}

	public List<T> getRowsData() {
		return rowsData;
	}

	public void setRowsData(List<T> rowsData) {
		this.rowsData = rowsData;
	}

	public int getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(int iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}

	public int getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(int iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}
}
