package com.bms.service.profiles.service.bd;

import com.base.common.core.service.IBaseService;
import com.bms.common.table.vo.DataTablesVO;
import com.bms.service.dto.bd.BdCargoOwnerDto;
import com.bms.service.dto.bd.LogChangeOwnerBalanceDto;
import com.bms.service.model.bd.BdCargoOwnerModel;
import com.bms.service.model.bd.LogChangeOwnerBalanceModel;
import com.bms.service.vo.bd.BdCargoOwnerVo;

import java.util.List;

public interface ILogChangeOwnerBalanceService extends IBaseService<LogChangeOwnerBalanceModel> {
    DataTablesVO<LogChangeOwnerBalanceModel> getLogBalanceList(LogChangeOwnerBalanceDto logChangeOwnerBalanceDto) throws Exception;
}
