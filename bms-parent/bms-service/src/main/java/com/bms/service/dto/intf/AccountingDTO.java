package com.bms.service.dto.intf;

import java.io.Serializable;
import java.util.List;

public class AccountingDTO implements Serializable {

	private static final long serialVersionUID = -3219401546101079284L;

	private ReceivableHeaderDTO receivableHeaderDTO;

	private List<ReceivableLineDTO> receivableLineList;

	public ReceivableHeaderDTO getReceivableHeaderDTO() {
		return receivableHeaderDTO;
	}

	public void setReceivableHeaderDTO(ReceivableHeaderDTO receivableHeaderDTO) {
		this.receivableHeaderDTO = receivableHeaderDTO;
	}

	public List<ReceivableLineDTO> getReceivableLineList() {
		return receivableLineList;
	}

	public void setReceivableLineList(List<ReceivableLineDTO> receivableLineList) {
		this.receivableLineList = receivableLineList;
	}

}
