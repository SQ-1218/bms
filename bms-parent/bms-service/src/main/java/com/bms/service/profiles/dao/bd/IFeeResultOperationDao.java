package com.bms.service.profiles.dao.bd;

import com.base.common.core.dao.IBaseDao;
import com.bms.service.dto.bd.FeeResultOperationDto;
import com.bms.service.model.bd.FeeResultOperationModel;
import com.bms.service.model.bd.FeeRuleOperReceivingModel;
import com.bms.service.vo.bd.FeeResultStorageVo;
import com.bms.service.vo.bd.FeeRuleOperDeliveryItemVo;
import com.bms.service.vo.bd.FeeTaskReceiveVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface IFeeResultOperationDao extends IBaseDao<FeeResultOperationModel> {
    //获取当前订单编号的所有商品
    FeeResultStorageVo getCalWay(@Param("doc_no")String doc_no);
    //获取所有计费项目名称
    List<FeeRuleOperDeliveryItemVo> getOperationItem();
    //导出获取list(不分页)
    List<FeeResultOperationModel> getExcelModelList(FeeResultOperationDto feeResultOperationDto);
    //获取当前货主的所有订单
    List<String> doc_noList(@Param("owner_no")String owner_no,@Param("doc_type")String doc_type);
    //获取feetask任务表里面的数据
    List<FeeTaskReceiveVo> getFeeTaskReceive();
    //根据当前的货主和计算方式去规则表里去读取单价
    FeeRuleOperReceivingModel getFeeByTask(FeeTaskReceiveVo feeTaskReceiveVo);
    //获取件数的数量
    Double getQty(String owner_no,String fee_category);



}
