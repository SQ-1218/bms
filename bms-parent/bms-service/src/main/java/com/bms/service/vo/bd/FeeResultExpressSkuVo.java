package com.bms.service.vo.bd;

import com.base.common.core.base.VO;

/**
 * Created by jsc on 2018/11/1.
 */
public class FeeResultExpressSkuVo extends VO {
    private static final long serialVersionUID = 1L;
    //商品体积
    private Double skuvolume;
    //计抛系数
    private int hollow_vol;
    //备注
    private String remark;

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public int getHollow_vol() {
        return hollow_vol;
    }

    public void setHollow_vol(int hollow_vol) {
        this.hollow_vol = hollow_vol;
    }

    public Double getSkuvolume() {
        return skuvolume;
    }

    public void setSkuvolume(Double skuvolume) {
        this.skuvolume = skuvolume;
    }
}
