package com.bms.service.profiles.impl.bd;

import com.base.common.core.base.DTO;
import com.base.common.core.dao.IBaseDao;
import com.base.common.core.pager.service.IPagerService;
import com.base.common.core.service.impl.BaseServiceImpl;
import com.bms.common.table.vo.AmazuePagerUtil;
import com.bms.common.table.vo.DataTablesVO;
import com.bms.common.table.vo.ExcelBean;
import com.bms.common.util.ExcelUtil;
import com.bms.service.dto.bd.FeeResultStorageDto;
import com.bms.service.dto.bd.FeeRuleStorageDto;
import com.bms.service.model.bd.FeeResultStorageModel;
import com.bms.service.model.bd.FeeRuleStorageModel;
import com.bms.service.profiles.dao.bd.IFeeResultStorageDao;
import com.bms.service.profiles.dao.bd.IFeeRuleSpecialDao;
import com.bms.service.profiles.dao.bd.IFeeRuleStorageDao;
import com.bms.service.profiles.service.bd.IFeeResultStorageService;
import com.bms.service.vo.bd.FeeResultStorageVo;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.sql.Timestamp;
import java.util.*;

@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class FeeResultStorageServiceImpl extends BaseServiceImpl<FeeResultStorageModel> implements IFeeResultStorageService {
    @Resource
    private IFeeResultStorageDao feeResultStorageDao;
    @Resource
    private IFeeRuleStorageDao feeRuleStorageDao;
    @Resource
    private IFeeRuleSpecialDao feeRuleSpecialDao;
    @Override
    protected IBaseDao<FeeResultStorageModel> getBaseDao() {
        return feeResultStorageDao;
    }

    @Override
    public List<FeeResultStorageModel> getFee() throws Exception {
        //获取货主的规则
        FeeRuleStorageDto feeRuleStorageDto=new FeeRuleStorageDto();
        feeRuleStorageDto.setIs_deleted(1.0);
        List<FeeRuleStorageModel> list=feeRuleStorageDao.getFeeRuleStorageModelList(feeRuleStorageDto);
        FeeResultStorageModel feeResultStorageModel=new FeeResultStorageModel();
        for(FeeRuleStorageModel feeRuleStorageModel:list){
            //通过当前计费规章里面的货主获取所有订单
                double result=1.0;
                double tray1=0.0;
                double tray2=0.0;
                double fee=1.0;
                double tray=0.0;
            String calWay=feeRuleStorageModel.getCal_way();
                FeeResultStorageVo feeResultStorageVo=feeResultStorageDao.getCalWay(feeRuleStorageModel.getOwner_no());
                switch (calWay){
                    //托盘
                    case "1" :
                         double tray1Fee=1.0;
                         double tray2Fee=1.0;
                        //按零捡
                        tray1=feeResultStorageDao.getTray(feeRuleStorageModel.getOwner_no(),"0");
                        //按存储
                        tray2=feeResultStorageDao.getTray(feeRuleStorageModel.getOwner_no(),"1");
                       //总托盘数
                        tray=tray1+tray2;
                        //系数一是零捡
                        tray1Fee=tray1*Double.parseDouble(feeRuleStorageModel.getFactor1_value());
                        //系数二是存储
                        tray2Fee= tray2*Double.parseDouble(feeRuleStorageModel.getFactor2_value());
                        fee=tray1Fee+tray2Fee;
                        feeResultStorageModel.setFee(fee);
                        break;
                    case "2" :
                        //体积
                        result= feeResultStorageVo.getVolume()/Math.pow(1000,3);
                        fee = result * Double.parseDouble(feeRuleStorageModel.getFactor1_value());
                        feeResultStorageModel.setFee(fee);
                        break;
                    case "3" :
                        //重量
                        result = feeResultStorageVo.getWeight() / 1000;
                        fee = result * Double.parseDouble(feeRuleStorageModel.getFactor1_value());
                        feeResultStorageModel.setFee(fee);
                        break;
                    case "4" :
                        //件数
                         result = feeResultStorageDao.getQty(feeRuleStorageModel.getOwner_no(), feeRuleStorageModel.getFactor2_value());
                        //按照读取的bd_sku配合一起算费用根据当前系数2的值
                        fee = result* Double.parseDouble(feeRuleStorageModel.getFactor1_value());
                        feeResultStorageModel.setFee(fee);
                        break;
                    //按面积
                    case "5" :
                      //系数一乘以系数二(面积乘以单价)
                      result=Double.parseDouble(feeRuleStorageModel.getFactor1_value())*Double.parseDouble(feeRuleStorageModel.getFactor2_value());
                       feeResultStorageModel.setFee(result);
                        break;
                    default:
                        break;
                }

            //根据当前货主去找特殊计费规则
            //只针对托盘
            if("1".equals(calWay)){
            List<String> itemList= feeRuleSpecialDao.getItemList(feeRuleStorageModel.getOwner_no());
            //算完费用后若为仓储费 按照该优惠费用减费
            //循环当前货主的所有计费方式如果存在日均托盘量，库存周转率，每日单量就在当前的计费基础上进行优惠
            for(String item_code : itemList){
              if("-3".equals(item_code)){
                //3.每日单量
                //获取今天的正常出库的订单数量
                int getDailyMono=feeRuleSpecialDao.getDailyMono(feeRuleStorageModel.getOwner_no());
                //如果今天单量是总托盘的N2倍时则不收费用
                String factor2Value=feeRuleSpecialDao.factor2Value(feeRuleStorageModel.getOwner_no());
                if(tray*Double.parseDouble(factor2Value)<=getDailyMono){
                    feeResultStorageModel.setFee(0.0);
                }else{
                double beforeFee=feeResultStorageModel.getFee();
                //获取当前的优惠方式
                 String factor1Value=feeRuleSpecialDao.factor1Value(feeRuleStorageModel.getOwner_no());
                 fee=getDailyMono*Double.parseDouble(factor1Value);
                 if(beforeFee-fee>=0){
                 feeResultStorageModel.setFee(beforeFee-fee);
                 }else
                 {
                     feeResultStorageModel.setFee(0.0);
                 }
              }
              }
            }
            }
            //把费用放入FEERESULTSTORAGE里面
            Date date = new Date();
            Timestamp ts = new Timestamp(date.getTime());
            feeResultStorageModel.setOwner_no(feeRuleStorageModel.getOwner_no());
            feeResultStorageModel.setCal_item_rank(feeRuleStorageModel.getCal_item_rank());
            feeResultStorageModel.setCal_item_name(feeRuleStorageModel.getCal_item_name());
            feeResultStorageModel.setCal_item_code(feeRuleStorageModel.getCal_item_code());
            feeResultStorageModel.setCal_way(feeRuleStorageModel.getCal_way());
            feeResultStorageModel.setFee_occur_time(ts);
            feeResultStorageModel.setCreated_by("admin");
            feeResultStorageModel.setCreated_time(ts);
            //执行存入数据库
            feeResultStorageDao.insert(feeResultStorageModel);
        }
        return null;
    }

    @Override
    public DataTablesVO<FeeResultStorageModel> getFeeResultStorage(FeeResultStorageDto feeResultStorageDto) throws Exception {
        return AmazuePagerUtil.getPagerModel(new IPagerService<FeeResultStorageModel>() {
            @Override
            public int queryCount(DTO dto) throws Exception {
                return feeResultStorageDao.getModelListCount(dto);
            }
            @Override
            public List<FeeResultStorageModel> queryList(DTO dto) throws Exception {
                return feeResultStorageDao.getModelList(dto);
            }
        }, feeResultStorageDto);
    }

    @Override
    public XSSFWorkbook exportExcelInfo(FeeResultStorageDto feeResultStorageDto) throws Exception {
        List<FeeResultStorageModel> list=feeResultStorageDao.getExcelModelList(feeResultStorageDto);
        List<ExcelBean> excel=new ArrayList<>();
        Map<Integer,List<ExcelBean>> map=new LinkedHashMap<>();
        XSSFWorkbook xssfWorkbook=null;
        //设置标题栏
        excel.add(new ExcelBean("货主","owner_no",0));
        excel.add(new ExcelBean("计费项名称","cal_item_name",0));
        excel.add(new ExcelBean("计算方式","cal_way",0));
        excel.add(new ExcelBean("计费结果","fee",0));
        excel.add(new ExcelBean("费用产生时间","fee_occur_time",0));
        excel.add(new ExcelBean("是否已结算(0为结算,1为未结算)","is_confirmed",0));
        excel.add(new ExcelBean("确认结算时间","confirmed_time",0));
        map.put(0, excel);
        String sheetName = "仓储费用计费结果";
        //调用ExcelUtil的方法
        xssfWorkbook = ExcelUtil.createExcelFile(FeeResultStorageModel.class, list, map, sheetName);
        return xssfWorkbook;
    }


}
