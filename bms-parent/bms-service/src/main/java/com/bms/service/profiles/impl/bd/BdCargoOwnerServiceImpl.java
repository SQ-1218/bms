package com.bms.service.profiles.impl.bd;

import java.util.List;

import javax.annotation.Resource;

import com.bms.service.model.bd.LogChangeOwnerBalanceModel;
import com.bms.service.vo.bd.BdCargoOwnerVo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.base.common.core.base.DTO;
import com.base.common.core.dao.IBaseDao;
import com.base.common.core.pager.service.IPagerService;
import com.base.common.core.service.impl.BaseServiceImpl;
import com.bms.common.table.vo.AmazuePagerUtil;
import com.bms.common.table.vo.DataTablesVO;
import com.bms.service.dto.bd.BdCargoOwnerDto;
import com.bms.service.model.bd.BdCargoOwnerModel;
import com.bms.service.profiles.dao.bd.IBdCargoOwnerDao;
import com.bms.service.profiles.service.bd.IBdCargoOwnerService;

/**
 * <b>description</b>：货主信息表业务实现<br>
 * <b>time</b>：2017-08-08 16:24:44 <br>
 * <b>author</b>： zhaochen
 */
@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class BdCargoOwnerServiceImpl extends BaseServiceImpl<BdCargoOwnerModel> implements IBdCargoOwnerService {

    @Resource
    private IBdCargoOwnerDao ibdCargoOwnerDao;

    @Override
    public IBaseDao<BdCargoOwnerModel> getBaseDao() {
        return this.ibdCargoOwnerDao;
    }

    public DataTablesVO<BdCargoOwnerModel> getOwnerList(BdCargoOwnerDto bdCargoOwnerDto) throws Exception {
        return AmazuePagerUtil.getPagerModel(new IPagerService<BdCargoOwnerModel>() {

            @Override
            public int queryCount(DTO dto) throws Exception {
                return ibdCargoOwnerDao.getModelListCount(dto);
            }

            @Override
            public List<BdCargoOwnerModel> queryList(DTO dto) throws Exception {
                return ibdCargoOwnerDao.getModelList(dto);
            }
        }, bdCargoOwnerDto);
    }

    @Override
    public List<BdCargoOwnerVo> getOwner() throws Exception {
        return ibdCargoOwnerDao.getOwner();
    }

    @Override
    public List<BdCargoOwnerModel> getowner_noList(BdCargoOwnerDto bdCargoOwnerDto) throws Exception {
        return ibdCargoOwnerDao.getowner_noList(bdCargoOwnerDto);
    }

    @Override
    public Integer updateBalance(BdCargoOwnerModel bdCargoOwnerModel) throws Exception {
        return ibdCargoOwnerDao.updateBalance(bdCargoOwnerModel);
    }

    @Override
    public Integer insertLogBalance(LogChangeOwnerBalanceModel logChangeOwnerBalanceModel) throws Exception {
        return ibdCargoOwnerDao.insertLogBalance(logChangeOwnerBalanceModel);
    }
}