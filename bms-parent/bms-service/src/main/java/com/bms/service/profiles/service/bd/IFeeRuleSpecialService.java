package com.bms.service.profiles.service.bd;

import com.base.common.core.service.IBaseService;
import com.bms.common.table.vo.DataTablesVO;
import com.bms.service.dto.bd.FeeRuleSpecialDto;
import com.bms.service.model.bd.FeeRuleSpecialModel;
import com.bms.service.vo.bd.FeeRuleSpecialCalVo;
import com.bms.service.vo.bd.FeeRuleSpecialItemVo;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.List;

public interface IFeeRuleSpecialService extends IBaseService<FeeRuleSpecialModel> {
    DataTablesVO<FeeRuleSpecialModel> getFeeRuleSpecialList(FeeRuleSpecialDto feeRuleSpecialDto) throws Exception;
    List<FeeRuleSpecialItemVo> getFeeRuleSpecialItem()throws Exception;
    List<FeeRuleSpecialCalVo> getFeeRuleSpecialCal()throws Exception;
    int batchDeleteByIds(List<String> list)throws Exception;
    int getFeeRuleSpecialCount(String owner_no)throws Exception;
    //导入
    int importExcelInfo(InputStream in, MultipartFile file);
    //模板下载
    XSSFWorkbook downloadExcelInfo(FeeRuleSpecialDto feeRuleSpecialDto) throws Exception;
    //导出
    XSSFWorkbook exportExcelInfo(FeeRuleSpecialDto feeRuleSpecialDto) throws Exception;
    //获取List
}
