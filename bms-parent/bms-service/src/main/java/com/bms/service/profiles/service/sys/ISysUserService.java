package com.bms.service.profiles.service.sys;

import com.base.common.core.service.IBaseService;
import com.bms.service.dto.sys.SysUserDto;
import com.bms.service.model.sys.SysUserModel;
import com.bms.service.vo.sys.SysUserVo;


/**
 * <b>description</b>：BMS系统登录业务接口<br>
 * <b>time</b>：2018-09-17 11:08:11 <br>
 * <b>author</b>：  zhaochen
 */
public interface ISysUserService extends IBaseService<SysUserModel> {

	SysUserVo login(SysUserDto sysUserDto) throws Exception;

	
}
