package com.bms.service.profiles.service.bd;

import java.util.List;

import com.base.common.core.service.IBaseService;
import com.bms.common.table.vo.DataTablesVO;
import com.bms.service.dto.bd.LogVerifyFeeResultDto;
import com.bms.service.dto.intf.VerifyResultDto;
import com.bms.service.model.bd.LogVerifyFeeResultModel;

/**
 * Created by jsc on 2019/1/11.
 */
public interface ILogVerifyFeeResultService extends IBaseService<LogVerifyFeeResultModel> {

	// 将审核记录或者驳回记录插入到记录表
	int save(LogVerifyFeeResultModel logVerifyFeeResultModel) throws Exception;

	void verifySettlement(List<VerifyResultDto> dtoList) throws Exception;

	DataTablesVO<LogVerifyFeeResultModel> getLogVerifyFeeResultList(LogVerifyFeeResultDto logVerifyFeeResultDto) throws Exception;
}
