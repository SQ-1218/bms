package com.bms.service.model.bd;

import com.base.common.core.model.Model;

import java.sql.Timestamp;
import java.util.List;

public class FeeResultPackMaterialModel extends Model {
    /**
     * @Fields serialVersionUID
     */
    private static final long serialVersionUID = 1L;
    private String id;
    private String owner_no;
    private String whos_no;
    private String doc_no;
    private String carton_type;
    private double fee;
    private Timestamp fee_occur_time;
    private int is_confirmed;
    private Timestamp confirmed_time;
    private Timestamp created_time;
    private String created_by;
    private Timestamp updated_time;
    private String updated_by;
    private int ver;
    private int is_deleted;
    private String start_time;
    private String end_time;
    private List<String> owner_noList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwner_no() {
        return owner_no;
    }

    public void setOwner_no(String owner_no) {
        this.owner_no = owner_no;
    }

    public String getWhos_no() {
        return whos_no;
    }

    public void setWhos_no(String whos_no) {
        this.whos_no = whos_no;
    }

    public String getDoc_no() {
        return doc_no;
    }

    public void setDoc_no(String doc_no) {
        this.doc_no = doc_no;
    }

    public String getCarton_type() {
        return carton_type;
    }

    public void setCarton_type(String carton_type) {
        this.carton_type = carton_type;
    }

    public double getFee() {
        return fee;
    }

    public void setFee(double fee) {
        this.fee = fee;
    }

    public Timestamp getFee_occur_time() {
        return fee_occur_time;
    }

    public void setFee_occur_time(Timestamp fee_occur_time) {
        this.fee_occur_time = fee_occur_time;
    }

    public int getIs_confirmed() {
        return is_confirmed;
    }

    public void setIs_confirmed(int is_confirmed) {
        this.is_confirmed = is_confirmed;
    }

    public Timestamp getConfirmed_time() {
        return confirmed_time;
    }

    public void setConfirmed_time(Timestamp confirmed_time) {
        this.confirmed_time = confirmed_time;
    }

    public Timestamp getCreated_time() {
        return created_time;
    }

    public void setCreated_time(Timestamp created_time) {
        this.created_time = created_time;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public Timestamp getUpdated_time() {
        return updated_time;
    }

    public void setUpdated_time(Timestamp updated_time) {
        this.updated_time = updated_time;
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public int getVer() {
        return ver;
    }

    public void setVer(int ver) {
        this.ver = ver;
    }

    public int getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(int is_deleted) {
        this.is_deleted = is_deleted;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public List<String> getOwner_noList() {
        return owner_noList;
    }

    public void setOwner_noList(List<String> owner_noList) {
        this.owner_noList = owner_noList;
    }
}
