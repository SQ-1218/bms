package com.bms.service.profiles.impl.bd;

import com.base.common.core.dao.IBaseDao;
import com.base.common.core.service.impl.BaseServiceImpl;
import com.bms.service.dto.bd.BdCargoOwnerDto;
import com.bms.service.model.bd.BdCargoOwnerModel;
import com.bms.service.model.bd.FeeResultOwnerDailyModel;
import com.bms.service.profiles.dao.bd.IBdCargoOwnerDao;
import com.bms.service.profiles.dao.bd.IFeeResultOwnerDailyDao;
import com.bms.service.profiles.service.bd.IFeeResultOwerDailyService;
import com.bms.service.vo.bd.FeeResultOwnerDailyVo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by jsc on 2018/11/21.
 */
@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class FeeResultOwnerDailyServiceImpl extends BaseServiceImpl<FeeResultOwnerDailyModel> implements IFeeResultOwerDailyService {
    @Resource
    private IFeeResultOwnerDailyDao iFeeResultOwnerDailyDao;
    @Resource
    private IBdCargoOwnerDao iBdCargoOwnerDao;

    @Override
    protected IBaseDao<FeeResultOwnerDailyModel> getBaseDao() {
        return this.iFeeResultOwnerDailyDao;
    }

    @Override
    public List<FeeResultOwnerDailyModel> getExpressFee() throws Exception {
        //获取货主
        BdCargoOwnerDto bdCargoOwnerDto = new BdCargoOwnerDto();
        bdCargoOwnerDto.setIs_deleted(1);
        //获取所有货主
        List<BdCargoOwnerModel> bdCargoOwnerModelList = iBdCargoOwnerDao.getowner_noList(bdCargoOwnerDto);
        for (BdCargoOwnerModel bdCargoOwnerModel : bdCargoOwnerModelList) {
            FeeResultOwnerDailyVo feeResultOwnerDailyVo = iFeeResultOwnerDailyDao.expressfee(bdCargoOwnerModel.getNo());
            FeeResultOwnerDailyModel feeResultOwnerDailyModel = new FeeResultOwnerDailyModel();
            // 获取当前时间
            Date date1 = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            //时间存储为字符串
            String str = sdf.format(date1);
            feeResultOwnerDailyModel.setOwner_no(bdCargoOwnerModel.getNo());
            feeResultOwnerDailyModel.setFee_type("1");
            feeResultOwnerDailyModel.setFee_result(feeResultOwnerDailyVo.getFee());
            feeResultOwnerDailyModel.setDate_str(str);
            feeResultOwnerDailyModel.setCreated_by("admin");
            iFeeResultOwnerDailyDao.insert(feeResultOwnerDailyModel);
        }
        return null;
    }

    @Override
    public List<FeeResultOwnerDailyModel> getStoageFee() throws Exception {
        //获取货主
        BdCargoOwnerDto bdCargoOwnerDto = new BdCargoOwnerDto();
        bdCargoOwnerDto.setIs_deleted(1);
        //获取所有货主
        List<BdCargoOwnerModel> bdCargoOwnerModelList = iBdCargoOwnerDao.getowner_noList(bdCargoOwnerDto);
        for (BdCargoOwnerModel bdCargoOwnerModel : bdCargoOwnerModelList) {
            FeeResultOwnerDailyVo feeResultOwnerDailyVo = iFeeResultOwnerDailyDao.storagefee(bdCargoOwnerModel.getNo());
            FeeResultOwnerDailyModel feeResultOwnerDailyModel = new FeeResultOwnerDailyModel();
            // 获取当前时间
            Date date1 = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            //时间存储为字符串
            String str = sdf.format(date1);
            feeResultOwnerDailyModel.setOwner_no(bdCargoOwnerModel.getNo());
            feeResultOwnerDailyModel.setFee_type("2");
            feeResultOwnerDailyModel.setFee_result(feeResultOwnerDailyVo.getFee());
            feeResultOwnerDailyModel.setDate_str(str);
            feeResultOwnerDailyModel.setCreated_by("admin");
            iFeeResultOwnerDailyDao.insert(feeResultOwnerDailyModel);
        }
        return null;
    }

    @Override
    public List<FeeResultOwnerDailyModel> getDeliverFee() throws Exception {
        //获取货主
        BdCargoOwnerDto bdCargoOwnerDto = new BdCargoOwnerDto();
        bdCargoOwnerDto.setIs_deleted(1);
        //获取所有货主
        List<BdCargoOwnerModel> bdCargoOwnerModelList = iBdCargoOwnerDao.getowner_noList(bdCargoOwnerDto);
        for (BdCargoOwnerModel bdCargoOwnerModel : bdCargoOwnerModelList) {
            FeeResultOwnerDailyVo feeResultOwnerDailyVo = iFeeResultOwnerDailyDao.deliveryfee(bdCargoOwnerModel.getNo());
            FeeResultOwnerDailyModel feeResultOwnerDailyModel = new FeeResultOwnerDailyModel();
            // 获取当前时间
            Date date1 = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            //时间存储为字符串
            String str = sdf.format(date1);
            feeResultOwnerDailyModel.setOwner_no(bdCargoOwnerModel.getNo());
            feeResultOwnerDailyModel.setFee_type("3");
            feeResultOwnerDailyModel.setFee_result(feeResultOwnerDailyVo.getFee());
            feeResultOwnerDailyModel.setDate_str(str);
            feeResultOwnerDailyModel.setCreated_by("admin");
            iFeeResultOwnerDailyDao.insert(feeResultOwnerDailyModel);
        }
        return null;
    }

    @Override
    public List<FeeResultOwnerDailyModel> getReceivingFee() throws Exception {
        //获取货主
        BdCargoOwnerDto bdCargoOwnerDto = new BdCargoOwnerDto();
        bdCargoOwnerDto.setIs_deleted(1);
        //获取所有货主
        List<BdCargoOwnerModel> bdCargoOwnerModelList = iBdCargoOwnerDao.getowner_noList(bdCargoOwnerDto);
        for (BdCargoOwnerModel bdCargoOwnerModel : bdCargoOwnerModelList) {
            FeeResultOwnerDailyVo feeResultOwnerDailyVo = iFeeResultOwnerDailyDao.receivingfee(bdCargoOwnerModel.getNo());
            FeeResultOwnerDailyModel feeResultOwnerDailyModel = new FeeResultOwnerDailyModel();
            // 获取当前时间
            Date date1 = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            //时间存储为字符串
            String str = sdf.format(date1);
            feeResultOwnerDailyModel.setOwner_no(bdCargoOwnerModel.getNo());
            feeResultOwnerDailyModel.setFee_type("4");
            feeResultOwnerDailyModel.setFee_result(feeResultOwnerDailyVo.getFee());
            feeResultOwnerDailyModel.setDate_str(str);
            feeResultOwnerDailyModel.setCreated_by("admin");
            iFeeResultOwnerDailyDao.insert(feeResultOwnerDailyModel);
        }
        return null;
    }

    @Override
    public List<FeeResultOwnerDailyModel> getSpecialFee() throws Exception {
        //获取货主
        BdCargoOwnerDto bdCargoOwnerDto = new BdCargoOwnerDto();
        bdCargoOwnerDto.setIs_deleted(1);
        //获取所有货主
        List<BdCargoOwnerModel> bdCargoOwnerModelList = iBdCargoOwnerDao.getowner_noList(bdCargoOwnerDto);
        for (BdCargoOwnerModel bdCargoOwnerModel : bdCargoOwnerModelList) {
            FeeResultOwnerDailyVo feeResultOwnerDailyVo = iFeeResultOwnerDailyDao.specialfee(bdCargoOwnerModel.getNo());
            FeeResultOwnerDailyModel feeResultOwnerDailyModel = new FeeResultOwnerDailyModel();
            // 获取当前时间
            Date date1 = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            //时间存储为字符串
            String str = sdf.format(date1);
            feeResultOwnerDailyModel.setOwner_no(bdCargoOwnerModel.getNo());
            feeResultOwnerDailyModel.setFee_type("6");
            feeResultOwnerDailyModel.setFee_result(feeResultOwnerDailyVo.getFee());
            feeResultOwnerDailyModel.setDate_str(str);
            feeResultOwnerDailyModel.setCreated_by("admin");
            iFeeResultOwnerDailyDao.insert(feeResultOwnerDailyModel);
        }
        return null;
    }

    @Override
    public List<FeeResultOwnerDailyModel> getMaterialFee() throws Exception {
        //获取货主
        BdCargoOwnerDto bdCargoOwnerDto = new BdCargoOwnerDto();
        bdCargoOwnerDto.setIs_deleted(1);
        //获取所有货主
        List<BdCargoOwnerModel> bdCargoOwnerModelList = iBdCargoOwnerDao.getowner_noList(bdCargoOwnerDto);
        for (BdCargoOwnerModel bdCargoOwnerModel : bdCargoOwnerModelList) {
            FeeResultOwnerDailyVo feeResultOwnerDailyVo = iFeeResultOwnerDailyDao.materialfee(bdCargoOwnerModel.getNo());
            FeeResultOwnerDailyModel feeResultOwnerDailyModel = new FeeResultOwnerDailyModel();
            // 获取当前时间
            Date date1 = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            //时间存储为字符串
            String str = sdf.format(date1);
            feeResultOwnerDailyModel.setOwner_no(bdCargoOwnerModel.getNo());
            feeResultOwnerDailyModel.setFee_type("5");
            feeResultOwnerDailyModel.setFee_result(feeResultOwnerDailyVo.getFee());
            feeResultOwnerDailyModel.setDate_str(str);
            feeResultOwnerDailyModel.setCreated_by("admin");
            iFeeResultOwnerDailyDao.insert(feeResultOwnerDailyModel);
        }
        return null;
    }
}
