package com.bms.service.profiles.impl.bd;

import com.base.common.core.dao.IBaseDao;
import com.base.common.core.service.impl.BaseServiceImpl;
import com.bms.service.dto.bd.*;
import com.bms.service.model.bd.*;
import com.bms.service.profiles.dao.bd.IFeeDao;
import com.bms.service.profiles.dao.bd.ILogVerifyFeeResultDao;
import com.bms.service.profiles.service.bd.IFeeService;
import com.bms.service.profiles.service.bd.ILogVerifyFeeResultService;
import com.bms.service.vo.bd.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by jsc on 2018/10/16.
 */
@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class FeeServiceImpl extends BaseServiceImpl<SumFeeModel> implements IFeeService {

    @Resource
    private IFeeDao iFeeDao;

    @Resource
    private ILogVerifyFeeResultDao iLogVerifyFeeResultDao;

    @Override
    protected IBaseDao<SumFeeModel> getBaseDao() {
        return this.iFeeDao;
    }


    @Override
    public List<FeeResultExpressFeeModel> getexpressfee(FeeResultExpressFeeDto feeResultExpressFeeDto) throws Exception {
        return iFeeDao.getexpressfee(feeResultExpressFeeDto);
    }

    @Override
    public List<FeeResultStorageFeeModel> getstoragefee(FeeResultStorageFeeDto feeResultStorageFeeDto) throws Exception {
        return iFeeDao.getstoragefee(feeResultStorageFeeDto);
    }

    @Override
    public List<FeeResultDeliveryFeeModel> getdeliveryfee(FeeResultDeliveryFeeDto feeResultDeliveryFeeDto) throws Exception {
        return iFeeDao.getdeliveryfee(feeResultDeliveryFeeDto);
    }

    @Override
    public List<FeeResultReceivingFeeModel> getreceivingfee(FeeResultReceivingFeeDto feeResultReceivingFeeDto) throws Exception {
        return iFeeDao.getreceivingfee(feeResultReceivingFeeDto);
    }

    @Override
    public List<FeeResultSpecialFeeModel> getspeciatfee(FeeResultSpecialFeeDto feeResultSpecialFeeDto) throws Exception {
        return iFeeDao.getspeciatfee(feeResultSpecialFeeDto);
    }

    @Override
    public List<FeeResultPackMaterialFeeModel> getMaterialfee(FeeResultPackMaterialFeeDto feeResultPackMaterialFeeDto) throws Exception {
        return iFeeDao.getMaterialfee(feeResultPackMaterialFeeDto);
    }

    @Override
    public void save(String owner_no,Double expressfee,String year,String month,Double dctstoragefee,Double deliveryfee,Double specialfee,Double receivingfee,Double packmaterialfee) throws Exception {
        FeeResultOwnerMonthlyModel feeResultOwnerMonthlyModel = new FeeResultOwnerMonthlyModel();
        feeResultOwnerMonthlyModel.setOwner_no(owner_no);
        feeResultOwnerMonthlyModel.setFee_result(expressfee);
        feeResultOwnerMonthlyModel.setYear_str(year);
        feeResultOwnerMonthlyModel.setMonth_str(month);
        feeResultOwnerMonthlyModel.setFee_type("1");
        feeResultOwnerMonthlyModel.setCreated_by("admin");
        iFeeDao.save(feeResultOwnerMonthlyModel);
        feeResultOwnerMonthlyModel.setOwner_no(owner_no);
        feeResultOwnerMonthlyModel.setFee_result(dctstoragefee);
        feeResultOwnerMonthlyModel.setYear_str(year);
        feeResultOwnerMonthlyModel.setMonth_str(month);
        feeResultOwnerMonthlyModel.setFee_type("2");
        feeResultOwnerMonthlyModel.setCreated_by("admin");
        iFeeDao.save(feeResultOwnerMonthlyModel);
        feeResultOwnerMonthlyModel.setOwner_no(owner_no);
        feeResultOwnerMonthlyModel.setFee_result(deliveryfee);
        feeResultOwnerMonthlyModel.setYear_str(year);
        feeResultOwnerMonthlyModel.setMonth_str(month);
        feeResultOwnerMonthlyModel.setFee_type("3");
        feeResultOwnerMonthlyModel.setCreated_by("admin");
        iFeeDao.save(feeResultOwnerMonthlyModel);
        feeResultOwnerMonthlyModel.setOwner_no(owner_no);
        feeResultOwnerMonthlyModel.setFee_result(receivingfee);
        feeResultOwnerMonthlyModel.setYear_str(year);
        feeResultOwnerMonthlyModel.setMonth_str(month);
        feeResultOwnerMonthlyModel.setFee_type("4");
        feeResultOwnerMonthlyModel.setCreated_by("admin");
        iFeeDao.save(feeResultOwnerMonthlyModel);
        feeResultOwnerMonthlyModel.setOwner_no(owner_no);
        feeResultOwnerMonthlyModel.setFee_result(packmaterialfee);
        feeResultOwnerMonthlyModel.setYear_str(year);
        feeResultOwnerMonthlyModel.setMonth_str(month);
        feeResultOwnerMonthlyModel.setFee_type("6");
        feeResultOwnerMonthlyModel.setCreated_by("admin");
        iFeeDao.save(feeResultOwnerMonthlyModel);
        feeResultOwnerMonthlyModel.setOwner_no(owner_no);
        feeResultOwnerMonthlyModel.setFee_result(specialfee);
        feeResultOwnerMonthlyModel.setFee_type("5");
        feeResultOwnerMonthlyModel.setCreated_by("admin");
        iFeeDao.save(feeResultOwnerMonthlyModel);
    }


    @Override
    public FeeResultExpressSkuVo getCount(String owner_no, String month_time) throws Exception {
        return iFeeDao.getCount(owner_no, month_time);
    }

    @Override
    public FeeResultOwnerMonthlyVo getRecord(String owner_no, String year, String month) throws Exception {
        return iFeeDao.getRecord(owner_no, year, month);
    }


    @Override
    public List<SumFeeModel> getOwnerMonth(SumFeeDto sumFeeDto) throws Exception {
        return iFeeDao.getOwnerMonth(sumFeeDto);
    }

    @Override
    public List<FeeResultOwnerMonthlyModel> getOwnerMonthly(String owner_no, String year, String month) {
        return iFeeDao.getOwnerMonthly(owner_no, year, month);
    }


    @Override
    public int updateOwnerMonthly(FeeResultOwnerMonthlyModel feeResultOwnerMonthlyModel) {
        return iFeeDao.updateOwnerMonthly(feeResultOwnerMonthlyModel);
    }

    @Override
    public int updateMonthly(FeeResultOwnerMonthlyModel feeResultOwnerMonthlyModel) {
        return iFeeDao.updateMonthly(feeResultOwnerMonthlyModel);
    }

    @Override
    public FeeResultOwnerMonthlyVo getCodeitem(String owner_no) {
        return iFeeDao.getCodeitem(owner_no);
    }

    @Override
    public FeeResultOwnerMonthlyVo getRateitem(String owner_no) {
        return iFeeDao.getRateitem(owner_no);
    }

    @Override
    public FeeRuleSpecialVo getSafetyitem(String owner_no) {
        return iFeeDao.getSafetyitem(owner_no);
    }

    @Override
    public FeeResultOwnerMonthlyVo getOwnerdelivery(String owner_no, String month_time) {
        return iFeeDao.getOwnerdelivery(owner_no, month_time);
    }

    @Override
    public FeeRuleSpecialVo getTurnover(String owner_no) {
        return iFeeDao.getTurnover(owner_no);
    }

    @Override
    public FeeRuleSpecialVo getPlt(String owner_no) throws Exception {
        return iFeeDao.getPlt(owner_no);
    }

    @Override
    public SnapshotStkTotalInfoVo getTotal(String owner_no, String starDay, String lastDay) {
        return iFeeDao.getTotal(owner_no, starDay, lastDay);
    }

    @Override
    public FeeRuleStorageVo getTray(String owner_no) {
        return iFeeDao.getTray(owner_no);
    }

    @Override
    public int operationAuditModify(String owner_no, String year, String month) throws Exception {
        int result1 = 0;
        FeeResultOwnerMonthlyModel feeResultOwnerMonthlyModel = new FeeResultOwnerMonthlyModel();
        feeResultOwnerMonthlyModel.setOwner_no(owner_no);
        feeResultOwnerMonthlyModel.setYear_str(year);
        feeResultOwnerMonthlyModel.setMonth_str(month);
        feeResultOwnerMonthlyModel.setIs_deleted(1);
        int result = iFeeDao.updateOwnerMonthly(feeResultOwnerMonthlyModel);
        if (result > 0) {
            LogVerifyFeeResultModel logVerifyFeeResultModel = new LogVerifyFeeResultModel();
            //获取当前时间
            Date date1 = new Date();
            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            //时间存储为字符串
            String str = sdf2.format(date1);
            logVerifyFeeResultModel.setCargo_owner_no(owner_no);
            logVerifyFeeResultModel.setYear_str(year);
            logVerifyFeeResultModel.setMonth_str(month);
            logVerifyFeeResultModel.setType(0);
            logVerifyFeeResultModel.setRemark("运营已确认");
            logVerifyFeeResultModel.setCreated_by("admin");
            logVerifyFeeResultModel.setCreated_time(Timestamp.valueOf(str));
            logVerifyFeeResultModel.setIs_deleted(1);
            result1 = iLogVerifyFeeResultDao.insert(logVerifyFeeResultModel);
        }
        return result1;

    }

    @Override
    public int financeCommissionerModify(String owner_no, String year, String month) throws Exception {
        int result1 = 0;
        FeeResultOwnerMonthlyModel feeResultOwnerMonthlyModel = new FeeResultOwnerMonthlyModel();
        feeResultOwnerMonthlyModel.setOwner_no(owner_no);
        feeResultOwnerMonthlyModel.setYear_str(year);
        feeResultOwnerMonthlyModel.setMonth_str(month);
        feeResultOwnerMonthlyModel.setIs_deleted(1);
        int result = iFeeDao.updateOwnerMonthly(feeResultOwnerMonthlyModel);
        if (result > 0) {
            LogVerifyFeeResultModel logVerifyFeeResultModel = new LogVerifyFeeResultModel();
            //获取当前时间
            Date date1 = new Date();
            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            //时间存储为字符串
            String str = sdf2.format(date1);
            logVerifyFeeResultModel.setCargo_owner_no(owner_no);
            logVerifyFeeResultModel.setYear_str(year);
            logVerifyFeeResultModel.setMonth_str(month);
            logVerifyFeeResultModel.setType(0);
            logVerifyFeeResultModel.setRemark("财务专员已确认");
            logVerifyFeeResultModel.setCreated_by("admin");
            logVerifyFeeResultModel.setCreated_time(Timestamp.valueOf(str));
            logVerifyFeeResultModel.setIs_deleted(1);
            result1 = iLogVerifyFeeResultDao.insert(logVerifyFeeResultModel);
        }
        return result1;

    }

    @Override
    public int financialManagerModify(String owner_no, String year, String month) throws Exception {
        int result1 = 0;
        FeeResultOwnerMonthlyModel feeResultOwnerMonthlyModel = new FeeResultOwnerMonthlyModel();
        feeResultOwnerMonthlyModel.setOwner_no(owner_no);
        feeResultOwnerMonthlyModel.setYear_str(year);
        feeResultOwnerMonthlyModel.setMonth_str(month);
        feeResultOwnerMonthlyModel.setIs_deleted(1);
        int result = iFeeDao.updateOwnerMonthly(feeResultOwnerMonthlyModel);
        if (result > 0) {
            LogVerifyFeeResultModel logVerifyFeeResultModel = new LogVerifyFeeResultModel();
            //获取当前时间
            Date date1 = new Date();
            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            //时间存储为字符串
            String str = sdf2.format(date1);
            logVerifyFeeResultModel.setCargo_owner_no(owner_no);
            logVerifyFeeResultModel.setYear_str(year);
            logVerifyFeeResultModel.setMonth_str(month);
            logVerifyFeeResultModel.setType(0);
            logVerifyFeeResultModel.setRemark("财务经理已确认");
            logVerifyFeeResultModel.setCreated_by("admin");
            logVerifyFeeResultModel.setCreated_time(Timestamp.valueOf(str));
            logVerifyFeeResultModel.setIs_deleted(1);
            result1 = iLogVerifyFeeResultDao.insert(logVerifyFeeResultModel);
        }
        return result1;

    }

    @Override
    public int operationAuditReject(String owner_no, String year, String month) throws Exception {
        int result1 = 0;
        LogVerifyFeeResultModel logVerifyFeeResultModel = new LogVerifyFeeResultModel();
        //获取当前时间
        Date date1 = new Date();
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //时间存储为字符串
        String str = sdf2.format(date1);
        logVerifyFeeResultModel.setCargo_owner_no(owner_no);
        logVerifyFeeResultModel.setYear_str(year);
        logVerifyFeeResultModel.setMonth_str(month);
        logVerifyFeeResultModel.setType(1);
        logVerifyFeeResultModel.setRemark("运营已驳回");
        logVerifyFeeResultModel.setCreated_by("admin");
        logVerifyFeeResultModel.setCreated_time(Timestamp.valueOf(str));
        logVerifyFeeResultModel.setIs_deleted(1);
        result1 = iLogVerifyFeeResultDao.insert(logVerifyFeeResultModel);
        return result1;
    }

    @Override
    public int financeCommissionerReject(String owner_no, String year, String month) throws Exception {
        int result1 = 0;
        FeeResultOwnerMonthlyModel feeResultOwnerMonthlyModel = new FeeResultOwnerMonthlyModel();
        feeResultOwnerMonthlyModel.setOwner_no(owner_no);
        feeResultOwnerMonthlyModel.setYear_str(year);
        feeResultOwnerMonthlyModel.setMonth_str(month);
        feeResultOwnerMonthlyModel.setIs_deleted(1);
        int result = iFeeDao.updateMonthly(feeResultOwnerMonthlyModel);
        if (result > 0) {
            LogVerifyFeeResultModel logVerifyFeeResultModel = new LogVerifyFeeResultModel();
            //获取当前时间
            Date date1 = new Date();
            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            //时间存储为字符串
            String str = sdf2.format(date1);
            logVerifyFeeResultModel.setCargo_owner_no(owner_no);
            logVerifyFeeResultModel.setYear_str(year);
            logVerifyFeeResultModel.setMonth_str(month);
            logVerifyFeeResultModel.setType(1);
            logVerifyFeeResultModel.setRemark("财务专员已驳回");
            logVerifyFeeResultModel.setCreated_by("admin");
            logVerifyFeeResultModel.setCreated_time(Timestamp.valueOf(str));
            logVerifyFeeResultModel.setIs_deleted(1);
            result1 = iLogVerifyFeeResultDao.insert(logVerifyFeeResultModel);
        }
        return result1;
    }

    @Override
    public int financialManagerReject(String owner_no, String year, String month) throws Exception {
        int result1 = 0;
        FeeResultOwnerMonthlyModel feeResultOwnerMonthlyModel = new FeeResultOwnerMonthlyModel();
        feeResultOwnerMonthlyModel.setOwner_no(owner_no);
        feeResultOwnerMonthlyModel.setYear_str(year);
        feeResultOwnerMonthlyModel.setMonth_str(month);
        feeResultOwnerMonthlyModel.setIs_deleted(1);
        int result = iFeeDao.updateMonthly(feeResultOwnerMonthlyModel);
        if (result > 0) {
            LogVerifyFeeResultModel logVerifyFeeResultModel = new LogVerifyFeeResultModel();
            //获取当前时间
            Date date1 = new Date();
            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            //时间存储为字符串
            String str = sdf2.format(date1);
            logVerifyFeeResultModel.setCargo_owner_no(owner_no);
            logVerifyFeeResultModel.setYear_str(year);
            logVerifyFeeResultModel.setMonth_str(month);
            logVerifyFeeResultModel.setType(1);
            logVerifyFeeResultModel.setRemark("财务经理已驳回");
            logVerifyFeeResultModel.setCreated_by("admin");
            logVerifyFeeResultModel.setCreated_time(Timestamp.valueOf(str));
            logVerifyFeeResultModel.setIs_deleted(1);
            result1 = iLogVerifyFeeResultDao.insert(logVerifyFeeResultModel);
        }
        return result1;
    }
}
