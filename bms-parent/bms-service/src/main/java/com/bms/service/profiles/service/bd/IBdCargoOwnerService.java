package com.bms.service.profiles.service.bd;

import com.base.common.core.service.IBaseService;
import com.bms.common.table.vo.DataTablesVO;
import com.bms.service.dto.bd.BdCargoOwnerDto;
import com.bms.service.model.bd.BdCargoOwnerModel;
import com.bms.service.model.bd.LogChangeOwnerBalanceModel;
import com.bms.service.vo.bd.BdCargoOwnerVo;

import java.util.List;

/**
 * <b>description</b>：货主信息表业务接口<br>
 * <b>time</b>：2017-08-08 16:24:44 <br>
 * <b>author</b>：  zhaochen
 */
public interface IBdCargoOwnerService extends IBaseService<BdCargoOwnerModel> {
	
	DataTablesVO<BdCargoOwnerModel> getOwnerList(BdCargoOwnerDto bdCargoOwnerDto) throws Exception;
	List<BdCargoOwnerVo> getOwner()throws Exception;

    //获取户主list
    List<BdCargoOwnerModel> getowner_noList(BdCargoOwnerDto bdCargoOwnerDto) throws Exception;
    //修改货主余额
    Integer updateBalance(BdCargoOwnerModel bdCargoOwnerModel) throws Exception;
    //新增货主修改余额记录
    Integer insertLogBalance(LogChangeOwnerBalanceModel logChangeOwnerBalanceModel) throws Exception;
}
