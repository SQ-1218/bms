package com.bms.service.profiles.impl.bd;

import com.base.common.core.base.DTO;
import com.base.common.core.dao.IBaseDao;
import com.base.common.core.pager.service.IPagerService;
import com.base.common.core.service.impl.BaseServiceImpl;
import com.bms.common.response.BaseResponse;
import com.bms.common.util.ExcelUtil;
import com.bms.common.table.vo.AmazuePagerUtil;
import com.bms.common.table.vo.DataTablesVO;
import com.bms.common.table.vo.ExcelBean;
import com.bms.service.dto.bd.FeeRuleExpressDto;
import com.bms.service.model.bd.BdCargoOwnerModel;
import com.bms.service.model.bd.FeeRuleExpressModel;
import com.bms.service.profiles.dao.bd.IBdCargoOwnerDao;
import com.bms.service.profiles.dao.bd.IFeeRuleExpressDao;
import com.bms.service.profiles.service.bd.IFeeRuleExpressService;
import com.bms.service.vo.bd.BdCargoOwnerVo;
import com.bms.service.vo.bd.CarrierVo;
import com.bms.service.vo.bd.ProvinceVo;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

//快递配置实现类
@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class FeeRuleExpressServiceImpl extends BaseServiceImpl<FeeRuleExpressModel> implements IFeeRuleExpressService {
    @Resource
    private IFeeRuleExpressDao feeRuleExpressDao;
    @Resource
    private IBdCargoOwnerDao bdCargoOwnerDao;
    @Override
    protected IBaseDao<FeeRuleExpressModel> getBaseDao() {
        return this.feeRuleExpressDao;
    }

    @Override
    public DataTablesVO<FeeRuleExpressModel> getFeeRuleExpressList(FeeRuleExpressDto feeRuleExpressDto) throws Exception {
        return AmazuePagerUtil.getPagerModel(new IPagerService<FeeRuleExpressModel>() {
            @Override
            public int queryCount(DTO dto) throws Exception {
                return feeRuleExpressDao.getModelListCount(dto);
            }
            @Override
            public List<FeeRuleExpressModel> queryList(DTO dto) throws Exception {
                return feeRuleExpressDao.getModelList(dto);
            }
        }, feeRuleExpressDto);
    }

    @Override
    public int batchDeleteByIds(List<String> list) throws Exception {
        int result = 0;
        result = feeRuleExpressDao.batchDeleteByIds(list);
        if (result <= 0) {
            throw new Exception("删除失败，请重试！");
        }
        return result;
    }
    //导入
    @Override
    public BaseResponse importExcelInfo(InputStream in, MultipartFile file) {
        BaseResponse baseResponse=new BaseResponse();
        baseResponse.setCode(0);
        try {
            List<List<Object>> listOb = ExcelUtil.getBankListByExcel(in,file.getOriginalFilename());
            List<FeeRuleExpressModel> feeRuleExpressModelList=new ArrayList<>();
            //获取货主列表
            //获取省份列表
            //获取快递列表
            List<BdCargoOwnerVo> ownerList = bdCargoOwnerDao.getOwner();
            List<String>owner=new ArrayList<String>();
            for(BdCargoOwnerVo ownerVo : ownerList){
                owner.add(ownerVo.getNo());
            }
            List<CarrierVo> carrierList = feeRuleExpressDao.getCarrier();
            List<String>carrier=new ArrayList<String>();
            for(CarrierVo carrierVo :carrierList){
                carrier.add(carrierVo.getCode());
            }
            List<ProvinceVo> provinceList=feeRuleExpressDao.getProvince();
            List<String>province=new ArrayList<String>();
            for(ProvinceVo provinceVo :provinceList){
                province.add(provinceVo.getCode());
            }
            //遍历listob数据，把数据放到List中
            for (int i = 0; i < listOb.size(); i++) {
                List<Object> ob = listOb.get(i);
                FeeRuleExpressModel feeRuleExpressModel = new FeeRuleExpressModel();
                //设置编号
                if (owner.contains(String.valueOf(ob.get(0)))) {
                    feeRuleExpressModel.setOwner_no(String.valueOf(ob.get(0)));
                }else{
                    baseResponse.setCode(2);
                    baseResponse.setMsg("导入失败，出错位置为第"+(i+2)+"行第一列(货主)");
                    break;
                }
                //通过遍历实现把每一列封装成一个model中，再把所有的model用List集合装载
                if(carrier.contains(String.valueOf(ob.get(1)))) {
                    feeRuleExpressModel.setExpress_corp_no(String.valueOf(ob.get(1)));
                }else {
                    baseResponse.setCode(2);
                    baseResponse.setMsg("导入失败，出错位置为第"+(i+2)+"行第2列(快递)");
                    break;
                }
                if(province.contains((String.valueOf(ob.get(2))).replace("省",""))||province.contains((String.valueOf(ob.get(2))).replace("市",""))) {
                    feeRuleExpressModel.setProvince(String.valueOf(ob.get(2)).substring(0,2));
                }else{
                    baseResponse.setCode(2);
                    baseResponse.setMsg("导入失败，出错位置为第"+(i+2)+"行第3列(省份)");
                    break;
                }
                //object类型转Double类型
                feeRuleExpressModel.setWeight_fm(Double.parseDouble(ob.get(3).toString()));
                feeRuleExpressModel.setWeight_to(Double.parseDouble(ob.get(4).toString()));
                feeRuleExpressModel.setFee_fixed(Double.parseDouble(ob.get(5).toString()));
                feeRuleExpressModel.setFee_extra(Double.parseDouble(ob.get(6).toString()));
                feeRuleExpressModel.setCreated_by("admin");
                feeRuleExpressModelList.add(feeRuleExpressModel);
            }
             //批量导入
            if(feeRuleExpressModelList.size()>0&&baseResponse.getCode()==0) {
                feeRuleExpressDao.insertBatch(feeRuleExpressModelList);
                baseResponse.setCode(1);
                baseResponse.setMsg("导入成功");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return baseResponse;
    }
    @Override
    public XSSFWorkbook downloadExcelInfo(FeeRuleExpressDto feeRuleExpressDto) throws Exception {
        List<FeeRuleExpressDto> list=new ArrayList<>();
        list.add(feeRuleExpressDto);
        List<ExcelBean> excel=new ArrayList<>();
        Map<Integer,List<ExcelBean>> map=new LinkedHashMap<>();
        XSSFWorkbook xssfWorkbook=null;
        //设置标题栏
        excel.add(new ExcelBean("货主","owner_no",0));
        excel.add(new ExcelBean("快递公司","express_corp_no",0));
        excel.add(new ExcelBean("省份","province",0));
        excel.add(new ExcelBean("初始重量","weight_fm",0));
        excel.add(new ExcelBean("最终重量","weight_to",0));
        excel.add(new ExcelBean("首重","fee_fixed",0));
        excel.add(new ExcelBean("额外重量","fee_extra",0));
        map.put(0, excel);
        String sheetName = "快递费用配置规则";
        //调用ExcelUtil的方法
        xssfWorkbook = ExcelUtil.createExcelFile(FeeRuleExpressDto.class, list, map, sheetName);
        return xssfWorkbook;
    }

    //导出
    @Override
    public XSSFWorkbook exportExcelInfo(FeeRuleExpressDto feeRuleExpressDto)throws Exception {
        //根据条件查询数据，把数据装载到一个list中
        List<FeeRuleExpressModel> list = feeRuleExpressDao.getExcelModelList(feeRuleExpressDto);
        List<ExcelBean> excel=new ArrayList<>();
        Map<Integer,List<ExcelBean>> map=new LinkedHashMap<>();
        XSSFWorkbook xssfWorkbook=null;
        //设置标题栏
        excel.add(new ExcelBean("货主","owner_no",0));
        excel.add(new ExcelBean("快递公司","express_corp_no",0));
        excel.add(new ExcelBean("省份","province",0));
        excel.add(new ExcelBean("初始重量","weight_fm",0));
        excel.add(new ExcelBean("最终重量","weight_to",0));
        excel.add(new ExcelBean("首重","fee_fixed",0));
        excel.add(new ExcelBean("额外重量","fee_extra",0));
        map.put(0, excel);
        String sheetName = "快递费用配置规则";
        //调用ExcelUtil的方法
        xssfWorkbook = ExcelUtil.createExcelFile(FeeRuleExpressModel.class, list, map, sheetName);
        return xssfWorkbook;
    }

    @Override
    public List<CarrierVo> getCarrier() throws Exception {
        return feeRuleExpressDao.getCarrier();
    }

    @Override
    public List<ProvinceVo> getProvince() throws Exception {
        return feeRuleExpressDao.getProvince();
    }
}
