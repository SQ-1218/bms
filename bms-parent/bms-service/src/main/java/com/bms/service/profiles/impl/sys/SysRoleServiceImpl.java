package com.bms.service.profiles.impl.sys;

import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.base.common.core.dao.IBaseDao;
import com.base.common.core.service.impl.BaseServiceImpl;
import com.bms.service.profiles.service.sys.ISysRoleService;
import com.bms.service.profiles.dao.sys.ISysRoleDao;
import com.bms.service.model.sys.SysRoleModel;

/**
 * <b>description</b>：角色信息业务实现<br>
 * <b>time</b>：2018-11-15 11:05:43 <br>
 * <b>author</b>：  zhaochen
 */
@Service("iSysRoleService")
@Transactional(readOnly = true,rollbackFor = Exception.class)
public class SysRoleServiceImpl extends BaseServiceImpl<SysRoleModel> implements ISysRoleService {

	@Resource
	private ISysRoleDao isysRoleDao;

	@Override
	public IBaseDao<SysRoleModel> getBaseDao() {
		return this.isysRoleDao;
	}

}
