package com.bms.service.profiles.dao.sys;

import com.base.common.core.dao.IBaseDao;
import com.bms.service.model.sys.SysUserModel;


/**
 * <b>description</b>：BMS系统登录数据访问接口<br>
 * <b>time</b>：2018-09-17 11:08:11 <br>
 * <b>author</b>：  zhaochen
 */
public interface ISysUserDao extends IBaseDao<SysUserModel>{


}