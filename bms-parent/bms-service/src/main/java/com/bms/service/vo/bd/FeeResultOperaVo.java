package com.bms.service.vo.bd;

import com.base.common.core.base.VO;

/**
 * Created by jsc on 2018/9/18.
 */
public class FeeResultOperaVo extends VO {
    private static final long serialVersionUID = 1L;
    //件数
    private int qty;
    //订单状态(70：已发货，99：已取消)
    private int status;
    //作业类型(0：普通，1：同品单品秒杀，2：自提)
    private int oper_flag;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getOper_flag() {
        return oper_flag;
    }

    public void setOper_flag(int oper_flag) {
        this.oper_flag = oper_flag;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }
}
