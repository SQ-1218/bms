package com.bms.service.profiles.dao.bd;

import com.base.common.core.dao.IBaseDao;
import com.bms.service.model.bd.LogChangeOwnerBalanceModel;

public interface ILogChangeOwnerBalanceDao extends IBaseDao<LogChangeOwnerBalanceModel> {


}
