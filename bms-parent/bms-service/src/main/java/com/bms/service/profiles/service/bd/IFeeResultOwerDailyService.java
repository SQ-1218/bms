package com.bms.service.profiles.service.bd;

import com.base.common.core.service.IBaseService;
import com.bms.service.model.bd.FeeResultExpressModel;
import com.bms.service.model.bd.FeeResultOwnerDailyModel;

import java.util.List;


/**
 * Created by jsc on 2018/11/21.
 */
public interface IFeeResultOwerDailyService extends IBaseService<FeeResultOwnerDailyModel> {
    //定时任务每一天的快递总费用
    List<FeeResultOwnerDailyModel> getExpressFee() throws Exception;

    //定时任务每一天的仓储总费用
    List<FeeResultOwnerDailyModel> getStoageFee() throws Exception;

    //定时任务每一天的出库总费用
    List<FeeResultOwnerDailyModel> getDeliverFee() throws Exception;

    //定时任务每一天的入库总费用
    List<FeeResultOwnerDailyModel> getReceivingFee() throws Exception;

    //定时任务每一天的特殊总费用
    List<FeeResultOwnerDailyModel> getSpecialFee() throws Exception;

    //定时任务每一天的包材总费用
    List<FeeResultOwnerDailyModel> getMaterialFee() throws Exception;

}
