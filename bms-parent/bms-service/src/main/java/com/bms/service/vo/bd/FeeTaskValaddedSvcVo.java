package com.bms.service.vo.bd;

import java.sql.Timestamp;

public class FeeTaskValaddedSvcVo {
    private String owner_no;
    private String doc_no;
    private String cal_item_code;
    private double qty1;
    private double qty2;
    private double qty3;
    private double qty4;
    private double qty5;
    private double qty6;
    private double qty7;
    private double qty8;
    private double qty9;
    private String status;
    private int is_deleted;
    private Timestamp verified_time;

    public String getOwner_no() {
        return owner_no;
    }

    public void setOwner_no(String owner_no) {
        this.owner_no = owner_no;
    }

    public String getDoc_no() {
        return doc_no;
    }

    public void setDoc_no(String doc_no) {
        this.doc_no = doc_no;
    }

    public String getCal_item_code() {
        return cal_item_code;
    }

    public void setCal_item_code(String cal_item_code) {
        this.cal_item_code = cal_item_code;
    }

    public double getQty1() {
        return qty1;
    }

    public void setQty1(double qty1) {
        this.qty1 = qty1;
    }

    public double getQty2() {
        return qty2;
    }

    public void setQty2(double qty2) {
        this.qty2 = qty2;
    }

    public double getQty3() {
        return qty3;
    }

    public void setQty3(double qty3) {
        this.qty3 = qty3;
    }

    public double getQty4() {
        return qty4;
    }

    public void setQty4(double qty4) {
        this.qty4 = qty4;
    }

    public double getQty5() {
        return qty5;
    }

    public void setQty5(double qty5) {
        this.qty5 = qty5;
    }

    public double getQty6() {
        return qty6;
    }

    public void setQty6(double qty6) {
        this.qty6 = qty6;
    }

    public double getQty7() {
        return qty7;
    }

    public void setQty7(double qty7) {
        this.qty7 = qty7;
    }

    public double getQty8() {
        return qty8;
    }

    public void setQty8(double qty8) {
        this.qty8 = qty8;
    }

    public double getQty9() {
        return qty9;
    }

    public void setQty9(double qty9) {
        this.qty9 = qty9;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(int is_deleted) {
        this.is_deleted = is_deleted;
    }

    public Timestamp getVerified_time() {
        return verified_time;
    }

    public void setVerified_time(Timestamp verified_time) {
        this.verified_time = verified_time;
    }
}
