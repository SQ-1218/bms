package com.bms.service.dto.intf;

import java.io.Serializable;

public class ReceivableHeaderDTO implements Serializable {

	private static final long serialVersionUID = 4537123096219003876L;

	/**
	 * 系统标识————必填
	 */
	private String sourceSystemId;
	/**
	 * 系统名称————必填
	 */
	private String sourceSystemName;
	/**
	 * 用户编码————必填
	 */
	private String userCode;
	/**
	 * 用户名称————必填
	 */
	private String userName;
	/**
	 * 最后更新时间————必填
	 */
	private String lastUpdateDate;
	/**
	 * 公司编码————必填
	 */
	private String companyCode;
	/**
	 * 公司名称————必填
	 */
	private String companyName;
	/**
	 * 结算单编号————必填
	 */
	private String settleNo;
	/**
	 * 结算金额————必填
	 */
	private String settleMoney;
	/**
	 * 客户编码————必填
	 */
	private String customerCode;
	/**
	 * 客户名称————必填
	 */
	private String customerName;
	/**
	 * 业务类型————必填
	 */
	private String operationTypeCode;
	/**
	 * 摘要
	 */
	private String receivableAbstract;
	/**
	 * 是否开票
	 */
	private String isBilling;
	/**
	 * 结算期间起————必填
	 */
	private String settleDateStart;
	/**
	 * 结算期间止————必填
	 */
	private String settleDateEnd;
	/**
	 * 暂估状态(暂估：0/非暂估：1)————必填
	 */
	private String tempEstimateStatus;

	public String getSourceSystemId() {
		return sourceSystemId;
	}

	public void setSourceSystemId(String sourceSystemId) {
		this.sourceSystemId = sourceSystemId;
	}

	public String getSourceSystemName() {
		return sourceSystemName;
	}

	public void setSourceSystemName(String sourceSystemName) {
		this.sourceSystemName = sourceSystemName;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(String lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getSettleNo() {
		return settleNo;
	}

	public void setSettleNo(String settleNo) {
		this.settleNo = settleNo;
	}

	public String getSettleMoney() {
		return settleMoney;
	}

	public void setSettleMoney(String settleMoney) {
		this.settleMoney = settleMoney;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getOperationTypeCode() {
		return operationTypeCode;
	}

	public void setOperationTypeCode(String operationTypeCode) {
		this.operationTypeCode = operationTypeCode;
	}

	public String getReceivableAbstract() {
		return receivableAbstract;
	}

	public void setReceivableAbstract(String receivableAbstract) {
		this.receivableAbstract = receivableAbstract;
	}

	public String getIsBilling() {
		return isBilling;
	}

	public void setIsBilling(String isBilling) {
		this.isBilling = isBilling;
	}

	public String getSettleDateStart() {
		return settleDateStart;
	}

	public void setSettleDateStart(String settleDateStart) {
		this.settleDateStart = settleDateStart;
	}

	public String getSettleDateEnd() {
		return settleDateEnd;
	}

	public void setSettleDateEnd(String settleDateEnd) {
		this.settleDateEnd = settleDateEnd;
	}

	public String getTempEstimateStatus() {
		return tempEstimateStatus;
	}

	public void setTempEstimateStatus(String tempEstimateStatus) {
		this.tempEstimateStatus = tempEstimateStatus;
	}

}
