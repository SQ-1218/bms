package com.bms.service.dto.bd;

import com.bms.common.table.vo.BmsBaseDto;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by jsc on 2018/9/18.
 */
public class FeeResultOperationdeliveryDto extends BmsBaseDto {
    private static final long serialVersionUID = 1L;
    private String id;
    private String owner_no;
    private String whs_no;
    private Double cal_item_rank;
    private String cal_item_name;
    private String cal_item_code;
    private String cal_way;
    private String doc_type;
    private String doc_no;
    private Double fee;
    private Timestamp fee_occur_time;
    private Double is_confirmed;
    private Timestamp confirmed_time;
    private Timestamp created_time;
    private String created_by;
    private Timestamp updated_time;
    private String updated_by;
    private Integer ver;
    private Integer is_deleted;
    private List<String> noList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwner_no() {
        return owner_no;
    }

    public void setOwner_no(String owner_no) {
        this.owner_no = owner_no;
    }

    public String getWhs_no() {
        return whs_no;
    }

    public void setWhs_no(String whs_no) {
        this.whs_no = whs_no;
    }

    public Double getCal_item_rank() {
        return cal_item_rank;
    }

    public void setCal_item_rank(Double cal_item_rank) {
        this.cal_item_rank = cal_item_rank;
    }

    public String getCal_item_name() {
        return cal_item_name;
    }

    public void setCal_item_name(String cal_item_name) {
        this.cal_item_name = cal_item_name;
    }

    public String getCal_item_code() {
        return cal_item_code;
    }

    public void setCal_item_code(String cal_item_code) {
        this.cal_item_code = cal_item_code;
    }

    public String getCal_way() {
        return cal_way;
    }

    public void setCal_way(String cal_way) {
        this.cal_way = cal_way;
    }

    public String getDoc_type() {
        return doc_type;
    }

    public void setDoc_type(String doc_type) {
        this.doc_type = doc_type;
    }

    public String getDoc_no() {
        return doc_no;
    }

    public void setDoc_no(String doc_no) {
        this.doc_no = doc_no;
    }

    public Double getFee() {
        return fee;
    }

    public void setFee(Double fee) {
        this.fee = fee;
    }

    public Timestamp getFee_occur_time() {
        return fee_occur_time;
    }

    public void setFee_occur_time(Timestamp fee_occur_time) {
        this.fee_occur_time = fee_occur_time;
    }

    public Double getIs_confirmed() {
        return is_confirmed;
    }

    public void setIs_confirmed(Double is_confirmed) {
        this.is_confirmed = is_confirmed;
    }

    public Timestamp getConfirmed_time() {
        return confirmed_time;
    }

    public void setConfirmed_time(Timestamp confirmed_time) {
        this.confirmed_time = confirmed_time;
    }

    public Timestamp getCreated_time() {
        return created_time;
    }

    public void setCreated_time(Timestamp created_time) {
        this.created_time = created_time;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public Timestamp getUpdated_time() {
        return updated_time;
    }

    public void setUpdated_time(Timestamp updated_time) {
        this.updated_time = updated_time;
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public Integer getVer() {
        return ver;
    }

    public void setVer(Integer ver) {
        this.ver = ver;
    }

    public Integer getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(Integer is_deleted) {
        this.is_deleted = is_deleted;
    }

    public List<String> getNoList() {
        return noList;
    }

    public void setNoList(List<String> noList) {
        this.noList = noList;
    }
}
