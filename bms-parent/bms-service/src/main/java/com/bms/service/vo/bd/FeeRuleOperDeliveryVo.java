package com.bms.service.vo.bd;

import com.base.common.core.base.VO;

import java.sql.Date;
import java.util.List;

public class FeeRuleOperDeliveryVo extends VO {
    private static final long serialVersionUID = 1L;
    private String id;
    private String owner_no;
    private double cal_item_rank;
    private String  cal_item_name;
    private String  cal_item_code;
    private String  cal_item_note;
    private String  doc_type;
    private String  cal_way;
    private String  factor1_note;
    private String  factor1_value;
    private String  factor2_note;
    private String  factor2_value;
    private String  factor3_note;
    private String  factor3_value;
    private String  factor4_note;
    private String  factor4_value;
    private String  factor5_note;
    private String  factor5_value;
    private String created_by;
    private Date created_time;
    private Date updated_time;
    private String updated_by;
    private String ver;
    private String is_deleted;
    private String remark;
    private List<String> noList;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwner_no() {
        return owner_no;
    }

    public void setOwner_no(String owner_no) {
        this.owner_no = owner_no;
    }

    public double getCal_item_rank() {
        return cal_item_rank;
    }

    public void setCal_item_rank(double cal_item_rank) {
        this.cal_item_rank = cal_item_rank;
    }

    public String getCal_item_name() {
        return cal_item_name;
    }

    public void setCal_item_name(String cal_item_name) {
        this.cal_item_name = cal_item_name;
    }

    public String getCal_item_code() {
        return cal_item_code;
    }

    public void setCal_item_code(String cal_item_code) {
        this.cal_item_code = cal_item_code;
    }

    public String getCal_item_note() {
        return cal_item_note;
    }

    public void setCal_item_note(String cal_item_note) {
        this.cal_item_note = cal_item_note;
    }

    public String getDoc_type() {
        return doc_type;
    }

    public void setDoc_type(String doc_type) {
        this.doc_type = doc_type;
    }

    public String getCal_way() {
        return cal_way;
    }

    public void setCal_way(String cal_way) {
        this.cal_way = cal_way;
    }

    public String getFactor1_note() {
        return factor1_note;
    }

    public void setFactor1_note(String factor1_note) {
        this.factor1_note = factor1_note;
    }

    public String getFactor1_value() {
        return factor1_value;
    }

    public void setFactor1_value(String factor1_value) {
        this.factor1_value = factor1_value;
    }

    public String getFactor2_note() {
        return factor2_note;
    }

    public void setFactor2_note(String factor2_note) {
        this.factor2_note = factor2_note;
    }

    public String getFactor2_value() {
        return factor2_value;
    }

    public void setFactor2_value(String factor2_value) {
        this.factor2_value = factor2_value;
    }

    public String getFactor3_note() {
        return factor3_note;
    }

    public void setFactor3_note(String factor3_note) {
        this.factor3_note = factor3_note;
    }

    public String getFactor3_value() {
        return factor3_value;
    }

    public void setFactor3_value(String factor3_value) {
        this.factor3_value = factor3_value;
    }

    public String getFactor4_note() {
        return factor4_note;
    }

    public void setFactor4_note(String factor4_note) {
        this.factor4_note = factor4_note;
    }

    public String getFactor4_value() {
        return factor4_value;
    }

    public void setFactor4_value(String factor4_value) {
        this.factor4_value = factor4_value;
    }

    public String getFactor5_note() {
        return factor5_note;
    }

    public void setFactor5_note(String factor5_note) {
        this.factor5_note = factor5_note;
    }

    public String getFactor5_value() {
        return factor5_value;
    }

    public void setFactor5_value(String factor5_value) {
        this.factor5_value = factor5_value;
    }

    public String getVer() {
        return ver;
    }

    public void setVer(String ver) {
        this.ver = ver;
    }

    public List<String> getNoList() {
        return noList;
    }

    public void setNoList(List<String> noList) {
        this.noList = noList;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public Date getCreated_time() {
        return created_time;
    }

    public void setCreated_time(Date created_time) {
        this.created_time = created_time;
    }

    public Date getUpdated_time() {
        return updated_time;
    }

    public void setUpdated_time(Date updated_time) {
        this.updated_time = updated_time;
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public String getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(String is_deleted) {
        this.is_deleted = is_deleted;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
