package com.bms.service.model.bd;

import com.base.common.core.model.Model;
import oracle.sql.TIMESTAMP;

/**
 * Created by jsc on 2018/11/12.
 */
public class FeeResultOwnerMonthlyModel extends Model {
    private static final long serialVersionUID = 1L;
    private String id;
    private String owner_no;
    private String whs_no;
    private String fee_type;
    private double fee_result;
    private String year_str;
    private String month_str;
    private int status;
    private int is_confirmed;
    private TIMESTAMP confirmed_time;
    private String remark;
    private TIMESTAMP created_time;
    private String created_by;
    private TIMESTAMP updated_time;
    private String updated_by;
    private int ver;
    private int is_deleted;
    //年份
    private String year;
    //月份
    private String month;

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwner_no() {
        return owner_no;
    }

    public void setOwner_no(String owner_no) {
        this.owner_no = owner_no;
    }

    public String getWhs_no() {
        return whs_no;
    }

    public void setWhs_no(String whs_no) {
        this.whs_no = whs_no;
    }

    public String getFee_type() {
        return fee_type;
    }

    public void setFee_type(String fee_type) {
        this.fee_type = fee_type;
    }

    public double getFee_result() {
        return fee_result;
    }

    public void setFee_result(double fee_result) {
        this.fee_result = fee_result;
    }

    public String getYear_str() {
        return year_str;
    }

    public void setYear_str(String year_str) {
        this.year_str = year_str;
    }

    public String getMonth_str() {
        return month_str;
    }

    public void setMonth_str(String month_str) {
        this.month_str = month_str;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getIs_confirmed() {
        return is_confirmed;
    }

    public void setIs_confirmed(int is_confirmed) {
        this.is_confirmed = is_confirmed;
    }

    public TIMESTAMP getConfirmed_time() {
        return confirmed_time;
    }

    public void setConfirmed_time(TIMESTAMP confirmed_time) {
        this.confirmed_time = confirmed_time;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public TIMESTAMP getCreated_time() {
        return created_time;
    }

    public void setCreated_time(TIMESTAMP created_time) {
        this.created_time = created_time;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public TIMESTAMP getUpdated_time() {
        return updated_time;
    }

    public void setUpdated_time(TIMESTAMP updated_time) {
        this.updated_time = updated_time;
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public int getVer() {
        return ver;
    }

    public void setVer(int ver) {
        this.ver = ver;
    }

    public int getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(int is_deleted) {
        this.is_deleted = is_deleted;
    }
}
