package com.bms.service.profiles.dao.bd;

import com.base.common.core.dao.IBaseDao;
import com.bms.service.dto.bd.FeeResultStorageDto;
import com.bms.service.model.bd.FeeResultStorageModel;
import com.bms.service.vo.bd.FeeResultStorageVo;

import java.util.List;

public interface IFeeResultStorageDao extends IBaseDao<FeeResultStorageModel> {
    //获取条件
    FeeResultStorageVo getCalWay(String owner_no);
    //按零捡或者存储
    Double getTray(String owner_no,String type);
    //获取件数的数量
    Double getQty(String owner_no,String fee_category);
    //导出获取list(不分页)
    List<FeeResultStorageModel> getExcelModelList(FeeResultStorageDto feeResultStorageDto) throws Exception;

}
