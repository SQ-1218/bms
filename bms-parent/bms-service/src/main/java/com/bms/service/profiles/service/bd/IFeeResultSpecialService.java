package com.bms.service.profiles.service.bd;

import com.base.common.core.service.IBaseService;
import com.bms.common.table.vo.DataTablesVO;
import com.bms.service.dto.bd.FeeResultSpecialDto;
import com.bms.service.dto.bd.FeeRuleSpecialDto;
import com.bms.service.model.bd.FeeResultSpecialModel;
import com.bms.service.model.bd.FeeRuleSpecialModel;
import com.bms.service.vo.bd.FeeRuleSpecialCalVo;
import com.bms.service.vo.bd.FeeRuleSpecialItemVo;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.List;

public interface IFeeResultSpecialService extends IBaseService<FeeResultSpecialModel> {
    DataTablesVO<FeeResultSpecialModel> getFeeResultSpecialList(FeeResultSpecialDto feeResultSpecialDto) throws Exception;
    int batchDeleteByIds(List<String> list)throws Exception;
    //导出
    XSSFWorkbook exportExcelInfo(FeeResultSpecialDto feeResultSpecialDto) throws Exception;
    //算费用
    List<FeeResultSpecialModel> getFee()throws Exception;
}
