package com.bms.service.dto.bd;

import com.bms.common.table.vo.BmsBaseDto;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by jsc on 2018/10/16.
 */
public class FeeResultStorageFeeDto extends BmsBaseDto {
    private static final long serialVersionUID = 1L;
    private Double fee;
    private Timestamp fee_occur_time;
    private List<String> owner_noList;
    private Integer is_deleted;
    //开始时间
    private String start_time;
    private String end_time;
    private String owner_no;
    private Integer is_confirmed;
    //按月
    private String month_time;

    public String getMonth_time() {
        return month_time;
    }

    public void setMonth_time(String month_time) {
        this.month_time = month_time;
    }

    public Integer getIs_confirmed() {
        return is_confirmed;
    }

    public void setIs_confirmed(Integer is_confirmed) {
        this.is_confirmed = is_confirmed;
    }

    public String getOwner_no() {
        return owner_no;
    }

    public void setOwner_no(String owner_no) {
        this.owner_no = owner_no;
    }

    public Double getFee() {
        return fee;
    }

    public void setFee(Double fee) {
        this.fee = fee;
    }

    public Timestamp getFee_occur_time() {
        return fee_occur_time;
    }

    public void setFee_occur_time(Timestamp fee_occur_time) {
        this.fee_occur_time = fee_occur_time;
    }

    public List<String> getOwner_noList() {
        return owner_noList;
    }

    public void setOwner_noList(List<String> owner_noList) {
        this.owner_noList = owner_noList;
    }

    public Integer getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(Integer is_deleted) {
        this.is_deleted = is_deleted;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

}
