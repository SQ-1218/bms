package com.bms.service.vo.bd;

public class FeeResultStorageVo {
    private double qty;
    private double weight;
    private double volume;
    private double tray;
    private String no;
    public double getQty() {
        return qty;
    }

    public void setQty(double qty) {
        this.qty = qty;
    }


    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }

    public double getTray() {
        return tray;
    }

    public void setTray(double tray) {
        this.tray = tray;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }
}
