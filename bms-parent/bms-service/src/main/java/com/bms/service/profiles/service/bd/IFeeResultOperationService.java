package com.bms.service.profiles.service.bd;

import com.base.common.core.service.IBaseService;
import com.bms.common.table.vo.DataTablesVO;
import com.bms.service.dto.bd.FeeResultOperationDto;
import com.bms.service.dto.bd.FeeResultSpecialDto;
import com.bms.service.model.bd.FeeResultOperationModel;
import com.bms.service.vo.bd.FeeRuleOperDeliveryItemVo;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.util.List;

public interface IFeeResultOperationService extends IBaseService<FeeResultOperationModel> {
    //获取规则中获取所有数据
    List<FeeResultOperationModel> getFee() throws Exception;
    //获取所有操作费用
    List<FeeRuleOperDeliveryItemVo> getOpeartionItem() throws Exception;
    DataTablesVO<FeeResultOperationModel> getFeeResultOperationList(FeeResultOperationDto feeResultOperationDto) throws Exception;
    //导出
    XSSFWorkbook exportExcelInfo(FeeResultOperationDto feeResultOperationDto) throws Exception;

}
