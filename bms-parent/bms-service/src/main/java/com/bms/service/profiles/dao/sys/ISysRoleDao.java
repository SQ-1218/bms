package com.bms.service.profiles.dao.sys;

import com.base.common.core.dao.IBaseDao;
import com.bms.service.model.sys.SysRoleModel;

/**
 * <b>description</b>：角色信息数据访问接口<br>
 * <b>time</b>：2018-11-15 11:05:43 <br>
 * <b>author</b>：  zhaochen
 */
public interface ISysRoleDao extends IBaseDao<SysRoleModel>{

}