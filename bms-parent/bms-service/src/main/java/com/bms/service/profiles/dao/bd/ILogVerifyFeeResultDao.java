package com.bms.service.profiles.dao.bd;

import com.base.common.core.dao.IBaseDao;

import com.bms.service.dto.bd.LogChangeOwnerBalanceDto;
import com.bms.service.model.bd.LogVerifyFeeResultModel;

import java.util.List;

/**
 * Created by jsc on 2019/1/11.
 */
public interface ILogVerifyFeeResultDao extends IBaseDao<LogVerifyFeeResultModel> {

}
