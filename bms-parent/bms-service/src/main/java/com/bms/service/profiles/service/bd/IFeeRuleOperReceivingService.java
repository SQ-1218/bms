package com.bms.service.profiles.service.bd;

import com.base.common.core.service.IBaseService;
import com.bms.common.table.vo.DataTablesVO;
import com.bms.service.dto.bd.FeeRuleOperReceivingDto;
import com.bms.service.model.bd.FeeRuleOperReceivingModel;
import com.bms.service.vo.bd.FeeRuleOperReceiDocVo;
import com.bms.service.vo.bd.FeeRuleOperReceiVo;
import com.bms.service.vo.bd.FeeRuleOperReceivingWayVo;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.List;

/**
 * Created by jsc on 2018/8/27.
 */
public interface IFeeRuleOperReceivingService extends IBaseService<FeeRuleOperReceivingModel> {
    DataTablesVO<FeeRuleOperReceivingModel> getReceivingList(FeeRuleOperReceivingDto feeRuleOperReceivingDto) throws Exception;
    //获取入库计费项目名称
    List<FeeRuleOperReceiVo> getReceiving()throws Exception;
    //根据id批量删除
    int delectByIds(List<String> list)throws Exception;
    //导入
    void importExcelInfo(InputStream in, MultipartFile file)throws Exception;
    //导出
    XSSFWorkbook exportExcelInfo(FeeRuleOperReceivingDto feeRuleOperReceivingDto) throws Exception;
    //下载
    XSSFWorkbook downloadExcelInfo(FeeRuleOperReceivingDto feeRuleOperReceivingDto) throws Exception;
    //获取计费项目序号
    int getReceivingNumber(String owner_no) throws Exception;
    //计费项目方式
    List<FeeRuleOperReceivingWayVo> getReceivingWay() throws Exception;
    //获取入库单据类型
    List<FeeRuleOperReceiDocVo> getReceivingDoc() throws Exception;
}
