package com.bms.service.profiles.service.bd;

import com.base.common.core.service.IBaseService;
import com.bms.common.table.vo.DataTablesVO;
import com.bms.service.dto.bd.FeeResultExpressDto;
import com.bms.service.model.bd.FeeResultExpressModel;
import com.bms.service.vo.bd.CarrierVo;
import com.bms.service.vo.bd.ProvinceVo;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.util.List;

/**
 * Created by jsc on 2018/9/12.
 */
public interface IFeeResultExpressService extends IBaseService<FeeResultExpressModel> {
    //定时任务
    List<FeeResultExpressModel> getFee() throws Exception;
    DataTablesVO<FeeResultExpressModel> getExpressList(FeeResultExpressDto feeResultExpressDto) throws Exception;
    //获取快递公司
    List<CarrierVo> getCarrier() throws Exception;
    //获取省份
    List<ProvinceVo> getProvince() throws Exception;
    //导出
    XSSFWorkbook exportExcelInfo(FeeResultExpressDto feeResultExpressDto) throws Exception;
}
