package com.bms.service.profiles.dao.bd;

import com.base.common.core.dao.IBaseDao;
import com.bms.service.model.bd.FeeResultOperationdeliveryModel;
import com.bms.service.vo.bd.FeeResultOperCountVo;
import com.bms.service.vo.bd.FeeResultOperaVo;
import com.bms.service.vo.bd.FeeResultOperationFeeVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by jsc on 2018/9/18.
 */
public interface IFeeResultOperationdeliveryDao extends IBaseDao<FeeResultOperationdeliveryModel> {
    //获取出库计算数据
	@Deprecated
    FeeResultOperaVo getdelivery(String dno);

    //获取商品种类
	@Deprecated
    FeeResultOperCountVo getCount(String dno);

    //获取所有符合规则（户主，type）订单的编号
	@Deprecated
    List<String> delivery_Listno(@Param("owner_no") String owner_no, @Param("doc_type") String doc_type);

    //数量从小到大排序到1...n，取4到n的件数之和
	@Deprecated
    FeeResultOperCountVo getSumQty(String no);

    //owner_no---户主 doc_type--类型
    // delivered---正常单  cancelled--取消单 no-秒杀单
    // factor1_value--单价 factor2_value--件数
    // factor3_value--商品种类
    // 普遍出库factor4_value--件型
    //大单factor4_value--首件
    //获取按单基础出库费//获取订单编号 类型 费用
    List<FeeResultOperationFeeVo> getBasicsUnitFees(@Param("owner_no") String owner_no, @Param("doc_type") String doc_type, @Param("delivered") int delivered, @Param("factor1_value") double factor1_value);

    //获取按件基础出库订单编号 类型 费用
    List<FeeResultOperationFeeVo> getBasicsPieceFees(@Param("owner_no") String owner_no, @Param("doc_type") String doc_type, @Param("delivered") int delivered, @Param("factor1_value") double factor1_value, @Param("factor2_value") double factor2_value);

    //获取按单秒杀出库订单编号 类型 费用
    List<FeeResultOperationFeeVo> getSpikeUnitFees(@Param("owner_no") String owner_no, @Param("doc_type") String doc_type, @Param("delivered") int delivered, @Param("no") int no, @Param("factor1_value") double factor1_value);

    //获取按件秒杀出库订单编号 类型 费用
    List<FeeResultOperationFeeVo> getSpikePieceFees(@Param("owner_no") String owner_no, @Param("doc_type") String doc_type, @Param("delivered") int delivered, @Param("no") int no, @Param("factor1_value") double factor1_value, @Param("factor2_value") double factor2_value);

    //获取按单取消出库订单编号 类型 费用
    List<FeeResultOperationFeeVo> getCancelUnitFees(@Param("owner_no") String owner_no, @Param("doc_type") String doc_type, @Param("cancelled") int cancelled, @Param("factor1_value") double factor1_value);

    //获取按件取消出库订单编号 类型 费用
    List<FeeResultOperationFeeVo> getCancelPieceFees(@Param("owner_no") String owner_no, @Param("doc_type") String doc_type, @Param("cancelled") int cancelled, @Param("factor1_value") double factor1_value, @Param("factor2_value") double factor2_value);

    //获取大单出库出库订单编号 类型 费用
    List<FeeResultOperationFeeVo> getLargePieceFees(@Param("owner_no") String owner_no, @Param("doc_type") String doc_type, @Param("delivered") int delivered, @Param("factor1_value") double factor1_value, @Param("factor2_value") double factor2_value, @Param("factor3_value") double factor3_value, @Param("factor4_value") double factor4_value);

    //获取大单取消出库订单编号 类型 费用
    List<FeeResultOperationFeeVo> getLargeCancelFees(@Param("owner_no") String owner_no, @Param("doc_type") String doc_type, @Param("cancelled") int cancelled, @Param("factor1_value") double factor1_value, @Param("factor2_value") double factor2_value, @Param("factor3_value") double factor3_value, @Param("factor4_value") double factor4_value);
}
