package com.bms.service.profiles.service.bd;


import com.base.common.core.service.IBaseService;
import com.bms.common.table.vo.DataTablesVO;
import com.bms.service.dto.bd.FeeRuleStorageDto;
import com.bms.service.model.bd.FeeRuleStorageModel;
import com.bms.service.vo.bd.FeeRuleStorageWayVo;
import com.bms.service.vo.bd.FeeRuleStoragenameVo;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.List;

/**
 * Created by jsc on 2018/8/23.
 */
public interface IFeeRuleStorageService extends IBaseService<FeeRuleStorageModel> {
    DataTablesVO<FeeRuleStorageModel> getFeeRuleStorageList(FeeRuleStorageDto feeRuleStorageDto) throws Exception;
    //获取计费的项目名称
    List<FeeRuleStoragenameVo> getStorage() throws Exception;
    //删除
    int delectByIds(List<String> list) throws Exception;
    //导入excel
    void importExcelInfo(InputStream in, MultipartFile file) throws Exception;
    //导出excel
    XSSFWorkbook exportExcelInfo(FeeRuleStorageDto feeRuleStorageDto) throws Exception;
    //下载excel
    XSSFWorkbook downloadExcelInfo(FeeRuleStorageDto feeRuleStorageDto) throws Exception;
    //获取计费方式
    List<FeeRuleStorageWayVo> getStorageWay() throws Exception;
    //获取计费项序号
    int getCount(String owner_no) throws Exception;
    //获取按件收费
    List<FeeRuleStorageWayVo> getCalWayByQty()throws Exception;


}
