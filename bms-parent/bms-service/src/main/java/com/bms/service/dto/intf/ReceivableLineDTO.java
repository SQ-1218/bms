package com.bms.service.dto.intf;

import java.io.Serializable;

public class ReceivableLineDTO implements Serializable {

	private static final long serialVersionUID = -5257582663748883705L;

	/**
	 * 业务类型-子类型
	 */
	private String operationSubTypeCode;
	/**
	 * 区域-明细
	 */
	private String area;
	/**
	 * 模型吨位
	 */
	private String modelTonnage;
	/**
	 * 房间号
	 */
	private String roomNumber;
	/**
	 * 金额————必填
	 */
	private String costAccount;
	/**
	 * 应税货物编码————必填
	 */
	private String taxableGoodsCode;
	/**
	 * 应税货物名称————必填
	 */
	private String taxableGoodsName;
	/**
	 * 备注
	 */
	private String remark;

	public String getOperationSubTypeCode() {
		return operationSubTypeCode;
	}

	public void setOperationSubTypeCode(String operationSubTypeCode) {
		this.operationSubTypeCode = operationSubTypeCode;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getModelTonnage() {
		return modelTonnage;
	}

	public void setModelTonnage(String modelTonnage) {
		this.modelTonnage = modelTonnage;
	}

	public String getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}

	public String getCostAccount() {
		return costAccount;
	}

	public void setCostAccount(String costAccount) {
		this.costAccount = costAccount;
	}

	public String getTaxableGoodsCode() {
		return taxableGoodsCode;
	}

	public void setTaxableGoodsCode(String taxableGoodsCode) {
		this.taxableGoodsCode = taxableGoodsCode;
	}

	public String getTaxableGoodsName() {
		return taxableGoodsName;
	}

	public void setTaxableGoodsName(String taxableGoodsName) {
		this.taxableGoodsName = taxableGoodsName;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}
