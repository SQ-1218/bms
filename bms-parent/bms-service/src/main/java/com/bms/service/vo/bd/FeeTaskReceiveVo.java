package com.bms.service.vo.bd;

public class FeeTaskReceiveVo {
    private String owner_no;
    private String doc_no;
    private String cal_way;
    private String fee_category;
    private double qty;
    public String getOwner_no() {
        return owner_no;
    }

    public void setOwner_no(String owner_no) {
        this.owner_no = owner_no;
    }

    public String getDoc_no() {
        return doc_no;
    }

    public void setDoc_no(String doc_no) {
        this.doc_no = doc_no;
    }

    public String getCal_way() {
        return cal_way;
    }

    public void setCal_way(String cal_way) {
        this.cal_way = cal_way;
    }

    public String getFee_category() {
        return fee_category;
    }

    public void setFee_category(String fee_category) {
        this.fee_category = fee_category;
    }

    public double getQty() {
        return qty;
    }

    public void setQty(double qty) {
        this.qty = qty;
    }
}
