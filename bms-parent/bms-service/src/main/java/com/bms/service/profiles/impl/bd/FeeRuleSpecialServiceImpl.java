package com.bms.service.profiles.impl.bd;

import com.base.common.core.base.DTO;
import com.base.common.core.dao.IBaseDao;
import com.base.common.core.pager.service.IPagerService;
import com.base.common.core.service.impl.BaseServiceImpl;
import com.bms.common.util.ExcelUtil;
import com.bms.common.table.vo.AmazuePagerUtil;
import com.bms.common.table.vo.DataTablesVO;
import com.bms.common.table.vo.ExcelBean;
import com.bms.service.dto.bd.FeeRuleSpecialDto;
import com.bms.service.model.bd.FeeRuleSpecialModel;
import com.bms.service.profiles.dao.bd.IFeeRuleSpecialDao;
import com.bms.service.profiles.service.bd.IFeeRuleSpecialService;
import com.bms.service.vo.bd.FeeRuleSpecialCalVo;
import com.bms.service.vo.bd.FeeRuleSpecialItemVo;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class FeeRuleSpecialServiceImpl  extends BaseServiceImpl<FeeRuleSpecialModel> implements IFeeRuleSpecialService {
    @Resource private IFeeRuleSpecialDao feeRuleSpecialDao;
    @Override
    protected IBaseDao<FeeRuleSpecialModel> getBaseDao() {
        return this.feeRuleSpecialDao;
    }

    @Override
    public DataTablesVO<FeeRuleSpecialModel> getFeeRuleSpecialList(FeeRuleSpecialDto feeRuleSpecialDto) throws Exception {
        return AmazuePagerUtil.getPagerModel(new IPagerService<FeeRuleSpecialModel>() {
            @Override
            public int queryCount(DTO dto) throws Exception {
                return feeRuleSpecialDao.getModelListCount(dto);
            }
            @Override
            public List<FeeRuleSpecialModel> queryList(DTO dto) throws Exception {
                return feeRuleSpecialDao.getModelList(dto);
            }
        }, feeRuleSpecialDto);
    }

    @Override
    public List<FeeRuleSpecialItemVo> getFeeRuleSpecialItem() throws Exception {
        return feeRuleSpecialDao.getFeeRuleSpecialItem();
    }

    @Override
    public List<FeeRuleSpecialCalVo> getFeeRuleSpecialCal() throws Exception {
        return feeRuleSpecialDao.getFeeRuleSpecialCal();
    }

    @Override
    public int batchDeleteByIds(List<String> list) throws Exception {
        int result = 0;
        result = feeRuleSpecialDao.batchDeleteByIds(list);
        if (result <= 0) {
            throw new Exception("删除失败，请重试！");
        }
        return result;
    }

    @Override
    public int getFeeRuleSpecialCount(String owner_no) throws Exception {
        return feeRuleSpecialDao.getFeeRuleSpecialCount(owner_no);
    }

    @Override
    public int importExcelInfo(InputStream in, MultipartFile file) {
        int status=0;
        try {
            List<List<Object>> listOb = ExcelUtil.getBankListByExcel(in,file.getOriginalFilename());
            List<FeeRuleSpecialModel> feeRuleSpecialModelList=new ArrayList<>();
            //遍历listob数据，把数据放到List中
            for (int i = 0; i < listOb.size(); i++) {
                //通过遍历实现把每一列封装成一个model中，再把所有的model用List集合装载
                List<Object> ob = listOb.get(i);
                FeeRuleSpecialModel feeRuleSpecialModel = new FeeRuleSpecialModel();
                //设置货主
                feeRuleSpecialModel.setOwner_no(String.valueOf(ob.get(0)));
                //获取序号
                int count=feeRuleSpecialDao.getFeeRuleSpecialCount(String.valueOf(ob.get(0)));
                if(i>0) {
                    List<Object>  ob1 = listOb.get(i - 1);
                    if(String.valueOf(ob1.get(0)).equals(String.valueOf(ob.get(0)))){
                        double b = feeRuleSpecialModelList.get(i - 1).getCal_item_rank();
                        b++;
                        feeRuleSpecialModel.setCal_item_rank(b);
                    }else{
                        feeRuleSpecialModel.setCal_item_rank((count+1));
                    }
                }else{
                    feeRuleSpecialModel.setCal_item_rank((count+1));
                }
                //计费名称
                feeRuleSpecialModel.setCal_item_name(String.valueOf(ob.get(1)));
                //计费code
                feeRuleSpecialModel.setCal_item_code(String.valueOf(ob.get(2)));
                //计费note
                feeRuleSpecialModel.setCal_item_note(String.valueOf(ob.get(3)));
                //计算方式
                feeRuleSpecialModel.setCal_type(String.valueOf(ob.get(4)));
                //计算系数1-9
                feeRuleSpecialModel.setFactor1_note(String.valueOf(ob.get(5)));
                feeRuleSpecialModel.setFactor1_value(String.valueOf(ob.get(6)));
                feeRuleSpecialModel.setFactor2_note(String.valueOf(ob.get(7)));
                feeRuleSpecialModel.setFactor2_value(String.valueOf(ob.get(8)));
                feeRuleSpecialModel.setFactor3_note(String.valueOf(ob.get(9)));
                feeRuleSpecialModel.setFactor3_value(String.valueOf(ob.get(10)));
                feeRuleSpecialModel.setFactor4_note(String.valueOf(ob.get(11)));
                feeRuleSpecialModel.setFactor4_value(String.valueOf(ob.get(12)));
                feeRuleSpecialModel.setFactor5_note(String.valueOf(ob.get(13)));
                feeRuleSpecialModel.setFactor5_value(String.valueOf(ob.get(14)));
                feeRuleSpecialModel.setFactor6_note(String.valueOf(ob.get(15)));
                feeRuleSpecialModel.setFactor6_value(String.valueOf(ob.get(16)));
                feeRuleSpecialModel.setFactor7_note(String.valueOf(ob.get(17)));
                feeRuleSpecialModel.setFactor7_value(String.valueOf(ob.get(18)));
                feeRuleSpecialModel.setFactor8_note(String.valueOf(ob.get(19)));
                feeRuleSpecialModel.setFactor8_value(String.valueOf(ob.get(20)));
                feeRuleSpecialModel.setFactor9_note(String.valueOf(ob.get(21)));
                feeRuleSpecialModel.setFactor9_value(String.valueOf(ob.get(22)));
                feeRuleSpecialModel.setRemark(String.valueOf(ob.get(23)));
                feeRuleSpecialModel.setCreated_by("admin");
                feeRuleSpecialModelList.add(feeRuleSpecialModel);
            }
            //处理modelList
            //批量导入
            if(feeRuleSpecialModelList.size()>0){
                feeRuleSpecialDao.insertBatch(feeRuleSpecialModelList);
                status=1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    @Override
    public XSSFWorkbook downloadExcelInfo(FeeRuleSpecialDto feeRuleSpecialDto) throws Exception {
        //获取计算项编码和计算方式
        List<FeeRuleSpecialItemVo> itemVoList=feeRuleSpecialDao.getFeeRuleSpecialItem();
        String item="";
        for(FeeRuleSpecialItemVo itemVo :itemVoList){
            item+=itemVo.getCode()+":"+itemVo.getName()+",";
        }
        item=item.substring(0,item.length()-1);
        List<FeeRuleSpecialCalVo> calVoList=feeRuleSpecialDao.getFeeRuleSpecialCal();
        String cal="";
        for(FeeRuleSpecialCalVo calVo:calVoList){
            cal+=calVo.getCode()+":"+calVo.getName()+",";
        }
        cal=cal.substring(0,cal.length()-1);
        List<FeeRuleSpecialDto> list=new ArrayList<>();
        list.add(feeRuleSpecialDto);
        List<ExcelBean> excel=new ArrayList<>();
        Map<Integer,List<ExcelBean>> map=new LinkedHashMap<>();
        XSSFWorkbook xssfWorkbook=null;
        //设置标题栏
        excel.add(new ExcelBean("货主","owner_no",0));
        excel.add(new ExcelBean("计费项名称","cal_item_name",0));
        excel.add(new ExcelBean("计费项编码("+item+")","cal_item_code",0));
        excel.add(new ExcelBean("计费项解释","cal_item_note",0));
        excel.add(new ExcelBean("计算方式编码("+cal+")","cal_type",0));
        excel.add(new ExcelBean("计算系数1","factor1_note",0));
        excel.add(new ExcelBean("计算系数1的值","factor1_value",0));
        excel.add(new ExcelBean("计算系数2","factor2_note",0));
        excel.add(new ExcelBean("计算系数2的值","factor2_value",0));
        excel.add(new ExcelBean("计算系数3","factor3_note",0));
        excel.add(new ExcelBean("计算系数3的值","factor3_value",0));
        excel.add(new ExcelBean("计算系数4","factor4_note",0));
        excel.add(new ExcelBean("计算系数4的值","factor4_value",0));
        excel.add(new ExcelBean("计算系数5","factor5_note",0));
        excel.add(new ExcelBean("计算系数5的值","factor5_value",0));
        excel.add(new ExcelBean("计算系数6","factor5_note",0));
        excel.add(new ExcelBean("计算系数6的值","factor5_value",0));
        excel.add(new ExcelBean("计算系数7","factor5_note",0));
        excel.add(new ExcelBean("计算系数7的值","factor5_value",0));
        excel.add(new ExcelBean("计算系数8","factor5_note",0));
        excel.add(new ExcelBean("计算系数8的值","factor5_value",0));
        excel.add(new ExcelBean("计算系数9","factor5_note",0));
        excel.add(new ExcelBean("计算系数9的值","factor5_value",0));
        excel.add(new ExcelBean("备注","remark",0));
        map.put(0, excel);
        String sheetName = "特殊费用配置规则";
        //调用ExcelUtil的方法
        xssfWorkbook = ExcelUtil.createExcelFile(FeeRuleSpecialDto.class, list, map, sheetName);
        return xssfWorkbook;
    }

    @Override
    public XSSFWorkbook exportExcelInfo(FeeRuleSpecialDto feeRuleSpecialDto) throws Exception {
        List<FeeRuleSpecialModel> list=feeRuleSpecialDao.getExcelModelList(feeRuleSpecialDto);
        List<ExcelBean> excel=new ArrayList<>();
        Map<Integer,List<ExcelBean>> map=new LinkedHashMap<>();
        XSSFWorkbook xssfWorkbook=null;
        //设置标题栏
        excel.add(new ExcelBean("货主","owner_no",0));
        excel.add(new ExcelBean("计费项名称","cal_item_name",0));
        excel.add(new ExcelBean("计费项解释","cal_item_note",0));
        excel.add(new ExcelBean("计算方式","cal_type",0));
        excel.add(new ExcelBean("计算系数1","factor1_note",0));
        excel.add(new ExcelBean("计算系数1的值","factor1_value",0));
        excel.add(new ExcelBean("计算系数2","factor2_note",0));
        excel.add(new ExcelBean("计算系数2的值","factor2_value",0));
        excel.add(new ExcelBean("计算系数3","factor3_note",0));
        excel.add(new ExcelBean("计算系数3的值","factor3_value",0));
        excel.add(new ExcelBean("计算系数4","factor4_note",0));
        excel.add(new ExcelBean("计算系数4的值","factor4_value",0));
        excel.add(new ExcelBean("计算系数5","factor5_note",0));
        excel.add(new ExcelBean("计算系数5的值","factor5_value",0));
        excel.add(new ExcelBean("备注","remark",0));
        map.put(0, excel);
        String sheetName = "出库费用配置规则";
        //调用ExcelUtil的方法
        xssfWorkbook = ExcelUtil.createExcelFile(FeeRuleSpecialModel.class, list, map, sheetName);
        return xssfWorkbook;
    }
}
