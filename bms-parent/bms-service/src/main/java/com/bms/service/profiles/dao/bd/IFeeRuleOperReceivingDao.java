package com.bms.service.profiles.dao.bd;

import com.base.common.core.dao.IBaseDao;
import com.bms.service.dto.bd.FeeRuleOperReceivingDto;
import com.bms.service.model.bd.FeeRuleOperReceivingModel;
import com.bms.service.vo.bd.FeeRuleOperReceiDocVo;
import com.bms.service.vo.bd.FeeRuleOperReceiVo;
import com.bms.service.vo.bd.FeeRuleOperReceivingWayVo;

import java.util.List;

/**
 * Created by jsc on 2018/8/27.
 */
public interface IFeeRuleOperReceivingDao extends IBaseDao<FeeRuleOperReceivingModel> {
    //获取入库计费项目名称
    List<FeeRuleOperReceiVo> getReceiving();
    //批量删除
    int delectByIds(List<String> list);
    //不分页
    List<FeeRuleOperReceivingModel> getModelListexcel(FeeRuleOperReceivingDto feeRuleOperReceivingDto);
    //获取计费项目序号
    int getReceivingNumber(String owner_no);
    //计费项目方式
    List<FeeRuleOperReceivingWayVo> getReceivingWay();
    //获取入库单据类型
    List<FeeRuleOperReceiDocVo> getReceivingDoc();
    //查询数据
    List<FeeRuleOperReceivingModel> getFeeRuleOperReceivingList(FeeRuleOperReceivingDto feeRuleOperReceivingDto);
}
