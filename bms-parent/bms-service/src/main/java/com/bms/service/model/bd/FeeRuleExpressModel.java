package com.bms.service.model.bd;

import com.base.common.core.model.Model;

import java.sql.Date;

//快递配置model
public class FeeRuleExpressModel extends Model {
    /**
     * @Fields serialVersionUID
     */
    private static final long serialVersionUID = 1L;
     private String id;
     private String owner_no;
     private String whs_no;
     private String express_corp_no;
     private String province;
    private Double weight_fm;
    private Double weight_to;
    private Double fee_fixed;
    private Double fee_extra;
    private Date created_time;
    private String created_by;
    private Date updated_time;
    private String updated_by;
    private String ver;
    private String is_deleted;

    public Double getFee_fixed() { return fee_fixed; }

    public void setFee_fixed(Double fee_fixed) { this.fee_fixed = fee_fixed; }

    public Double getFee_extra() { return fee_extra; }

    public void setFee_extra(Double fee_extra) { this.fee_extra = fee_extra; }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwner_no() {
        return owner_no;
    }

    public void setOwner_no(String owner_no) {
        this.owner_no = owner_no;
    }

    public String getExpress_corp_no() {
        return express_corp_no;
    }

    public void setExpress_corp_no(String express_corp_no) {
        this.express_corp_no = express_corp_no;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public Double getWeight_fm() {
        return weight_fm;
    }

    public void setWeight_fm(Double weight_fm) {
        this.weight_fm = weight_fm;
    }

    public Double getWeight_to() {
        return weight_to;
    }

    public void setWeight_to(Double weight_to) {
        this.weight_to = weight_to;
    }

    public String getVer() {
        return ver;
    }

    public void setVer(String ver) {
        this.ver = ver;
    }

    public Date getCreated_time() {
        return created_time;
    }

    public void setCreated_time(Date created_time) {
        this.created_time = created_time;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public Date getUpdated_time() {
        return updated_time;
    }

    public void setUpdated_time(Date updated_time) {
        this.updated_time = updated_time;
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public String getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(String is_deleted) {
        this.is_deleted = is_deleted;
    }

    public String getWhs_no() {

        return whs_no;
    }

    public void setWhs_no(String whs_no) {
        this.whs_no = whs_no;
    }
}
