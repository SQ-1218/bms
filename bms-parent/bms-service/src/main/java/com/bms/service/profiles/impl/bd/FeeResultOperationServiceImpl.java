package com.bms.service.profiles.impl.bd;

import com.base.common.core.base.DTO;
import com.base.common.core.dao.IBaseDao;
import com.base.common.core.pager.service.IPagerService;
import com.base.common.core.service.impl.BaseServiceImpl;
import com.bms.common.table.vo.AmazuePagerUtil;
import com.bms.common.table.vo.DataTablesVO;
import com.bms.common.table.vo.ExcelBean;
import com.bms.common.util.ExcelUtil;
import com.bms.service.dto.bd.FeeResultOperationDto;
import com.bms.service.dto.bd.FeeRuleOperReceivingDto;
import com.bms.service.model.bd.FeeResultOperationModel;
import com.bms.service.model.bd.FeeResultSpecialModel;
import com.bms.service.model.bd.FeeRuleOperReceivingModel;
import com.bms.service.profiles.dao.bd.IBdCargoOwnerDao;
import com.bms.service.profiles.dao.bd.IFeeResultOperationDao;
import com.bms.service.profiles.dao.bd.IFeeRuleOperReceivingDao;
import com.bms.service.profiles.service.bd.IFeeResultOperationService;
import com.bms.service.vo.bd.BdCargoOwnerVo;
import com.bms.service.vo.bd.FeeResultStorageVo;
import com.bms.service.vo.bd.FeeRuleOperDeliveryItemVo;
import com.bms.service.vo.bd.FeeTaskReceiveVo;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class FeeResultOperationServiceImpl extends BaseServiceImpl<FeeResultOperationModel> implements IFeeResultOperationService {
    @Resource
    private IFeeResultOperationDao feeResultOperationDao;
    @Resource
    private IFeeRuleOperReceivingDao feeRuleOperReceivingDao;
    @Resource
    private IBdCargoOwnerDao bdCargoOwnerDao;
    @Override
    protected IBaseDao<FeeResultOperationModel> getBaseDao() {
        return feeResultOperationDao;
    }

    @Override
    public List<FeeResultOperationModel> getFee() throws Exception {
        //获取所有货主的规则
        FeeRuleOperReceivingDto feeRuleOperReceivingDto = new FeeRuleOperReceivingDto();
        feeRuleOperReceivingDto.setIs_deleted(1.0);
        List<FeeRuleOperReceivingModel> list = feeRuleOperReceivingDao.getFeeRuleOperReceivingList(feeRuleOperReceivingDto);
        //获取在审核时间内的Fee_TASK_RECEIVE的数据
        List<FeeTaskReceiveVo> feeTaskReceiveVoList=feeResultOperationDao.getFeeTaskReceive();
        if(feeTaskReceiveVoList.size()>0){
        for(FeeTaskReceiveVo feeTaskReceiveVo:feeTaskReceiveVoList){
         FeeResultOperationModel feeResultOperationModel = new FeeResultOperationModel();
         //根据当前的货主和计算方式(和件型)去规则表里去读取单价(和其他参数)
         FeeRuleOperReceivingModel feeRuleOperReceivingModel=feeResultOperationDao.getFeeByTask(feeTaskReceiveVo);
         //计算价格
          double result=Double.parseDouble(feeRuleOperReceivingModel.getFactor1_value())*feeTaskReceiveVo.getQty();
         //存入数据库
            Date date = new Date();
            Timestamp ts = new Timestamp(date.getTime());
            feeResultOperationModel.setOwner_no(feeTaskReceiveVo.getOwner_no());
            feeResultOperationModel.setCal_item_code(feeRuleOperReceivingModel.getCal_item_code());
            feeResultOperationModel.setCal_item_name(feeRuleOperReceivingModel.getCal_item_name());
            feeResultOperationModel.setCal_item_rank(feeRuleOperReceivingModel.getCal_item_rank());
            feeResultOperationModel.setCal_way(feeTaskReceiveVo.getCal_way());
            feeResultOperationModel.setDoc_type("0");
            feeResultOperationModel.setFee(result);
            feeResultOperationModel.setDoc_no(feeTaskReceiveVo.getDoc_no());
            feeResultOperationModel.setFee_occur_time(ts);
            feeResultOperationModel.setCreated_by("admin");
            feeResultOperationModel.setCreated_time(ts);
            //插入数据库
            feeResultOperationDao.insert(feeResultOperationModel);
        }
        }else {
            //通过计费规则去判断所用计费方式
            for (FeeRuleOperReceivingModel feeRuleOperReceivingModel : list) {
                FeeResultOperationModel feeResultOperationModel = new FeeResultOperationModel();
                //通过计费方式去获取不同的数据(体积，数量，重量，箱数)
                String calWay = feeRuleOperReceivingModel.getCal_way();
                //获取所有符合规则的订单编号
          List<String> doc_noList = feeResultOperationDao.doc_noList(feeRuleOperReceivingModel.getOwner_no(), feeRuleOperReceivingModel.getDoc_type());
                for (String doc_no : doc_noList) {
                    FeeResultStorageVo feeResultStorageVo = feeResultOperationDao.getCalWay(doc_no);
                    if (feeResultStorageVo != null) {
                        double result = 1.0;
                        double fee = 1.0;
                        switch (calWay) {
                            case "1":
                                //按max(重量，体积取大)
                                result = Math.max((feeResultStorageVo.getVolume() / Math.pow(1000, 3)), feeResultStorageVo.getWeight());
                                fee = result * Double.parseDouble(feeRuleOperReceivingModel.getFactor1_value());
                                feeResultOperationModel.setFee(fee);
                                break;
                            case "2":
                                //体积
                                result = feeResultStorageVo.getVolume();
                                fee = (result / Math.pow(1000, 3)) * Double.parseDouble(feeRuleOperReceivingModel.getFactor1_value());
                                feeResultOperationModel.setFee(fee);
                                break;
                            case "3":
                                //重量
                                result = feeResultStorageVo.getWeight();
                                fee = result * Double.parseDouble(feeRuleOperReceivingModel.getFactor1_value());
                                feeResultOperationModel.setFee(fee);
                                break;
                            case "4":
                                //件数
                                result = feeResultOperationDao.getQty(feeRuleOperReceivingModel.getOwner_no(), feeRuleOperReceivingModel.getFactor2_value());
                                fee = result * Double.parseDouble(feeRuleOperReceivingModel.getFactor1_value());
                                feeResultOperationModel.setFee(fee);
                                break;
                            case "5":
                                //箱数
                                break;
                            default:
                                break;
                        }
                        Date date = new Date();
                        Timestamp ts = new Timestamp(date.getTime());
                        feeResultOperationModel.setOwner_no(feeRuleOperReceivingModel.getOwner_no());
                        feeResultOperationModel.setCal_item_code(feeRuleOperReceivingModel.getCal_item_code());
                        feeResultOperationModel.setCal_item_name(feeRuleOperReceivingModel.getCal_item_name());
                        feeResultOperationModel.setCal_item_rank(feeRuleOperReceivingModel.getCal_item_rank());
                        feeResultOperationModel.setCal_way(feeRuleOperReceivingModel.getCal_way());
                        feeResultOperationModel.setDoc_type("0");
                        feeResultOperationModel.setDoc_no(doc_no);
                        feeResultOperationModel.setFee_occur_time(ts);
                        feeResultOperationModel.setCreated_by("admin");
                        feeResultOperationModel.setCreated_time(ts);
                        //插入数据库
                        feeResultOperationDao.insert(feeResultOperationModel);

                    }
                }
            }
        }
        System.out.println("插入完成");
        return null;
    }

    @Override
    public List<FeeRuleOperDeliveryItemVo> getOpeartionItem() throws Exception {
        return feeResultOperationDao.getOperationItem();
    }

    @Override
    public DataTablesVO<FeeResultOperationModel> getFeeResultOperationList(FeeResultOperationDto feeResultOperationDto) throws Exception {
        return AmazuePagerUtil.getPagerModel(new IPagerService<FeeResultOperationModel>() {
            @Override
            public int queryCount(DTO dto) throws Exception {
                return feeResultOperationDao.getModelListCount(dto);
            }
            @Override
            public List<FeeResultOperationModel> queryList(DTO dto) throws Exception {
                return feeResultOperationDao.getModelList(dto);
            }
        }, feeResultOperationDto);
    }

    @Override
    public XSSFWorkbook exportExcelInfo(FeeResultOperationDto feeResultOperationDto) throws Exception {
        List<FeeResultOperationModel> list=feeResultOperationDao.getExcelModelList(feeResultOperationDto);
        List<ExcelBean> excel=new ArrayList<>();
        Map<Integer,List<ExcelBean>> map=new LinkedHashMap<>();
        XSSFWorkbook xssfWorkbook=null;
        //设置标题栏
        excel.add(new ExcelBean("货主","owner_no",0));
        excel.add(new ExcelBean("计费项名称","cal_item_name",0));
        excel.add(new ExcelBean("计算方式","cal_way",0));
        excel.add(new ExcelBean("单据类型(0为入库,1为出库)","doc_type",0));
        excel.add(new ExcelBean("单据号","doc_no",0));
        excel.add(new ExcelBean("计费结果","fee",0));
        excel.add(new ExcelBean("费用产生时间","fee_occur_time",0));
        excel.add(new ExcelBean("是否已结算(0为结算,1为未结算)","is_confirmed",0));
        excel.add(new ExcelBean("确认结算时间","confirmed_time",0));
        excel.add(new ExcelBean("备注","remark",0));
        map.put(0, excel);
        String sheetName = "操作费用计费结果";
        //调用ExcelUtil的方法
        xssfWorkbook = ExcelUtil.createExcelFile(FeeResultOperationModel.class, list, map, sheetName);
        return xssfWorkbook;
    }
}
