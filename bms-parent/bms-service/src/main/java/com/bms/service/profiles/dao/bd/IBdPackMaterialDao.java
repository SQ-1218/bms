package com.bms.service.profiles.dao.bd;

import com.base.common.core.dao.IBaseDao;
import com.bms.service.dto.bd.BdPackMaterialDto;
import com.bms.service.dto.bd.FeeRuleExpressDto;
import com.bms.service.model.bd.BdPackMaterialModel;
import com.bms.service.model.bd.FeeRuleExpressModel;

import java.util.List;

public interface IBdPackMaterialDao  extends IBaseDao<BdPackMaterialModel> {
    //批量删除
    int batchDeleteByIds(List<String> list);
    //导出获取list(不分页)
    List<BdPackMaterialModel> getExcelModelList(BdPackMaterialDto bdPackMaterialDto) throws Exception;
}
