package com.bms.service.profiles.dao.bd;

import com.base.common.core.dao.IBaseDao;
import com.bms.service.dto.bd.FeeRuleStorageDto;
import com.bms.service.model.bd.FeeRuleStorageModel;
import com.bms.service.vo.bd.FeeRuleStorageVo;
import com.bms.service.vo.bd.FeeRuleStorageWayVo;
import com.bms.service.vo.bd.FeeRuleStoragenameVo;

import java.util.List;

/**
 * Created by jsc on 2018/8/23.
 */
public interface IFeeRuleStorageDao extends IBaseDao<FeeRuleStorageModel> {
  //获取项目名称
  List<FeeRuleStoragenameVo> getStorage();
  //批量删除
  int delectByIds(List<String> list);
  //获取计费项目方式
  List<FeeRuleStorageWayVo> getStorageWay();
  //获取订单号
  int getCount(String owner_no);
  List<FeeRuleStorageModel> getModelListexcel(FeeRuleStorageDto feeRuleStorageDto);
  List<FeeRuleStorageModel> getFeeRuleStorageModelList(FeeRuleStorageDto feeRuleStorageDto);
  //获取数量
  List<FeeRuleStorageWayVo> getCalWayByQty();
}
