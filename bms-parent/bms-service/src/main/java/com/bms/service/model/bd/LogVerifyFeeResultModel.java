package com.bms.service.model.bd;

import com.base.common.core.model.Model;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by jsc on 2019/1/11.
 */
public class LogVerifyFeeResultModel extends Model {
    /**
     * @Fields serialVersionUID
     */
    private static final long serialVersionUID = 1L;
    private String id; //id
    private String cargo_owner_no; //户主
    private String year_str; //年份
    private String month_str; //月份
    private int type;         //类型（0-确认，1-驳回 ）
    private String remark;  //备注
    private String created_by; //创建者
    private Timestamp created_time; //创建时间
    private int ver;
    private int is_deleted;  //是否删除默认1 0为逻辑删除
    private List<String> owner_noList;
    private String year_time; //按年
    private String month_time; //按月
    private String day_time;   //按日

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCargo_owner_no() {
        return cargo_owner_no;
    }

    public void setCargo_owner_no(String cargo_owner_no) {
        this.cargo_owner_no = cargo_owner_no;
    }

    public String getYear_str() {
        return year_str;
    }

    public void setYear_str(String year_str) {
        this.year_str = year_str;
    }

    public String getMonth_str() {
        return month_str;
    }

    public void setMonth_str(String month_str) {
        this.month_str = month_str;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public Timestamp getCreated_time() {
        return created_time;
    }

    public void setCreated_time(Timestamp created_time) {
        this.created_time = created_time;
    }

    public int getVer() {
        return ver;
    }

    public void setVer(int ver) {
        this.ver = ver;
    }

    public int getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(int is_deleted) {
        this.is_deleted = is_deleted;
    }

    public List<String> getOwner_noList() {
        return owner_noList;
    }

    public void setOwner_noList(List<String> owner_noList) {
        this.owner_noList = owner_noList;
    }

    public String getYear_time() {
        return year_time;
    }

    public void setYear_time(String year_time) {
        this.year_time = year_time;
    }

    public String getMonth_time() {
        return month_time;
    }

    public void setMonth_time(String month_time) {
        this.month_time = month_time;
    }

    public String getDay_time() {
        return day_time;
    }

    public void setDay_time(String day_time) {
        this.day_time = day_time;
    }
}
