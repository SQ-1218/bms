package com.bms.service.vo.bd;

import com.base.common.core.base.VO;
import oracle.sql.TIMESTAMP;

/**
 * Created by jsc on 2018/11/16.
 */
public class FeeResultOwnerMonthlyVo extends VO {
    private static final long serialVersionUID = 1L;
    //记录
    private int record;
    //日均托盘量
    private int code;
    //优惠周转率
    private int rate;
    //订单总数
    private int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public int getRecord() {
        return record;
    }

    public void setRecord(int record) {
        this.record = record;
    }
}
