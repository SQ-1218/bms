package com.bms.service.profiles.impl.bd;

import javax.annotation.Resource;

import com.base.common.core.base.DTO;
import com.base.common.core.pager.service.IPagerService;
import com.bms.common.table.vo.AmazuePagerUtil;
import com.bms.common.table.vo.DataTablesVO;
import com.bms.service.dto.bd.LogVerifyFeeResultDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.base.common.constant.BusinessConstant.YesNoStr;
import com.base.common.core.dao.IBaseDao;
import com.base.common.core.service.impl.BaseServiceImpl;
import com.base.common.util.DateUtil;
import com.bms.service.dto.intf.VerifyResultDto;
import com.bms.service.model.bd.FeeResultOwnerMonthlyModel;
import com.bms.service.model.bd.LogVerifyFeeResultModel;
import com.bms.service.profiles.dao.bd.ILogVerifyFeeResultDao;
import com.bms.service.profiles.service.bd.IFeeService;
import com.bms.service.profiles.service.bd.ILogVerifyFeeResultService;

import java.util.List;

/**
 * Created by jsc on 2019/1/11.
 */
@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class LogVerifyFeeResultServiceImpl extends BaseServiceImpl<LogVerifyFeeResultModel> implements ILogVerifyFeeResultService {

    @Resource
    private IFeeService iFeeService;

	@Resource
    private ILogVerifyFeeResultDao iLogVerifyFeeResultDao;

    @Override
    protected IBaseDao<LogVerifyFeeResultModel> getBaseDao() {
        return iLogVerifyFeeResultDao;
    }

	public DataTablesVO<LogVerifyFeeResultModel> getLogVerifyFeeResultList(LogVerifyFeeResultDto logVerifyFeeResultDto) throws Exception {
		return AmazuePagerUtil.getPagerModel(new IPagerService<LogVerifyFeeResultModel>() {

			@Override
			public int queryCount(DTO dto) throws Exception {
				return iLogVerifyFeeResultDao.getModelListCount(dto);
			}

			@Override
			public List<LogVerifyFeeResultModel> queryList(DTO dto) throws Exception {
				return iLogVerifyFeeResultDao.getModelList(dto);
			}
		}, logVerifyFeeResultDto);

	}

    @Override
    public int save(LogVerifyFeeResultModel logVerifyFeeResultModel) throws Exception {
        return iLogVerifyFeeResultDao.insert(logVerifyFeeResultModel);
    }

	@Override
	public void verifySettlement(List<VerifyResultDto> dtoList) throws Exception {
		for (VerifyResultDto verifyResultDto : dtoList) {

			String operator = "FSSC";
			String settleNo = verifyResultDto.getSettleNo();
			int lengthOfNo = settleNo.length();
			String monthStr = settleNo.substring(lengthOfNo - 2);
			String yearStr = settleNo.substring(lengthOfNo - 6, lengthOfNo - 2);
			String ownerNo = settleNo.substring(0, lengthOfNo - 6);
			String isVerified = verifyResultDto.getIsVerified();

			// 记录审核日志
			LogVerifyFeeResultModel logVerifyFeeResultModel = new LogVerifyFeeResultModel();
			logVerifyFeeResultModel.setCargo_owner_no(ownerNo);
			logVerifyFeeResultModel.setYear_str(yearStr);
			logVerifyFeeResultModel.setMonth_str(monthStr);
			if (YesNoStr.YES.getValue().equals(isVerified)) {
				logVerifyFeeResultModel.setType(0);
				logVerifyFeeResultModel.setRemark("FSSC初审通过");
			} else {
				logVerifyFeeResultModel.setType(1);
				logVerifyFeeResultModel.setRemark("FSSC初审驳回");
			}
			logVerifyFeeResultModel.setCreated_by(operator);
			logVerifyFeeResultModel.setCreated_time(DateUtil.getNowTime());
			save(logVerifyFeeResultModel);

			// 更新费用的审核状态
			FeeResultOwnerMonthlyModel updateParam = new FeeResultOwnerMonthlyModel();
			updateParam.setOwner_no(ownerNo);
			updateParam.setYear_str(yearStr);
			updateParam.setMonth_str(monthStr);
			updateParam.setUpdated_by(operator);
			int result = 0;
			if (YesNoStr.YES.getValue().equals(isVerified)) {
				// 通过
				result = iFeeService.updateOwnerMonthly(updateParam);
			} else {
				// 驳回
				result = iFeeService.updateMonthly(updateParam);
			}
			if (result <= 0) {
				throw new Exception(settleNo+"-审核状态更新失败");
			}
		}
	}

}
