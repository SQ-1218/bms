package com.bms.service.profiles.impl.bd;

import com.base.common.core.base.DTO;
import com.base.common.core.dao.IBaseDao;
import com.base.common.core.pager.service.IPagerService;
import com.base.common.core.service.impl.BaseServiceImpl;
import com.bms.common.table.vo.AmazuePagerUtil;
import com.bms.common.table.vo.DataTablesVO;
import com.bms.service.dto.bd.LogChangeOwnerBalanceDto;
import com.bms.service.model.bd.LogChangeOwnerBalanceModel;
import com.bms.service.profiles.dao.bd.ILogChangeOwnerBalanceDao;
import com.bms.service.profiles.service.bd.ILogChangeOwnerBalanceService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class LogChangeOwnerBalanceServiceImp  extends BaseServiceImpl<LogChangeOwnerBalanceModel> implements ILogChangeOwnerBalanceService {
    @Resource
    private ILogChangeOwnerBalanceDao logChangeOwnerBalanceDao;
    @Override
    protected IBaseDao<LogChangeOwnerBalanceModel> getBaseDao() {
        return logChangeOwnerBalanceDao;
    }

    @Override
    public DataTablesVO<LogChangeOwnerBalanceModel> getLogBalanceList(LogChangeOwnerBalanceDto logChangeOwnerBalanceDto) throws Exception {
        return AmazuePagerUtil.getPagerModel(new IPagerService<LogChangeOwnerBalanceModel>() {

            @Override
            public int queryCount(DTO dto) throws Exception {
                return logChangeOwnerBalanceDao.getModelListCount(dto);
            }

            @Override
            public List<LogChangeOwnerBalanceModel> queryList(DTO dto) throws Exception {
                return logChangeOwnerBalanceDao.getModelList(dto);
            }
        }, logChangeOwnerBalanceDto);
    }
}
