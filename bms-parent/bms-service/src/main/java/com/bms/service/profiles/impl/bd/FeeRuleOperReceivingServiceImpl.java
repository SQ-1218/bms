package com.bms.service.profiles.impl.bd;

import com.base.common.core.base.DTO;
import com.base.common.core.dao.IBaseDao;
import com.base.common.core.pager.service.IPagerService;
import com.base.common.core.service.impl.BaseServiceImpl;

import com.bms.common.table.vo.AmazuePagerUtil;
import com.bms.common.table.vo.DataTablesVO;
import com.bms.common.table.vo.ExcelBean;
import com.bms.common.util.ExcelUtil;
import com.bms.service.dto.bd.FeeRuleOperReceivingDto;
import com.bms.service.model.bd.FeeRuleOperReceivingModel;
import com.bms.service.profiles.dao.bd.IFeeRuleOperReceivingDao;
import com.bms.service.profiles.service.bd.IFeeRuleOperReceivingService;
import com.bms.service.vo.bd.FeeRuleOperReceiDocVo;
import com.bms.service.vo.bd.FeeRuleOperReceiVo;
import com.bms.service.vo.bd.FeeRuleOperReceivingWayVo;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jsc on 2018/8/27.
 */
@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class FeeRuleOperReceivingServiceImpl extends BaseServiceImpl<FeeRuleOperReceivingModel> implements IFeeRuleOperReceivingService {
    @Resource
    private IFeeRuleOperReceivingDao iFeeRuleOperReceivingDao;

    @Override
    public IBaseDao<FeeRuleOperReceivingModel> getBaseDao() {
        return this.iFeeRuleOperReceivingDao;
    }

    public DataTablesVO<FeeRuleOperReceivingModel> getReceivingList(FeeRuleOperReceivingDto feeRuleOperReceivingDto) throws Exception {
        return AmazuePagerUtil.getPagerModel(new IPagerService<FeeRuleOperReceivingModel>() {

            @Override
            public int queryCount(DTO dto) throws Exception {
                return iFeeRuleOperReceivingDao.getModelListCount(dto);
            }

            @Override
            public List<FeeRuleOperReceivingModel> queryList(DTO dto) throws Exception {
                return iFeeRuleOperReceivingDao.getModelList(dto);
            }
        }, feeRuleOperReceivingDto);

    }

    //获取计费项目名称
    @Override
    public List<FeeRuleOperReceiVo> getReceiving() throws Exception {
        return iFeeRuleOperReceivingDao.getReceiving();
    }

    //批量删除
    @Override
    public int delectByIds(List<String> list) throws Exception {
        int result = 0;
        result = iFeeRuleOperReceivingDao.delectByIds(list);
        if (result <= 0) {
            throw new Exception("删除失败，请重试！");
        }
        return result;
    }

    @Override
    public void importExcelInfo(InputStream in, MultipartFile file) throws Exception {
        List<List<Object>> listOb = ExcelUtil.getBankListByExcel(in, file.getOriginalFilename());
        List<FeeRuleOperReceivingModel> feeRuleOperReceivingModelList = new ArrayList<>();
        //遍历listob数据，把数据放到List中
        for (int i = 0; i < listOb.size(); i++) {
            List<Object> ob = listOb.get(i);
            FeeRuleOperReceivingModel feeRuleOperReceivingModel = new FeeRuleOperReceivingModel();
            //通过遍历实现把每一列封装成一个model中，再把所有的model用List集合装载
            feeRuleOperReceivingModel.setOwner_no(String.valueOf(ob.get(0)));
            //编号
            int count = iFeeRuleOperReceivingDao.getReceivingNumber(String.valueOf(ob.get(0)));
            if (i > 0) {
                List<Object> ob1 = listOb.get(i - 1);
                if (String.valueOf(ob1.get(0)).equals(String.valueOf(ob.get(0)))) {
                    double t = feeRuleOperReceivingModelList.get(i - 1).getCal_item_rank();
                    t++;
                    feeRuleOperReceivingModel.setCal_item_rank(t);
                } else {
                    feeRuleOperReceivingModel.setCal_item_rank((count + 1));
                }
            } else {
                feeRuleOperReceivingModel.setCal_item_rank((count + 1));
            }
            feeRuleOperReceivingModel.setCal_item_name(String.valueOf(ob.get(2)));
            feeRuleOperReceivingModel.setCal_item_code(String.valueOf(ob.get(3)));
            feeRuleOperReceivingModel.setCal_item_note(String.valueOf(ob.get(4)));
            feeRuleOperReceivingModel.setDoc_type(String.valueOf(ob.get(5)));
            feeRuleOperReceivingModel.setCal_way(String.valueOf(ob.get(6)));
            feeRuleOperReceivingModel.setFactor1_note(String.valueOf(ob.get(7)));
            feeRuleOperReceivingModel.setFactor1_value(String.valueOf(ob.get(8)));
            feeRuleOperReceivingModel.setFactor2_note(String.valueOf(ob.get(9)));
            feeRuleOperReceivingModel.setFactor2_value(String.valueOf(ob.get(10)));
            feeRuleOperReceivingModel.setFactor3_note(String.valueOf(ob.get(11)));
            feeRuleOperReceivingModel.setFactor3_value(String.valueOf(ob.get(12)));
            feeRuleOperReceivingModel.setFactor4_note(String.valueOf(ob.get(13)));
            feeRuleOperReceivingModel.setFactor4_value(String.valueOf(ob.get(14)));
            feeRuleOperReceivingModel.setFactor5_note(String.valueOf(ob.get(15)));
            feeRuleOperReceivingModel.setFactor5_value(String.valueOf(ob.get(16)));
            feeRuleOperReceivingModel.setRemark(String.valueOf(ob.get(17)));
            feeRuleOperReceivingModel.setCreated_by("admin");
            feeRuleOperReceivingModelList.add(feeRuleOperReceivingModel);
        }
        ;
        //批量导入
        iFeeRuleOperReceivingDao.insertBatch(feeRuleOperReceivingModelList);
    }

    //导出
    @Override
    public XSSFWorkbook exportExcelInfo(FeeRuleOperReceivingDto feeRuleOperReceivingDto) throws Exception {
        List<FeeRuleOperReceivingModel> list = iFeeRuleOperReceivingDao.getModelListexcel(feeRuleOperReceivingDto);
        List<ExcelBean> excel = new ArrayList<>();
        Map<Integer, List<ExcelBean>> map = new LinkedHashMap<>();
        XSSFWorkbook xssfWorkbook = null;
        //设置标题栏
        excel.add(new ExcelBean("货主", "owner_no", 0));
        excel.add(new ExcelBean("计费项序号", "cal_item_rank", 0));
        excel.add(new ExcelBean("计费项名称", "cal_item_name", 0));
        excel.add(new ExcelBean("计费项解释", "cal_item_note", 0));
        excel.add(new ExcelBean("单据类型", "doc_type", 0));
        excel.add(new ExcelBean("计算方式", "cal_way", 0));
        excel.add(new ExcelBean("计算系数1", "factor1_note", 0));
        excel.add(new ExcelBean("计算系数1的值", "factor1_value", 0));
        excel.add(new ExcelBean("计算系数2", "factor2_note", 0));
        excel.add(new ExcelBean("计算系数2的值", "factor2_value", 0));
        excel.add(new ExcelBean("计算系数3", "factor3_note", 0));
        excel.add(new ExcelBean("计算系数3的值", "factor3_value", 0));
        excel.add(new ExcelBean("计算系数4", "factor4_note", 0));
        excel.add(new ExcelBean("计算系数4的值", "factor4_value", 0));
        excel.add(new ExcelBean("计算系数5", "factor5_note", 0));
        excel.add(new ExcelBean("计算系数5的值", "factor5_value", 0));
        excel.add(new ExcelBean("备注", "remark", 0));
        map.put(0, excel);
        String sheetName = "入库操作规则";
        //调用ExcelUtil的方法
        xssfWorkbook = ExcelUtil.createExcelFile(FeeRuleOperReceivingModel.class, list, map, sheetName);
        return xssfWorkbook;
    }

    //下载
    @Override
    public XSSFWorkbook downloadExcelInfo(FeeRuleOperReceivingDto feeRuleOperReceivingDto) throws Exception {
        //获取计费项编码
        List<FeeRuleOperReceiVo> feeRuleOperReceiVoList = iFeeRuleOperReceivingDao.getReceiving();
        String item = "";
        for (FeeRuleOperReceiVo itemVo : feeRuleOperReceiVoList) {
            item += itemVo.getCode() + ":" + itemVo.getName() + ",";
        }
        item = item.substring(0, item.length() - 1);
        //获取单据类型
        List<FeeRuleOperReceiDocVo> docVoList = iFeeRuleOperReceivingDao.getReceivingDoc();
        String doc = "";
        for (FeeRuleOperReceiDocVo docVo : docVoList) {
            doc += docVo.getCode() + ":" + docVo.getName() + ",";
        }
        doc = doc.substring(0, doc.length() - 1);
        //获取计费方式
        List<FeeRuleOperReceivingWayVo> wayVoList = iFeeRuleOperReceivingDao.getReceivingWay();
        String way = "";
        for (FeeRuleOperReceivingWayVo WayVo : wayVoList) {
            way += WayVo.getCode() + ":" + WayVo.getName() + ",";
        }
        way = way.substring(0, way.length() - 1);
        List<FeeRuleOperReceivingDto> list = new ArrayList<>();
        list.add(feeRuleOperReceivingDto);
        List<ExcelBean> excel = new ArrayList<>();
        Map<Integer, List<ExcelBean>> map = new LinkedHashMap<>();
        XSSFWorkbook xssfWorkbook = null;
        //设置标题栏
        excel.add(new ExcelBean("货主", "owner_no", 0));
        excel.add(new ExcelBean("计费项序号", "cal_item_rank", 0));
        excel.add(new ExcelBean("计费项名称(" + item + ")", "cal_item_name", 0));
        excel.add(new ExcelBean("计费项目代码", "Cal_item_code", 0));
        excel.add(new ExcelBean("计费项解释", "cal_item_note", 0));
        excel.add(new ExcelBean("单据类型(" + doc + ")", "doc_type", 0));
        excel.add(new ExcelBean("计算方式(" + way + ")", "cal_way", 0));
        excel.add(new ExcelBean("计算系数1", "factor1_note", 0));
        excel.add(new ExcelBean("计算系数1的值", "factor1_value", 0));
        excel.add(new ExcelBean("计算系数2", "factor2_note", 0));
        excel.add(new ExcelBean("计算系数2的值", "factor2_value", 0));
        excel.add(new ExcelBean("计算系数3", "factor3_note", 0));
        excel.add(new ExcelBean("计算系数3的值", "factor3_value", 0));
        excel.add(new ExcelBean("计算系数4", "factor4_note", 0));
        excel.add(new ExcelBean("计算系数4的值", "factor4_value", 0));
        excel.add(new ExcelBean("计算系数5", "factor5_note", 0));
        excel.add(new ExcelBean("计算系数5的值", "factor5_value", 0));
        excel.add(new ExcelBean("备注", "remark", 0));
        map.put(0, excel);
        String sheetName = "入库操作规则";
        //调用ExcelUtil的方法
        xssfWorkbook = ExcelUtil.createExcelFile(FeeRuleOperReceivingDto.class, list, map, sheetName);
        return xssfWorkbook;
    }

    //获取货主编号
    @Override
    public int getReceivingNumber(String owner_no) throws Exception {
        return iFeeRuleOperReceivingDao.getReceivingNumber(owner_no);
    }

    //查询计费方式
    @Override
    public List<FeeRuleOperReceivingWayVo> getReceivingWay() throws Exception {
        return iFeeRuleOperReceivingDao.getReceivingWay();
    }

    //单据类型
    @Override
    public List<FeeRuleOperReceiDocVo> getReceivingDoc() throws Exception {
        return iFeeRuleOperReceivingDao.getReceivingDoc();
    }
}


