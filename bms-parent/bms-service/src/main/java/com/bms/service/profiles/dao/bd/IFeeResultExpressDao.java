package com.bms.service.profiles.dao.bd;

import com.base.common.core.dao.IBaseDao;
import com.bms.service.dto.bd.FeeResultExpressDto;
import com.bms.service.model.bd.FeeResultExpressModel;
import com.bms.service.model.bd.FeeRuleExpressModel;
import com.bms.service.vo.bd.*;

import java.util.List;


/**
 * Created by jsc on 2018/9/12.
 */
public interface IFeeResultExpressDao extends IBaseDao<FeeResultExpressModel> {
    //获取订单编号下的包裹信息
    List<FeeResultExpressweightVo> getdelivery(String no);
    //获取快递公司
    List<CarrierVo> getCarrier() ;
    //获取省份
    List<ProvinceVo> getProvince() ;
    //导出获取list(不分页)
    List<FeeResultExpressModel> getExcelModelList(FeeResultExpressDto feeResultExpressDto) ;

    //获取当前户主的所有订单编号
    List<String> delivery_noList(String owner_no);

    //通过快递编号获取商品的总体积
    FeeResultExpressSkuVo getSkuVolume(String express_no);

    //通过快递公司编号获取计抛体积
    FeeResultExpressSkuVo getHollow_vol(String express_corp);

    //通过户主.省份.重量 去找符合条件的规则
    FeeRuleExpressVo getPrice(String no, String province, Double weight);
}
