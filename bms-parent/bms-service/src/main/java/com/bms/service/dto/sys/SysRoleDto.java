package com.bms.service.dto.sys;

import com.base.common.core.base.DTO;

/**
 * <b>description</b>：角色信息参数bean<br>
 * <b>time</b>：2018-11-15 11:05:43 <br>
 * <b>author</b>：  zhaochen
 */
public class SysRoleDto extends DTO{

	/**
	* @Fields serialVersionUID
	*/
	private static final long serialVersionUID = 1L;
	
	 /*
	  * 
	  */
	private java.lang.String id;
	 /*
	  *角色名称 
	  */
	private java.lang.String name;
	 /*
	  * 
	  */
	private java.sql.Timestamp created_time;
	 /*
	  * 
	  */
	private java.lang.String created_by;
	 /*
	  * 
	  */
	private java.sql.Timestamp updated_time;
	 /*
	  * 
	  */
	private java.lang.String updated_by;
	 /*
	  * 
	  */
	private java.lang.Integer ver;
	 /*
	  * 
	  */
	private java.lang.Integer is_deleted;

		
	public java.lang.String getId()
	{
		return id;
	}
	
	public void setId(java.lang.String id)
	{
		this.id = id;
	}
		
	public java.lang.String getName()
	{
		return name;
	}
	
	public void setName(java.lang.String name)
	{
		this.name = name;
	}
		
	public java.sql.Timestamp getCreated_time()
	{
		return created_time;
	}
	
	public void setCreated_time(java.sql.Timestamp created_time)
	{
		this.created_time = created_time;
	}
		
	public java.lang.String getCreated_by()
	{
		return created_by;
	}
	
	public void setCreated_by(java.lang.String created_by)
	{
		this.created_by = created_by;
	}
		
	public java.sql.Timestamp getUpdated_time()
	{
		return updated_time;
	}
	
	public void setUpdated_time(java.sql.Timestamp updated_time)
	{
		this.updated_time = updated_time;
	}
		
	public java.lang.String getUpdated_by()
	{
		return updated_by;
	}
	
	public void setUpdated_by(java.lang.String updated_by)
	{
		this.updated_by = updated_by;
	}
		
	public java.lang.Integer getVer()
	{
		return ver;
	}
	
	public void setVer(java.lang.Integer ver)
	{
		this.ver = ver;
	}
		
	public java.lang.Integer getIs_deleted()
	{
		return is_deleted;
	}
	
	public void setIs_deleted(java.lang.Integer is_deleted)
	{
		this.is_deleted = is_deleted;
	}

	

}
