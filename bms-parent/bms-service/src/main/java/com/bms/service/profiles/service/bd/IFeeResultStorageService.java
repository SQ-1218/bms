package com.bms.service.profiles.service.bd;

import com.base.common.core.service.IBaseService;
import com.bms.common.table.vo.DataTablesVO;
import com.bms.service.dto.bd.FeeResultSpecialDto;
import com.bms.service.dto.bd.FeeResultStorageDto;
import com.bms.service.model.bd.FeeResultStorageModel;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.util.List;

public interface IFeeResultStorageService extends IBaseService<FeeResultStorageModel> {
//获取规则中获取所有数据
   List<FeeResultStorageModel> getFee() throws Exception;
//获取所有计费结果
   DataTablesVO<FeeResultStorageModel> getFeeResultStorage(FeeResultStorageDto feeResultStorageDto) throws Exception;
   XSSFWorkbook exportExcelInfo(FeeResultStorageDto feeResultStorageDto) throws Exception;
}
