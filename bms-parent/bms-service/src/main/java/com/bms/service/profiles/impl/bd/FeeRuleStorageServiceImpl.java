package com.bms.service.profiles.impl.bd;

import com.base.common.core.base.DTO;
import com.base.common.core.dao.IBaseDao;
import com.base.common.core.pager.service.IPagerService;
import com.base.common.core.service.impl.BaseServiceImpl;

import com.bms.common.table.vo.AmazuePagerUtil;
import com.bms.common.table.vo.DataTablesVO;
import com.bms.common.table.vo.ExcelBean;
import com.bms.common.util.ExcelUtil;
import com.bms.service.dto.bd.FeeRuleStorageDto;
import com.bms.service.model.bd.FeeRuleStorageModel;
import com.bms.service.profiles.dao.bd.IFeeRuleStorageDao;
import com.bms.service.profiles.service.bd.IFeeRuleStorageService;
import com.bms.service.vo.bd.FeeRuleStorageWayVo;
import com.bms.service.vo.bd.FeeRuleStoragenameVo;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jsc on 2018/8/23.
 */
@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class FeeRuleStorageServiceImpl extends BaseServiceImpl<FeeRuleStorageModel> implements IFeeRuleStorageService {
    @Resource
    private IFeeRuleStorageDao iFeeRuleStorageDao;

    @Override
    public IBaseDao<FeeRuleStorageModel> getBaseDao() {
        return this.iFeeRuleStorageDao;
    }

    public DataTablesVO<FeeRuleStorageModel> getFeeRuleStorageList(FeeRuleStorageDto feeRuleStorageDto) throws Exception {
        return AmazuePagerUtil.getPagerModel(new IPagerService<FeeRuleStorageModel>() {

            @Override
            public int queryCount(DTO dto) throws Exception {
                return iFeeRuleStorageDao.getModelListCount(dto);
            }

            @Override
            public List<FeeRuleStorageModel> queryList(DTO dto) throws Exception {
                return iFeeRuleStorageDao.getModelList(dto);
            }
        }, feeRuleStorageDto);

    }

    //查询计费项目名称
    @Override
    public List<FeeRuleStoragenameVo> getStorage() throws Exception {
        return iFeeRuleStorageDao.getStorage();
    }

    //批量删除
    @Override
    public int delectByIds(List<String> list) throws Exception {
        int result = 0;
        result = iFeeRuleStorageDao.delectByIds(list);
        if (result <= 0) {
            throw new Exception("删除失败，请重试！");
        }
        return result;
    }

    @Override
    public void importExcelInfo(InputStream in, MultipartFile file) throws Exception {
        List<List<Object>> listOb = ExcelUtil.getBankListByExcel(in, file.getOriginalFilename());
        List<FeeRuleStorageModel> feeRuleStorageModelList = new ArrayList<>();

        //遍历listob数据，把数据放到List中
        for (int i = 0; i < listOb.size(); i++) {
            List<Object> ob = listOb.get(i);
            FeeRuleStorageModel feeRuleStorageModel = new FeeRuleStorageModel();
            //通过遍历实现把每一列封装成一个model中，再把所有的model用List集合装载
            //货主
            feeRuleStorageModel.setOwner_no(String.valueOf(ob.get(0)));
            //计费项编号
            int count = iFeeRuleStorageDao.getCount(String.valueOf(ob.get(0)));
            if (i > 0) {
                List<Object> ob1 = listOb.get(i - 1);
                if (String.valueOf(ob1.get(0)).equals(String.valueOf(ob.get(0)))) {
                    double t = feeRuleStorageModelList.get(i - 1).getCal_item_rank();
                    t++;
                    feeRuleStorageModel.setCal_item_rank(t);
                } else {
                    feeRuleStorageModel.setCal_item_rank((count + 1));
                }
            } else {
                feeRuleStorageModel.setCal_item_rank((count + 1));
            }
            feeRuleStorageModel.setCal_item_name(String.valueOf(ob.get(2)));
            feeRuleStorageModel.setCal_item_code(String.valueOf(ob.get(3)));
            feeRuleStorageModel.setCal_item_note(String.valueOf(ob.get(4)));
            feeRuleStorageModel.setCal_way(String.valueOf(ob.get(5)));
            feeRuleStorageModel.setFactor1_note(String.valueOf(ob.get(6)));
            feeRuleStorageModel.setFactor1_value(String.valueOf(ob.get(7)));
            feeRuleStorageModel.setFactor2_note(String.valueOf(ob.get(8)));
            feeRuleStorageModel.setFactor2_value(String.valueOf(ob.get(9)));
            feeRuleStorageModel.setFactor3_note(String.valueOf(ob.get(10)));
            feeRuleStorageModel.setFactor3_value(String.valueOf(ob.get(11)));
            feeRuleStorageModel.setFactor4_note(String.valueOf(ob.get(12)));
            feeRuleStorageModel.setFactor4_value(String.valueOf(ob.get(13)));
            feeRuleStorageModel.setFactor5_note(String.valueOf(ob.get(14)));
            feeRuleStorageModel.setFactor5_value(String.valueOf(ob.get(15)));
            feeRuleStorageModel.setRemark(String.valueOf(ob.get(16)));
            feeRuleStorageModel.setCreated_by("admin");
            feeRuleStorageModelList.add(feeRuleStorageModel);
        }
        ;
        //批量插入
        iFeeRuleStorageDao.insertBatch(feeRuleStorageModelList);
    }

    /*
   * 导出excel*/
    @Override
    public XSSFWorkbook exportExcelInfo(FeeRuleStorageDto feeRuleStorageDto) throws Exception {
        List<FeeRuleStorageModel> list = iFeeRuleStorageDao.getModelListexcel(feeRuleStorageDto);
        List<ExcelBean> excel = new ArrayList<>();
        Map<Integer, List<ExcelBean>> map = new LinkedHashMap<>();
        XSSFWorkbook xssfWorkbook = null;
        //设置标题栏
        excel.add(new ExcelBean("货主", "owner_no", 0));
        excel.add(new ExcelBean("计费项序号", "cal_item_rank", 0));
        excel.add(new ExcelBean("计费项名称", "cal_item_name", 0));
        excel.add(new ExcelBean("计费项解释", "cal_item_note", 0));
        excel.add(new ExcelBean("计算方式", "cal_way", 0));
        excel.add(new ExcelBean("计算系数1", "factor1_note", 0));
        excel.add(new ExcelBean("计算系数1的值", "factor1_value", 0));
        excel.add(new ExcelBean("计算系数2", "factor2_note", 0));
        excel.add(new ExcelBean("计算系数2的值", "factor2_value", 0));
        excel.add(new ExcelBean("计算系数3", "factor3_note", 0));
        excel.add(new ExcelBean("计算系数3的值", "factor3_value", 0));
        excel.add(new ExcelBean("计算系数4", "factor4_note", 0));
        excel.add(new ExcelBean("计算系数4的值", "factor4_value", 0));
        excel.add(new ExcelBean("计算系数5", "factor5_note", 0));
        excel.add(new ExcelBean("计算系数5的值", "factor5_value", 0));
        excel.add(new ExcelBean("备注", "remark", 0));
        map.put(0, excel);
        String sheetName = "仓储费用规则";
        //调用ExcelUtil的方法
        xssfWorkbook = ExcelUtil.createExcelFile(FeeRuleStorageModel.class, list, map, sheetName);
        return xssfWorkbook;

    }

    /*
    * 下载模板*/
    @Override
    public XSSFWorkbook downloadExcelInfo(FeeRuleStorageDto feeRuleStorageDto) throws Exception {
        //获取计费项目名称
        List<FeeRuleStoragenameVo> feeRuleStoragenameVoList = iFeeRuleStorageDao.getStorage();
        String item = "";
        for (FeeRuleStoragenameVo itemVo : feeRuleStoragenameVoList) {
            item += itemVo.getCode() + ":" + itemVo.getName() + ",";
        }
        item = item.substring(0, item.length() - 1);
        //获取计费方式
        List<FeeRuleStorageWayVo> wayVoList = iFeeRuleStorageDao.getStorageWay();
        String way = "";
        for (FeeRuleStorageWayVo WayVo : wayVoList) {
            way += WayVo.getCode() + ":" + WayVo.getName() + ",";
        }
        way = way.substring(0, way.length() - 1);
        //获取计费项序号
        List<FeeRuleStorageDto> list = new ArrayList<>();
        list.add(feeRuleStorageDto);
        List<ExcelBean> excel = new ArrayList<>();
        Map<Integer, List<ExcelBean>> map = new LinkedHashMap<>();
        XSSFWorkbook xssfWorkbook = null;
        //设置标题栏
        excel.add(new ExcelBean("货主", "owner_no", 0));
        excel.add(new ExcelBean("计费项序号", "cal_item_rank", 0));
        excel.add(new ExcelBean("计费项名称(" + item + ")", "cal_item_name", 0));
        excel.add(new ExcelBean("计费项目代码", "Cal_item_code", 0));
        excel.add(new ExcelBean("计费项解释", "cal_item_note", 0));
        excel.add(new ExcelBean("计算方式(" + way + ")", "cal_way", 0));
        excel.add(new ExcelBean("计算系数1", "factor1_note", 0));
        excel.add(new ExcelBean("计算系数1的值", "factor1_value", 0));
        excel.add(new ExcelBean("计算系数2", "factor2_note", 0));
        excel.add(new ExcelBean("计算系数2的值", "factor2_value", 0));
        excel.add(new ExcelBean("计算系数3", "factor3_note", 0));
        excel.add(new ExcelBean("计算系数3的值", "factor3_value", 0));
        excel.add(new ExcelBean("计算系数4", "factor4_note", 0));
        excel.add(new ExcelBean("计算系数4的值", "factor4_value", 0));
        excel.add(new ExcelBean("计算系数5", "factor5_note", 0));
        excel.add(new ExcelBean("计算系数5的值", "factor5_value", 0));
        excel.add(new ExcelBean("备注", "remark", 0));
        map.put(0, excel);
        String sheetName = "仓储费用配置规则";
        //调用ExcelUtil的方法
        xssfWorkbook = ExcelUtil.createExcelFile(FeeRuleStorageDto.class, list, map, sheetName);
        return xssfWorkbook;
    }

    //查询计费方式
    @Override
    public List<FeeRuleStorageWayVo> getStorageWay() throws Exception {
        return iFeeRuleStorageDao.getStorageWay();
    }

    //获取货主编号
    @Override
    public int getCount(String owner_no) throws Exception {
        return iFeeRuleStorageDao.getCount(owner_no);
    }

    @Override
    public List<FeeRuleStorageWayVo> getCalWayByQty() throws Exception {
        return iFeeRuleStorageDao.getCalWayByQty();
    }

}