package com.bms.service.profiles.dao.bd;

import com.base.common.core.dao.IBaseDao;
import com.bms.service.dto.bd.BdCargoOwnerDto;
import com.bms.service.model.bd.BdCargoOwnerModel;
import com.bms.service.model.bd.LogChangeOwnerBalanceModel;
import com.bms.service.vo.bd.BdCargoOwnerVo;

import java.util.List;

/**
 * <b>description</b>：货主信息表数据访问接口<br>
 * <b>time</b>：2017-08-08 16:24:44 <br>
 * <b>author</b>：  zhaochen
 */
public interface IBdCargoOwnerDao extends IBaseDao<BdCargoOwnerModel>{
   List<BdCargoOwnerVo> getOwner();

    //获取所有户主
    List<BdCargoOwnerModel> getowner_noList(BdCargoOwnerDto bdCargoOwnerDto);
    //更改余额
    Integer updateBalance(BdCargoOwnerModel bdCargoOwnerModel);
    //插入余额
    Integer insertLogBalance(LogChangeOwnerBalanceModel logChangeOwnerBalanceModel);
}