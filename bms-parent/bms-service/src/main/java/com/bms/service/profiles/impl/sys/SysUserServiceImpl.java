package com.bms.service.profiles.impl.sys;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.base.common.core.dao.IBaseDao;
import com.base.common.core.service.impl.BaseServiceImpl;
import com.base.common.exception.AlikAssert;
import com.base.common.exception.BusinessException;
import com.bms.service.dto.sys.SysUserDto;
import com.bms.service.model.sys.SysUserModel;
import com.bms.service.profiles.dao.sys.ISysUserDao;
import com.bms.service.profiles.service.sys.ISysUserService;
import com.bms.service.vo.sys.SysUserVo;

/**
 * <b>description</b>：BMS系统登录业务实现<br>
 * <b>time</b>：2018-09-17 11:08:11 <br>
 * <b>author</b>：  zhaochen
 */
@Service("iSysUserService")
@Transactional(readOnly = true,rollbackFor = Exception.class)
public class SysUserServiceImpl extends BaseServiceImpl<SysUserModel> implements ISysUserService {

	@Resource
	private ISysUserDao isysUserDao;

	@Override
	public IBaseDao<SysUserModel> getBaseDao() {
		return this.isysUserDao;
	}

	/*
	 * 登录验证
	 * @param SysUserDto
	 */
	public SysUserVo login(SysUserDto sysUserDto) throws Exception{
		// 参数校验
        AlikAssert.isNotNull(sysUserDto, "参数错误");
        AlikAssert.isNotBlank(sysUserDto.getPassword(), "密码不能为空");
        AlikAssert.isNotBlank(sysUserDto.getLogin_name(), "用户名不能为空");
        
        // 查找用户信息
    	SysUserDto userDto = new SysUserDto();
    	userDto.setLogin_name(sysUserDto.getLogin_name());
    	SysUserModel sysUserModel = isysUserDao.getModelOne(userDto);
    	
    	if (sysUserModel == null) {
			BusinessException.throwMessage("用户不存在");
		}
    	
    	if (sysUserModel.getStaus() == 1) {
    		BusinessException.throwMessage("用户已停用");
		}
        
        SysUserVo sysUserVo = new SysUserVo();
        BeanUtils.copyProperties(sysUserModel, sysUserVo);
        
        return sysUserVo;
	}

}
