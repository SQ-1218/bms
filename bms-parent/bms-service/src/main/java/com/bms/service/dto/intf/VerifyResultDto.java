package com.bms.service.dto.intf;

import java.io.Serializable;

public class VerifyResultDto implements Serializable {

	private static final long serialVersionUID = 8022870037258211881L;

	/**
	 * 结算单编号————必填
	 */
	private String settleNo;

	/**
	 * 是否已审核：0-是； 1-否。
	 */
	private String isVerified;

	/**
	 * 备注（记录审核失败的原因）
	 */
	private String remark;

	public String getSettleNo() {
		return settleNo;
	}

	public void setSettleNo(String settleNo) {
		this.settleNo = settleNo;
	}

	public String getIsVerified() {
		return isVerified;
	}

	public void setIsVerified(String isVerified) {
		this.isVerified = isVerified;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}
