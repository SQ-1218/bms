package com.bms.service.profiles.dao.bd;

import com.base.common.core.dao.IBaseDao;
import com.bms.service.dto.bd.FeeResultSpecialDto;
import com.bms.service.model.bd.FeeResultSpecialModel;
import com.bms.service.vo.bd.FeeRuleSpecialCalVo;
import com.bms.service.vo.bd.FeeRuleSpecialItemVo;
import com.bms.service.vo.bd.FeeTaskValaddedSvcVo;

import java.util.List;

public interface IFeeResultSpecialDao extends IBaseDao<FeeResultSpecialModel> {
    List<FeeRuleSpecialItemVo> getFeeRuleSpecialItem();
    List<FeeRuleSpecialCalVo> getFeeRuleSpecialCal();
    //批量删除
    int batchDeleteByIds(List<String> list);
   // int getFeeRuleSpecialCount(String owner_no);
    //导出获取list(不分页)
    List<FeeResultSpecialModel> getExcelModelList(FeeResultSpecialDto feeResultSpecialDto) throws Exception;
    //获取符合货主的信息
    FeeTaskValaddedSvcVo getFeeTaskValaddedSvcVoList(String owner_no,String cal_item_code) throws Exception;
}
