package com.bms.service.profiles.service.sys;

import com.base.common.core.service.IBaseService;
import com.bms.service.model.sys.SysUserRoleModel;
/**
 * <b>description</b>：用户角色对应关系业务接口<br>
 * <b>time</b>：2018-11-15 10:29:03 <br>
 * <b>author</b>：  zhaochen
 */
public interface ISysUserRoleService extends IBaseService<SysUserRoleModel> {

	
}
