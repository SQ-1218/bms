package com.bms.service.profiles.dao.bd;

import com.base.common.core.dao.IBaseDao;
import com.bms.service.dto.bd.FeeRuleExpressDto;
import com.bms.service.model.bd.FeeRuleExpressModel;
import com.bms.service.vo.bd.CarrierVo;
import com.bms.service.vo.bd.ProvinceVo;

import java.util.List;

//快递配置dao层接口
public interface IFeeRuleExpressDao extends IBaseDao<FeeRuleExpressModel> {
    //批量删除
    int batchDeleteByIds(List<String> list);
    //获取快递公司
    List<CarrierVo> getCarrier();
    //获取省份
    List<ProvinceVo> getProvince();
    //导出获取list(不分页)
    List<FeeRuleExpressModel> getExcelModelList(FeeRuleExpressDto feeRuleExpressDto) throws Exception;
    List<FeeRuleExpressModel> getFeeRuleExpressList(FeeRuleExpressDto feeRuleExpressDto) throws Exception;
}
