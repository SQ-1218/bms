package com.bms.service.profiles.dao.bd;

import com.base.common.core.dao.IBaseDao;
import com.bms.service.dto.bd.*;
import com.bms.service.model.bd.*;
import com.bms.service.vo.bd.*;

import java.util.List;

/**
 * Created by jsc on 2018/10/16.
 */
public interface IFeeDao extends IBaseDao<SumFeeModel> {
    //获取快递总费用
    List<FeeResultExpressFeeModel> getexpressfee(FeeResultExpressFeeDto feeResultExpressFeeDto) ;
    //获取快递总费用
    List<FeeResultStorageFeeModel> getstoragefee(FeeResultStorageFeeDto feeResultStorageFeeDto);
    //获取快递总费用
    List<FeeResultDeliveryFeeModel> getdeliveryfee(FeeResultDeliveryFeeDto feeResultDeliveryFeeDto) ;
    //获取快递总费用
    List<FeeResultReceivingFeeModel> getreceivingfee(FeeResultReceivingFeeDto feeResultReceivingFeeDto) ;
    //获取快递总费用
    List<FeeResultSpecialFeeModel> getspeciatfee(FeeResultSpecialFeeDto feeResultSpecialFeeDto) ;
    //获取包材总费用
    List<FeeResultPackMaterialFeeModel>getMaterialfee(FeeResultPackMaterialFeeDto feeResultPackMaterialFeeDto) throws Exception;

    //将快递费用结果插入到月份结果表
    int save(FeeResultOwnerMonthlyModel feeResultOwnerMonthlyModel);

    //获取快递remake备注信息记录
    FeeResultExpressSkuVo getCount(String owner_no, String month_time);

    //获取月份记录
    FeeResultOwnerMonthlyVo getRecord(String owner_no, String year, String month);

    // 查询指定月月份结果表数据
    List<SumFeeModel> getOwnerMonth(SumFeeDto sumFeeDto);

    //查询确认审核月份数据
    List<FeeResultOwnerMonthlyModel> getOwnerMonthly(String owner_no, String year, String month);

    //通过时修改月份结果表审核状态
    int updateOwnerMonthly(FeeResultOwnerMonthlyModel feeResultOwnerMonthlyModel);

    //驳回时修改月份结果表审核状态
    int updateMonthly(FeeResultOwnerMonthlyModel feeResultOwnerMonthlyModel);

    //判断货主是否存在类型为仓储优惠-日均托盘量
    FeeResultOwnerMonthlyVo getCodeitem(String owner_no);

    //判断货主是否存在类型为仓储优惠-库存周转率
    FeeResultOwnerMonthlyVo getRateitem(String owner_no);

    //判断货主是否存在保底费类型
    FeeRuleSpecialVo getSafetyitem(String owner_no);

    //获取当前月订单的出库总件数之和
    FeeResultOwnerMonthlyVo getOwnerdelivery(String owner_no, String month_time);

    //获取仓储优惠-库存周转率的对应的值
    FeeRuleSpecialVo getTurnover(String owner_no);

    //获取储位托盘仓储费托盘量和对应的值
    FeeRuleSpecialVo getPlt(String owner_no);

    //获取指定月储位托盘数量 丶库存总量
    SnapshotStkTotalInfoVo getTotal(String owner_no, String starDay, String lastDay);

    //获取规则中按存储托盘方式计费的价格
    FeeRuleStorageVo getTray(String owner_no);


}
