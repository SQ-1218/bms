package com.bms.service.profiles.impl.bd;

import com.base.common.core.base.DTO;
import com.base.common.core.dao.IBaseDao;
import com.base.common.core.pager.service.IPagerService;
import com.base.common.core.service.impl.BaseServiceImpl;
import com.bms.common.util.ExcelUtil;
import com.bms.common.table.vo.AmazuePagerUtil;
import com.bms.common.table.vo.DataTablesVO;
import com.bms.common.table.vo.ExcelBean;
import com.bms.service.dto.bd.FeeRuleOperDeliveryDto;
import com.bms.service.model.bd.FeeRuleOperDeliveryModel;
import com.bms.service.profiles.dao.bd.IFeeRuleOperDeliveryDao;
import com.bms.service.profiles.service.bd.IFeeRuleOperDeliveryService;
import com.bms.service.vo.bd.FeeRuleOperDeliveryCalVo;
import com.bms.service.vo.bd.FeeRuleOperDeliveryDocVo;
import com.bms.service.vo.bd.FeeRuleOperDeliveryItemVo;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class FeeRuleOperDeliveryServiceImpl  extends BaseServiceImpl<FeeRuleOperDeliveryModel> implements IFeeRuleOperDeliveryService {
    @Resource
    private IFeeRuleOperDeliveryDao feeRuleOperDeliveryDao;
    @Override
    protected IBaseDao<FeeRuleOperDeliveryModel> getBaseDao() {
        return this.feeRuleOperDeliveryDao;
    }

    @Override
    public DataTablesVO<FeeRuleOperDeliveryModel> getFeeRuleOperDeliveryList(FeeRuleOperDeliveryDto feeRuleOperDeliveryDto) throws Exception {
        return AmazuePagerUtil.getPagerModel(new IPagerService<FeeRuleOperDeliveryModel>() {
            @Override
            public int queryCount(DTO dto) throws Exception {
                return feeRuleOperDeliveryDao.getModelListCount(dto);
            }
            @Override
            public List<FeeRuleOperDeliveryModel> queryList(DTO dto) throws Exception {
                return feeRuleOperDeliveryDao.getModelList(dto);
            }
        }, feeRuleOperDeliveryDto);
    }

    @Override
    public List<FeeRuleOperDeliveryItemVo> getFeeOperDeliveryItemList() throws Exception {
        return feeRuleOperDeliveryDao.getFeeOperDeliveryItem();
    }

    @Override
    public List<FeeRuleOperDeliveryCalVo> getFeeOperDeliveryCalList() throws Exception {
        return feeRuleOperDeliveryDao.getFeeOperDeliveryCal();
    }

    @Override
    public List<FeeRuleOperDeliveryDocVo> getFeeOperDeliveryDocList() throws Exception {
        return feeRuleOperDeliveryDao.getFeeOperDeliveryDoc();
    }

    @Override
    public int getFeeOperDeliveryCount(String owner_no) throws Exception {
        return feeRuleOperDeliveryDao.getFeeOperDeliveryCount(owner_no);
    }

    @Override
    public int batchDeleteByIds(List<String> list) throws Exception {
        int result = 0;
        result = feeRuleOperDeliveryDao.batchDeleteByIds(list);
        if (result <= 0) {
            throw new Exception("删除失败，请重试！");
        }
        return result;
    }
    //导入
    @Override
    public int importExcelInfo(InputStream in, MultipartFile file) {
        int status=0;
       try {
            List<List<Object>> listOb = ExcelUtil.getBankListByExcel(in,file.getOriginalFilename());
            List<FeeRuleOperDeliveryModel> feeRuleOperDeliveryModelList=new ArrayList<>();
            //遍历listob数据，把数据放到List中
            for (int i = 0; i < listOb.size(); i++) {
                //通过遍历实现把每一列封装成一个model中，再把所有的model用List集合装载
                List<Object> ob = listOb.get(i);
                FeeRuleOperDeliveryModel feeRuleOperDeliveryModel = new FeeRuleOperDeliveryModel();
                //设置货主
                feeRuleOperDeliveryModel.setOwner_no(String.valueOf(ob.get(0)));
                //设置序号
                int count =feeRuleOperDeliveryDao.getFeeOperDeliveryCount(String.valueOf(ob.get(0)));
                if(i>0) {
                    List<Object>  ob1 = listOb.get(i - 1);
                    if(String.valueOf(ob1.get(0)).equals(String.valueOf(ob.get(0)))){
                        double a = feeRuleOperDeliveryModelList.get(i - 1).getCal_item_rank();
                        a++;
                        feeRuleOperDeliveryModel.setCal_item_rank(a);
                    }else{
                        feeRuleOperDeliveryModel.setCal_item_rank((count+1));
                    }
                }else{
                    feeRuleOperDeliveryModel.setCal_item_rank((count+1));
                }
                //计费名称
                feeRuleOperDeliveryModel.setCal_item_name(String.valueOf(ob.get(1)));
                //计费code
                feeRuleOperDeliveryModel.setCal_item_code(String.valueOf(ob.get(2)));
                //计费note
                feeRuleOperDeliveryModel.setCal_item_note(String.valueOf(ob.get(3)));
                //单据类型
                feeRuleOperDeliveryModel.setDoc_type(String.valueOf(ob.get(4)));
                //计算方式
                feeRuleOperDeliveryModel.setCal_way(String.valueOf(ob.get(5)));
                //计算系数1-5
                feeRuleOperDeliveryModel.setFactor1_note(String.valueOf(ob.get(6)));
                feeRuleOperDeliveryModel.setFactor1_value(String.valueOf(ob.get(7)));
                feeRuleOperDeliveryModel.setFactor2_note(String.valueOf(ob.get(8)));
                feeRuleOperDeliveryModel.setFactor2_value(String.valueOf(ob.get(9)));
                feeRuleOperDeliveryModel.setFactor3_note(String.valueOf(ob.get(10)));
                feeRuleOperDeliveryModel.setFactor3_value(String.valueOf(ob.get(11)));
                feeRuleOperDeliveryModel.setFactor4_note(String.valueOf(ob.get(12)));
                feeRuleOperDeliveryModel.setFactor4_value(String.valueOf(ob.get(13)));
                feeRuleOperDeliveryModel.setFactor5_note(String.valueOf(ob.get(14)));
                feeRuleOperDeliveryModel.setFactor5_value(String.valueOf(ob.get(15)));
                feeRuleOperDeliveryModel.setRemark(String.valueOf(ob.get(16)));
                feeRuleOperDeliveryModel.setCreated_by("admin");
                feeRuleOperDeliveryModelList.add(feeRuleOperDeliveryModel);
            }
            //批量导入
           if(feeRuleOperDeliveryModelList.size()>0){
            feeRuleOperDeliveryDao.insertBatch(feeRuleOperDeliveryModelList);
            status=1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    @Override
    public XSSFWorkbook downloadExcelInfo(FeeRuleOperDeliveryDto feeRuleOperDeliveryDto) throws Exception {
       //获取计费项编码，单据类型，计算方式
        List<FeeRuleOperDeliveryItemVo> itemVoList=feeRuleOperDeliveryDao.getFeeOperDeliveryItem();
        String item="";
        for(FeeRuleOperDeliveryItemVo itemVo :itemVoList){
            item+=itemVo.getCode()+":"+itemVo.getName()+",";
        }
        item=item.substring(0,item.length()-1);
        List<FeeRuleOperDeliveryDocVo> docVoList=feeRuleOperDeliveryDao.getFeeOperDeliveryDoc();
        String doc="";
        for(FeeRuleOperDeliveryDocVo docVo :docVoList){
            doc+=docVo.getCode()+":"+docVo.getName()+",";
        }
        doc=doc.substring(0,doc.length()-1);
        List<FeeRuleOperDeliveryCalVo> calVoList=feeRuleOperDeliveryDao.getFeeOperDeliveryCal();
        String cal="";
        for(FeeRuleOperDeliveryCalVo calVo :calVoList){
            cal+=calVo.getCode()+":"+calVo.getName()+",";
        }
        cal=cal.substring(0,cal.length()-1);
        List<FeeRuleOperDeliveryDto> list=new ArrayList<>();
        list.add(feeRuleOperDeliveryDto);
        List<ExcelBean> excel=new ArrayList<>();
        Map<Integer,List<ExcelBean>> map=new LinkedHashMap<>();
        XSSFWorkbook xssfWorkbook=null;
        //设置标题栏
        excel.add(new ExcelBean("货主","owner_no",0));
        excel.add(new ExcelBean("计费项名称","cal_item_name",0));
        excel.add(new ExcelBean("计费项编码("+item+")","cal_item_code",0));
        excel.add(new ExcelBean("计费项解释","cal_item_note",0));
        excel.add(new ExcelBean("单据类型编码("+doc+")","doc_type",0));
        excel.add(new ExcelBean("计算方式编码("+cal+")","cal_way",0));
        excel.add(new ExcelBean("计算系数1","factor1_note",0));
        excel.add(new ExcelBean("计算系数1的值","factor1_value",0));
        excel.add(new ExcelBean("计算系数2","factor2_note",0));
        excel.add(new ExcelBean("计算系数2的值","factor2_value",0));
        excel.add(new ExcelBean("计算系数3","factor3_note",0));
        excel.add(new ExcelBean("计算系数3的值","factor3_value",0));
        excel.add(new ExcelBean("计算系数4","factor4_note",0));
        excel.add(new ExcelBean("计算系数4的值","factor4_value",0));
        excel.add(new ExcelBean("计算系数5","factor5_note",0));
        excel.add(new ExcelBean("计算系数5的值","factor5_value",0));
        excel.add(new ExcelBean("备注","remark",0));
        map.put(0, excel);
        String sheetName = "出库费用配置规则";
        //调用ExcelUtil的方法
        xssfWorkbook = ExcelUtil.createExcelFile(FeeRuleOperDeliveryDto.class, list, map, sheetName);
        return xssfWorkbook;
    }

    @Override
    public XSSFWorkbook exportExcelInfo(FeeRuleOperDeliveryDto feeRuleOperDeliveryDto) throws Exception {
        List<FeeRuleOperDeliveryModel> list=feeRuleOperDeliveryDao.getExcelModelList(feeRuleOperDeliveryDto);
        List<ExcelBean> excel=new ArrayList<>();
        Map<Integer,List<ExcelBean>> map=new LinkedHashMap<>();
        XSSFWorkbook xssfWorkbook=null;
        //设置标题栏
        excel.add(new ExcelBean("货主","owner_no",0));
        excel.add(new ExcelBean("计费项名称","cal_item_name",0));
        excel.add(new ExcelBean("计费项解释","cal_item_note",0));
        excel.add(new ExcelBean("单据类型","doc_type",0));
        excel.add(new ExcelBean("计算方式","cal_way",0));
        excel.add(new ExcelBean("计算系数1","factor1_note",0));
        excel.add(new ExcelBean("计算系数1的值","factor1_value",0));
        excel.add(new ExcelBean("计算系数2","factor2_note",0));
        excel.add(new ExcelBean("计算系数2的值","factor2_value",0));
        excel.add(new ExcelBean("计算系数3","factor3_note",0));
        excel.add(new ExcelBean("计算系数3的值","factor3_value",0));
        excel.add(new ExcelBean("计算系数4","factor4_note",0));
        excel.add(new ExcelBean("计算系数4的值","factor4_value",0));
        excel.add(new ExcelBean("计算系数5","factor5_note",0));
        excel.add(new ExcelBean("计算系数5的值","factor5_value",0));
        excel.add(new ExcelBean("计算系数6","factor6_note",0));
        excel.add(new ExcelBean("计算系数6的值","factor6_value",0));
        excel.add(new ExcelBean("计算系数7","factor7_note",0));
        excel.add(new ExcelBean("计算系数7的值","factor7_value",0));
        excel.add(new ExcelBean("计算系数8","factor8_note",0));
        excel.add(new ExcelBean("计算系数8的值","factor8_value",0));
        excel.add(new ExcelBean("计算系数9","factor9_note",0));
        excel.add(new ExcelBean("计算系数9的值","factor9_value",0));
      excel.add(new ExcelBean("备注","remark",0));
        map.put(0, excel);
        String sheetName = "特殊费用配置规则";
        //调用ExcelUtil的方法
        xssfWorkbook = ExcelUtil.createExcelFile(FeeRuleOperDeliveryModel.class, list, map, sheetName);
        return xssfWorkbook;
    }
}
