package com.bms.service.profiles.impl.bd;

import com.base.common.core.base.DTO;
import com.base.common.core.dao.IBaseDao;
import com.base.common.core.pager.service.IPagerService;
import com.base.common.core.service.impl.BaseServiceImpl;
import com.bms.common.table.vo.AmazuePagerUtil;
import com.bms.common.table.vo.DataTablesVO;
import com.bms.common.table.vo.ExcelBean;
import com.bms.common.util.ExcelUtil;
import com.bms.service.dto.bd.FeeResultSpecialDto;
import com.bms.service.dto.bd.FeeRuleSpecialDto;
import com.bms.service.model.bd.FeeResultSpecialModel;
import com.bms.service.model.bd.FeeRuleSpecialModel;
import com.bms.service.profiles.dao.bd.IFeeResultSpecialDao;
import com.bms.service.profiles.dao.bd.IFeeRuleSpecialDao;
import com.bms.service.profiles.service.bd.IFeeResultSpecialService;
import com.bms.service.vo.bd.FeeTaskValaddedSvcVo;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.util.*;

@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class FeeResultSpecialServiceImpl extends BaseServiceImpl<FeeResultSpecialModel> implements IFeeResultSpecialService {
    @Resource
    private IFeeResultSpecialDao feeResultSpecialDao;
    @Resource
    private IFeeRuleSpecialDao feeRuleSpecialDao;
    @Override
    protected IBaseDao<FeeResultSpecialModel> getBaseDao() {
        return this.feeResultSpecialDao;
    }

    @Override
    public DataTablesVO<FeeResultSpecialModel> getFeeResultSpecialList(FeeResultSpecialDto feeResultSpecialDto) throws Exception {
        return AmazuePagerUtil.getPagerModel(new IPagerService<FeeResultSpecialModel>() {
            @Override
            public int queryCount(DTO dto) throws Exception {
                return feeResultSpecialDao.getModelListCount(dto);
            }
            @Override
            public List<FeeResultSpecialModel> queryList(DTO dto) throws Exception {
                return feeResultSpecialDao.getModelList(dto);
            }
        }, feeResultSpecialDto);
    }

    @Override
    public int batchDeleteByIds(List<String> list) throws Exception {
        int result = 0;
        result = feeResultSpecialDao.batchDeleteByIds(list);
        if (result <= 0) {
            throw new Exception("删除失败，请重试！");
        }
        return result;
    }


    @Override
    public XSSFWorkbook exportExcelInfo(FeeResultSpecialDto feeResultSpecialDto) throws Exception {
       List<FeeResultSpecialModel> list=feeResultSpecialDao.getExcelModelList(feeResultSpecialDto);
        List<ExcelBean> excel=new ArrayList<>();
        Map<Integer,List<ExcelBean>> map=new LinkedHashMap<>();
        XSSFWorkbook xssfWorkbook=null;
        //设置标题栏
        excel.add(new ExcelBean("货主","owner_no",0));
        excel.add(new ExcelBean("计费项名称","cal_item_name",0));
        excel.add(new ExcelBean("计算方式","cal_type",0));
        excel.add(new ExcelBean("单据号","doc_no",0));
        excel.add(new ExcelBean("计费结果","fee",0));
        excel.add(new ExcelBean("费用产生时间","fee_occur_time",0));
        excel.add(new ExcelBean("是否已结算(0为结算,1为未结算)","is_confirmed",0));
        excel.add(new ExcelBean("确认结算时间","confirmed_time",0));
        excel.add(new ExcelBean("备注","remark",0));
        map.put(0, excel);
        String sheetName = "特殊费用计费结果";
        //调用ExcelUtil的方法
        xssfWorkbook = ExcelUtil.createExcelFile(FeeResultSpecialModel.class, list, map, sheetName);
        return xssfWorkbook;
    }

    @Override
    public List<FeeResultSpecialModel> getFee() throws Exception{
        //获取所有特殊计费规则
        FeeRuleSpecialDto feeRuleSpecialDto=new FeeRuleSpecialDto();
        feeRuleSpecialDto.setIs_deleted("1");
        List<FeeRuleSpecialModel> list= feeRuleSpecialDao.getFeeRuleSpecialList();
        //获取list的值然后计算结果
        for(FeeRuleSpecialModel feeRuleSpecialModel : list) {
            double result=0.0;
          //先获取所有符合货主的fee_task
             FeeTaskValaddedSvcVo feeTaskValaddedSvcVo =feeResultSpecialDao.getFeeTaskValaddedSvcVoList(feeRuleSpecialModel.getOwner_no(),feeRuleSpecialModel.getCal_item_code());
             if(feeTaskValaddedSvcVo!=null){
             if(feeRuleSpecialModel.getFactor1_value()!=null){
                 result+=Double.parseDouble(feeRuleSpecialModel.getFactor1_value())*feeTaskValaddedSvcVo.getQty1();
             }
             if(feeRuleSpecialModel.getFactor2_value()!=null){
                 result+=Double.parseDouble(feeRuleSpecialModel.getFactor2_value())*feeTaskValaddedSvcVo.getQty2();
             }
             if(feeRuleSpecialModel.getFactor3_value()!=null){
                 result+=Double.parseDouble(feeRuleSpecialModel.getFactor3_value())*feeTaskValaddedSvcVo.getQty3();
             }
             if(feeRuleSpecialModel.getFactor4_value()!=null){
                 result+=Double.parseDouble(feeRuleSpecialModel.getFactor4_value())*feeTaskValaddedSvcVo.getQty4();
             }
             if(feeRuleSpecialModel.getFactor5_value()!=null){
                 result+=Double.parseDouble(feeRuleSpecialModel.getFactor5_value())*feeTaskValaddedSvcVo.getQty5();
             }
             if(feeRuleSpecialModel.getFactor6_value()!=null){
                 result+=Double.parseDouble(feeRuleSpecialModel.getFactor6_value())*feeTaskValaddedSvcVo.getQty6();
             }
             if(feeRuleSpecialModel.getFactor7_value()!=null){
                result+= Double.parseDouble(feeRuleSpecialModel.getFactor7_value())*feeTaskValaddedSvcVo.getQty7();
             }
             if(feeRuleSpecialModel.getFactor8_value()!=null){
                 result+=Double.parseDouble(feeRuleSpecialModel.getFactor8_value())*feeTaskValaddedSvcVo.getQty8();
             }
             if(feeRuleSpecialModel.getFactor9_value()!=null){
                 result+=Double.parseDouble(feeRuleSpecialModel.getFactor9_value())*feeTaskValaddedSvcVo.getQty9();
             }
               //插入数据库
                 Date date = new Date();
                 Timestamp ts = new Timestamp(date.getTime());
                 FeeResultSpecialModel feeResultSpecialModel=new FeeResultSpecialModel();
                 feeResultSpecialModel.setOwner_no(feeRuleSpecialModel.getOwner_no());
                 feeResultSpecialModel.setCal_item_name(feeRuleSpecialModel.getCal_item_name());
                 feeResultSpecialModel.setCal_item_code(feeRuleSpecialModel.getCal_item_code());
                 feeResultSpecialModel.setDoc_type(feeRuleSpecialModel.getDoc_type());
                 feeResultSpecialModel.setCal_type(feeRuleSpecialModel.getCal_type());
                 feeResultSpecialModel.setDoc_no(feeTaskValaddedSvcVo.getDoc_no());
                 feeResultSpecialModel.setFee(result);
                 feeResultSpecialModel.setFee_occur_time(ts);
                 feeResultSpecialModel.setIs_deleted(1);
                 feeResultSpecialModel.setCreated_by("admin");
                 feeResultSpecialDao.insert(feeResultSpecialModel);
                 System.out.println("插入成功");
             }
        }

        return null;
    }
}
