package com.bms.service.vo.bd;

import com.base.common.core.base.VO;

/**
 * Created by jsc on 2018/11/27.
 */
public class SnapshotStkTotalInfoVo extends VO {
    private static final long serialVersionUID = 1L;
    //每月总件数
    private int qty;
    //每月储位托盘数量
    private int loc;

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getLoc() {
        return loc;
    }

    public void setLoc(int loc) {
        this.loc = loc;
    }
}
