package com.bms.service.profiles.service.bd;

import com.base.common.core.service.IBaseService;
import com.bms.common.table.vo.DataTablesVO;
import com.bms.service.dto.bd.FeeRuleOperDeliveryDto;
import com.bms.service.model.bd.FeeRuleOperDeliveryModel;
import com.bms.service.vo.bd.FeeRuleOperDeliveryCalVo;
import com.bms.service.vo.bd.FeeRuleOperDeliveryDocVo;
import com.bms.service.vo.bd.FeeRuleOperDeliveryItemVo;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.List;

public interface IFeeRuleOperDeliveryService extends IBaseService<FeeRuleOperDeliveryModel> {
    DataTablesVO<FeeRuleOperDeliveryModel> getFeeRuleOperDeliveryList(FeeRuleOperDeliveryDto feeRuleExpressDto) throws Exception;
    List<FeeRuleOperDeliveryItemVo> getFeeOperDeliveryItemList()throws Exception;
    List<FeeRuleOperDeliveryCalVo> getFeeOperDeliveryCalList()throws Exception;
    List<FeeRuleOperDeliveryDocVo> getFeeOperDeliveryDocList()throws Exception;
    int getFeeOperDeliveryCount(String owner_no)throws Exception;
    int batchDeleteByIds(List<String> list)throws Exception;
    //导入
    int importExcelInfo(InputStream in, MultipartFile file);
    //模板下载
    XSSFWorkbook downloadExcelInfo(FeeRuleOperDeliveryDto feeRuleOperDeliveryDto) throws Exception;
    //导出
    XSSFWorkbook exportExcelInfo(FeeRuleOperDeliveryDto feeRuleOperDeliveryDto) throws Exception;
}
