package com.bms.service.vo.bd;

import com.base.common.core.base.VO;

/**
 * Created by jsc on 2018/11/26.
 */
public class FeeResultOwnerDailyVo extends VO {
    private static final long serialVersionUID = 1L;
    private Double fee;

    public Double getFee() {
        return fee;
    }

    public void setFee(Double fee) {
        this.fee = fee;
    }
}
