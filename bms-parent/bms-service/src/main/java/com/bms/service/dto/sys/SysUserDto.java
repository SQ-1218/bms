package com.bms.service.dto.sys;

import com.base.common.core.base.DTO;

/**
 * <b>description</b>：BMS系统登录参数bean<br>
 * <b>time</b>：2018-09-17 11:08:11 <br>
 * <b>author</b>：  zhaochen
 */
public class SysUserDto extends DTO{

	/**
	* @Fields serialVersionUID
	*/
	private static final long serialVersionUID = 1L;
	
	 /*
	  * 
	  */
	private java.lang.String id;
	 /*
	  *登录名 
	  */
	private java.lang.String login_name;
	 /*
	  *密码 
	  */
	private java.lang.String password;
	 /*
	  *姓名 
	  */
	private java.lang.String full_name;
	 /*
	  *状态(0-正常,1-停用) 
	  */
	private java.lang.Integer staus;
	 /*
	  *是否超级用户(0-是,1-否) 
	  */
	private java.lang.Integer is_super_user;
	 /*
	  * 
	  */
	private java.sql.Timestamp created_time;
	 /*
	  * 
	  */
	private java.lang.String created_by;
	 /*
	  * 
	  */
	private java.sql.Timestamp updated_time;
	 /*
	  * 
	  */
	private java.lang.String updated_by;
	 /*
	  * 
	  */
	private java.lang.Integer ver;
	 /*
	  * 
	  */
	private java.lang.Integer is_deleted;

		
	public java.lang.String getId()
	{
		return id;
	}
	
	public void setId(java.lang.String id)
	{
		this.id = id;
	}
		
	public java.lang.String getLogin_name()
	{
		return login_name;
	}
	
	public void setLogin_name(java.lang.String login_name)
	{
		this.login_name = login_name;
	}
		
	public java.lang.String getPassword()
	{
		return password;
	}
	
	public void setPassword(java.lang.String password)
	{
		this.password = password;
	}
		
	public java.lang.String getFull_name()
	{
		return full_name;
	}
	
	public void setFull_name(java.lang.String full_name)
	{
		this.full_name = full_name;
	}
		
	public java.lang.Integer getStaus()
	{
		return staus;
	}
	
	public void setStaus(java.lang.Integer staus)
	{
		this.staus = staus;
	}
		
	public java.lang.Integer getIs_super_user()
	{
		return is_super_user;
	}
	
	public void setIs_super_user(java.lang.Integer is_super_user)
	{
		this.is_super_user = is_super_user;
	}
		
	public java.sql.Timestamp getCreated_time()
	{
		return created_time;
	}
	
	public void setCreated_time(java.sql.Timestamp created_time)
	{
		this.created_time = created_time;
	}
		
	public java.lang.String getCreated_by()
	{
		return created_by;
	}
	
	public void setCreated_by(java.lang.String created_by)
	{
		this.created_by = created_by;
	}
		
	public java.sql.Timestamp getUpdated_time()
	{
		return updated_time;
	}
	
	public void setUpdated_time(java.sql.Timestamp updated_time)
	{
		this.updated_time = updated_time;
	}
		
	public java.lang.String getUpdated_by()
	{
		return updated_by;
	}
	
	public void setUpdated_by(java.lang.String updated_by)
	{
		this.updated_by = updated_by;
	}
		
	public java.lang.Integer getVer()
	{
		return ver;
	}
	
	public void setVer(java.lang.Integer ver)
	{
		this.ver = ver;
	}
		
	public java.lang.Integer getIs_deleted()
	{
		return is_deleted;
	}
	
	public void setIs_deleted(java.lang.Integer is_deleted)
	{
		this.is_deleted = is_deleted;
	}

	

}
