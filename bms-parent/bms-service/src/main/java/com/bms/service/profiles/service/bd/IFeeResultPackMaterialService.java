package com.bms.service.profiles.service.bd;

import com.base.common.core.service.IBaseService;
import com.bms.common.table.vo.DataTablesVO;
import com.bms.service.dto.bd.FeeResultPackMaterialDto;
import com.bms.service.model.bd.FeeResultPackMaterialModel;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.util.List;

public interface IFeeResultPackMaterialService extends IBaseService<FeeResultPackMaterialModel> {
    //获取费用
    List<FeeResultPackMaterialModel> getFee() throws Exception;
    //获取所有计费结果
    DataTablesVO<FeeResultPackMaterialModel> getFeeResultPackMaterial(FeeResultPackMaterialDto feeResultPackMaterialDto) throws Exception;
    //导出
    XSSFWorkbook exportExcelInfo(FeeResultPackMaterialDto feeResultPackMaterialDto) throws Exception;
}
