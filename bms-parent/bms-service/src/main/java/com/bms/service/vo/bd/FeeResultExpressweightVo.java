package com.bms.service.vo.bd;

import com.base.common.core.base.VO;

/**
 * Created by jsc on 2018/9/21.
 */

public class FeeResultExpressweightVo extends VO {
    private static final long serialVersionUID = 1L;
    //关联单号
    private String ref_no;
    //快递单号
    private String express_no;
    //包裹重量
    private Double weight;
    //快递公司
    private String express_corp;
    //包材体积
    private double materialvolume;
    //省份
    private String province;

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getExpress_corp() {
        return express_corp;
    }

    public void setExpress_corp(String express_corp) {
        this.express_corp = express_corp;
    }

    public double getMaterialvolume() {
        return materialvolume;
    }

    public void setMaterialvolume(double materialvolume) {
        this.materialvolume = materialvolume;
    }

    public String getRef_no() {
        return ref_no;
    }

    public void setRef_no(String ref_no) {
        this.ref_no = ref_no;
    }

    public String getExpress_no() {
        return express_no;
    }

    public void setExpress_no(String express_no) {
        this.express_no = express_no;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }


}

