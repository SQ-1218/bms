package com.bms.service.vo.bd;

import com.base.common.core.base.VO;

public class CarrierVo extends VO {
    private String code;
    private String name;
    public String getName() {
        return name;
    }
    public void setName(String name) { this.name = name; }
    public String getCode() { return code; }
    public void setCode(String code) { this.code = code; }

}
