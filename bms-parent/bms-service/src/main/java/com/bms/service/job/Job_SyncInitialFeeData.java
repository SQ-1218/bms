package com.bms.service.job;

import com.bms.service.model.bd.*;
import com.bms.service.profiles.service.bd.*;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

@Component
public class Job_SyncInitialFeeData {
    @Resource
    private IFeeResultStorageService feeResultStorageService;
    @Resource
    private IFeeResultOperationService feeResultOperationService;
   @Resource
    private IFeeResultExpressService iFeeResultExpressService;
   @Resource
   private IFeeResultOperationdeliveryService iFeeResultOperationdeliveryService;
   @Resource
   private IFeeResultPackMaterialService feeResultPackMaterialService;
   @Resource
   private IFeeResultSpecialService feeResultSpecialService;
    @Resource
    private IFeeResultOwerDailyService iFeeResultOwerDailyService;

    //cron任务触发器运行的任务
    public void doCronTask() throws Exception {
        //执行快递费用
        List<FeeResultExpressModel> feeResultExpressModelList=iFeeResultExpressService.getFee();
        //执行出库费用
        List<FeeResultOperationdeliveryModel> feeResultOperationModelDeliveryList = iFeeResultOperationdeliveryService.getFee();
        //执行仓储费用
        List<FeeResultStorageModel> feeResultStorageModelList=feeResultStorageService.getFee();
        //执行入库费用
         List<FeeResultOperationModel> feeResultOperationModelList=feeResultOperationService.getFee();
        //执行包装费用
         List<FeeResultPackMaterialModel> feeResultPackMaterialModelList=feeResultPackMaterialService.getFee();
       //执行特殊费用
        List<FeeResultSpecialModel> feeResultSpecialModelList=feeResultSpecialService.getFee();
        //执行快递费用到户主每日记录表中
        //List<FeeResultOwnerDailyModel> feeResultOwnerDailyModelExpressList=iFeeResultOwerDailyService.getExpressFee();
        //执行仓储费用到户主每日记录表中
        //List<FeeResultOwnerDailyModel> feeResultOwnerDailyModelStorageList=iFeeResultOwerDailyService.getStoageFee();
        //执行出库费用到户主每日记录表中
        //List<FeeResultOwnerDailyModel> feeResultOwnerDailyModelDeliveryList=iFeeResultOwerDailyService.getDeliverFee();
        //执行入库费用到户主每日记录表中
        //List<FeeResultOwnerDailyModel> feeResultOwnerDailyModelOReceivingList=iFeeResultOwerDailyService.getReceivingFee();
        //执行包材费用到户主每日记录表中
        //List<FeeResultOwnerDailyModel> feeResultOwnerDailyModelMaterialList=iFeeResultOwerDailyService.getMaterialFee();
        //执行特殊费用到户主每日记录表中
        //List<FeeResultOwnerDailyModel> feeResultOwnerDailyModelList=iFeeResultOwerDailyService.getSpecialFee();
    }
}

