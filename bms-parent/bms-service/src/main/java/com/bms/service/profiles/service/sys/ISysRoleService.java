package com.bms.service.profiles.service.sys;

import com.base.common.core.service.IBaseService;
import com.bms.service.model.sys.SysRoleModel;

/**
 * <b>description</b>：角色信息业务接口<br>
 * <b>time</b>：2018-11-15 11:05:43 <br>
 * <b>author</b>：  zhaochen
 */
public interface ISysRoleService extends IBaseService<SysRoleModel> {
	
}
