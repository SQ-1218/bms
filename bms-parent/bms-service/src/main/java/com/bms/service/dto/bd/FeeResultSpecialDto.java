package com.bms.service.dto.bd;

import com.bms.common.table.vo.BmsBaseDto;

import java.sql.Timestamp;
import java.util.List;

public class FeeResultSpecialDto extends BmsBaseDto {
    private static final long serialVersionUID = 1L;
    private String id;
    private String owner_no;
    private String  cal_item_name;
    private String  cal_item_code;
    private String  doc_type;
    private String  cal_type;
    private String  doc_no;
    private double fee;
    private Timestamp fee_occur_time;
    private String is_confirmed;
    private Timestamp confirmed_time;
    private String remark;
    private Timestamp created_time;
    private String created_by;
    private Timestamp updated_time;
    private String updated_by;
    private int ver;
    private int is_deleted;
    private List<String> owner_noList;
    private List<String>cal_item_codeList;
    private List<Integer> cal_typeList;
    private List<String> noList;
    private String start_time;
    private String end_time;
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwner_no() {
        return owner_no;
    }

    public void setOwner_no(String owner_no) {
        this.owner_no = owner_no;
    }

    public String getCal_item_name() {
        return cal_item_name;
    }

    public void setCal_item_name(String cal_item_name) {
        this.cal_item_name = cal_item_name;
    }

    public String getCal_item_code() {
        return cal_item_code;
    }

    public void setCal_item_code(String cal_item_code) {
        this.cal_item_code = cal_item_code;
    }

    public String getDoc_type() {
        return doc_type;
    }

    public void setDoc_type(String doc_type) {
        this.doc_type = doc_type;
    }

    public String getCal_type() {
        return cal_type;
    }

    public void setCal_type(String cal_type) {
        this.cal_type = cal_type;
    }

    public String getDoc_no() {
        return doc_no;
    }

    public void setDoc_no(String doc_no) {
        this.doc_no = doc_no;
    }

    public double getFee() {
        return fee;
    }

    public void setFee(double fee) {
        this.fee = fee;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public int getVer() {
        return ver;
    }

    public void setVer(int ver) {
        this.ver = ver;
    }

    public int getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(int is_deleted) {
        this.is_deleted = is_deleted;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public List<String> getOwner_noList() {
        return owner_noList;
    }

    public void setOwner_noList(List<String> owner_noList) {
        this.owner_noList = owner_noList;
    }

    public List<String> getCal_item_codeList() {
        return cal_item_codeList;
    }

    public void setCal_item_codeList(List<String> cal_item_codeList) {
        this.cal_item_codeList = cal_item_codeList;
    }

    public List<String> getNoList() {
        return noList;
    }

    public void setNoList(List<String> noList) {
        this.noList = noList;
    }

    public List<Integer> getCal_typeList() {
        return cal_typeList;
    }

    public void setCal_typeList(List<Integer> cal_typeList) {
        this.cal_typeList = cal_typeList;
    }

    public Timestamp getFee_occur_time() {
        return fee_occur_time;
    }

    public void setFee_occur_time(Timestamp fee_occur_time) {
        this.fee_occur_time = fee_occur_time;
    }

    public Timestamp getConfirmed_time() {
        return confirmed_time;
    }

    public void setConfirmed_time(Timestamp confirmed_time) {
        this.confirmed_time = confirmed_time;
    }

    public Timestamp getCreated_time() {
        return created_time;
    }

    public void setCreated_time(Timestamp created_time) {
        this.created_time = created_time;
    }

    public Timestamp getUpdated_time() {
        return updated_time;
    }

    public void setUpdated_time(Timestamp updated_time) {
        this.updated_time = updated_time;
    }

    public String getIs_confirmed() {
        return is_confirmed;
    }

    public void setIs_confirmed(String is_confirmed) {
        this.is_confirmed = is_confirmed;
    }
}
