package com.bms.service.profiles.dao.sys;

import com.base.common.core.dao.IBaseDao;
import com.bms.service.model.sys.SysUserRoleModel;

/**
 * <b>description</b>：用户角色对应关系数据访问接口<br>
 * <b>time</b>：2018-11-15 10:29:03 <br>
 * <b>author</b>：  zhaochen
 */
public interface ISysUserRoleDao extends IBaseDao<SysUserRoleModel>{

}