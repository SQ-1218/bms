package com.bms.service.profiles.impl.bd;

import com.base.common.core.dao.IBaseDao;
import com.base.common.core.service.impl.BaseServiceImpl;
import com.bms.common.constant.ConstClass;
import com.bms.service.dto.bd.BdCargoOwnerDto;
import com.bms.service.dto.bd.FeeRuleOperDeliveryDto;
import com.bms.service.model.bd.BdCargoOwnerModel;
import com.bms.service.model.bd.FeeResultOperationdeliveryModel;
import com.bms.service.model.bd.FeeRuleOperDeliveryModel;
import com.bms.service.profiles.dao.bd.IBdCargoOwnerDao;
import com.bms.service.profiles.dao.bd.IFeeResultOperationdeliveryDao;
import com.bms.service.profiles.dao.bd.IFeeRuleOperDeliveryDao;
import com.bms.service.profiles.service.bd.IFeeResultOperationdeliveryService;
import com.bms.service.vo.bd.FeeResultOperCountVo;
import com.bms.service.vo.bd.FeeResultOperaVo;
import com.bms.service.vo.bd.FeeResultOperationFeeVo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by jsc on 2018/9/18.
 */
@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class FeeResultOperationdeliveryServiceImpl extends BaseServiceImpl<FeeResultOperationdeliveryModel> implements IFeeResultOperationdeliveryService {
    @Resource
    private IFeeResultOperationdeliveryDao iFeeResultOperationdeliveryDao;
    @Resource
    private IFeeRuleOperDeliveryDao iFeeRuleOperDeliveryDao;

    @Override
    protected IBaseDao<FeeResultOperationdeliveryModel> getBaseDao() {
        return iFeeResultOperationdeliveryDao;
    }

    @Override
    public List<FeeResultOperationdeliveryModel> getFee() throws Exception {
        FeeRuleOperDeliveryDto feeRuleOperDeliveryDto = new FeeRuleOperDeliveryDto();
        feeRuleOperDeliveryDto.setIs_deleted("1");
        //获取计费规则
        List<FeeRuleOperDeliveryModel> feeRuleOperDeliveryModelList = iFeeRuleOperDeliveryDao.getFeeOperDeliveryList(feeRuleOperDeliveryDto);
        List<FeeResultOperationdeliveryModel> feeResultOperationdeliveryModelList = new ArrayList<>();
        for (FeeRuleOperDeliveryModel feeRuleOperDeliveryModel : feeRuleOperDeliveryModelList) {
            FeeResultOperationdeliveryModel feeResultOperationdeliveryModel = new FeeResultOperationdeliveryModel();
            List<FeeResultOperationFeeVo> list = new ArrayList<>();
            //获取规则计算方式
            String getCal_way = feeRuleOperDeliveryModel.getCal_way();
            //获取规则收费类型
            String code = feeRuleOperDeliveryModel.getCal_item_code();
            if (getCal_way.equals("1") && code.equals("1")) {
                //基础出库费    按单
                list = iFeeResultOperationdeliveryDao.getBasicsUnitFees(feeRuleOperDeliveryModel.getOwner_no(), feeRuleOperDeliveryModel.getDoc_type(), ConstClass.Status_Delivery.DELIVERED, Double.parseDouble(feeRuleOperDeliveryModel.getFactor1_value()));
            } else if (getCal_way.equals("1") && code.equals("3")) {
                //基础秒杀出库费    按单
                list = iFeeResultOperationdeliveryDao.getSpikeUnitFees(feeRuleOperDeliveryModel.getOwner_no(), feeRuleOperDeliveryModel.getDoc_type(), ConstClass.Status_Delivery.DELIVERED, ConstClass.YesNo_Int.NO, Double.parseDouble(feeRuleOperDeliveryModel.getFactor1_value()));
            } else if (getCal_way.equals("1") && code.equals("4")) {
                //基础取消费    按单
                list = iFeeResultOperationdeliveryDao.getCancelUnitFees(feeRuleOperDeliveryModel.getOwner_no(), feeRuleOperDeliveryModel.getDoc_type(), ConstClass.Status_Delivery.CANCELLED, Double.parseDouble(feeRuleOperDeliveryModel.getFactor1_value()));
            } else if (getCal_way.equals("2") && code.equals("1")) {
                //基础出库费    按件
                list = iFeeResultOperationdeliveryDao.getBasicsPieceFees(feeRuleOperDeliveryModel.getOwner_no(), feeRuleOperDeliveryModel.getDoc_type(), ConstClass.Status_Delivery.DELIVERED, Double.parseDouble(feeRuleOperDeliveryModel.getFactor1_value()), Double.parseDouble(feeRuleOperDeliveryModel.getFactor2_value()));
            } else if (getCal_way.equals("2") && code.equals("2")) {
                //大单出库附加费    按件
                list = iFeeResultOperationdeliveryDao.getLargePieceFees(feeRuleOperDeliveryModel.getOwner_no(), feeRuleOperDeliveryModel.getDoc_type(), ConstClass.Status_Delivery.DELIVERED, Double.parseDouble(feeRuleOperDeliveryModel.getFactor1_value()), Double.valueOf(feeRuleOperDeliveryModel.getFactor2_value()), Double.parseDouble(feeRuleOperDeliveryModel.getFactor3_value()), Double.parseDouble(feeRuleOperDeliveryModel.getFactor4_value()));
            } else if (getCal_way.equals("2") && code.equals("3")) {
                //基础秒杀出库费    按件
                list = iFeeResultOperationdeliveryDao.getSpikePieceFees(feeRuleOperDeliveryModel.getOwner_no(), feeRuleOperDeliveryModel.getDoc_type(), ConstClass.Status_Delivery.DELIVERED, ConstClass.YesNo_Int.NO, Double.parseDouble(feeRuleOperDeliveryModel.getFactor1_value()), Double.parseDouble(feeRuleOperDeliveryModel.getFactor2_value()));

            } else if (getCal_way.equals("2") && code.equals("4")) {
                //基础取消费    按件
                list = iFeeResultOperationdeliveryDao.getCancelPieceFees(feeRuleOperDeliveryModel.getOwner_no(), feeRuleOperDeliveryModel.getDoc_type(), ConstClass.Status_Delivery.CANCELLED, Double.parseDouble(feeRuleOperDeliveryModel.getFactor1_value()), Double.parseDouble(feeRuleOperDeliveryModel.getFactor2_value()));

            } else if (getCal_way.equals("2") && code.equals("5")) {
                //大单取消附加费    按件
                list = iFeeResultOperationdeliveryDao.getLargeCancelFees(feeRuleOperDeliveryModel.getOwner_no(), feeRuleOperDeliveryModel.getDoc_type(), ConstClass.Status_Delivery.CANCELLED, Double.parseDouble(feeRuleOperDeliveryModel.getFactor1_value()), Double.parseDouble(feeRuleOperDeliveryModel.getFactor2_value()), Double.parseDouble(feeRuleOperDeliveryModel.getFactor3_value()), Double.parseDouble(feeRuleOperDeliveryModel.getFactor4_value()));
            }
            if (list.size() > 0) {

                for (FeeResultOperationFeeVo feeResultOperationFeeVo : list) {
                    feeResultOperationdeliveryModel.setFee(feeResultOperationFeeVo.getFee());
                    //获取当前时间
                    Date date1 = new Date();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    //时间存储为字符串
                    String str = sdf.format(date1);
                    feeResultOperationdeliveryModel.setOwner_no(feeRuleOperDeliveryModel.getOwner_no());
                    feeResultOperationdeliveryModel.setCal_item_rank(feeRuleOperDeliveryModel.getCal_item_rank());
                    feeResultOperationdeliveryModel.setCal_item_name(feeRuleOperDeliveryModel.getCal_item_name());
                    feeResultOperationdeliveryModel.setCal_item_code(code);
                    feeResultOperationdeliveryModel.setCal_way(getCal_way);
                    feeResultOperationdeliveryModel.setDoc_type("1");
                    feeResultOperationdeliveryModel.setDoc_no(feeResultOperationFeeVo.getNo());
                    feeResultOperationdeliveryModel.setFee_occur_time(Timestamp.valueOf(str));
                    feeResultOperationdeliveryModel.setCreated_by("admin");
                    iFeeResultOperationdeliveryDao.insert(feeResultOperationdeliveryModel);

                }
            }
        }
        return null;
    }

}
