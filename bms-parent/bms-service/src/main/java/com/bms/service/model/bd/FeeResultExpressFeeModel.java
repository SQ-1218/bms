package com.bms.service.model.bd;

import com.base.common.core.model.Model;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by jsc on 2018/10/17.
 */
public class FeeResultExpressFeeModel extends Model {
    private static final long serialVersionUID = 1L;
    private String owner_no;
    private Double fee;
    private Timestamp fee_occur_time;
    private List<String> owner_noList;
    private Integer is_deleted;

    private String name;
    //按月
    private String month_time;
    //状态
    private int status;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMonth_time() {
        return month_time;
    }

    public void setMonth_time(String month_time) {
        this.month_time = month_time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwner_no() {
        return owner_no;
    }

    public void setOwner_no(String owner_no) {
        this.owner_no = owner_no;
    }

    public Double getFee() {
        return fee;
    }

    public void setFee(Double fee) {
        this.fee = fee;
    }

    public Timestamp getFee_occur_time() {
        return fee_occur_time;
    }

    public void setFee_occur_time(Timestamp fee_occur_time) {
        this.fee_occur_time = fee_occur_time;
    }

    public List<String> getOwner_noList() {
        return owner_noList;
    }

    public void setOwner_noList(List<String> owner_noList) {
        this.owner_noList = owner_noList;
    }

    public Integer getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(Integer is_deleted) {
        this.is_deleted = is_deleted;
    }


}
