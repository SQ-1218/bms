package com.bms.service.profiles.service.bd;

import com.base.common.core.service.IBaseService;
import com.bms.service.model.bd.FeeResultOperationdeliveryModel;

import java.util.List;

/**
 * Created by jsc on 2018/9/18.
 */
public interface IFeeResultOperationdeliveryService extends IBaseService<FeeResultOperationdeliveryModel> {
    //计算费用
    List<FeeResultOperationdeliveryModel> getFee() throws Exception;

}
