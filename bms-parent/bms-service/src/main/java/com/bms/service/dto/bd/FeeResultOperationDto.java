package com.bms.service.dto.bd;

import com.bms.common.table.vo.BmsBaseDto;

import java.sql.Timestamp;
import java.util.List;

public class FeeResultOperationDto extends BmsBaseDto {
    /**
     * @Fields serialVersionUID
     */
    private static final long serialVersionUID = 1L;
    private String id;
    private String owner_no;
    private String whos_no;
    private int cal_item_rank;
    private String cal_item_name;
    private String cal_item_code;
    private int doc_type;
    private String cal_way;
    private double fee;
    private Timestamp fee_occur_time;
    private int is_confirmed;
    private Timestamp confirmed_time;
    private String remark;
    private Timestamp created_time;
    private String created_by;
    private Timestamp updated_time;
    private String updated_by;
    private int ver;
    private int is_deleted;
    private List<String> owner_noList;
    private List<String>cal_item_codeList;
    private List<Integer> cal_typeList;
    private List<String> noList;
    private String start_time;
    private String end_time;

    public int getDoc_type() {
        return doc_type;
    }

    public void setDoc_type(int doc_type) {
        this.doc_type = doc_type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwner_no() {
        return owner_no;
    }

    public void setOwner_no(String owner_no) {
        this.owner_no = owner_no;
    }

    public String getWhos_no() {
        return whos_no;
    }

    public void setWhos_no(String whos_no) {
        this.whos_no = whos_no;
    }

    public int getCal_item_rank() {
        return cal_item_rank;
    }

    public void setCal_item_rank(int cal_item_rank) {
        this.cal_item_rank = cal_item_rank;
    }

    public String getCal_item_name() {
        return cal_item_name;
    }

    public void setCal_item_name(String cal_item_name) {
        this.cal_item_name = cal_item_name;
    }

    public String getCal_item_code() {
        return cal_item_code;
    }

    public void setCal_item_code(String cal_item_code) {
        this.cal_item_code = cal_item_code;
    }

    public String getCal_way() {
        return cal_way;
    }

    public void setCal_way(String cal_way) {
        this.cal_way = cal_way;
    }

    public double getFee() {
        return fee;
    }

    public void setFee(double fee) {
        this.fee = fee;
    }

    public int getIs_confirmed() {
        return is_confirmed;
    }

    public void setIs_confirmed(int is_confirmed) {
        this.is_confirmed = is_confirmed;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public int getVer() {
        return ver;
    }

    public void setVer(int ver) {
        this.ver = ver;
    }

    public int getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(int is_deleted) {
        this.is_deleted = is_deleted;
    }

    public Timestamp getFee_occur_time() {
        return fee_occur_time;
    }

    public void setFee_occur_time(Timestamp fee_occur_time) {
        this.fee_occur_time = fee_occur_time;
    }

    public Timestamp getConfirmed_time() {
        return confirmed_time;
    }

    public void setConfirmed_time(Timestamp confirmed_time) {
        this.confirmed_time = confirmed_time;
    }

    public Timestamp getCreated_time() {
        return created_time;
    }

    public void setCreated_time(Timestamp created_time) {
        this.created_time = created_time;
    }

    public Timestamp getUpdated_time() {
        return updated_time;
    }

    public void setUpdated_time(Timestamp updated_time) {
        this.updated_time = updated_time;
    }

    public List<String> getOwner_noList() {
        return owner_noList;
    }

    public void setOwner_noList(List<String> owner_noList) {
        this.owner_noList = owner_noList;
    }

    public List<String> getCal_item_codeList() {
        return cal_item_codeList;
    }

    public void setCal_item_codeList(List<String> cal_item_codeList) {
        this.cal_item_codeList = cal_item_codeList;
    }

    public List<Integer> getCal_typeList() {
        return cal_typeList;
    }

    public void setCal_typeList(List<Integer> cal_typeList) {
        this.cal_typeList = cal_typeList;
    }

    public List<String> getNoList() {
        return noList;
    }

    public void setNoList(List<String> noList) {
        this.noList = noList;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }
}
