package com.bms.service.profiles.dao.bd;

import com.base.common.core.dao.IBaseDao;
import com.bms.service.dto.bd.FeeRuleOperDeliveryDto;
import com.bms.service.model.bd.FeeRuleOperDeliveryModel;
import com.bms.service.vo.bd.FeeRuleOperDeliveryCalVo;
import com.bms.service.vo.bd.FeeRuleOperDeliveryDocVo;
import com.bms.service.vo.bd.FeeRuleOperDeliveryItemVo;

import java.util.List;

//出库配置dao层
public interface IFeeRuleOperDeliveryDao extends IBaseDao<FeeRuleOperDeliveryModel> {
    List<FeeRuleOperDeliveryItemVo> getFeeOperDeliveryItem();
    List<FeeRuleOperDeliveryCalVo> getFeeOperDeliveryCal();
    List<FeeRuleOperDeliveryDocVo> getFeeOperDeliveryDoc();
    int getFeeOperDeliveryCount(String owner_no);
    //批量删除
    int batchDeleteByIds(List<String> list);
    //导出获取list(不分页)
    List<FeeRuleOperDeliveryModel> getExcelModelList(FeeRuleOperDeliveryDto feeRuleOperDeliveryDto) throws Exception;
    //获取所需要的计费费用结果数据
    List<FeeRuleOperDeliveryModel> getFeeOperDeliveryList(FeeRuleOperDeliveryDto feeRuleOperDeliveryDto);

}
