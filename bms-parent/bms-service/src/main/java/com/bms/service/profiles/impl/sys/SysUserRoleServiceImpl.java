package com.bms.service.profiles.impl.sys;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.base.common.core.dao.IBaseDao;
import com.base.common.core.service.impl.BaseServiceImpl;
import com.bms.service.model.sys.SysUserRoleModel;
import com.bms.service.profiles.dao.sys.ISysUserRoleDao;
import com.bms.service.profiles.service.sys.ISysUserRoleService;

/**
 * <b>description</b>：用户角色对应关系业务实现<br>
 * <b>time</b>：2018-11-15 10:29:03 <br>
 * <b>author</b>：  zhaochen
 */
@Service("iSysUserRoleService")
@Transactional(readOnly = true,rollbackFor = Exception.class)
public class SysUserRoleServiceImpl extends BaseServiceImpl<SysUserRoleModel> implements ISysUserRoleService {

	@Resource
	private ISysUserRoleDao isysUserRoleDao;

	@Override
	public IBaseDao<SysUserRoleModel> getBaseDao() {
		return this.isysUserRoleDao;
	}

}
