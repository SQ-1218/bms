package com.bms.service.vo.bd;

import com.base.common.core.base.VO;

/**
 * Created by jsc on 2018/9/30.
 */

public class FeeResultOperCountVo extends VO {
    private static final long serialVersionUID = 1L;
    //商品种类
   private int kind;
    //件数
    private int qty;

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getKind() {
        return kind;
    }

    public void setKind(int kind) {
        this.kind = kind;
    }
}
