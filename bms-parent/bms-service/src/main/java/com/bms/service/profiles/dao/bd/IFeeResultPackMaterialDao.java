package com.bms.service.profiles.dao.bd;

import com.base.common.core.dao.IBaseDao;
import com.bms.service.dto.bd.FeeResultPackMaterialDto;
import com.bms.service.model.bd.FeeResultPackMaterialModel;
import com.bms.service.vo.bd.FeeResultPackMaterialVo;

import java.util.List;

public interface IFeeResultPackMaterialDao  extends IBaseDao<FeeResultPackMaterialModel> {
//获取费用
    //导出获取list(不分页)
    List<FeeResultPackMaterialModel> getExcelModelList(FeeResultPackMaterialDto feeResultPackMaterialDto) throws Exception;

    //循环插入
    void insertFeeResultPackMaterial();
}
