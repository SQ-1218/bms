package com.bms.service.vo.bd;

import com.base.common.core.base.VO;

/**
 * Created by jsc on 2018/9/4.
 */
public class FeeRuleOperReceiVo extends VO {
    private static final long serialVersionUID = 1L;
    private String code;
    private String name;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
