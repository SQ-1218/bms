package com.bms.service.dto.bd;

import com.base.common.core.base.DTO;

import java.sql.Timestamp;
import java.util.List;

public class LogChangeOwnerBalanceDto extends DTO {
    /**
     * @Fields serialVersionUID
     */
    private static final long serialVersionUID = 1L;
    private String id;
    private String no;
    private String cargo_owner_no;
    private double balance_fm;
    private double balance_to;
    private String remark;
    private String created_by;
    private Timestamp created_time;
    private String updated_by;
    private Timestamp updated_time;
    private int is_deleted;
    private int ver;
    private List<String> noList;
    private String start_time;
    private String end_time;

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getCargo_owner_no() {
        return cargo_owner_no;
    }

    public void setCargo_owner_no(String cargo_owner_no) {
        this.cargo_owner_no = cargo_owner_no;
    }

    public double getBalance_fm() {
        return balance_fm;
    }

    public void setBalance_fm(double balance_fm) {
        this.balance_fm = balance_fm;
    }

    public double getBalance_to() {
        return balance_to;
    }

    public void setBalance_to(double balance_to) {
        this.balance_to = balance_to;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
    public Timestamp getCreated_time() {
        return created_time;
    }

    public void setCreated_time(Timestamp created_time) {
        this.created_time = created_time;
    }

    public Timestamp getUpdated_time() {
        return updated_time;
    }

    public void setUpdated_time(Timestamp updated_time) {
        this.updated_time = updated_time;
    }

    public int getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(int is_deleted) {
        this.is_deleted = is_deleted;
    }

    public int getVer() {
        return ver;
    }

    public void setVer(int ver) {
        this.ver = ver;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public String getId() { return id; }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getNoList() {
        return noList;
    }

    public void setNoList(List<String> noList) {
        this.noList = noList;
    }
}
