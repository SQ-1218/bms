package com.bms.service.profiles.dao.bd;

import com.base.common.core.dao.IBaseDao;
import com.bms.service.model.bd.FeeResultOwnerDailyModel;
import com.bms.service.vo.bd.FeeResultOwnerDailyVo;


/**
 * Created by jsc on 2018/11/21.
 */
public interface IFeeResultOwnerDailyDao extends IBaseDao<FeeResultOwnerDailyModel> {
    //获取当前货主快递总费用
    FeeResultOwnerDailyVo expressfee(String owner_no);

    //获取当前货主仓储总费用
    FeeResultOwnerDailyVo storagefee(String owner_no);

    //获取当前货主出库总费用
    FeeResultOwnerDailyVo deliveryfee(String owner_no);

    //获取当前货主入库总费用
    FeeResultOwnerDailyVo receivingfee(String owner_no);

    //获取当前货主特殊总费用
    FeeResultOwnerDailyVo specialfee(String owner_no);

    //获取当前货主包材总费用
    FeeResultOwnerDailyVo materialfee(String owner_no);
}
