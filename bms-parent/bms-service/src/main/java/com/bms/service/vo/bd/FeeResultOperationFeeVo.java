package com.bms.service.vo.bd;

import com.base.common.core.base.VO;

/**
 * Created by jsc on 2018/12/21.
 */
public class FeeResultOperationFeeVo extends VO {
    private static final long serialVersionUID = 1L;

    private String no;       //订单编号
    private String type;     //订单状态
    private double fee;     //费用

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getFee() {
        return fee;
    }

    public void setFee(double fee) {
        this.fee = fee;
    }

    private double qty;              //订单数量

    public double getQty() {
        return qty;
    }

    public void setQty(double qty) {
        this.qty = qty;
    }


}
