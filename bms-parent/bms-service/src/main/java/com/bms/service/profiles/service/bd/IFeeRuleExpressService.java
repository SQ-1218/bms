package com.bms.service.profiles.service.bd;

import com.base.common.core.service.IBaseService;
import com.bms.common.response.BaseResponse;
import com.bms.common.table.vo.DataTablesVO;
import com.bms.service.dto.bd.FeeRuleExpressDto;
import com.bms.service.model.bd.FeeRuleExpressModel;
import com.bms.service.vo.bd.CarrierVo;
import com.bms.service.vo.bd.ProvinceVo;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.List;

//快递配置接口
public interface IFeeRuleExpressService extends IBaseService<FeeRuleExpressModel> {
    DataTablesVO<FeeRuleExpressModel> getFeeRuleExpressList(FeeRuleExpressDto feeRuleExpressDto) throws Exception;
    int batchDeleteByIds(List<String> list)throws Exception;
    //导入
    BaseResponse importExcelInfo(InputStream in, MultipartFile file);
    //模板下载
    XSSFWorkbook downloadExcelInfo(FeeRuleExpressDto feeRuleExpressDto) throws Exception;
    //导出
    XSSFWorkbook exportExcelInfo(FeeRuleExpressDto feeRuleExpressDto) throws Exception;
    //获取快递公司
    List<CarrierVo> getCarrier() throws Exception;
    //获取省份
    List<ProvinceVo> getProvince() throws Exception;
}
