package com.bms.service.profiles.impl.bd;

import com.base.common.core.base.DTO;
import com.base.common.core.dao.IBaseDao;
import com.base.common.core.pager.service.IPagerService;
import com.base.common.core.service.impl.BaseServiceImpl;
import com.bms.common.table.vo.AmazuePagerUtil;
import com.bms.common.table.vo.DataTablesVO;
import com.bms.common.table.vo.ExcelBean;
import com.bms.common.util.ExcelUtil;
import com.bms.service.dto.bd.FeeResultPackMaterialDto;
import com.bms.service.model.bd.FeeResultPackMaterialModel;
import com.bms.service.profiles.dao.bd.IFeeResultPackMaterialDao;
import com.bms.service.profiles.service.bd.IFeeResultPackMaterialService;
import com.bms.service.vo.bd.FeeResultPackMaterialVo;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.util.*;

@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class FeeResultPackMaterialServiceImpl  extends BaseServiceImpl<FeeResultPackMaterialModel> implements IFeeResultPackMaterialService {
    @Resource
    private IFeeResultPackMaterialDao feeResultPackMaterialDao;
    @Override
    protected IBaseDao<FeeResultPackMaterialModel> getBaseDao() {
        return feeResultPackMaterialDao;
    }

    @Override
    public List<FeeResultPackMaterialModel> getFee() throws Exception {
        //插入
        feeResultPackMaterialDao.insertFeeResultPackMaterial();
        System.out.println("插入成功");
        return null;
    }

    @Override
    public DataTablesVO<FeeResultPackMaterialModel> getFeeResultPackMaterial(FeeResultPackMaterialDto feeResultPackMaterialDto) throws Exception {
        return AmazuePagerUtil.getPagerModel(new IPagerService<FeeResultPackMaterialModel>() {
            @Override
            public int queryCount(DTO dto) throws Exception {
                return feeResultPackMaterialDao.getModelListCount(dto);
            }
            @Override
            public List<FeeResultPackMaterialModel> queryList(DTO dto) throws Exception {
                return feeResultPackMaterialDao.getModelList(dto);
            }
        }, feeResultPackMaterialDto);
    }

    @Override
    public XSSFWorkbook exportExcelInfo(FeeResultPackMaterialDto feeResultPackMaterialDto) throws Exception {
        List<FeeResultPackMaterialModel> list=feeResultPackMaterialDao.getExcelModelList(feeResultPackMaterialDto);
        List<ExcelBean> excel=new ArrayList<>();
        Map<Integer,List<ExcelBean>> map=new LinkedHashMap<>();
        XSSFWorkbook xssfWorkbook=null;
        //设置标题栏
        excel.add(new ExcelBean("货主","owner_no",0));
        excel.add(new ExcelBean("单据号","doc_no",0));
        excel.add(new ExcelBean("包材编号","carton_type",0));
        excel.add(new ExcelBean("计费结果","fee",0));
        excel.add(new ExcelBean("费用产生时间","fee_occur_time",0));
        excel.add(new ExcelBean("是否已结算(0为结算,1为未结算)","is_confirmed",0));
        excel.add(new ExcelBean("确认结算时间","confirmed_time",0));
        map.put(0, excel);
        String sheetName = "包材费用计费结果";
        //调用ExcelUtil的方法
        xssfWorkbook = ExcelUtil.createExcelFile(FeeResultPackMaterialModel.class, list, map, sheetName);
        return xssfWorkbook;
    }
}
