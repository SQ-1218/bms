package com.bms.service.profiles.service.bd;

import com.base.common.core.service.IBaseService;
import com.bms.common.table.vo.DataTablesVO;
import com.bms.service.dto.bd.BdPackMaterialDto;
import com.bms.service.dto.bd.FeeRuleExpressDto;
import com.bms.service.model.bd.BdPackMaterialModel;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.List;

public interface IBdPackMaterialService extends IBaseService<BdPackMaterialModel> {
    DataTablesVO<BdPackMaterialModel> getBdPackMaterialList(BdPackMaterialDto bdPackMaterialDto) throws Exception;
    int batchDeleteByIds(List<String> list)throws Exception;
    //模板下载
    XSSFWorkbook downloadExcelInfo(BdPackMaterialDto bdPackMaterialDto) throws Exception;
    //导出
    XSSFWorkbook exportExcelInfo(BdPackMaterialDto bdPackMaterialDto) throws Exception;
    //导入
    int importExcelInfo(InputStream in, MultipartFile file);
}
