package com.bms.service.vo.bd;

import com.base.common.core.base.VO;

/**
 * <b>description</b>：货主信息表返回bean<br>
 * <b>time</b>：2017-08-08 16:24:44 <br>
 * <b>author</b>：  zhaochen
 */
public class BdCargoOwnerVo extends VO{

	/**
	* @Fields serialVersionUID
	*/
	private static final long serialVersionUID = 1L;
	
	 /*
	  * 
	  */
	private java.lang.String id;
	 /*
	  *货主编号 
	  */
	private java.lang.String no;
	 /*
	  *货主名称 
	  */
	private java.lang.String name;
	 /*
	  *是否可用 
	  */
	private java.lang.Integer is_available;
	 /*
	  * 
	  */
	private java.sql.Timestamp created_time;
	 /*
	  * 
	  */
	private java.lang.String created_by;
	 /*
	  * 
	  */
	private java.sql.Timestamp updated_time;
	 /*
	  * 
	  */
	private java.lang.String updated_by;
	 /*
	  *版本号 
	  */
	private java.lang.Integer ver;
	 /*
	  * 
	  */
	private java.lang.Integer is_deleted;
	 /*
	  *货主简称 
	  */
	private java.lang.String alias;
	 /*
	  *地址 
	  */
	private java.lang.String address;
	 /*
	  *电话 
	  */
	private java.lang.String phone;
	 /*
	  *传真 
	  */
	private java.lang.String fax;
	 /*
	  *备注 
	  */
	private java.lang.String remark;
	
	private java.lang.Double balance;
	
	private java.lang.Double warning_balance;
	
	private java.lang.Double oper_cost;

		
	public java.lang.String getId()
	{
		return id;
	}
	
	public void setId(java.lang.String id)
	{
		this.id = id;
	}
		
	public java.lang.String getNo()
	{
		return no;
	}
	
	public void setNo(java.lang.String no)
	{
		this.no = no;
	}
		
	public java.lang.String getName()
	{
		return name;
	}
	
	public void setName(java.lang.String name)
	{
		this.name = name;
	}
		
	public java.lang.Integer getIs_available()
	{
		return is_available;
	}
	
	public void setIs_available(java.lang.Integer is_available)
	{
		this.is_available = is_available;
	}
		
	public java.sql.Timestamp getCreated_time()
	{
		return created_time;
	}
	
	public void setCreated_time(java.sql.Timestamp created_time)
	{
		this.created_time = created_time;
	}
		
	public java.lang.String getCreated_by()
	{
		return created_by;
	}
	
	public void setCreated_by(java.lang.String created_by)
	{
		this.created_by = created_by;
	}
		
	public java.sql.Timestamp getUpdated_time()
	{
		return updated_time;
	}
	
	public void setUpdated_time(java.sql.Timestamp updated_time)
	{
		this.updated_time = updated_time;
	}
		
	public java.lang.String getUpdated_by()
	{
		return updated_by;
	}
	
	public void setUpdated_by(java.lang.String updated_by)
	{
		this.updated_by = updated_by;
	}
		
	public java.lang.Integer getVer()
	{
		return ver;
	}
	
	public void setVer(java.lang.Integer ver)
	{
		this.ver = ver;
	}
		
	public java.lang.Integer getIs_deleted()
	{
		return is_deleted;
	}
	
	public void setIs_deleted(java.lang.Integer is_deleted)
	{
		this.is_deleted = is_deleted;
	}
		
	public java.lang.String getAlias()
	{
		return alias;
	}
	
	public void setAlias(java.lang.String alias)
	{
		this.alias = alias;
	}
		
	public java.lang.String getAddress()
	{
		return address;
	}
	
	public void setAddress(java.lang.String address)
	{
		this.address = address;
	}
		
	public java.lang.String getPhone()
	{
		return phone;
	}
	
	public void setPhone(java.lang.String phone)
	{
		this.phone = phone;
	}
		
	public java.lang.String getFax()
	{
		return fax;
	}
	
	public void setFax(java.lang.String fax)
	{
		this.fax = fax;
	}
		
	public java.lang.String getRemark()
	{
		return remark;
	}
	
	public void setRemark(java.lang.String remark)
	{
		this.remark = remark;
	}

	public java.lang.Double getBalance() {
		return balance;
	}

	public void setBalance(java.lang.Double balance) {
		this.balance = balance;
	}

	public java.lang.Double getOper_cost() {
		return oper_cost;
	}

	public void setOper_cost(java.lang.Double oper_cost) {
		this.oper_cost = oper_cost;
	}

	public java.lang.Double getWarning_balance() {
		return warning_balance;
	}

	public void setWarning_balance(java.lang.Double warning_balance) {
		this.warning_balance = warning_balance;
	}
}
