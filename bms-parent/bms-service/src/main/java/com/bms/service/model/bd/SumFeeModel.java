package com.bms.service.model.bd;

import com.base.common.core.model.Model;

/**
 * Created by jsc on 2018/10/17.
 */
public class SumFeeModel extends Model {
    private static final long serialVersionUID = 1L;
    private String owner_no;
    private Double fee;
    private String name;
    private String fee_type;
    private int status;
    //按月
    private String month_time;
    //年份
    private String year;
    //月份
    private String month;

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getMonth_time() {
        return month_time;
    }

    public void setMonth_time(String month_time) {
        this.month_time = month_time;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getOwner_no() {
        return owner_no;
    }

    public void setOwner_no(String owner_no) {
        this.owner_no = owner_no;
    }

    public Double getFee() {
        return fee;
    }

    public void setFee(Double fee) {
        this.fee = fee;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

	public String getFee_type() {
		return fee_type;
	}

	public void setFee_type(String fee_type) {
		this.fee_type = fee_type;
	}
    
}
