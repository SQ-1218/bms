package com.bms.service.vo.sys;

import com.base.common.core.base.VO;

/**
 * <b>description</b>：用户角色对应关系返回bean<br>
 * <b>time</b>：2018-11-15 10:29:03 <br>
 * <b>author</b>：  zhaochen
 */
public class SysUserRoleVo extends VO{

	/**
	* @Fields serialVersionUID
	*/
	private static final long serialVersionUID = 1L;
	
	 /*
	  * 
	  */
	private java.lang.String id;
	 /*
	  *用户Id 
	  */
	private java.lang.String user_id;
	 /*
	  *角色Id 
	  */
	private java.lang.String role_id;
	 /*
	  * 
	  */
	private java.sql.Timestamp created_time;
	 /*
	  * 
	  */
	private java.lang.String created_by;
	 /*
	  * 
	  */
	private java.sql.Timestamp updated_time;
	 /*
	  * 
	  */
	private java.lang.String updated_by;
	 /*
	  * 
	  */
	private java.lang.Integer ver;
	 /*
	  * 
	  */
	private java.lang.Integer is_deleted;

		
	public java.lang.String getId()
	{
		return id;
	}
	
	public void setId(java.lang.String id)
	{
		this.id = id;
	}
		
	public java.lang.String getUser_id()
	{
		return user_id;
	}
	
	public void setUser_id(java.lang.String user_id)
	{
		this.user_id = user_id;
	}
		
	public java.lang.String getRole_id()
	{
		return role_id;
	}
	
	public void setRole_id(java.lang.String role_id)
	{
		this.role_id = role_id;
	}
		
	public java.sql.Timestamp getCreated_time()
	{
		return created_time;
	}
	
	public void setCreated_time(java.sql.Timestamp created_time)
	{
		this.created_time = created_time;
	}
		
	public java.lang.String getCreated_by()
	{
		return created_by;
	}
	
	public void setCreated_by(java.lang.String created_by)
	{
		this.created_by = created_by;
	}
		
	public java.sql.Timestamp getUpdated_time()
	{
		return updated_time;
	}
	
	public void setUpdated_time(java.sql.Timestamp updated_time)
	{
		this.updated_time = updated_time;
	}
		
	public java.lang.String getUpdated_by()
	{
		return updated_by;
	}
	
	public void setUpdated_by(java.lang.String updated_by)
	{
		this.updated_by = updated_by;
	}
		
	public java.lang.Integer getVer()
	{
		return ver;
	}
	
	public void setVer(java.lang.Integer ver)
	{
		this.ver = ver;
	}
		
	public java.lang.Integer getIs_deleted()
	{
		return is_deleted;
	}
	
	public void setIs_deleted(java.lang.Integer is_deleted)
	{
		this.is_deleted = is_deleted;
	}

	

}
