package com.bms.service.profiles.dao.bd;

import com.base.common.core.dao.IBaseDao;
import com.bms.service.dto.bd.FeeRuleSpecialDto;
import com.bms.service.model.bd.FeeRuleSpecialModel;
import com.bms.service.vo.bd.FeeRuleSpecialCalVo;
import com.bms.service.vo.bd.FeeRuleSpecialItemVo;

import java.util.List;

public interface IFeeRuleSpecialDao extends IBaseDao<FeeRuleSpecialModel> {
    List<FeeRuleSpecialItemVo> getFeeRuleSpecialItem();
    List<FeeRuleSpecialCalVo> getFeeRuleSpecialCal();
    //批量删除
    int batchDeleteByIds(List<String> list);
    int getFeeRuleSpecialCount(String owner_no);
    //导出获取list(不分页)
    List<FeeRuleSpecialModel> getExcelModelList(FeeRuleSpecialDto feeRuleSpecialDto) throws Exception;
    //获取特殊计算费用所需要的数据
    List<FeeRuleSpecialModel> getFeeRuleSpecialList() throws Exception;
    //获取计费code
    List<String> getItemList(String owner_no);
    //获取每日正常出库订单数量(每日单量)
    int getDailyMono(String owner_no) throws Exception;
    //获取不同货主对应不同每日单量的优惠方式
    String factor1Value(String owner_no)throws Exception;
    String factor2Value(String owner_no)throws Exception;
}
