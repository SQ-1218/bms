package com.bms.service.dto.bd;

import com.bms.common.table.vo.BmsBaseDto;

import java.sql.Timestamp;

import java.util.List;

/**
 * Created by jsc on 2018/9/12.
 */
public class FeeResultExpressDto extends BmsBaseDto {
    private static final long serialVersionUID = 1L;
    private String id;
    private String owner_no;
    private String whs_no;
    private String doc_no;
    private String ref_no;
    private String express_corp_no;
    private String express_no;
    private String province;
    private Double weight;
    private Double fee;
    private Timestamp fee_occur_time;
    private int is_confirmed;
    private Timestamp confirmed_time;
    private String remark;
    private Timestamp created_time;
    private String created_by;
    private Timestamp updated_time;
    private String updated_by;
    private Integer ver;
    private Integer is_deleted;
    private List<String> noList;
    private List<String> owner_noList;
    private List<String> provinceList;
    private List<String> express_corp_noList;
    private String start_time;
    private String end_time;

    public int getIs_confirmed() {
        return is_confirmed;
    }

    public void setIs_confirmed(int is_confirmed) {
        this.is_confirmed = is_confirmed;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public List<String> getOwner_noList() {
        return owner_noList;
    }

    public void setOwner_noList(List<String> owner_noList) {
        this.owner_noList = owner_noList;
    }

    public List<String> getProvinceList() {
        return provinceList;
    }

    public void setProvinceList(List<String> provinceList) {
        this.provinceList = provinceList;
    }

    public List<String> getExpress_corp_noList() {
        return express_corp_noList;
    }

    public void setExpress_corp_noList(List<String> express_corp_noList) {
        this.express_corp_noList = express_corp_noList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwner_no() {
        return owner_no;
    }

    public void setOwner_no(String owner_no) {
        this.owner_no = owner_no;
    }

    public String getWhs_no() {
        return whs_no;
    }

    public void setWhs_no(String whs_no) {
        this.whs_no = whs_no;
    }

    public String getDoc_no() {
        return doc_no;
    }

    public void setDoc_no(String doc_no) {
        this.doc_no = doc_no;
    }

    public String getRef_no() {
        return ref_no;
    }

    public void setRef_no(String ref_no) {
        this.ref_no = ref_no;
    }

    public String getExpress_corp_no() {
        return express_corp_no;
    }

    public void setExpress_corp_no(String express_corp_no) {
        this.express_corp_no = express_corp_no;
    }

    public String getExpress_no() {
        return express_no;
    }

    public void setExpress_no(String express_no) {
        this.express_no = express_no;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getFee() {
        return fee;
    }

    public void setFee(Double fee) {
        this.fee = fee;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }



    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }


    public String getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public Integer getVer() {
        return ver;
    }

    public void setVer(Integer ver) {
        this.ver = ver;
    }

    public Integer getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(Integer is_deleted) {
        this.is_deleted = is_deleted;
    }

    public List<String> getNoList() {
        return noList;
    }

    public Timestamp getFee_occur_time() {
        return fee_occur_time;
    }

    public void setFee_occur_time(Timestamp fee_occur_time) {
        this.fee_occur_time = fee_occur_time;
    }



    public Timestamp getConfirmed_time() {
        return confirmed_time;
    }

    public void setConfirmed_time(Timestamp confirmed_time) {
        this.confirmed_time = confirmed_time;
    }

    public Timestamp getCreated_time() {
        return created_time;
    }

    public void setCreated_time(Timestamp created_time) {
        this.created_time = created_time;
    }

    public Timestamp getUpdated_time() {
        return updated_time;
    }

    public void setUpdated_time(Timestamp updated_time) {
        this.updated_time = updated_time;
    }

    public void setNoList(List<String> noList) {
        this.noList = noList;
    }
}
