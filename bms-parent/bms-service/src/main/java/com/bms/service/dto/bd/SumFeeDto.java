package com.bms.service.dto.bd;

import com.bms.common.table.vo.BmsBaseDto;

/**
 * Created by jsc on 2018/10/16.
 */
public class SumFeeDto extends BmsBaseDto {
    private static final long serialVersionUID = 1L;
    private String owner_no;
    private Double fee;
    private String name;
    private int status;
    private int is_deleted;
    //按月
    private String month_time;
    //年份
    private String year;
    //月份
    private String month;

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public int getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(int is_deleted) {
        this.is_deleted = is_deleted;
    }


    public String getMonth_time() {
        return month_time;
    }

    public void setMonth_time(String month_time) {
        this.month_time = month_time;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwner_no() {
        return owner_no;
    }

    public void setOwner_no(String owner_no) {
        this.owner_no = owner_no;
    }

    public Double getFee() {
        return fee;
    }

    public void setFee(Double fee) {
        this.fee = fee;
    }
}
