package com.bms.service.profiles.impl.bd;

import com.base.common.core.base.DTO;
import com.base.common.core.dao.IBaseDao;
import com.base.common.core.pager.service.IPagerService;
import com.base.common.core.service.impl.BaseServiceImpl;
import com.bms.common.table.vo.AmazuePagerUtil;
import com.bms.common.table.vo.DataTablesVO;
import com.bms.common.table.vo.ExcelBean;
import com.bms.common.util.ExcelUtil;
import com.bms.service.dto.bd.BdCargoOwnerDto;
import com.bms.service.dto.bd.FeeResultExpressDto;
import com.bms.service.dto.bd.FeeRuleExpressDto;
import com.bms.service.model.bd.BdCargoOwnerModel;
import com.bms.service.model.bd.FeeResultExpressModel;
import com.bms.service.model.bd.FeeRuleExpressModel;
import com.bms.service.profiles.dao.bd.IBdCargoOwnerDao;
import com.bms.service.profiles.dao.bd.IFeeResultExpressDao;
import com.bms.service.profiles.dao.bd.IFeeRuleExpressDao;
import com.bms.service.profiles.service.bd.IFeeResultExpressService;
import com.bms.service.vo.bd.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by jsc on 2018/9/12.
 */
@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class FeeResultExpressServiceImpl extends BaseServiceImpl<FeeResultExpressModel> implements IFeeResultExpressService {
    @Resource
    private IFeeResultExpressDao iFeeResultExpressDao;
    @Resource
    private IBdCargoOwnerDao iBdCargoOwnerDao;

    public static final int num = 1;

    @Override
    protected IBaseDao<FeeResultExpressModel> getBaseDao() {
        return this.iFeeResultExpressDao;
    }

    @Override
    public List<FeeResultExpressModel> getFee() throws Exception {
        //获取货主
        BdCargoOwnerDto bdCargoOwnerDto = new BdCargoOwnerDto();
        bdCargoOwnerDto.setIs_deleted(1);
        //获取所有货主
        List<BdCargoOwnerModel> bdCargoOwnerModelList = iBdCargoOwnerDao.getowner_noList(bdCargoOwnerDto);
        List<FeeResultExpressModel> feeResultExpressModelList = new ArrayList<>();
        //循环货主
        for (BdCargoOwnerModel bdCargoOwnerModel : bdCargoOwnerModelList) {
            FeeResultExpressModel feeResultExpressModel = new FeeResultExpressModel();
            //获取当前户主的所有订单编号
            List<String> del_noList = iFeeResultExpressDao.delivery_noList(bdCargoOwnerModel.getNo());
            if (del_noList.size() > 0) {
                //循环订单
                for (String no : del_noList) {
                    //获取订单编号下的包裹信息
                    List<FeeResultExpressweightVo> feeResultExpressweightVoList = iFeeResultExpressDao.getdelivery(no);
                    //一个订单号有多个包裹
                    for (FeeResultExpressweightVo feeResultExpressweightVo : feeResultExpressweightVoList) {
                        double result = 0.0;
                        //费用
                        double fee = 1.0;
                        //包裹最终重量
                        double weight = 0.0;
                        //通过快递编号获取商品的总体积
                        FeeResultExpressSkuVo feeResultExpressSkuVo = iFeeResultExpressDao.getSkuVolume(feeResultExpressweightVo.getExpress_no());
                        //通过快递公司编号获取计抛体积
                        FeeResultExpressSkuVo feeResultExpressSkuVo1 = iFeeResultExpressDao.getHollow_vol(feeResultExpressweightVo.getExpress_corp());
                        if (feeResultExpressweightVo.getMaterialvolume() != 0) {
                            //包材体积除以计抛系数与包裹重量取大
                            weight = Math.max(((feeResultExpressweightVo.getMaterialvolume() / Math.pow(1000, 3)) / (feeResultExpressSkuVo1.getHollow_vol() / Math.pow(1000, 2))), feeResultExpressweightVo.getWeight());
                        } else if (feeResultExpressSkuVo.getSkuvolume() != 0) {
                            //包裹重量与(商品体积除以计抛系数)取大
                            weight = Math.max(((feeResultExpressweightVo.getMaterialvolume() / Math.pow(1000, 3)) / (feeResultExpressSkuVo1.getHollow_vol() / Math.pow(1000, 2))), feeResultExpressweightVo.getWeight());
                        } else {
                            weight = feeResultExpressweightVo.getWeight();
                        }

                        //通过户主.省份 重量去找符合的规则
                        FeeRuleExpressVo feeRuleExpressVo = iFeeResultExpressDao.getPrice(bdCargoOwnerModel.getNo(), feeResultExpressweightVo.getProvince(), weight);
                        //有匹配到规则
                        if (feeRuleExpressVo != null) {
                            //价格 首重的费用+（实际重量-首重）*续重价格
                            fee = feeRuleExpressVo.getFee_fixed() + (Math.ceil(weight - num) * feeRuleExpressVo.getFee_extra());
                            feeResultExpressModel.setRemark("");
                            feeResultExpressModel.setFee(fee);
                        } else {
                            feeResultExpressModel.setFee(0.0);
                            feeResultExpressModel.setRemark("省份未配置");
                        }
                        // 获取当前时间
                        Date date1 = new Date();
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        //时间存储为字符串
                        String str = sdf.format(date1);
                        feeResultExpressModel.setOwner_no(bdCargoOwnerModel.getNo());
                        feeResultExpressModel.setDoc_no(no);
                        feeResultExpressModel.setRef_no(feeResultExpressweightVo.getRef_no());
                        feeResultExpressModel.setExpress_corp_no(feeResultExpressweightVo.getExpress_corp());
                        feeResultExpressModel.setExpress_no(feeResultExpressweightVo.getExpress_no());
                        feeResultExpressModel.setProvince(feeResultExpressweightVo.getProvince());
                        feeResultExpressModel.setWeight(weight);
                        feeResultExpressModel.setFee_occur_time(Timestamp.valueOf(str));
                        feeResultExpressModel.setCreated_by("admin");
                        //插入数据库
                        iFeeResultExpressDao.insert(feeResultExpressModel);
                    }
                }
            }
        }
        return null;
    }

    @Override
    public DataTablesVO<FeeResultExpressModel> getExpressList(FeeResultExpressDto feeResultExpressDto) throws Exception {
        return AmazuePagerUtil.getPagerModel(new IPagerService<FeeResultExpressModel>() {

            @Override
            public int queryCount(DTO dto) throws Exception {
                return iFeeResultExpressDao.getModelListCount(dto);
            }

            @Override
            public List<FeeResultExpressModel> queryList(DTO dto) throws Exception {
                return iFeeResultExpressDao.getModelList(dto);
            }
        }, feeResultExpressDto);
    }

    @Override
    public List<CarrierVo> getCarrier() throws Exception {
        return iFeeResultExpressDao.getCarrier();
    }

    @Override
    public List<ProvinceVo> getProvince() throws Exception {
        return iFeeResultExpressDao.getProvince();
    }

    @Override
    public XSSFWorkbook exportExcelInfo(FeeResultExpressDto feeResultExpressDto) throws Exception {
        //根据条件查询数据，把数据装载到一个list中
        List<FeeResultExpressModel> list = iFeeResultExpressDao.getExcelModelList(feeResultExpressDto);
        List<ExcelBean> excel = new ArrayList<>();
        Map<Integer, List<ExcelBean>> map = new LinkedHashMap<>();
        XSSFWorkbook xssfWorkbook = null;
        //设置标题栏
        excel.add(new ExcelBean("货主", "owner_no", 0));
        excel.add(new ExcelBean("订单号", "doc_no", 0));
        excel.add(new ExcelBean("关联单号", "ref_no", 0));
        excel.add(new ExcelBean("快递公司", "express_corp_no", 0));
        excel.add(new ExcelBean("快递单号", "express_no", 0));
        excel.add(new ExcelBean("省份", "province", 0));
        excel.add(new ExcelBean("重量", "weight", 0));
        excel.add(new ExcelBean("计费结果", "fee", 0));
        excel.add(new ExcelBean("费用产生时间", "fee_occur_time", 0));
        excel.add(new ExcelBean("是否已确认结算(0是未结算，1是已结算)", "is_confirmed", 0));
        excel.add(new ExcelBean("确认结算时间", "confirmed_time", 0));
        excel.add(new ExcelBean("备注", "remark", 0));
        map.put(0, excel);
        String sheetName = "快递计费结果";
        //调用ExcelUtil的方法
        xssfWorkbook = ExcelUtil.createExcelFile(FeeResultExpressModel.class, list, map, sheetName);
        return xssfWorkbook;
    }


}