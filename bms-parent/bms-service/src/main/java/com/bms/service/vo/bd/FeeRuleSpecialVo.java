package com.bms.service.vo.bd;

import com.base.common.core.base.VO;

import java.sql.Date;
import java.util.List;

public class FeeRuleSpecialVo  extends VO {
    private static final long serialVersionUID = 1L;
    private String id;
    private String owner_no;
    private double cal_item_rank;
    private String  cal_item_name;
    private String  cal_item_code;
    private String  cal_item_note;
    private String  doc_type;
    private String  cal_type;
    private String  factor1_note;
    private String  factor1_value;
    private String  factor2_note;
    private String  factor2_value;
    private String  factor3_note;
    private String  factor3_value;
    private String  factor4_note;
    private String  factor4_value;
    private String  factor5_note;
    private String  factor5_value;
    private String  factor6_note;
    private String  factor6_value;
    private String  factor7_note;
    private String  factor7_value;
    private String  factor8_note;
    private String  factor8_value;
    private String  factor9_note;
    private String  factor9_value;
    private String created_by;
    private Date created_time;
    private Date updated_time;
    private String updated_by;
    private String ver;
    private String is_deleted;
    private String remark;
    private List<String> owner_noList;
    private List<String>cal_item_codeList;
    private List<String> noList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwner_no() {
        return owner_no;
    }

    public void setOwner_no(String owner_no) {
        this.owner_no = owner_no;
    }

    public double getCal_item_rank() {
        return cal_item_rank;
    }

    public void setCal_item_rank(double cal_item_rank) {
        this.cal_item_rank = cal_item_rank;
    }

    public String getCal_item_name() {
        return cal_item_name;
    }

    public void setCal_item_name(String cal_item_name) {
        this.cal_item_name = cal_item_name;
    }

    public String getCal_item_code() {
        return cal_item_code;
    }

    public void setCal_item_code(String cal_item_code) {
        this.cal_item_code = cal_item_code;
    }

    public String getCal_item_note() {
        return cal_item_note;
    }

    public void setCal_item_note(String cal_item_note) {
        this.cal_item_note = cal_item_note;
    }

    public String getDoc_type() {
        return doc_type;
    }

    public void setDoc_type(String doc_type) {
        this.doc_type = doc_type;
    }

    public String getCal_type() {
        return cal_type;
    }

    public void setCal_type(String cal_type) {
        this.cal_type = cal_type;
    }

    public String getFactor1_note() {
        return factor1_note;
    }

    public void setFactor1_note(String factor1_note) {
        this.factor1_note = factor1_note;
    }

    public String getFactor1_value() {
        return factor1_value;
    }

    public void setFactor1_value(String factor1_value) {
        this.factor1_value = factor1_value;
    }

    public String getFactor2_note() {
        return factor2_note;
    }

    public void setFactor2_note(String factor2_note) {
        this.factor2_note = factor2_note;
    }

    public String getFactor2_value() {
        return factor2_value;
    }

    public void setFactor2_value(String factor2_value) {
        this.factor2_value = factor2_value;
    }

    public String getFactor3_note() {
        return factor3_note;
    }

    public void setFactor3_note(String factor3_note) {
        this.factor3_note = factor3_note;
    }

    public String getFactor3_value() {
        return factor3_value;
    }

    public void setFactor3_value(String factor3_value) {
        this.factor3_value = factor3_value;
    }

    public String getFactor4_note() {
        return factor4_note;
    }

    public void setFactor4_note(String factor4_note) {
        this.factor4_note = factor4_note;
    }

    public String getFactor4_value() {
        return factor4_value;
    }

    public void setFactor4_value(String factor4_value) {
        this.factor4_value = factor4_value;
    }

    public String getFactor5_note() {
        return factor5_note;
    }

    public void setFactor5_note(String factor5_note) {
        this.factor5_note = factor5_note;
    }

    public String getFactor5_value() {
        return factor5_value;
    }

    public void setFactor5_value(String factor5_value) {
        this.factor5_value = factor5_value;
    }

    public String getFactor6_note() {
        return factor6_note;
    }

    public void setFactor6_note(String factor6_note) {
        this.factor6_note = factor6_note;
    }

    public String getFactor6_value() {
        return factor6_value;
    }

    public void setFactor6_value(String factor6_value) {
        this.factor6_value = factor6_value;
    }

    public String getFactor7_note() {
        return factor7_note;
    }

    public void setFactor7_note(String factor7_note) {
        this.factor7_note = factor7_note;
    }

    public String getFactor7_value() {
        return factor7_value;
    }

    public void setFactor7_value(String factor7_value) {
        this.factor7_value = factor7_value;
    }

    public String getFactor8_note() {
        return factor8_note;
    }

    public void setFactor8_note(String factor8_note) {
        this.factor8_note = factor8_note;
    }

    public String getFactor8_value() {
        return factor8_value;
    }

    public void setFactor8_value(String factor8_value) {
        this.factor8_value = factor8_value;
    }

    public String getFactor9_note() {
        return factor9_note;
    }

    public void setFactor9_note(String factor9_note) {
        this.factor9_note = factor9_note;
    }

    public String getFactor9_value() {
        return factor9_value;
    }

    public void setFactor9_value(String factor9_value) {
        this.factor9_value = factor9_value;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public Date getCreated_time() {
        return created_time;
    }

    public void setCreated_time(Date created_time) {
        this.created_time = created_time;
    }

    public Date getUpdated_time() {
        return updated_time;
    }

    public void setUpdated_time(Date updated_time) {
        this.updated_time = updated_time;
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public String getVer() {
        return ver;
    }

    public void setVer(String ver) {
        this.ver = ver;
    }

    public String getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(String is_deleted) {
        this.is_deleted = is_deleted;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public List<String> getOwner_noList() {
        return owner_noList;
    }

    public void setOwner_noList(List<String> owner_noList) {
        this.owner_noList = owner_noList;
    }

    public List<String> getCal_item_codeList() {
        return cal_item_codeList;
    }

    public void setCal_item_codeList(List<String> cal_item_codeList) {
        this.cal_item_codeList = cal_item_codeList;
    }

    public List<String> getNoList() {
        return noList;
    }

    public void setNoList(List<String> noList) {
        this.noList = noList;
    }
}
