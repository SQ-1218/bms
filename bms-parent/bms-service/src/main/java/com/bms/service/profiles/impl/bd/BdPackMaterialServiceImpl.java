package com.bms.service.profiles.impl.bd;

import com.base.common.core.base.DTO;
import com.base.common.core.dao.IBaseDao;
import com.base.common.core.pager.service.IPagerService;
import com.base.common.core.service.impl.BaseServiceImpl;
import com.bms.common.table.vo.AmazuePagerUtil;
import com.bms.common.table.vo.DataTablesVO;
import com.bms.common.table.vo.ExcelBean;
import com.bms.common.util.ExcelUtil;
import com.bms.service.dto.bd.BdPackMaterialDto;
import com.bms.service.model.bd.BdPackMaterialModel;
import com.bms.service.model.bd.FeeResultPackMaterialModel;
import com.bms.service.model.bd.FeeRuleExpressModel;
import com.bms.service.profiles.dao.bd.IBdPackMaterialDao;
import com.bms.service.profiles.service.bd.IBdPackMaterialService;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class BdPackMaterialServiceImpl extends BaseServiceImpl<BdPackMaterialModel> implements IBdPackMaterialService {
    @Resource
    private IBdPackMaterialDao bdPackMaterialDao;
    @Override
    protected IBaseDao<BdPackMaterialModel> getBaseDao() {
        return this.bdPackMaterialDao;
    }

    @Override
    public DataTablesVO<BdPackMaterialModel> getBdPackMaterialList(BdPackMaterialDto bdPackMaterialDto) throws Exception {
        return AmazuePagerUtil.getPagerModel(new IPagerService<BdPackMaterialModel>() {
            @Override
            public int queryCount(DTO dto) throws Exception {
                return bdPackMaterialDao.getModelListCount(dto);
            }
            @Override
            public List<BdPackMaterialModel> queryList(DTO dto) throws Exception {
                return bdPackMaterialDao.getModelList(dto);
            }
        }, bdPackMaterialDto);
    }

    @Override
    public int batchDeleteByIds(List<String> list) throws Exception {
        int result = 0;
        result = bdPackMaterialDao.batchDeleteByIds(list);
        if (result <= 0) {
            throw new Exception("删除失败，请重试！");
        }
        return result;
    }

    @Override
    public XSSFWorkbook downloadExcelInfo(BdPackMaterialDto bdPackMaterialDto) throws Exception {
        List<BdPackMaterialDto> list=new ArrayList<>();
        list.add(bdPackMaterialDto);
        List<ExcelBean> excel=new ArrayList<>();
        Map<Integer,List<ExcelBean>> map=new LinkedHashMap<>();
        XSSFWorkbook xssfWorkbook=null;
        //设置标题栏
        excel.add(new ExcelBean("货主","cargo_owner_no",0));
        excel.add(new ExcelBean("包材条码","barcode",0));
        excel.add(new ExcelBean("包材名称","name",0));
        excel.add(new ExcelBean("价格(元)","price",0));
        excel.add(new ExcelBean("长(mm)","LENGTH",0));
        excel.add(new ExcelBean("宽(mm)","width",0));
        excel.add(new ExcelBean("高(mm)","height",0));
        excel.add(new ExcelBean("重量(g)","weight",0));
        map.put(0, excel);
        String sheetName = "快递费用配置规则";
        //调用ExcelUtil的方法
        xssfWorkbook = ExcelUtil.createExcelFile(BdPackMaterialDto.class, list, map, sheetName);
        return xssfWorkbook;
    }

    @Override
    public XSSFWorkbook exportExcelInfo(BdPackMaterialDto bdPackMaterialDto) throws Exception {
        //根据条件查询数据，把数据装载到一个list中
        List<BdPackMaterialModel> list = bdPackMaterialDao.getExcelModelList(bdPackMaterialDto);
        List<ExcelBean> excel=new ArrayList<>();
        Map<Integer,List<ExcelBean>> map=new LinkedHashMap<>();
        XSSFWorkbook xssfWorkbook=null;
        //设置标题栏
        excel.add(new ExcelBean("货主","cargo_owner_no",0));
        excel.add(new ExcelBean("包材条码","barcode",0));
        excel.add(new ExcelBean("包材名称","name",0));
        excel.add(new ExcelBean("价格(元)","price",0));
        excel.add(new ExcelBean("长(mm)","LENGTH",0));
        excel.add(new ExcelBean("宽(mm)","width",0));
        excel.add(new ExcelBean("高(mm)","height",0));
        excel.add(new ExcelBean("重量(g)","weight",0));
        map.put(0, excel);
        String sheetName = "包材配置规则";
        //调用ExcelUtil的方法
        xssfWorkbook = ExcelUtil.createExcelFile(BdPackMaterialModel.class, list, map, sheetName);
        return xssfWorkbook;
    }

    @Override
    public int importExcelInfo(InputStream in, MultipartFile file) {
        int status=0;
        try {
            List<List<Object>> listOb = ExcelUtil.getBankListByExcel(in,file.getOriginalFilename());
            List<BdPackMaterialModel> list=new ArrayList<>();
            //遍历listob数据，把数据放到List中
            for (int i = 0; i < listOb.size(); i++) {
                List<Object> ob = listOb.get(i);
                BdPackMaterialModel bdPackMaterialModel = new BdPackMaterialModel();
                //设置编号
                bdPackMaterialModel.setCargo_owner_no(String.valueOf(ob.get(0)));
                //通过遍历实现把每一列封装成一个model中，再把所有的model用List集合装载
                bdPackMaterialModel.setBarcode(String.valueOf(ob.get(1)));
                bdPackMaterialModel.setName(String.valueOf(ob.get(2)));
                //object类型转Double类型
                bdPackMaterialModel.setPrice(Double.parseDouble(ob.get(3).toString()));
                bdPackMaterialModel.setLENGTH(Double.parseDouble(ob.get(4).toString()));
                bdPackMaterialModel.setWidth(Double.parseDouble(ob.get(5).toString()));
                bdPackMaterialModel.setHeight(Double.parseDouble(ob.get(6).toString()));
                bdPackMaterialModel.setWeight(Double.parseDouble(ob.get(6).toString()));
                bdPackMaterialModel.setCreated_by("admin");
                bdPackMaterialModel.setNo(bdPackMaterialModel.getCargo_owner_no()+bdPackMaterialModel.getBarcode());
                list.add(bdPackMaterialModel);
            }
            //批量导入
            if(list.size()>0) {
                bdPackMaterialDao.insertBatch(list);
                status=1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }
}
