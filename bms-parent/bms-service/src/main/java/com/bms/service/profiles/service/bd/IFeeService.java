package com.bms.service.profiles.service.bd;

import com.base.common.core.service.IBaseService;
import com.bms.service.dto.bd.*;
import com.bms.service.model.bd.*;
import com.bms.service.vo.bd.*;

import java.util.List;

/**
 * Created by jsc on 2018/10/16.
 */
public interface IFeeService extends IBaseService<SumFeeModel> {
    //获取快递的总费用
    List<FeeResultExpressFeeModel> getexpressfee(FeeResultExpressFeeDto feeResultExpressFeeDto) throws Exception;
    //获取仓储总费用
    List<FeeResultStorageFeeModel> getstoragefee(FeeResultStorageFeeDto feeResultStorageFeeDto) throws Exception;
    //获取总费用
    List<FeeResultDeliveryFeeModel> getdeliveryfee(FeeResultDeliveryFeeDto feeResultDeliveryFeeDto) throws Exception;
    //获取快递总费用
    List<FeeResultReceivingFeeModel> getreceivingfee(FeeResultReceivingFeeDto feeResultReceivingFeeDto) throws Exception;
    //获取快递总费用
    List<FeeResultSpecialFeeModel> getspeciatfee(FeeResultSpecialFeeDto feeResultSpecialFeeDto) throws Exception;
    //获取包材总费用
    List<FeeResultPackMaterialFeeModel>getMaterialfee(FeeResultPackMaterialFeeDto feeResultPackMaterialFeeDto) throws Exception;

    //将费用结果插入到月份结果表
    void save(String owner_no,Double expressfee,String year,String month,Double dctstoragefee,Double deliveryfee,Double specialfee,Double receivingfee,Double packmaterialfee) throws Exception;


    //获取快递remake备注信息记录
    FeeResultExpressSkuVo getCount(String owner_no, String month_time) throws Exception;

    //获取月份记录
    FeeResultOwnerMonthlyVo getRecord(String owner_no, String year, String month) throws Exception;

    // 查询指定月月份结果表数据
    List<SumFeeModel> getOwnerMonth(SumFeeDto sumFeeDto) throws Exception;

    //查询确认审核月份数据
    List<FeeResultOwnerMonthlyModel> getOwnerMonthly(String owner_no, String year, String month) throws Exception;

    //驳回修改月份结果表状态
    int updateOwnerMonthly(FeeResultOwnerMonthlyModel feeResultOwnerMonthlyModel) throws Exception;

    //驳回时修改月份结果表审核状态
    int updateMonthly(FeeResultOwnerMonthlyModel feeResultOwnerMonthlyModel) throws Exception;

    //判断货主是否存在类型为仓储优惠-日均托盘量
    FeeResultOwnerMonthlyVo getCodeitem(String owner_no) throws Exception;

    //判断货主是否存在类型为仓储优惠-库存周转率
    FeeResultOwnerMonthlyVo getRateitem(String owner_no) throws Exception;

    //判断货主是否存在保底费类型
    FeeRuleSpecialVo getSafetyitem(String owner_no);

    //获取当前月订单的出库总件数之和
    FeeResultOwnerMonthlyVo getOwnerdelivery(String owner_no, String month_time) throws Exception;

    //获取仓储优惠-库存周转率的对应的值
    FeeRuleSpecialVo getTurnover(String owner_no) throws Exception;

    //获取储位托盘仓储费托盘量和对应的值
    FeeRuleSpecialVo getPlt(String owner_no) throws Exception;

    //获取指定月储位托盘数量 丶库存总量
    SnapshotStkTotalInfoVo getTotal(String owner_no, String starDay, String lastDay) throws Exception;

    //获取规则中按存储托盘方式计费的价格
    FeeRuleStorageVo getTray(String owner_no);

    //运营审核
    int operationAuditModify(String owner_no, String year, String month) throws Exception;

    //财务专员审核
    int financeCommissionerModify(String owner_no, String year, String month) throws Exception;

    //财务经理审核
    int financialManagerModify(String owner_no, String year, String month) throws Exception;

    //运营驳回
    int operationAuditReject(String owner_no, String year, String month) throws Exception;

    //财务专员驳回
    int financeCommissionerReject(String owner_no, String year, String month) throws Exception;

    //财务经理驳回
    int financialManagerReject(String owner_no, String year, String month) throws Exception;
}
